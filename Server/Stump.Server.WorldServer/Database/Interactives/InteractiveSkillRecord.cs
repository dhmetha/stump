using Stump.ORM;
using Stump.ORM.SubSonic.SQLGeneration.Schema;
using Stump.Server.BaseServer.Database;
using Stump.Server.WorldServer.Game.Actors.RolePlay.Characters;
using Stump.Server.WorldServer.Game.Conditions;
using Stump.Server.WorldServer.Game.Interactives;
using Stump.Server.WorldServer.Game.Interactives.Skills;

namespace Stump.Server.WorldServer.Database.Interactives
{
    public class InteractiveSkillRelator
    {
        public static string FetchQuery = "SELECT * FROM interactives_skills";
    }

    [TableName("interactives_skills")]
    public class InteractiveSkillRecord : ParameterizableRecord, IAutoGeneratedRecord
    {
        public const int DEFAULT_TEMPLATE = 184;

        private int? m_customTemplateId;
        private InteractiveSkillTemplate m_template;

        public InteractiveSkillRecord()
        {
        }

        [PrimaryKey("Id")]
        public int Id
        {
            get;
            set;
        }

        public string Type
        {
            get;
            set;
        }

        // note : not used at the moment
        public int Duration
        {
            get;
            set;
        }

        public int? CustomTemplateId
        {
            get { return m_customTemplateId; }
            set
            {
                m_customTemplateId = value;
                m_template = null;
            }
        }

        protected virtual int GenericTemplateId // determin the name
        {
            get { return DEFAULT_TEMPLATE; }
        }

        [Ignore]
        public virtual InteractiveSkillTemplate Template
        {
            get
            {
                return m_template ??
                       (m_template =
                        InteractiveManager.Instance.GetSkillTemplate(CustomTemplateId.HasValue
                                                                         ? CustomTemplateId.Value
                                                                         : GenericTemplateId));
            }
        }

        #region Condition

        private ConditionExpression m_conditionExpression;

        [NullString]
        public string Condition
        {
            get;
            set;
        }

        [Ignore]
        public ConditionExpression ConditionExpression
        {
            get
            {
                if (string.IsNullOrEmpty(Condition) || Condition == "null")
                    return null;

                return m_conditionExpression ?? (m_conditionExpression = ConditionExpression.Parse(Condition));
            }
            set
            {
                m_conditionExpression = value;
                Condition = value.ToString();
            }
        }

        public bool IsConditionFilled(Character character)
        {
            return ConditionExpression == null || ConditionExpression.Eval(character);
        }

        #endregion Condition

        public virtual Skill GenerateSkill(int id, InteractiveObject interactiveObject)
        {
            return DiscriminatorManager<Skill>.Instance.Generate(Type, id, this, interactiveObject);
        }
    }
}