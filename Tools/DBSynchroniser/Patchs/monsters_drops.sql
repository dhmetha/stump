-- EXECUTE ON: monsters_templates

DELETE FROM monsters_drops;
ALTER TABLE `monsters_drops` AUTO_INCREMENT=1;

-- Monster 31 : Blue Larva
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (31, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (31, 362, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (31, 7423, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (31, 2474, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (31, 643, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);


-- Monster 34 : Green Larva
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (34, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (34, 364, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (34, 7423, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (34, 643, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (34, 1681, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 0);


-- Monster 36 : Gobball

-- Monster 37 : Crackler

-- Monster 39 : Coney

-- Monster 40 : Boar

-- Monster 41 : Prespic

-- Monster 42 : The Ultra-Powerful

-- Monster 43 : Tofu

-- Monster 44 : Bwork Magus

-- Monster 45 : Kitten

-- Monster 46 : Orange Larva
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (46, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (46, 363, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (46, 7423, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (46, 643, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);


-- Monster 47 : Treechnid
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (47, 437, 63, 20, 24, 28, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (47, 435, 63, 15, 20, 30, 40, 50, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (47, 434, 63, 15, 20, 30, 40, 50, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (47, 463, 63, 2, 4, 6, 8, 10, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (47, 464, 63, 1, 2, 3, 4, 5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (47, 792, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (47, 926, 63, 0.01, 0.02, 0.03, 0.04, 0.05, 1, 400);


-- Monster 48 : Wild Sunflower
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (48, 288, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (48, 2659, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (48, 2661, 63, 5, 6, 7, 8, 9, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (48, 2663, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (48, 720, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 52 : Arachnee
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (52, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (52, 365, 63, 45, 46, 47, 48, 49, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (52, 1673, 63, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (52, 6911, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (52, 2478, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (52, 6914, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (52, 6913, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (52, 6912, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);


-- Monster 53 : Bwork Magus
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (53, 8145, 63, 50, 52, 54, 56, 58, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (53, 409, 63, 20, 21, 22, 23, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (53, 391, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (53, 2330, 63, 10, 11, 12, 13, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (53, 2273, 63, 10, 11, 12, 13, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (53, 1671, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (53, 720, 1, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (53, 410, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (53, 3209, 63, 0.3, 0.3, 0.3, 0.3, 0.3, 1, 200);


-- Monster 54 : Chafer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (54, 310, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (54, 2336, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (54, 2323, 63, 4, 4, 4, 4, 4, 1, 400);


-- Monster 55 : Blue Jelly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (55, 311, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (55, 757, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (55, 995, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);


-- Monster 56 : Mint Jelly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (56, 369, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (56, 380, 63, 10, 11, 12, 13, 14, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (56, 993, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);


-- Monster 57 : Strawberry Jelly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (57, 381, 63, 10, 11, 12, 13, 14, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (57, 368, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (57, 994, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);


-- Monster 58 : Royal Blue Jelly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (58, 757, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (58, 370, 63, 20, 20, 20, 20, 20, 1, 0);


-- Monster 59 : Mush Mush
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (59, 300, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (59, 290, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (59, 377, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (59, 2473, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (59, 6922, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (59, 6919, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (59, 6920, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (59, 6921, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (59, 378, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (59, 1674, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 120);


-- Monster 61 : Moskito
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (61, 307, 63, 55, 60, 65, 70, 75, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (61, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (61, 371, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (61, 2477, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (61, 6917, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (61, 6915, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (61, 6916, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (61, 6918, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);


-- Monster 62 : Karne Rider
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (62, 382, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (62, 2259, 63, 10, 10, 10, 10, 10, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (62, 2264, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (62, 533, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (62, 1679, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 63 : Crab
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (63, 311, 63, 95, 95, 95, 95, 95, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (63, 379, 63, 30, 31, 32, 33, 34, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (63, 2583, 63, 5, 6, 7, 8, 9, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (63, 2302, 63, 1, 1.5, 2, 2.5, 3, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (63, 2303, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 64 : Wabbit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (64, 2288, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (64, 305, 63, 25, 27, 28, 30, 32, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (64, 648, 63, 8, 9, 10, 11, 12, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (64, 361, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (64, 654, 63, 1, 2, 3, 4, 5, 1, 0);


-- Monster 65 : Black Wabbit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (65, 305, 63, 12, 14, 16, 18, 20, 1, 0);


-- Monster 68 : Black Tiwabbit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (68, 372, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (68, 646, 63, 5, 6, 7, 8, 9, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (68, 305, 63, 2, 4, 6, 8, 10, 1, 0);


-- Monster 70 : Boxing Goblimp

-- Monster 72 : Tiwabbit Wosungwee
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (72, 361, 63, 1, 2, 3, 4, 5, 1, 0);


-- Monster 74 : Bwork Archer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (74, 8145, 63, 50, 52, 54, 56, 58, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (74, 429, 63, 35, 36, 37, 38, 39, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (74, 391, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (74, 420, 63, 15, 16, 17, 18, 19, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (74, 3208, 63, 10, 11, 12, 13, 14, 1, 150);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (74, 2271, 63, 1, 1.5, 2, 2.5, 3, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (74, 2268, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);


-- Monster 75 : Immature Golden Dreggon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (75, 842, 63, 25, 27.5, 30, 32.5, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (75, 544, 63, 0.5, 1, 1.5, 2, 2.5, 1, 0);


-- Monster 76 : White Dreggon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (76, 308, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (76, 545, 63, 1, 2, 3, 4, 5, 1, 0);


-- Monster 78 : Demonic Rose
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (78, 309, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (78, 2662, 63, 4, 5, 6, 7, 8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (78, 2663, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (78, 641, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (78, 719, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);


-- Monster 79 : Evil Dandelion
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (79, 306, 63, 22, 24, 26, 28, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (79, 374, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (79, 373, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (79, 2665, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (79, 642, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (79, 718, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (79, 2663, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);


-- Monster 81 : Wind Kwak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (81, 416, 63, 15, 16, 17, 18, 19, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (81, 413, 63, 5, 6, 7, 8, 9, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (81, 2648, 63, 5, 6, 7, 8, 9, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (81, 1670, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (81, 2652, 63, 0.06, 0.07, 0.08, 0.09, 0.1, 1, 400);


-- Monster 82 : Immature Black Dreggon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (82, 843, 63, 25, 27.5, 30, 32.5, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (82, 547, 63, 1, 1.5, 2, 2.5, 3, 1, 0);


-- Monster 83 : Fire Kwak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (83, 415, 63, 15, 16, 17, 18, 19, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (83, 412, 63, 5, 6, 7, 8, 9, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (83, 2648, 63, 5, 6, 7, 8, 9, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (83, 2651, 63, 0.06, 0.07, 0.08, 0.09, 0.1, 1, 400);


-- Monster 84 : Ice Kwak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (84, 414, 63, 15, 16, 17, 18, 19, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (84, 2648, 63, 5, 6, 7, 8, 9, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (84, 411, 63, 5, 6, 7, 8, 9, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (84, 2650, 63, 0.06, 0.07, 0.08, 0.09, 0.1, 1, 400);


-- Monster 85 : Royal Mint Jelly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (85, 369, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (85, 2241, 63, 20, 20, 20, 20, 20, 1, 0);


-- Monster 86 : Royal Strawberry Jelly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (86, 368, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (86, 2242, 63, 20, 20, 20, 20, 20, 1, 0);


-- Monster 87 : Golden Dreggon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (87, 842, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (87, 544, 63, 1, 2, 3, 4, 5, 1, 400);


-- Monster 88 : Sapphire Dreggon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (88, 1129, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (88, 546, 63, 1, 2, 3, 4, 5, 1, 400);


-- Monster 89 : Black Dreggon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (89, 843, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (89, 547, 63, 1, 2, 3, 4, 5, 1, 400);


-- Monster 90 : Immature White Dreggon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (90, 308, 63, 25, 27.5, 30, 32.5, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (90, 545, 63, 1, 1.5, 2, 2.5, 3, 1, 0);


-- Monster 91 : Alert Black Dreggon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (91, 843, 63, 36, 37, 38, 39, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (91, 846, 63, 10, 15, 20, 25, 30, 1, 130);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (91, 547, 63, 1, 2, 3, 4, 5, 1, 400);


-- Monster 93 : Alert White Dreggon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (93, 308, 63, 36, 37, 38, 39, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (93, 847, 63, 10, 15, 20, 25, 30, 1, 130);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (93, 545, 63, 1, 2, 3, 4, 5, 1, 400);


-- Monster 94 : Alert Golden Dreggon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (94, 842, 63, 36, 37, 38, 39, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (94, 845, 63, 10, 15, 20, 25, 30, 1, 130);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (94, 544, 63, 1, 2, 3, 4, 5, 1, 400);


-- Monster 95 : Alert Sapphire Dreggon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (95, 1129, 63, 36, 37, 38, 39, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (95, 844, 63, 10, 15, 20, 25, 30, 1, 130);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (95, 546, 63, 1, 2, 3, 4, 5, 1, 400);


-- Monster 96 : Tiwabbit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (96, 360, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (96, 305, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (96, 361, 63, 1, 2, 3, 4, 5, 1, 0);


-- Monster 97 : Wo Wabbit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (97, 305, 63, 16, 18, 20, 22, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (97, 361, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (97, 649, 63, 5, 6, 7, 8, 9, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (97, 406, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 98 : Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (98, 287, 63, 25, 26, 27, 28, 29, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (98, 367, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (98, 301, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (98, 366, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (98, 2476, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 99 : Gwandpa Wabbit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (99, 419, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (99, 418, 63, 15, 16, 17, 18, 19, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (99, 650, 63, 5, 6, 7, 8, 9, 1, 0);


-- Monster 100 : Fire Bowl

-- Monster 101 : Gobball
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (101, 384, 63, 60, 65, 70, 75, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (101, 304, 63, 40, 45, 50, 55, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (101, 383, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (101, 385, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (101, 395, 63, 15, 20, 25, 30, 35, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (101, 2448, 63, 2, 2, 2, 2, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (101, 2451, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (101, 2453, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (101, 2449, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (101, 2455, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (101, 2422, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (101, 2425, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (101, 2416, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (101, 2454, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 0);


-- Monster 102 : Boowolf
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (102, 438, 63, 10, 20, 30, 40, 50, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (102, 439, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (102, 291, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (102, 440, 63, 10, 20, 30, 40, 50, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (102, 2578, 63, 4, 5, 6, 7, 9, 1, 150);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (102, 8396, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (102, 2575, 63, 2, 2, 2.5, 3, 3, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (102, 292, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (102, 2805, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (102, 651, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 103 : Prespic
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (103, 407, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (103, 2571, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (103, 2573, 63, 10, 11, 12, 13, 14, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (103, 2572, 63, 2, 2, 2, 2, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (103, 2574, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (103, 653, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (103, 1672, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 104 : Boar
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (104, 388, 63, 50, 52, 54, 56, 58, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (104, 394, 63, 50, 52, 54, 56, 58, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (104, 393, 63, 42, 44, 46, 48, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (104, 486, 63, 40, 45, 50, 55, 60, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (104, 386, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (104, 387, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (104, 652, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (104, 6909, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (104, 6910, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (104, 6908, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);


-- Monster 106 : Crackler
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (106, 431, 63, 6, 7, 9, 14, 16, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (106, 2252, 63, 3, 3, 4, 5, 6, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (106, 447, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (106, 547, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (106, 545, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (106, 546, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (106, 543, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (106, 2306, 63, 0.5, 0.5, 0.6, 0.6, 0.7, 1, 400);


-- Monster 107 : Dark Vlad
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (107, 545, 63, 30, 30, 30, 30, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (107, 544, 63, 30, 30, 30, 30, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (107, 543, 63, 30, 30, 30, 30, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (107, 546, 63, 30, 30, 30, 30, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (107, 547, 63, 30, 30, 30, 30, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (107, 1677, 63, 10, 10, 10, 10, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (107, 2299, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (107, 737, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 108 : Rib
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (108, 432, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (108, 2642, 63, 6, 7, 8, 9, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (108, 2323, 63, 0.5, 1, 1.5, 2, 2.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (108, 1132, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (108, 1685, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 110 : Invisible Chafer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (110, 310, 63, 35, 36, 37, 38, 39, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (110, 430, 63, 3, 3.5, 4, 4.5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (110, 2278, 63, 3, 3.5, 4, 4.5, 5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (110, 2323, 63, 0.5, 1, 1.5, 2, 2.5, 1, 400);


-- Monster 111 : Evil Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (111, 376, 2, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (111, 375, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (111, 2675, 63, 4, 5, 6, 8, 10, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (111, 2673, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);


-- Monster 112 : Mushd
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (112, 417, 63, 30, 32, 34, 36, 38, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (112, 2584, 63, 10, 12, 14, 16, 18, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (112, 2585, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 113 : Dragon Pig
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (113, 479, 63, 40, 40, 40, 40, 40, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (113, 2645, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (113, 2643, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (113, 481, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (113, 477, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (113, 8393, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (113, 2267, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (113, 487, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (113, 2644, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (113, 739, 1, 0.02, 0.02, 0.02, 0.02, 0.02, 1, 400);


-- Monster 114 : The Madoll

-- Monster 115 : The Block

-- Monster 116 : The Sacrificial Doll

-- Monster 117 : The Inflatable

-- Monster 118 : Dark Miner
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (118, 13505, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (118, 350, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (118, 2274, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (118, 313, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (118, 446, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (118, 312, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (118, 2282, 63, 0.6, 0.7, 0.8, 0.9, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (118, 543, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (118, 545, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (118, 547, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (118, 546, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (118, 6476, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (118, 480, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (118, 544, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (118, 930, 63, 0.06, 0.07, 0.08, 0.09, 0.1, 1, 0);


-- Monster 119 : Dark Smith
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (119, 446, 63, 1, 2, 3, 4, 5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (119, 313, 63, 1, 2, 3, 4, 5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (119, 350, 63, 1, 2, 3, 4, 5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (119, 747, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (119, 750, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (119, 749, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (119, 6458, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (119, 746, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (119, 6457, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (119, 748, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 300);


-- Monster 120 : Thrown Tofu

-- Monster 121 : Minotoror
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (121, 840, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (121, 2998, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (121, 2666, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (121, 1680, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (121, 8313, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (121, 2667, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (121, 2999, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (121, 694, 1, 0.02, 0.02, 0.02, 0.02, 0.02, 1, 400);


-- Monster 123 : Piglet
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (123, 2647, 63, 15, 16, 17, 18, 19, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (123, 901, 63, 15, 16, 17, 18, 19, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (123, 479, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (123, 2266, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (123, 2646, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 124 : Vampyre Master
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (124, 2279, 63, 20, 21, 22, 23, 24, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (124, 752, 63, 10, 15, 20, 25, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (124, 756, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (124, 2280, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (124, 2285, 63, 0.4, 0.4, 0.5, 0.5, 0.6, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (124, 2281, 63, 0.3, 0.3, 0.4, 0.4, 0.4, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (124, 2669, 63, 0.15, 0.16, 0.17, 0.17, 0.2, 1, 100);


-- Monster 126 : Grey Mouse
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (126, 761, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 127 : Vampyre
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (127, 2279, 63, 10, 11, 12, 13, 14, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (127, 752, 63, 5, 8, 10, 12, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (127, 756, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (127, 2285, 63, 0.2, 0.3, 0.4, 0.4, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (127, 2281, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (127, 2669, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 100);


-- Monster 134 : White Gobbly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (134, 881, 63, 60, 65, 70, 75, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (134, 883, 63, 40, 50, 60, 70, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (134, 385, 63, 25, 30, 35, 40, 45, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (134, 2460, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (134, 2419, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);


-- Monster 147 : Royal Gobball
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (147, 886, 63, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (147, 880, 63, 1, 1, 1, 1, 1, 1, 100);


-- Monster 148 : Gobball War Chief
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (148, 2466, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (148, 882, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (148, 887, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (148, 2467, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (148, 2465, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (148, 2463, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (148, 2464, 63, 0.6, 0.6, 0.6, 0.6, 0.6, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (148, 2462, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (148, 2414, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (148, 2411, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (148, 2468, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (148, 911, 1, 0.01, 0.01, 0.01, 0.01, 0.01, 1, 400);


-- Monster 149 : Black Gobbly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (149, 885, 63, 70, 71, 72, 73, 74, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (149, 884, 63, 60, 65, 70, 75, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (149, 385, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (149, 2460, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (149, 2428, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 150 : Neye
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (150, 2676, 63, 1, 1, 1, 1, 1.5, 1, 100);


-- Monster 152 : The Swindling

-- Monster 153 : Dark Baker
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (153, 519, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (153, 311, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (153, 1986, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (153, 13730, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (153, 586, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (153, 528, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (153, 1626, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (153, 743, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (153, 931, 63, 0.06, 0.07, 0.08, 0.09, 0.1, 1, 0);


-- Monster 154 : Kwoan
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (154, 2283, 63, 20, 21, 22, 23, 24, 1, 100);


-- Monster 155 : Rogue Clan Bandit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (155, 13339, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (155, 276, 63, 1, 2, 3, 4, 5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (155, 274, 63, 1, 2, 3, 4, 5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (155, 6924, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (155, 6923, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (155, 6961, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (155, 6925, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (155, 1683, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (155, 1626, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (155, 1684, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 157 : Arachnid

-- Monster 158 : Explosive Shell

-- Monster 159 : Miliboowolf
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (159, 1690, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (159, 2579, 63, 5, 6, 7, 8, 9, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (159, 2576, 63, 1, 1.5, 2, 2.5, 3, 1, 400);


-- Monster 160 : Feca Dopple

-- Monster 161 : Osamodas Dopple

-- Monster 162 : Enutrof Dopple

-- Monster 163 : Sram Dopple

-- Monster 164 : Xelor Dopple

-- Monster 165 : Ecaflip Dopple

-- Monster 166 : Eniripsa Dopple

-- Monster 167 : Iop Dopple

-- Monster 168 : Cra Dopple

-- Monster 169 : Sadida Dopple

-- Monster 170 : Immature Sapphire Dreggon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (170, 1129, 63, 25, 27.5, 30, 32.5, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (170, 546, 63, 1, 1.5, 2, 2.5, 3, 1, 0);


-- Monster 171 : Wild Almond Dragoturkey
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (171, 2599, 63, 20, 23, 26, 32, 36, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (171, 2179, 63, 15, 16, 17, 18, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (171, 2596, 63, 10, 11, 12, 13, 14, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (171, 2588, 63, 5, 6, 7, 8, 10, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (171, 2594, 63, 2, 2, 2.5, 3, 3, 1, 0);


-- Monster 173 : Ancestral Treechnid
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (173, 449, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (173, 8496, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (173, 918, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (173, 8494, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (173, 8495, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (173, 926, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 178 : Goblin
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (178, 2318, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 179 : Skeleton Wabbit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (179, 305, 63, 10, 11, 12, 13, 14, 1, 0);


-- Monster 180 : Wa Wabbit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (180, 361, 63, 30, 30, 30, 30, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (180, 8397, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (180, 8621, 63, 10, 10, 10, 10, 10, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (180, 804, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (180, 812, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (180, 808, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (180, 8398, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 181 : Wobot
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (181, 546, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 182 : GM Wabbit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (182, 6498, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 194 : Red Scaraleaf
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (194, 1465, 63, 16, 17, 18, 19, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (194, 398, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (194, 2292, 63, 4, 5, 6, 8, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (194, 1457, 63, 2, 2, 3, 4, 5, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (194, 2294, 63, 0.05, 0.06, 0.07, 0.08, 0.1, 1, 400);


-- Monster 198 : Blue Scaraleaf
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (198, 1464, 63, 16, 17, 18, 19, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (198, 398, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (198, 2291, 63, 4, 5, 6, 8, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (198, 1455, 63, 2, 2, 3, 4, 5, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (198, 2294, 63, 0.05, 0.06, 0.07, 0.08, 0.1, 1, 400);


-- Monster 200 : Wild Ginger Dragoturkey
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (200, 2598, 63, 20, 23, 26, 32, 36, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (200, 2179, 63, 15, 16, 17, 18, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (200, 2586, 63, 5, 6, 7, 8, 10, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (200, 2591, 63, 2, 2, 2.5, 3, 3, 1, 0);


-- Monster 207 : Pumpkwin
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (207, 2674, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (207, 302, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 0);


-- Monster 208 : Tikokoko
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (208, 2618, 63, 10, 11, 12, 13, 14, 1, 100);


-- Monster 209 : Kokoko
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (209, 997, 63, 50, 52, 53, 54, 55, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (209, 1002, 63, 40, 41, 42, 43, 44, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (209, 2618, 63, 8, 8, 9, 9, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (209, 2624, 63, 2, 2, 2.5, 3.5, 4, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (209, 2619, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (209, 1678, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 100);


-- Monster 211 : Kanniball Archer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (211, 2628, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 212 : Kanniball Thierry
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (212, 2625, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 80);


-- Monster 213 : Kanniball Jav
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (213, 2626, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 214 : Kanniball Sarbak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (214, 2627, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 215 : Glukoko
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (215, 1018, 63, 22, 24, 26, 28, 30, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (215, 2602, 63, 20, 22, 25, 28, 32, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (215, 2605, 63, 4, 5, 6, 7, 8, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (215, 2607, 63, 1, 1.2, 1.4, 1.6, 1.8, 1, 400);


-- Monster 216 : Greedovore
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (216, 2253, 63, 16, 18, 20, 22, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (216, 1018, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (216, 2632, 63, 4, 5, 7, 9, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (216, 2630, 63, 2, 2, 2.5, 3, 3.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (216, 2631, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (216, 2607, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 300);


-- Monster 217 : Ambusher
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (217, 448, 63, 20, 35, 30, 35, 40, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (217, 2254, 63, 10, 10, 11, 12, 13, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (217, 6736, 63, 10, 12, 16, 18, 20, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (217, 2634, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (217, 6737, 63, 1.5, 2, 2.5, 3, 3.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (217, 2633, 2, 0.2, 0.3, 0.4, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (217, 6735, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 400);


-- Monster 218 : Kokonut
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (218, 2618, 63, 4, 5, 6, 8, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (218, 2617, 63, 2, 2, 2, 2, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (218, 997, 63, 2, 3, 4, 5, 6, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (218, 2619, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 220 : Red Turtle
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (220, 2610, 1, 10, 12, 14, 16, 18, 1, 200);


-- Monster 221 : Blue Turtle
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (221, 2613, 63, 10, 12, 14, 16, 18, 1, 200);


-- Monster 222 : Green Turtle
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (222, 2609, 1, 10, 12, 14, 16, 20, 1, 200);


-- Monster 223 : Yellow Turtle
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (223, 2611, 1, 10, 12, 14, 16, 18, 1, 200);


-- Monster 225 : Darkli Moon

-- Monster 226 : Moon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (226, 8383, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (226, 6441, 63, 15, 15, 15, 15, 15, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (226, 8384, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 228 : Boomba
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (228, 13343, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (228, 466, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 229 : Hazwonarm
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (229, 13496, 63, 10, 11, 12, 13, 14, 1, 0);


-- Monster 230 : LeChouque
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (230, 998, 63, 70, 70, 70, 70, 70, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (230, 2620, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (230, 2622, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (230, 2623, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (230, 2621, 63, 1, 1, 1, 1, 1, 1, 200);


-- Monster 231 : Cannon Dorf
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (231, 467, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 232 : Moowolf
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (232, 1691, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (232, 2577, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (232, 2581, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (232, 8144, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (232, 1696, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (232, 2582, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (232, 2806, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (232, 2580, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (232, 315, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (232, 1574, 1, 1, 1, 1, 1, 1, 1, 200);


-- Monster 233 : Trool
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (233, 8519, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (233, 2559, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (233, 547, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (233, 546, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (233, 1475, 1, 1, 1.1, 1.2, 1.3, 1.4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (233, 2562, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (233, 2561, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (233, 1694, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (233, 2560, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 235 : Earth Kwak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (235, 1141, 63, 15, 16, 17, 18, 19, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (235, 1142, 63, 5, 6, 7, 8, 9, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (235, 2648, 63, 5, 6, 7, 8, 9, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (235, 2649, 63, 0.06, 0.07, 0.08, 0.09, 0.1, 1, 400);


-- Monster 236 : Purple Piwi
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (236, 6898, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (236, 287, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (236, 2475, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 237 : Living Bag

-- Monster 238 : Living Shovel

-- Monster 239 : Red Wyrmling

-- Monster 240 : Green Scaraleaf
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (240, 1466, 63, 16, 17, 18, 19, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (240, 398, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (240, 2293, 63, 4, 5, 6, 8, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (240, 1458, 63, 2, 2, 3, 4, 5, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (240, 2294, 63, 0.05, 0.06, 0.07, 0.08, 0.1, 1, 400);


-- Monster 241 : White Scaraleaf
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (241, 1467, 63, 16, 17, 18, 19, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (241, 398, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (241, 2290, 63, 4, 5, 6, 8, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (241, 1456, 63, 2, 2, 3, 4, 5, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (241, 2294, 63, 0.05, 0.06, 0.07, 0.08, 0.1, 1, 400);


-- Monster 242 : Fire Spark
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (242, 1526, 1, 1, 1, 1, 1, 1, 1, 0);


-- Monster 243 : Water Spark
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (243, 1527, 1, 1, 1, 1, 1, 1, 1, 0);


-- Monster 244 : Earth Spark
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (244, 1528, 1, 1, 1, 1, 1, 1, 1, 0);


-- Monster 245 : Air Spark
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (245, 1529, 1, 1, 1, 1, 1, 1, 1, 0);


-- Monster 246 : Arachnee

-- Monster 248 : Impette

-- Monster 249 : Oni
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (249, 838, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (249, 546, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (249, 1536, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (249, 1535, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (249, 1538, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (249, 1537, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 250 : Needle

-- Monster 252 : Smiths' Chest
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 966, 1, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 443, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 313, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 350, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 444, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 446, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 447, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 441, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 312, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 445, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 442, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 13140, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 543, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 546, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 547, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 544, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (252, 450, 63, 1, 1, 1, 1, 1, 1, 0);


-- Monster 253 : Dark Treechnid
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (253, 1611, 63, 5, 6, 7, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (253, 1610, 63, 5, 7, 8, 9, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (253, 1612, 63, 5, 6, 7, 8, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (253, 1682, 63, 1, 1.2, 1.4, 1.6, 1.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (253, 1660, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (253, 926, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 254 : Venerable Treechnid
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (254, 926, 63, 0.01, 0.02, 0.03, 0.04, 0.05, 1, 400);


-- Monster 255 : Aggressive Arachnee
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (255, 365, 63, 55, 60, 65, 70, 75, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (255, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (255, 1673, 63, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (255, 2478, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 256 : Trunknid
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (256, 2249, 63, 23, 26, 29, 32, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (256, 2563, 63, 4, 5, 6, 7, 8, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (256, 2250, 63, 4, 5, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (256, 2565, 63, 4, 5, 6, 7, 8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (256, 2564, 63, 2, 2, 2, 2, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (256, 2566, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 257 : Soft Oak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (257, 6488, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (257, 6486, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (257, 926, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (257, 6490, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (257, 6487, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (257, 739, 1, 0.01, 0.01, 0.01, 0.01, 0.01, 1, 400);


-- Monster 258 : Summoning Branch

-- Monster 259 : Major Arachnee
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (259, 1652, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (259, 2656, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (259, 2654, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (259, 2653, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 400);


-- Monster 260 : Healing Branch
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (260, 13529, 63, 10, 11, 12, 13, 14, 1, 0);


-- Monster 261 : Crocodyl
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (261, 1664, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (261, 1663, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (261, 6739, 63, 1.5, 2, 2.5, 3, 3.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (261, 1635, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (261, 544, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (261, 2664, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (261, 1719, 1, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (261, 1718, 1, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 0);


-- Monster 262 : Chaferfu

-- Monster 263 : Crocodyl Chief
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (263, 1613, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (263, 2664, 63, 2, 2, 2, 2, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (263, 1614, 63, 2, 3, 4, 5, 6, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (263, 6740, 63, 1.5, 2, 2.5, 3, 3.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (263, 2296, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (263, 6738, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (263, 2295, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (263, 1635, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (263, 1676, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (263, 1719, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (263, 1718, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 264 : Xelor's Dial

-- Monster 265 : Fire Bwak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (265, 2074, 1, 100, 100, 100, 100, 100, 1, 0);


-- Monster 266 : Earth Bwak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (266, 2077, 1, 100, 100, 100, 100, 100, 1, 0);


-- Monster 267 : Air Bwak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (267, 2076, 1, 100, 100, 100, 100, 100, 1, 0);


-- Monster 268 : Water Bwak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (268, 2075, 1, 100, 100, 100, 100, 100, 1, 0);


-- Monster 269 : Fire Kwakere
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (269, 1515, 63, 5, 5, 5, 5, 5, 1, 0);


-- Monster 270 : Earth Kwakere
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (270, 1516, 63, 5, 5, 5, 5, 5, 1, 0);


-- Monster 271 : Ice Kwakere
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (271, 1518, 63, 5, 5, 5, 5, 5, 1, 0);


-- Monster 272 : Wind Kwakere
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (272, 1517, 63, 5, 5, 5, 5, 5, 1, 0);


-- Monster 273 : Coco Blop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (273, 1770, 63, 60, 60, 60, 60, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (273, 1974, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (273, 2556, 63, 20, 22, 24, 26, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (273, 2557, 63, 5, 6, 7, 8, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (273, 1772, 63, 4, 4, 4, 4, 4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (273, 2558, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 400);


-- Monster 274 : Indigo Blop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (274, 1777, 63, 60, 60, 60, 60, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (274, 1974, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (274, 2556, 63, 20, 22, 24, 26, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (274, 2557, 63, 5, 6, 7, 8, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (274, 1778, 63, 4, 4, 4, 4, 4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (274, 2558, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 400);


-- Monster 275 : Morello Cherry Blop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (275, 1775, 63, 60, 60, 60, 60, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (275, 1974, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (275, 2556, 63, 20, 22, 24, 26, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (275, 2557, 63, 5, 6, 7, 8, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (275, 1776, 63, 4, 4, 4, 4, 4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (275, 2558, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 400);


-- Monster 276 : Pippin Blop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (276, 1773, 63, 60, 60, 60, 60, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (276, 1974, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (276, 2556, 63, 20, 22, 24, 26, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (276, 2557, 63, 5, 6, 7, 8, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (276, 1774, 63, 4, 4, 4, 4, 4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (276, 2558, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 400);


-- Monster 277 : Indigo Biblop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (277, 1974, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (277, 1777, 63, 20, 20, 20, 20, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (277, 1778, 63, 4, 4, 4, 4, 4, 1, 0);


-- Monster 278 : Coco Biblop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (278, 1974, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (278, 1770, 63, 20, 20, 20, 20, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (278, 1772, 63, 4, 4, 4, 4, 4, 1, 0);


-- Monster 279 : Morello Cherry Biblop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (279, 1974, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (279, 1775, 63, 20, 20, 20, 20, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (279, 1776, 63, 4, 4, 4, 4, 4, 1, 0);


-- Monster 280 : Pippin Biblop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (280, 1974, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (280, 1773, 63, 20, 20, 20, 20, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (280, 1774, 63, 4, 4, 4, 4, 4, 1, 0);


-- Monster 281 : Crow
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (281, 2060, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (281, 1889, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (281, 2056, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (281, 2058, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (281, 2059, 63, 0.03, 0.03, 0.03, 0.03, 0.03, 1, 400);


-- Monster 282 : Tree

-- Monster 285 : Living Chest

-- Monster 287 : Kaniger
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (287, 1890, 63, 25, 28, 32, 34, 38, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (287, 2528, 63, 20, 22, 24, 26, 28, 1, 60);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (287, 2551, 63, 14, 16, 18, 20, 22, 1, 160);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (287, 7030, 63, 10, 12, 14, 16, 18, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (287, 2550, 63, 4, 4, 5, 6, 7, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (287, 2549, 63, 3, 3, 3, 3, 3, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (287, 2552, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (287, 796, 1, 0.02, 0.02, 0.02, 0.02, 0.02, 1, 0);


-- Monster 288 : Plissken
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (288, 2509, 63, 12, 14, 16, 18, 20, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (288, 1891, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (288, 2507, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (288, 2506, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (288, 2508, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 200);


-- Monster 289 : Lord Crow
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (289, 1889, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (289, 2059, 63, 25, 25, 25, 25, 25, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (289, 13165, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (289, 1892, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (289, 2285, 63, 10, 10, 10, 10, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (289, 2527, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (289, 2525, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (289, 967, 1, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 400);


-- Monster 290 : Chafer Lancer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (290, 310, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (290, 2323, 63, 4, 4, 4, 4, 4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (290, 485, 63, 2, 3, 3, 3, 4, 1, 0);


-- Monster 291 : Chafer Archer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (291, 310, 63, 45, 45, 45, 45, 45, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (291, 2323, 63, 4, 4, 4, 4, 4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (291, 433, 63, 2, 3, 4, 5, 6, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (291, 2297, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 292 : Elite Chafer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (292, 310, 63, 30, 35, 40, 45, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (292, 408, 63, 0.5, 1, 1.5, 2, 2.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (292, 2300, 63, 0.5, 1, 1.5, 2, 2.5, 1, 200);


-- Monster 293 : Plain Crackler
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (293, 2305, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (293, 2304, 63, 4, 5, 6, 8, 10, 1, 130);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (293, 2252, 63, 4, 5, 6, 7, 8, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (293, 547, 63, 3, 3.5, 4, 4.5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (293, 546, 63, 3, 3.5, 4, 4.5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (293, 543, 63, 3, 3.5, 4, 4.5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (293, 544, 63, 3, 3.5, 4, 4.5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (293, 545, 63, 3, 3.5, 4, 4.5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (293, 2306, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (293, 2251, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 296 : Militiaman

-- Monster 297 : Plain Boar
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (297, 2511, 63, 50, 50, 55, 55, 58, 1, 50);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (297, 393, 63, 30, 32, 36, 38, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (297, 2515, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (297, 2514, 63, 5, 5, 5, 5, 5, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (297, 1894, 63, 5, 6, 7, 8, 9, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (297, 2513, 63, 4, 5, 6, 7, 9, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (297, 2516, 63, 4, 5, 6, 8, 10, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (297, 2512, 63, 0.4, 1.5, 1.6, 1.7, 1.8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (297, 2510, 63, 0.2, 0.3, 0.5, 0.7, 0.9, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (297, 2082, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 298 : Scurvion
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (298, 2150, 63, 30, 31, 32, 33, 34, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (298, 2245, 63, 15, 16, 17, 18, 19, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (298, 1893, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (298, 2487, 63, 5, 6, 7, 8, 9, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (298, 2012, 63, 5, 6, 7, 8, 9, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (298, 2490, 63, 4, 5, 6, 7, 8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (298, 2488, 63, 0.05, 0.07, 0.08, 0.09, 0.1, 1, 200);


-- Monster 299 : Whitish Fang
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (299, 2315, 63, 30, 35, 40, 45, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (299, 2316, 63, 25, 30, 35, 40, 45, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (299, 2501, 63, 10, 10, 11, 11, 12, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (299, 2502, 63, 10, 11, 12, 13, 15, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (299, 2317, 63, 4, 5, 6, 8, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (299, 2504, 63, 2, 2, 3, 3, 4, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (299, 2503, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (299, 2505, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 300 : Kolerat
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (300, 2496, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (300, 2499, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (300, 2301, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (300, 2497, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (300, 2495, 63, 5, 6, 7, 8, 10, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (300, 2500, 63, 0.01, 0.02, 0.03, 0.04, 0.05, 1, 100);


-- Monster 301 : Ouginak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (301, 2321, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (301, 2248, 63, 42, 44, 46, 48, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (301, 2491, 63, 12, 14, 16, 18, 20, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (301, 2286, 63, 10, 13, 16, 19, 22, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (301, 2492, 63, 2, 2.5, 3, 3.5, 4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (301, 350, 63, 1, 2, 3, 4, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (301, 2320, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (301, 2285, 63, 0.1, 0.3, 0.5, 0.7, 0.9, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (301, 2494, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (301, 315, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (301, 2104, 1, 0.05, 0.06, 0.07, 0.08, 0.1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (301, 2493, 63, 0.01, 0.02, 0.03, 0.04, 0.05, 1, 100);


-- Monster 303 : Let Emoliug the Saturnine

-- Monster 304 : Let Emoliug the White

-- Monster 305 : Susej the Pure

-- Monster 306 : Susej the Impure

-- Monster 308 : Osurc the Vile

-- Monster 310 : Osurc the Kindly

-- Monster 314 : Torche

-- Monster 337 : Nipul the Saviour

-- Monster 343 : Lousy Pig Knight
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (343, 2481, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (343, 2486, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (343, 2275, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (343, 2482, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (343, 7510, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (343, 2554, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (343, 2483, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (343, 2553, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (343, 2555, 63, 0.05, 0.06, 0.07, 0.08, 0.09, 1, 300);


-- Monster 344 : Lousy Pig Shepherd
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (344, 2484, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (344, 2275, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (344, 2486, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (344, 2481, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (344, 2482, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (344, 2554, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (344, 7510, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (344, 2483, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (344, 2479, 63, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (344, 2480, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 400);


-- Monster 345 : Kirevam the Determined

-- Monster 346 : Kirevam the Terrifying

-- Monster 347 : Amlub the Protector

-- Monster 348 : Amlub the Gravedigger

-- Monster 349 : Nebgib the Pacifier

-- Monster 350 : Nebgib the Tormentor

-- Monster 351 : Nipul the Destroyer

-- Monster 353 : Aboub the Social

-- Monster 355 : Aboub the Antisocial

-- Monster 356 : Gink the Swindling

-- Monster 357 : Gink the Sincere

-- Monster 359 : Codem the Altruist

-- Monster 360 : Codem the Exile

-- Monster 363 : Heart of the Dopple Territory

-- Monster 364 : Heart of the Dopple Territory

-- Monster 365 : Lost Crow

-- Monster 368 : Hunted Gobball
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (368, 383, 1, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (368, 384, 1, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (368, 385, 1, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (368, 304, 1, 25, 25, 25, 25, 25, 1, 0);


-- Monster 371 : Fungi Master
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (371, 290, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (371, 424, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (371, 426, 63, 10, 12, 14, 16, 18, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (371, 1674, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (371, 2035, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 300);


-- Monster 372 : Warrior

-- Monster 373 : Terrifying Squirrel
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (373, 315, 1, 1, 1, 1, 1, 1, 1, 0);


-- Monster 374 : Eratz the Protester
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (374, 737, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 375 : Nomekop Wodly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (375, 1511, 1, 1, 1, 1, 1, 1, 1, 400);


-- Monster 376 : Kikim Innkeeper
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (376, 1935, 63, 100, 100, 100, 100, 100, 1, 0);


-- Monster 377 : Edasse the Killjoy

-- Monster 378 : Burning Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (378, 2277, 63, 2, 2, 2, 2, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (378, 6478, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 379 : Brave Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (379, 2277, 63, 2, 2, 2, 2, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (379, 6480, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 380 : Arepo Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (380, 2277, 63, 2, 2, 2, 2, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (380, 6479, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);


-- Monster 382 : Royal Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (382, 301, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (382, 2247, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (382, 2246, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (382, 8387, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 384 : Aboub
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (384, 13729, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (384, 965, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 385 : Amlub
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (385, 13489, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (385, 965, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 386 : Codem
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (386, 13489, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (386, 965, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 387 : Gink
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (387, 13729, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (387, 965, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 388 : Kirevam
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (388, 13489, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (388, 965, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 389 : Let Emoliug
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (389, 13489, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (389, 965, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 390 : Nebgib
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (390, 13729, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (390, 965, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 391 : Nipul
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (391, 13729, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (391, 965, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 392 : Osurc
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (392, 13729, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (392, 965, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 393 : Susej
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (393, 13489, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (393, 965, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 394 : Knight

-- Monster 396 : Chafer Foot Soldier
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (396, 310, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (396, 2323, 63, 0.5, 1, 1.5, 2, 2.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (396, 1675, 63, 0.5, 1.1, 1.2, 1.3, 1.4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (396, 6475, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 400);


-- Monster 397 : Furious Whitish Fang
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (397, 2315, 63, 30, 30, 30, 30, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (397, 2316, 63, 25, 25, 25, 25, 25, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (397, 2501, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (397, 2502, 63, 10, 10, 10, 10, 10, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (397, 2317, 63, 4, 4, 4, 4, 4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (397, 2504, 63, 2, 2, 2, 2, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (397, 2503, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (397, 2505, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 398 : Dike Tarak

-- Monster 399 : Krachan Porterr

-- Monster 400 : Jiaye Djaul

-- Monster 401 : Knight Norgard

-- Monster 402 : Azra Lazarus

-- Monster 405 : Puja Mustam

-- Monster 406 : William Lenglad

-- Monster 408 : Healing Shell

-- Monster 411 : Little Golden Larva
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (411, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (411, 363, 1, 5, 10, 15, 20, 25, 1, 0);


-- Monster 412 : Sapphire Larva
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (412, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (412, 362, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (412, 13713, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (412, 13593, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (412, 546, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 400);


-- Monster 413 : Ruby Larva
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (413, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (413, 13714, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (413, 363, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (413, 13595, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (413, 547, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 300);


-- Monster 414 : Emerald Larva
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (414, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (414, 13500, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (414, 364, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (414, 13594, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (414, 544, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (414, 1681, 63, 0.05, 0.06, 0.08, 0.08, 0.09, 1, 0);


-- Monster 422 : Dark Vlad Dopple
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (422, 13489, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (422, 965, 1, 2, 2, 2, 2, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (422, 737, 1, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 400);


-- Monster 423 : Giant Kralove
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (423, 8813, 63, 25, 25, 25, 25, 25, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (423, 311, 63, 20, 20, 20, 20, 20, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (423, 8812, 63, 5, 5, 5, 5, 5, 1, 400);


-- Monster 424 : First Tentacle

-- Monster 426 : Little Jan

-- Monster 427 : Lady Meriane

-- Monster 429 : Lemon Jelly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (429, 1736, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (429, 2436, 63, 10, 11, 12, 13, 14, 1, 0);


-- Monster 430 : Royal Lemon Jelly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (430, 2436, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (430, 2437, 63, 20, 20, 20, 20, 20, 1, 0);


-- Monster 432 : Weak Arachnee

-- Monster 433 : Dancing Sword

-- Monster 434 : Flying Sword

-- Monster 435 : Small White Gobbly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (435, 260, 1, 100, 100, 100, 100, 100, 1, 0);


-- Monster 436 : Rabbit

-- Monster 437 : Mili-Miliboowolf

-- Monster 438 : Imp

-- Monster 439 : Robot Mace

-- Monster 440 : Pushy Robot

-- Monster 441 : Handyman

-- Monster 442 : Grossewer Rat
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (442, 8481, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (442, 2322, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (442, 8482, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 443 : Handyman

-- Monster 444 : Robot Mace

-- Monster 445 : Pushy Robot

-- Monster 447 : Grossewer Shaman
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (447, 2322, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (447, 8483, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (447, 8484, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (447, 686, 1, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (447, 2404, 1, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (447, 816, 1, 0.01, 0.02, 0.03, 0.04, 0.05, 1, 400);


-- Monster 448 : Bondu

-- Monster 449 : Hyoactive Rat

-- Monster 450 : Sewer Keeper
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (450, 1662, 1, 80, 82, 84, 86, 88, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (450, 13341, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (450, 13734, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (450, 6654, 1, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (450, 721, 1, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 400);


-- Monster 453 : Bearman
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (453, 6842, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (453, 6844, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (453, 6841, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (453, 2318, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (453, 6843, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (453, 6845, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (453, 6479, 1, 2.5, 2.5, 2.5, 2.5, 2.5, 1, 0);


-- Monster 454 : Bear

-- Monster 455 : Sacrier Dopple

-- Monster 456 : Golden Larva
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (456, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (456, 13715, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (456, 13596, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 457 : Shin Larva
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (457, 2328, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (457, 8385, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (457, 8386, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 458 : Radlu Minite the Dark Miner

-- Monster 465 : Sick Grossewer Rat

-- Monster 466 : One-armed Bandit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (466, 13342, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (466, 6923, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);


-- Monster 467 : Dark Rose
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (467, 2498, 1, 0.3, 0.3, 0.3, 0.3, 0.3, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (467, 2663, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (467, 2485, 1, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (467, 2489, 1, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 200);


-- Monster 468 : Louse Degraine

-- Monster 469 : Mean Squirrel

-- Monster 472 : Perverse Arachnee
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (472, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (472, 365, 6, 45, 46, 47, 48, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (472, 2478, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (472, 1673, 1, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 400);


-- Monster 473 : Sick Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (473, 367, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (473, 301, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (473, 366, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (473, 764, 1, 0, 10, 20, 30, 40, 1, 0);


-- Monster 474 : Sick Arachnee
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (474, 365, 63, 45, 46, 47, 48, 49, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (474, 854, 1, 10, 20, 30, 40, 50, 1, 0);


-- Monster 475 : Sick Grossewer Milirat
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (475, 2322, 63, 20, 30, 40, 50, 60, 1, 0);


-- Monster 476 : Kaba

-- Monster 478 : Bworker
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (478, 6885, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (478, 8145, 63, 60, 60, 60, 60, 60, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (478, 448, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (478, 13157, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (478, 9401, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 479 : Mama Bwork
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (479, 8145, 63, 50, 52, 54, 56, 58, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (479, 8388, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (479, 13736, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (479, 13737, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (479, 8389, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 482 : Artempth Rose

-- Monster 483 : Crackrock
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (483, 448, 63, 5.5, 6, 6.5, 7, 7.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (483, 450, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (483, 546, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (483, 543, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (483, 544, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (483, 545, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);


-- Monster 484 : Elemental Fire Bwork
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (484, 391, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (484, 13738, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (484, 13739, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 485 : Elemental Water Bwork
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (485, 391, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (485, 13742, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (485, 13743, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 486 : Elemental Air Bwork
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (486, 391, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (486, 13744, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (486, 13745, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 487 : Elemental Earth Bwork
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (487, 391, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (487, 13740, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (487, 13741, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 488 : Cybwork
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (488, 8145, 63, 50, 52, 54, 56, 58, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (488, 13746, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (488, 13747, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 489 : Red Piwi
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (489, 6900, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (489, 287, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (489, 2475, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 490 : Green Piwi
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (490, 287, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (490, 6899, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (490, 2475, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 491 : Blue Piwi
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (491, 287, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (491, 6897, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (491, 2475, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 492 : Pink Piwi
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (492, 287, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (492, 6903, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (492, 2475, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 493 : Yellow Piwi
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (493, 287, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (493, 6902, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (493, 2475, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 494 : Poutch Ingball

-- Monster 495 : Ouassingal
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (495, 8801, 1, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (495, 8807, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (495, 1575, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 496 : Ouassingue
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (496, 8801, 1, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (496, 8807, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (496, 1575, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 498 : Gargoyl
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (498, 718, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);


-- Monster 499 : Kwismas Minotoball
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (499, 6962, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (499, 6963, 63, 100, 100, 100, 100, 100, 1, 0);


-- Monster 500 : Animated Gift

-- Monster 501 : Goriya

-- Monster 502 : Timouss

-- Monster 503 : Tofawu

-- Monster 504 : Akwadala Guard

-- Monster 505 : Evil Dopple

-- Monster 506 : Ikwa Disciple

-- Monster 507 : Akwadala Guard

-- Monster 508 : Aerdala Guard

-- Monster 509 : Aerdala Guard

-- Monster 510 : Terrdala Guard

-- Monster 511 : Terrdala Guard

-- Monster 512 : Feudala Guard

-- Monster 513 : Feudala Guard

-- Monster 514 : Pandawushu Master

-- Monster 515 : Bulbiflor
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (515, 7262, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (515, 7266, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (515, 7270, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 516 : Pandawasta

-- Monster 517 : Pandit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (517, 7025, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (517, 350, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (517, 2287, 63, 1, 1, 1, 1, 1, 1, 101);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (517, 7017, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (517, 6923, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);


-- Monster 518 : Bulbush
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (518, 7263, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (518, 7267, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (518, 7223, 63, 5, 10, 15, 20, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (518, 7271, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 519 : Bulbig
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (519, 7223, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (519, 7222, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (519, 7225, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (519, 7224, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (519, 7265, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (519, 7269, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);


-- Monster 520 : Araknawa
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (520, 7285, 63, 20, 22, 24, 26, 28, 1, 0);


-- Monster 521 : Micromata
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (521, 7225, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (521, 7025, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (521, 7024, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (521, 7023, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (521, 7224, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (521, 7223, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (521, 7222, 1, 5, 5, 5, 5, 5, 1, 0);


-- Monster 522 : Grass Snake
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (522, 7273, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (522, 7223, 63, 10, 12, 14, 16, 18, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (522, 7222, 63, 2, 3, 4, 5, 6, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (522, 7274, 63, 1, 1, 1, 1, 1, 1, 300);


-- Monster 523 : Kitsou Nakwa
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (523, 7222, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (523, 7282, 63, 30, 32, 34, 36, 38, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (523, 7278, 63, 5, 6, 7, 8, 9, 1, 0);


-- Monster 524 : Bambooto
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (524, 7287, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (524, 7286, 63, 15, 16, 17, 18, 19, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (524, 7369, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (524, 7290, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 300);


-- Monster 525 : Poacher
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (525, 12739, 63, 10, 11, 12, 13, 14, 1, 100);


-- Monster 527 : Yokai Firefoux
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (527, 7294, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (527, 7225, 1, 5, 10, 15, 20, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (527, 7025, 1, 5, 5, 5, 5, 5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (527, 7300, 1, 5, 6, 7, 8, 9, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (527, 7306, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);


-- Monster 528 : Soryo Firefoux
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (528, 7293, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (528, 7225, 1, 5, 10, 15, 20, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (528, 7299, 1, 5, 6, 7, 8, 9, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (528, 7024, 1, 5, 5, 5, 5, 5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (528, 7305, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);


-- Monster 529 : Maho Firefoux
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (529, 7292, 63, 20, 20, 20, 20, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (529, 7225, 3, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (529, 7298, 1, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (529, 7023, 1, 5, 5, 5, 5, 5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (529, 7304, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);


-- Monster 530 : Kitsou Nere
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (530, 7224, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (530, 7281, 63, 30, 32, 34, 36, 38, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (530, 7277, 63, 5, 6, 7, 8, 9, 1, 0);


-- Monster 531 : Kitsou Nae
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (531, 7223, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (531, 7279, 63, 30, 32, 34, 36, 38, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (531, 7275, 63, 5, 6, 7, 8, 9, 1, 0);


-- Monster 532 : Kitsou Nufeu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (532, 7225, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (532, 7280, 63, 30, 32, 34, 36, 38, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (532, 7276, 63, 5, 6, 7, 8, 9, 1, 0);


-- Monster 534 : Drunken Pandawa
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (534, 7222, 1, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (534, 680, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);


-- Monster 535 : Leopardo
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (535, 7343, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (535, 7225, 1, 20, 30, 40, 50, 60, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (535, 7301, 63, 15, 16, 17, 18, 19, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (535, 7024, 1, 1, 1, 1, 1, 1, 1, 300);


-- Monster 536 : Heitit

-- Monster 537 : Pandora
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (537, 7295, 63, 20, 20, 20, 20, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (537, 7261, 63, 10, 11, 12, 13, 14, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (537, 7302, 63, 6, 6, 6, 6, 6, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (537, 7307, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);


-- Monster 538 : Feudala's Heart

-- Monster 539 : Terrdala's Heart

-- Monster 540 : Aerdala's Heart

-- Monster 541 : Akwadala's Heart

-- Monster 542 : Aerdala's Heart

-- Monster 543 : Terrdala's Heart

-- Monster 544 : Akwadala's Heart

-- Monster 545 : Feudala's Heart

-- Monster 546 : Holy Bambooto
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (546, 7288, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (546, 7370, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (546, 7289, 63, 5, 6, 7, 8, 9, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (546, 7291, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);


-- Monster 547 : Drunken Pandalette
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (547, 7222, 63, 5, 10, 15, 20, 25, 1, 100);


-- Monster 548 : Bulbamboo
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (548, 7268, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (548, 7264, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (548, 7272, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 300);


-- Monster 549 : Pandulum
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (549, 7297, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (549, 7222, 63, 10, 15, 20, 25, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (549, 7303, 63, 5, 6, 7, 8, 9, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (549, 7308, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);


-- Monster 550 : Rok Gnorok
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (550, 448, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (550, 450, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (550, 543, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (550, 546, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (550, 544, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (550, 545, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);


-- Monster 551 : Guard

-- Monster 552 : Musha the Oni
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (552, 838, 63, 10, 10, 10, 10, 10, 1, 150);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (552, 546, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (552, 1538, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (552, 1537, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (552, 1536, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (552, 1535, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 553 : Guard

-- Monster 554 : Marzwel the Goblin
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (554, 2318, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 555 : Zatoishwan

-- Monster 556 : Cawwot

-- Monster 557 : Air Pandawashu Disciple
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (557, 7223, 1, 100, 100, 100, 100, 100, 1, 0);


-- Monster 558 : Water Pandawushu Disciple
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (558, 7222, 1, 100, 100, 100, 100, 100, 1, 0);


-- Monster 559 : Fire Pandawushu Disciple
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (559, 7225, 1, 100, 100, 100, 100, 100, 1, 0);


-- Monster 560 : Earth Pandawushu Disciple
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (560, 7224, 1, 100, 100, 100, 100, 100, 1, 0);


-- Monster 561 : Pandawushu Disciple

-- Monster 562 : Kitsou Nae

-- Monster 563 : Kitsou Nakwa

-- Monster 564 : Kitsou Nere

-- Monster 565 : Kitsou Nufeu

-- Monster 566 : Pandikaze
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (566, 7258, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (566, 7260, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (566, 7025, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (566, 7017, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (566, 7259, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 567 : Sankwa Disciple

-- Monster 568 : Tanukouï San
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (568, 7224, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (568, 7223, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (568, 7225, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (568, 7222, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (568, 7283, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (568, 7284, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (568, 7386, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 569 : Tanuki Chan

-- Monster 570 : Yadrutas
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (570, 13489, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (570, 965, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 571 : Tanuki Chan

-- Monster 572 : Tanuki Chan

-- Monster 574 : Tanuki Chan

-- Monster 575 : Yadrutas the Bloodthirsty
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (575, 965, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 576 : Yadrutas the Launderer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (576, 965, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 577 : Tanuki Chan

-- Monster 578 : Aerial Pandikaze
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (578, 7258, 63, 20, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (578, 7260, 63, 20, 20, 20, 20, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (578, 7259, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 579 : Giddy Pandikaze
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (579, 7258, 63, 28, 28, 28, 28, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (579, 7260, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (579, 13497, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (579, 7025, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (579, 7259, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 580 : Pandikwakaze
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (580, 1517, 63, 2, 0, 0, 0, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (580, 1518, 63, 0, 0, 2, 0, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (580, 1515, 63, 0, 0, 0, 2, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (580, 1516, 63, 0, 2, 0, 0, 0.5, 1, 200);


-- Monster 581 : Pandulkaze
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (581, 7297, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (581, 7303, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (581, 7308, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);


-- Monster 582 : Touchparak

-- Monster 583 : Pandikaze Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (583, 7025, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (583, 7224, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (583, 7223, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (583, 7225, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (583, 7222, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (583, 7405, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (583, 7379, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 584 : Maho Firefoux Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (584, 7224, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (584, 7223, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (584, 7023, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (584, 7225, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (584, 7222, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (584, 7261, 63, 10, 11, 12, 13, 14, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (584, 7404, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (584, 7380, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 585 : Soryo Firefoux Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (585, 7224, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (585, 7222, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (585, 7225, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (585, 7223, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (585, 7024, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (585, 7408, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (585, 7381, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 586 : Leopardo Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (586, 7024, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (586, 7224, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (586, 7225, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (586, 7222, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (586, 7223, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (586, 7403, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (586, 7382, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 587 : Yokai Firefoux Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (587, 7225, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (587, 7025, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (587, 7222, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (587, 7223, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (587, 7224, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (587, 7411, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (587, 7383, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 588 : Tanukouï San Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (588, 7224, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (588, 7223, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (588, 7025, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (588, 7225, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (588, 7222, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (588, 7410, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (588, 7385, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 589 : Pandulum Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (589, 7223, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (589, 7023, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (589, 7224, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (589, 7222, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (589, 7225, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (589, 7407, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (589, 7384, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 590 : Tanuki Chan Ghost

-- Monster 594 : Tanuki Chan Ghost

-- Monster 595 : Tanuki Chan Ghost

-- Monster 596 : Tanuki Chan Ghost

-- Monster 597 : Pandora Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (597, 7224, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (597, 7223, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (597, 7222, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (597, 7225, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (597, 7024, 63, 20, 21, 22, 23, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (597, 7406, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (597, 7393, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 598 : Tanuki Chan Ghost

-- Monster 599 : Zilla

-- Monster 600 : Blodz Uker
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (600, 8392, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (600, 479, 63, 1, 1, 1, 1, 1, 1, 300);


-- Monster 601 : Dorgan Ation
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (601, 8391, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (601, 479, 63, 1, 1, 1, 1, 1, 1, 300);


-- Monster 603 : Farle's Pig
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (603, 8390, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (603, 479, 63, 1, 1, 1, 1, 1, 1, 300);


-- Monster 604 : Iop Mass Test

-- Monster 605 : Peki Peki
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (605, 7224, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (605, 7223, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (605, 7225, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (605, 7222, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (605, 7023, 63, 30, 30, 30, 30, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (605, 7024, 63, 30, 30, 30, 30, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (605, 7025, 63, 30, 30, 30, 30, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (605, 8251, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (605, 8380, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (605, 8381, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 606 : Pandora Bokse

-- Monster 607 : Pandikaze Warrior

-- Monster 608 : Pandikaze Box

-- Monster 609 : Pandamani

-- Monster 611 : Pandora Ghost Bokse

-- Monster 612 : Pandora Master
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (612, 7225, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (612, 7222, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (612, 7224, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (612, 7223, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (612, 7295, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (612, 7025, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (612, 7023, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (612, 7024, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (612, 7302, 63, 20, 20, 20, 20, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (612, 13169, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (612, 7307, 63, 2, 2, 2, 2, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (612, 8401, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (612, 8399, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (612, 8400, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 613 : Test 2
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (613, 1662, 2, 10, 20, 30, 40, 50, 1, 0);


-- Monster 616 : Bow Meow Ghost

-- Monster 631 : Crow Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (631, 2239, 1, 5, 5, 5, 5, 5, 1, 0);


-- Monster 632 : Young Wild Boar Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (632, 2239, 1, 5, 5, 5, 5, 5, 1, 0);


-- Monster 634 : Nomoon Ghost

-- Monster 649 : Dark Chest
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (649, 1660, 1, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (649, 1575, 1, 80, 80, 80, 80, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (649, 543, 1, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (649, 810, 1, 30, 30, 30, 30, 30, 1, 0);


-- Monster 650 : Treechnee
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (650, 463, 63, 10, 11, 12, 13, 14, 1, 0);


-- Monster 651 : Dark Treechnee
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (651, 544, 63, 10, 10, 10, 10, 10, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (651, 463, 63, 10, 11, 12, 13, 14, 1, 0);


-- Monster 652 : Green Spimush
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (652, 300, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (652, 377, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (652, 290, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (652, 1676, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (652, 1674, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 120);


-- Monster 653 : Red Spimush
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (653, 300, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (653, 290, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (653, 377, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (653, 378, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (653, 1674, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 120);


-- Monster 654 : Blue Spimush
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (654, 300, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (654, 290, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (654, 377, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (654, 378, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (654, 1674, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 120);


-- Monster 655 : Brown Spimush
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (655, 377, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (655, 290, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (655, 300, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (655, 378, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (655, 1674, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 120);


-- Monster 659 : Dragoone Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (659, 8012, 63, 100, 100, 100, 100, 100, 1, 100);


-- Monster 661 : Croum Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (661, 2239, 1, 5, 5, 5, 5, 5, 1, 0);


-- Monster 662 : Ghast Ghost

-- Monster 663 : Leopardo Ghost

-- Monster 666 : Wild Golden Dragoturkey
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (666, 13488, 63, 10, 11, 12, 13, 14, 1, 0);


-- Monster 667 : Summon Killer

-- Monster 668 : Minokid
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (668, 8311, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (668, 13504, 63, 10, 11, 12, 13, 14, 1, 100);


-- Monster 669 : Legendary Crackler
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (669, 450, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (669, 8102, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (669, 543, 63, 5, 5, 5, 5, 5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (669, 546, 63, 5, 5, 5, 5, 5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (669, 7024, 63, 5, 5, 5, 5, 5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (669, 544, 63, 5, 5, 5, 5, 5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (669, 547, 63, 5, 5, 5, 5, 5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (669, 7023, 63, 5, 5, 5, 5, 5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (669, 545, 63, 5, 5, 5, 5, 5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (669, 7025, 63, 3, 3.5, 4, 4.5, 5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (669, 8394, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 670 : Koolich
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (670, 7904, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (670, 7903, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (670, 8403, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (670, 8402, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 671 : Cave Gobball
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (671, 383, 63, 55, 57, 59, 61, 63, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (671, 385, 63, 50, 52, 54, 56, 58, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (671, 384, 63, 50, 52, 54, 56, 58, 1, 100);


-- Monster 672 : Gobkool
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (672, 7906, 63, 55, 57, 59, 61, 63, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (672, 7907, 63, 50, 52, 54, 56, 58, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (672, 7905, 63, 50, 52, 54, 56, 58, 1, 100);


-- Monster 673 : Emeralda
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (673, 13702, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (673, 13706, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 674 : Emerald Doll

-- Monster 675 : Sapphira
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (675, 13703, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (675, 13707, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 676 : Starving Doll

-- Monster 677 : Ruby
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (677, 13704, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (677, 13708, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 678 : Motivating Totem

-- Monster 679 : Explosive Totem

-- Monster 680 : Healing Totem

-- Monster 681 : Diamondine
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (681, 13705, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (681, 13709, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 682 : Lethal Doll

-- Monster 684 : Wheat Scarecrow

-- Monster 685 : Barley Scarecrow

-- Monster 686 : Oat Scarecrow

-- Monster 687 : Hop Scarecrow

-- Monster 688 : Flax Scarecrow

-- Monster 689 : Rye Scarecrow

-- Monster 690 : Rice Scarecrow

-- Monster 691 : Malt Scarecrow

-- Monster 692 : Hemp Scarecrow

-- Monster 693 : Iron Crackrock

-- Monster 694 : Copper Crackrock

-- Monster 695 : Bronze Crackrock

-- Monster 696 : Cobalt Crackrock

-- Monster 697 : Manganese Crackrock

-- Monster 698 : Tin Crackrock

-- Monster 699 : Silicate Crackrock

-- Monster 700 : Silver Crackrock

-- Monster 701 : Bauxite Crackrock

-- Monster 702 : Gold Crackrock

-- Monster 703 : Dolomite Crackrock

-- Monster 704 : Bulbiflax

-- Monster 705 : Bulbihemp

-- Monster 706 : Bulbiclover

-- Monster 707 : Bulbimint

-- Monster 708 : Bulborchid

-- Monster 709 : Bulbedelweiss

-- Monster 710 : Bulpandkin

-- Monster 711 : Bewitched Ash

-- Monster 712 : Bewitched Chestnut

-- Monster 713 : Bewitched Walnut

-- Monster 714 : Bewitched Oak

-- Monster 715 : Bewitched Bombu

-- Monster 716 : Bewitched Oliviolet

-- Monster 717 : Bewitched Maple

-- Monster 718 : Bewitched Yew

-- Monster 719 : Bewitched Bamboo

-- Monster 720 : Bewitched Kaliptus

-- Monster 721 : Bewitched Cherry

-- Monster 722 : Bewitched Ebony

-- Monster 723 : Bewitched Dark Bamboo

-- Monster 724 : Bewitched Elm

-- Monster 725 : Bewitched Holy Bamboo

-- Monster 726 : Gudgeon Devourer

-- Monster 727 : Trout Devourer

-- Monster 728 : Kittenfish Devourer

-- Monster 729 : Grawn Devourer

-- Monster 730 : Crab Devourer

-- Monster 731 : Breaded Fish Devourer

-- Monster 732 : Pike Devourer

-- Monster 733 : Carp Devourer

-- Monster 734 : Sardine Devourer

-- Monster 735 : Kralove Devourer

-- Monster 736 : Bass Devourer

-- Monster 737 : Skate Devourer

-- Monster 738 : Perch Devourer

-- Monster 739 : Shark Devourer

-- Monster 744 : Bloody Koalak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (744, 13698, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (744, 7904, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (744, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (744, 8064, 63, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (744, 8063, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 745 : Purple Warko
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (745, 13700, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (745, 7904, 63, 2, 2, 2, 2, 2, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (745, 7903, 63, 2, 2, 2, 2, 2, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (745, 8066, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 746 : Brown Warko
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (746, 7904, 63, 2, 2, 2, 2, 2, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (746, 7903, 63, 2, 2, 2, 2, 2, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (746, 8077, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (746, 8065, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 747 : Dok Alako
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (747, 8002, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (747, 13731, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (747, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (747, 7904, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (747, 8075, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 100);


-- Monster 748 : Immature Koalak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (748, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (748, 7904, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (748, 8050, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 749 : Koalak Warrior
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (749, 13697, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (749, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (749, 7904, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (749, 8001, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 751 : Indigo Koalak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (751, 8062, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (751, 7904, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (751, 7903, 63, 5, 5, 5, 5, 5, 1, 100);


-- Monster 752 : Coco Koalak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (752, 7904, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (752, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (752, 8060, 63, 5, 5.5, 6, 6.5, 7, 1, 300);


-- Monster 753 : Morello Cherry Koalak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (753, 8059, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (753, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (753, 7904, 63, 5, 5, 5, 5, 5, 1, 100);


-- Monster 754 : Pippin Koalak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (754, 7904, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (754, 8061, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (754, 7903, 63, 5, 5, 5, 5, 5, 1, 100);


-- Monster 755 : Mama Koalak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (755, 7904, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (755, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (755, 8055, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 756 : Wild Koalak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (756, 13699, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (756, 7904, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (756, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (756, 8056, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 758 : Koalak Master
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (758, 8052, 63, 10, 11, 12, 13, 14, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (758, 7904, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (758, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (758, 8076, 63, 1, 1, 1, 1, 1, 1, 100);


-- Monster 759 : Koalak Rider
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (759, 7904, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (759, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (759, 8053, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 760 : Koalak Mummy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (760, 8058, 63, 10, 11, 12, 13, 14, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (760, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (760, 7904, 63, 5, 5, 5, 5, 5, 1, 100);


-- Monster 761 : Koalak Gravedigger
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (761, 8057, 63, 10, 11, 12, 13, 14, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (761, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (761, 7904, 63, 5, 5, 5, 5, 5, 1, 100);


-- Monster 762 : Workette

-- Monster 763 : Drakoalak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (763, 8054, 63, 10, 11, 12, 13, 14, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (763, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (763, 7904, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (763, 8086, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 765 : Willy Peninzias Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (765, 2239, 1, 5, 5, 5, 5, 5, 1, 0);


-- Monster 767 : Bworky Ghost

-- Monster 776 : Alka Traz
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (776, 2318, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 777 : Guan Tanamault
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (777, 2318, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 778 : Luki Loushiano
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (778, 2318, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 779 : Edgg Komb

-- Monster 780 : Skeunk
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (780, 8405, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (780, 8404, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 782 : Bewitched Hornbeam

-- Monster 783 : Reapalak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (783, 13495, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (783, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (783, 7904, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (783, 8082, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 784 : Piralak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (784, 8084, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (784, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (784, 7904, 63, 5, 5, 5, 5, 5, 1, 100);


-- Monster 785 : Koalak Forester
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (785, 8085, 63, 10, 11, 12, 13, 14, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (785, 7903, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (785, 7904, 63, 5, 5, 5, 5, 5, 1, 100);


-- Monster 786 : Fisheralak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (786, 8083, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (786, 7904, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (786, 7903, 63, 5, 5, 5, 5, 5, 1, 100);


-- Monster 788 : Fiery Adventurer Ghost

-- Monster 789 : Arepo Adventurer Ghost

-- Monster 790 : Brave Adventurer Ghost

-- Monster 791 : Tamed Trool
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (791, 8519, 1, 30, 30, 30, 30, 30, 1, 100);


-- Monster 792 : Bworkette
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (792, 8145, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (792, 8388, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (792, 678, 63, 15, 15, 15, 15, 15, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (792, 680, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (792, 8389, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (792, 479, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 793 : Al Howin Gobball
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (793, 384, 63, 60, 65, 70, 75, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (793, 304, 63, 40, 45, 50, 55, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (793, 383, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (793, 385, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (793, 395, 63, 15, 20, 25, 30, 35, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (793, 13456, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (793, 2453, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (793, 2451, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (793, 2449, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (793, 2422, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (793, 2425, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (793, 2416, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (793, 2448, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (793, 2455, 63, 0.09, 0.09, 0.09, 0.09, 0.09, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (793, 2454, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 0);


-- Monster 794 : Al Howin Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (794, 367, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (794, 366, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (794, 301, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (794, 13454, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (794, 2476, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 795 : Black Scaraleaf
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (795, 8140, 63, 30, 31, 32, 33, 34, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (795, 8141, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (795, 8138, 63, 5, 5.5, 6, 6.5, 7, 1, 400);


-- Monster 796 : Black Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (796, 367, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (796, 301, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (796, 366, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (796, 2476, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 797 : Golden Scarabugly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (797, 8140, 63, 60, 60, 60, 60, 60, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (797, 8160, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (797, 8159, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (797, 8161, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (797, 8137, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 798 : Immature Scaraleaf

-- Monster 799 : Famished Sunflower
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (799, 8123, 1, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (799, 8126, 1, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (799, 8128, 1, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (799, 8124, 1, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (799, 8125, 1, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (799, 8127, 1, 5, 5, 5, 5, 5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (799, 8131, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (799, 8157, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 800 : Batofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (800, 301, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (800, 367, 63, 35, 35, 35, 35, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (800, 366, 63, 30, 30, 30, 30, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (800, 8557, 63, 15, 15, 15, 15, 15, 1, 100);


-- Monster 801 : Transgenic Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (801, 367, 1, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (801, 366, 1, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (801, 301, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (801, 2476, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 802 : Bomberfu

-- Monster 804 : Tofoone
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (804, 367, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (804, 301, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (804, 366, 63, 10, 15, 20, 25, 30, 1, 0);


-- Monster 805 : Mutant Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (805, 367, 1, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (805, 301, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (805, 366, 1, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (805, 2476, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 806 : Tofukaz
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (806, 367, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (806, 301, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (806, 366, 63, 10, 15, 20, 25, 30, 1, 0);


-- Monster 807 : Tofuzmo
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (807, 13719, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (807, 13721, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 808 : Tofurby
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (808, 367, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (808, 8158, 63, 10, 11, 12, 13, 14, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (808, 366, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (808, 301, 63, 10, 20, 30, 40, 50, 1, 0);


-- Monster 813 : Bworky Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (813, 2239, 1, 5, 5, 5, 5, 5, 1, 0);


-- Monster 815 : El Scarador Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (815, 2239, 1, 5, 5, 5, 5, 5, 1, 0);


-- Monster 816 : Gobtubby Ghost

-- Monster 817 : Treechster Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (817, 2239, 1, 5, 5, 5, 5, 5, 1, 0);


-- Monster 818 : Tamed Crow
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (818, 2060, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (818, 1889, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (818, 8252, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (818, 2056, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (818, 2058, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (818, 2059, 63, 0.03, 0.03, 0.03, 0.03, 0.03, 1, 400);


-- Monster 819 : Drinker
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (819, 2060, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (819, 1889, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (819, 8249, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (819, 2056, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (819, 2058, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (819, 2059, 63, 0.03, 0.03, 0.03, 0.03, 0.03, 1, 400);


-- Monster 820 : Crowfox
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (820, 2060, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (820, 1889, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (820, 8250, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (820, 2056, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (820, 2058, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (820, 2059, 63, 0.03, 0.03, 0.03, 0.03, 0.03, 1, 400);


-- Monster 823 : Foxo the Crowfox
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (823, 2060, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (823, 1889, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (823, 8250, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (823, 2056, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (823, 2058, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (823, 2059, 63, 0.03, 0.03, 0.03, 0.03, 0.03, 1, 400);


-- Monster 824 : Horace the Tamed Crow
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (824, 2060, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (824, 1889, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (824, 8252, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (824, 2056, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (824, 2058, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (824, 2059, 63, 0.03, 0.03, 0.03, 0.03, 0.03, 1, 400);


-- Monster 825 : Kapotie the Drinker
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (825, 2060, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (825, 1889, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (825, 8249, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (825, 2056, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (825, 2058, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (825, 2059, 63, 0.03, 0.03, 0.03, 0.03, 0.03, 1, 400);


-- Monster 826 : Eroded Kirball

-- Monster 827 : Minotot
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (827, 13168, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (827, 8409, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (827, 8410, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (827, 8407, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (827, 8406, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (827, 8408, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 828 : Jellidice
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (828, 8310, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 100);


-- Monster 829 : Quetsnakiatl
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (829, 8309, 63, 20, 21, 22, 23, 24, 1, 100);


-- Monster 830 : Scaratos
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (830, 8308, 63, 10, 11, 12, 13, 14, 1, 100);


-- Monster 831 : Mumminotor
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (831, 8322, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (831, 8324, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (831, 8326, 63, 10, 10, 10, 10, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (831, 8328, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 832 : Deminoball
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (832, 8323, 63, 25, 25, 25, 25, 25, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (832, 8321, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (832, 8327, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 834 : Minoskito
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (834, 8312, 63, 20, 21, 22, 23, 24, 1, 100);


-- Monster 835 : Manderisha
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (835, 8315, 63, 20, 21, 22, 23, 24, 1, 100);


-- Monster 836 : Khamelerost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (836, 8314, 63, 20, 21, 22, 23, 24, 1, 100);


-- Monster 839 : Abominable Snow Yiti
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (839, 12839, 63, 20, 25, 0, 0, 0, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (839, 12132, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (839, 12840, 63, 0, 0, 5, 10, 15, 1, 0);


-- Monster 840 : Wintry Kaniger
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (840, 12838, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (840, 12132, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 841 : Frozen Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (841, 12132, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (841, 12838, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 842 : Unruly Goblimp
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (842, 12838, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (842, 12132, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 843 : Nefarious Goblimp
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (843, 12132, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (843, 12840, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 844 : Mastogob

-- Monster 845 : Snowman
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (845, 12132, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (845, 12839, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 846 : Icikle

-- Monster 847 : Hibernal Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (847, 12132, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (847, 12839, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 848 : Black Dragoss
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (848, 8352, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (848, 8348, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (848, 8344, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 849 : Pricky
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (849, 12838, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (849, 12132, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 851 : Cranky Goblimp
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (851, 12132, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (851, 12839, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 853 : Dreggon Warrior
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (853, 8362, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (853, 8364, 63, 2, 2.1, 2.2, 2.3, 2.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (853, 8363, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 854 : Crocabulia
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (854, 8367, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (854, 8365, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (854, 8368, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (854, 6980, 1, 0.001, 0.001, 0.001, 0.001, 0.001, 1, 400);


-- Monster 855 : Dragostess
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (855, 8356, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (855, 8357, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (855, 8358, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 858 : Alert Black Dragoss
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (858, 8352, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (858, 8348, 63, 2, 2.1, 2.2, 2.3, 2.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (858, 8344, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 100);


-- Monster 862 : Flying Dreggon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (862, 8360, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (862, 8359, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (862, 8361, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 864 : Sakai Firefoux
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (864, 12132, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (864, 12840, 63, 15, 15, 15, 15, 15, 1, 0);


-- Monster 867 : Snowball
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (867, 12132, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (867, 12839, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 868 : Snowy Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (868, 12132, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (868, 12838, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 869 : Ice Crackler
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (869, 12839, 63, 15, 20, 25, 0, 0, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (869, 12132, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (869, 12840, 63, 0, 0, 0, 5, 10, 1, 0);


-- Monster 870 : Larvicy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (870, 12132, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (870, 12838, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 871 : Larvicily
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (871, 12132, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (871, 12839, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 872 : Father Kwismas
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (872, 12839, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (872, 12132, 63, 100, 100, 100, 100, 100, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (872, 10913, 63, 30, 30, 30, 30, 30, 1, 400);


-- Monster 874 : Half Father Kwismas

-- Monster 876 : Bwork
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (876, 8145, 63, 50, 52, 54, 56, 58, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (876, 392, 63, 25, 37, 30, 32, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (876, 391, 63, 25, 27, 30, 32, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (876, 448, 63, 18, 20, 22, 24, 26, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (876, 3001, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (876, 3002, 63, 4, 5, 5, 6, 6, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (876, 2271, 63, 2, 2.5, 2.5, 3, 3, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (876, 3000, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 877 : Aqualikros the Merciless
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (877, 8353, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (877, 8349, 63, 2, 2.1, 2.2, 2.3, 2.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (877, 8345, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 878 : Alert White Dragoss
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (878, 8354, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (878, 8350, 63, 2, 2.1, 2.2, 2.3, 2.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (878, 8346, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 100);


-- Monster 879 : Alert Golden Dragoss
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (879, 8355, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (879, 8351, 63, 2, 2.1, 2.2, 2.3, 2.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (879, 8347, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 100);


-- Monster 880 : Stuk'kiddy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (880, 8335, 63, 50, 51, 52, 53, 54, 1, 0);


-- Monster 881 : Chop'kiddy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (881, 8335, 63, 50, 51, 52, 53, 54, 1, 0);


-- Monster 882 : Pop'Kiddy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (882, 8335, 63, 50, 51, 52, 53, 54, 1, 0);


-- Monster 884 : Sapphire Dragoss
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (884, 8353, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (884, 8349, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (884, 8345, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 885 : White Dragoss
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (885, 8354, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (885, 8350, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (885, 8346, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 886 : Golden Dragoss
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (886, 8355, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (886, 8351, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (886, 8347, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 887 : Prez' Ent
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (887, 8335, 63, 50, 51, 52, 53, 54, 1, 0);


-- Monster 888 : Prez' Plozion
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (888, 12132, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (888, 12840, 63, 15, 15, 15, 15, 15, 1, 0);


-- Monster 889 : Prez' Pertize

-- Monster 890 : Baby Prez

-- Monster 891 : Mini Inuit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (891, 12132, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (891, 12840, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 892 : Terrakubiack the Warrior
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (892, 8362, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (892, 8364, 63, 2, 2.1, 2.2, 2.3, 2.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (892, 8363, 3, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 893 : Ignilicrobur the Warrior
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (893, 8362, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (893, 8364, 63, 2, 2.1, 2.2, 2.3, 2.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (893, 8363, 3, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 894 : Aeroktor the Warrior
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (894, 8362, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (894, 8364, 63, 2, 2.1, 2.2, 2.3, 2.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (894, 8363, 3, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 895 : Aquabralak the Warrior
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (895, 8362, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (895, 8364, 63, 2, 2.1, 2.2, 2.3, 2.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (895, 8363, 3, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 896 : Kitsou Nakwatus
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (896, 12838, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (896, 12132, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 899 : Black Tiwabbitus
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (899, 12838, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (899, 12132, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 901 : Kwakus
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (901, 12132, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (901, 12838, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 902 : Ignirkocropos the Famished
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (902, 8352, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (902, 8348, 63, 2, 2.1, 2.2, 2.3, 2.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (902, 8344, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 903 : Terraburkahl the Perfidious
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (903, 8354, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (903, 8350, 63, 2, 2.1, 2.2, 2.3, 2.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (903, 8346, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 904 : Aerogoburius the Malicious
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (904, 8355, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (904, 8351, 63, 2, 2.1, 2.2, 2.3, 2.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (904, 8347, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 905 : Alert Sapphire Dragoss
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (905, 8353, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (905, 8349, 63, 2, 2.1, 2.2, 2.3, 2.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (905, 8345, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 100);


-- Monster 906 : Prez
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (906, 12132, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (906, 12840, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 907 : Minimini Inuit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (907, 12132, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (907, 12839, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 917 : Arachnotron

-- Monster 919 : Sick Treechnid

-- Monster 920 : Orange Snapper
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (920, 13727, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (920, 8680, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (920, 598, 63, 1, 1, 1, 1, 1, 1, 0);


-- Monster 921 : Blue Snapper
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (921, 13494, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (921, 8680, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (921, 598, 63, 1, 1, 1, 1, 1, 1, 0);


-- Monster 922 : White Snapper
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (922, 13487, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (922, 8680, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (922, 598, 63, 1, 1, 1, 1, 1, 1, 0);


-- Monster 923 : Green Snapper
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (923, 13726, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (923, 8680, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (923, 598, 63, 1, 1, 1, 1, 1, 1, 0);


-- Monster 924 : Kloon Snapper
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (924, 13502, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (924, 8680, 63, 5, 6, 7, 8, 9, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (924, 598, 63, 1, 2, 3, 4, 5, 1, 0);


-- Monster 926 : Raul Mops
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (926, 8680, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (926, 8681, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 927 : Starfish Trooper
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (927, 13728, 63, 10, 11, 12, 13, 14, 1, 100);


-- Monster 928 : Sponge Mob
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (928, 8682, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (928, 8253, 63, 5, 5, 5, 5, 5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (928, 8680, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (928, 8254, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (928, 7077, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (928, 8256, 63, 2, 2, 2, 2, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (928, 8258, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (928, 8259, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (928, 8257, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (928, 8255, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);


-- Monster 932 : Apero Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (932, 13491, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (932, 351, 63, 10, 11, 12, 13, 14, 1, 100);


-- Monster 934 : The Block

-- Monster 935 : Rat Suenami
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (935, 13340, 1, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (935, 2322, 63, 10, 11, 12, 13, 14, 1, 0);


-- Monster 936 : Rat Tchet
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (936, 13340, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (936, 2322, 63, 10, 11, 12, 13, 14, 1, 0);


-- Monster 937 : Rat Bag
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (937, 13340, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (937, 2322, 63, 10, 11, 12, 13, 14, 1, 0);


-- Monster 938 : Rat Basher
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (938, 13492, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (938, 2322, 63, 10, 11, 12, 13, 14, 1, 0);


-- Monster 939 : Black Rat
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (939, 8491, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (939, 8485, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (939, 8488, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (939, 2322, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (939, 13340, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (939, 680, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 100);


-- Monster 940 : White Rat
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (940, 8492, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (940, 8489, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (940, 13492, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (940, 8486, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (940, 2322, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (940, 680, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 100);


-- Monster 941 : Rat Pakk
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (941, 2322, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (941, 13492, 63, 10, 11, 12, 13, 14, 1, 0);


-- Monster 942 : Rat Rah
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (942, 13492, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (942, 2322, 63, 10, 11, 12, 13, 14, 1, 0);


-- Monster 943 : Sphincter Cell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (943, 7714, 1, 90, 90, 90, 90, 90, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (943, 13155, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (943, 8493, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (943, 8490, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (943, 2322, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (943, 8487, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 944 : Raphaela

-- Monster 945 : Donatella

-- Monster 946 : Michelangela

-- Monster 947 : Leonardawa

-- Monster 954 : Mumussel
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (954, 8518, 63, 20, 25, 30, 35, 35, 1, 100);


-- Monster 955 : Feca Dopple

-- Monster 957 : Enutrof Dopple

-- Monster 958 : Sram Dopple

-- Monster 959 : Xelor Dopple

-- Monster 960 : Ecaflip Dopple

-- Monster 961 : Eniripsa Dopple

-- Monster 962 : Iop Dopple

-- Monster 963 : Cra Dopple

-- Monster 964 : Sadida Dopple

-- Monster 969 : Pandawa Dopple

-- Monster 970 : Small Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (970, 367, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (970, 301, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (970, 366, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (970, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (970, 2476, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 971 : Little Gobball
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (971, 384, 63, 60, 65, 70, 75, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (971, 304, 63, 40, 45, 50, 55, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (971, 383, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (971, 385, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (971, 395, 63, 15, 20, 25, 30, 35, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (971, 2448, 63, 2, 2, 2, 2, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (971, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (971, 2449, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (971, 2451, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (971, 2453, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (971, 2422, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (971, 2416, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (971, 2425, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (971, 2455, 63, 0.09, 0.09, 0.09, 0.09, 0.09, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (971, 2454, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 0);


-- Monster 972 : Young White Gobbly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (972, 881, 63, 60, 65, 70, 75, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (972, 883, 63, 40, 50, 60, 70, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (972, 385, 63, 25, 30, 35, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (972, 2460, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (972, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (972, 2419, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);


-- Monster 973 : Young Black Gobbly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (973, 885, 63, 70, 71, 72, 73, 74, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (973, 884, 63, 60, 65, 70, 75, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (973, 385, 63, 19, 20, 21, 22, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (973, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (973, 2460, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (973, 2428, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 974 : Fearful Moskito
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (974, 307, 63, 55, 60, 65, 80, 85, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (974, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (974, 371, 63, 15, 20, 25, 40, 45, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (974, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (974, 6917, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (974, 6918, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (974, 2477, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (974, 6916, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (974, 6915, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 975 : Immature Blue Larva
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (975, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (975, 362, 63, 20, 20, 30, 30, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (975, 7423, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (975, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (975, 2474, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (975, 643, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);


-- Monster 976 : Immature Orange Larva
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (976, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (976, 363, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (976, 7423, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (976, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (976, 643, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);


-- Monster 977 : Immature Green Larva
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (977, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (977, 364, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (977, 7423, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (977, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (977, 643, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (977, 1681, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 0);


-- Monster 978 : Vulnerable Mush Mush
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (978, 290, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (978, 300, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (978, 377, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (978, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (978, 6920, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (978, 6922, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (978, 6921, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (978, 2473, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (978, 6919, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (978, 378, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (978, 1674, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 120);


-- Monster 979 : Frightened Evil Dandelion
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (979, 306, 63, 22, 24, 26, 28, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (979, 373, 63, 5, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (979, 374, 63, 5, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (979, 2665, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (979, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (979, 642, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (979, 718, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (979, 2663, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);


-- Monster 980 : Fragile Demonic Rose
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (980, 309, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (980, 2662, 63, 4, 5, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (980, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (980, 719, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (980, 641, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (980, 2663, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);


-- Monster 981 : Small Wild Sunflower
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (981, 2659, 63, 10, 11, 13, 15, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (981, 288, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (981, 2661, 63, 4, 5, 6, 7, 9, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (981, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (981, 2663, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (981, 720, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 982 : Young Arachnee
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (982, 519, 63, 50, 51, 52, 53, 54, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (982, 365, 63, 45, 46, 47, 48, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (982, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (982, 1673, 63, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (982, 6912, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (982, 2478, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (982, 6911, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (982, 6914, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (982, 6913, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);


-- Monster 983 : Young Boar
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (983, 394, 63, 50, 52, 54, 56, 58, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (983, 388, 63, 50, 52, 54, 56, 58, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (983, 486, 63, 50, 55, 58, 59, 60, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (983, 393, 63, 42, 44, 46, 48, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (983, 386, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (983, 387, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (983, 652, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (983, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (983, 6908, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (983, 6910, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (983, 6909, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);


-- Monster 984 : Small Gobball War Chief
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (984, 882, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (984, 887, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (984, 2466, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (984, 2463, 63, 3, 3, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (984, 2465, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (984, 2467, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (984, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (984, 2464, 63, 0.6, 0.6, 0.6, 0.6, 0.6, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (984, 2411, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (984, 2414, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (984, 2462, 63, 0.5, 0.5, 0.5, 0.5, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (984, 2468, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (984, 911, 1, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 400);


-- Monster 989 : Short-Tempered Treechnid
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (989, 435, 63, 55, 60, 65, 70, 75, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (989, 434, 63, 55, 60, 65, 70, 75, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (989, 437, 63, 38, 40, 41, 43, 45, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (989, 463, 63, 10, 15, 20, 25, 30, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (989, 464, 63, 6, 7, 8, 9, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (989, 792, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (989, 926, 63, 0.01, 0.02, 0.03, 0.04, 0.05, 1, 400);


-- Monster 990 : Short-Tempered Arachnotron

-- Monster 991 : Short-Tempered Dark Treechnid
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (991, 13528, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (991, 926, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 992 : Short-Tempered Dark Treechnee

-- Monster 996 : Prepubescent Chafer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (996, 310, 63, 30, 30, 30, 30, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (996, 2336, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (996, 2323, 63, 4, 4, 4, 4, 4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (996, 8545, 63, 1, 1, 1, 1, 1, 1, 0);


-- Monster 998 : Daredevil Crab
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (998, 311, 63, 95, 95, 95, 95, 95, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (998, 379, 63, 28, 31, 33, 34, 36, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (998, 2583, 63, 4, 5, 6, 6, 7, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (998, 2302, 63, 2, 2, 2.5, 3, 3, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (998, 8545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (998, 2303, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 999 : Incarnam Scarecrow

-- Monster 1001 : Snoowolf
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1001, 8545, 63, 1, 1, 1, 1, 1, 1, 0);


-- Monster 1002 : Samash Nehofitt

-- Monster 1003 : Training Scarecrow

-- Monster 1005 : Pandawa Cub Ghost

-- Monster 1011 : Obese Tofu Mummy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1011, 367, 1, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1011, 301, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1011, 366, 1, 10, 15, 20, 25, 30, 1, 0);


-- Monster 1012 : Force-fed Obese Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1012, 367, 1, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1012, 301, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1012, 366, 1, 10, 15, 20, 25, 30, 1, 0);


-- Monster 1013 : Obese Tofu Daddy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1013, 367, 1, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1013, 301, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1013, 366, 1, 10, 15, 20, 25, 30, 1, 0);


-- Monster 1015 : Wa Wabbit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1015, 361, 1, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1015, 8397, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1015, 812, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1015, 804, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1015, 808, 1, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1015, 8398, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 1016 : Pandawushu Disciple

-- Monster 1019 : Plain Pikoko
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1019, 8767, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1019, 8769, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1020 : Air Pikoko
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1020, 8788, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1020, 8792, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1022 : Coralator
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1022, 8730, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1022, 8729, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1022, 9940, 63, 0.5, 1, 1.5, 2, 2.5, 1, 0);


-- Monster 1025 : Polished Crackler
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1025, 8762, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1025, 8736, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1026 : Polished Crackrock
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1026, 8737, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1027 : Great Coralator
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1027, 8738, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1027, 9940, 63, 2.5, 2.5, 2.5, 2.5, 2.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1027, 8994, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1027, 8731, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 1029 : Floramor
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1029, 8778, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1029, 8771, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1037 : Ross Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1037, 2239, 1, 5, 5, 5, 5, 5, 1, 0);


-- Monster 1038 : Bilby Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1038, 2239, 1, 5, 5, 5, 5, 5, 1, 0);


-- Monster 1039 : Prince of Wa
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1039, 546, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 1041 : Dark Pikoko
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1041, 8783, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1041, 8776, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1043 : Cheeken
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1043, 8787, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1043, 8786, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1044 : Snailmet
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1044, 8832, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1044, 8793, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1045 : Kimbo
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1045, 8795, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1045, 13164, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1045, 8998, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1045, 8789, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 1046 : Moopet
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1046, 8790, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1046, 8791, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1047 : Light Treeckler
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1047, 8797, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1047, 8796, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1048 : Sparo
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1048, 8760, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1048, 8739, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1049 : Barbrossa
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1049, 8757, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1049, 8754, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 1050 : Ze Flib
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1050, 8740, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1050, 8755, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1051 : Gourlo the Terrible
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1051, 8758, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1051, 8995, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1051, 8761, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 1052 : Zoth Master
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1052, 13503, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1052, 8800, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1052, 8804, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1052, 929, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 1053 : Zoth Girl
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1053, 13499, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1053, 8800, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1053, 8805, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1053, 929, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 1054 : Zoth Warrior
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1054, 8800, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1054, 8802, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1054, 929, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 1055 : Zoth Disciple
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1055, 13338, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1055, 8800, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1055, 8803, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1055, 929, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 1056 : Zoth Sergeant
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1056, 13733, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1056, 8800, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1056, 8806, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1056, 929, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 1057 : Mopy King
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1057, 8808, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1057, 8809, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1058 : Mopeat
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1058, 8801, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1058, 13732, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1058, 8810, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1059 : Miremop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1059, 8801, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1059, 13498, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1059, 8811, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1060 : Kurasso Craboral
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1060, 8744, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1060, 8732, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1060, 9940, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 1061 : Mahlibuh Craboral
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1061, 8744, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1061, 8733, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1061, 9940, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 1062 : Passaoh Craboral
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1062, 8744, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1062, 8734, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1062, 9940, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 1063 : Mojeeto Craboral
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1063, 8744, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1063, 8735, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1063, 9940, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0);


-- Monster 1064 : Kurasso Palmflower
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1064, 8749, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1064, 8745, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1065 : Mahlibuh Palmflower
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1065, 8750, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1065, 8746, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1066 : Passaoh Palmflower
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1066, 8751, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1066, 8747, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1067 : Mojeeto Palmflower
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1067, 8752, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1067, 8748, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1068 : Mufafah
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1068, 8763, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1068, 8753, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1069 : Kido
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1069, 8766, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1069, 8741, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1070 : Kilibriss
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1070, 8756, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1070, 8765, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1071 : Silf the Greater Bherb
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1071, 13167, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1071, 8764, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1071, 8798, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1071, 8996, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 1072 : Dismayed Tynril

-- Monster 1073 : Rotaflor
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1073, 8779, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1073, 8772, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1074 : Gwass
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1074, 8780, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1074, 8773, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1075 : Barkritter
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1075, 8781, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1075, 8774, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1076 : Warguerite
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1076, 8782, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1076, 8775, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1077 : Dark Treeckler
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1077, 8785, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1077, 8784, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 1078 : Flib's Cursed Chest
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1078, 8759, 63, 100, 100, 100, 100, 100, 1, 100);


-- Monster 1080 : Pirate Barrel

-- Monster 1082 : Boomba Shika

-- Monster 1084 : Lesser Bherb

-- Monster 1085 : Disconcerted Tynril

-- Monster 1086 : Perfidious Tynril
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1086, 8777, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1086, 13166, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1086, 8916, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1086, 8770, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1086, 8997, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 1087 : Stunned Tynril

-- Monster 1088 : Kimbo's Disciple

-- Monster 1090 : Fourth Tentacle

-- Monster 1091 : Third Tentacle

-- Monster 1092 : Second Tentacle

-- Monster 1096 : Boggedown Ouassingue
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1096, 8801, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1096, 13490, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1096, 8807, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1096, 1575, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 1097 : Prism

-- Monster 1098 : Zoth Disciple
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1098, 13338, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1098, 8800, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1098, 8803, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1098, 929, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 1099 : Zoth Disciple
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1099, 13338, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1099, 8800, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1099, 8803, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1099, 929, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 1100 : Zoth Girl
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1100, 8800, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1100, 8805, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1100, 929, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 1101 : Zoth Girl
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1101, 8800, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1101, 8805, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1101, 929, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 1102 : Zoth Warrior
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1102, 8800, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1102, 8802, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1102, 929, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 1103 : Zoth Warrior
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1103, 8800, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1103, 8802, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1103, 929, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 1104 : Zoth Master
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1104, 8800, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1104, 8804, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1104, 929, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 1105 : Zoth Master
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1105, 8800, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1105, 8804, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1105, 929, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 1106 : Zoth Sergeant
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1106, 8800, 1, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1106, 8806, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1106, 929, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 1107 : Zoth Sergeant
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1107, 8800, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1107, 8806, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1107, 929, 63, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);


-- Monster 1108 : Chaferfu Lancer

-- Monster 1109 : Heart of Zoth Village

-- Monster 1110 : Heart of Zoth Village

-- Monster 1111 : Bontarian Prism

-- Monster 1112 : Brakmarian Prism

-- Monster 1113 : Pandawushu Disciple

-- Monster 1124 : Bontarian Warrior

-- Monster 1125 : Brakmarian Warrior

-- Monster 1127 : Satiated Whitish Fang

-- Monster 1129 : Nara
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1129, 7036, 63, 2, 2, 2, 2, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1129, 749, 63, 2, 2, 2, 2, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1129, 750, 63, 2, 2, 2, 2, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1129, 747, 63, 2, 2, 2, 2, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1129, 7035, 63, 2, 2, 2, 2, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1129, 6458, 63, 2, 2, 2, 2, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1129, 748, 2, 2, 2, 2, 2, 2, 1, 300);


-- Monster 1133 : Equipped Militiaman

-- Monster 1134 : Bontarian Commander Kad

-- Monster 1141 : Brakmarian Pursuer

-- Monster 1142 : Iop in a Brakmarian Cape

-- Monster 1143 : Bontarian Pursuer

-- Monster 1144 : Sadida in a Bontarian Cape

-- Monster 1153 : Trumperelle
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1153, 290, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1153, 426, 63, 10, 12, 14, 16, 18, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1153, 377, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1153, 300, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1153, 424, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1153, 9267, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1153, 378, 63, 1, 2, 3, 4, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1153, 6919, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1153, 6920, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1153, 6921, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1153, 6922, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1153, 2035, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1153, 1674, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 120);


-- Monster 1154 : Mushnid
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1154, 290, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1154, 424, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1154, 300, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1154, 377, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1154, 426, 63, 10, 12, 14, 16, 18, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1154, 9263, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1154, 378, 63, 1, 2, 3, 4, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1154, 6921, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1154, 6919, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1154, 6920, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1154, 6922, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1154, 2035, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1154, 1674, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 120);


-- Monster 1155 : Mushmunch
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1155, 290, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1155, 426, 63, 10, 12, 14, 16, 18, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1155, 300, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1155, 377, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1155, 424, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1155, 9269, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1155, 378, 63, 1, 2, 3, 4, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1155, 6919, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1155, 6920, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1155, 6921, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1155, 6922, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1155, 2035, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1155, 1674, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 120);


-- Monster 1156 : Mush Mish
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1156, 290, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1156, 300, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1156, 377, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1156, 426, 63, 10, 12, 14, 16, 18, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1156, 424, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1156, 9277, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1156, 378, 63, 1, 2, 3, 4, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1156, 6922, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1156, 6920, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1156, 6921, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1156, 6919, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1156, 2035, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1156, 1674, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 120);


-- Monster 1157 : Mush Tup
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1157, 290, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1157, 426, 63, 10, 12, 14, 16, 18, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1157, 300, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1157, 424, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1157, 377, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1157, 9278, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1157, 378, 63, 1, 2, 3, 4, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1157, 6922, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1157, 6920, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1157, 6921, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1157, 6919, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1157, 2035, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1157, 1674, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 120);


-- Monster 1158 : Mush Rhume
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1158, 290, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1158, 300, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1158, 426, 63, 10, 12, 14, 16, 18, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1158, 377, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1158, 424, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1158, 9279, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1158, 378, 63, 1, 2, 3, 4, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1158, 6919, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1158, 6922, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1158, 6921, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1158, 6920, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1158, 2035, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1158, 1674, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 120);


-- Monster 1159 : Ougaa
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1159, 290, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1159, 377, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1159, 300, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1159, 426, 63, 20, 20, 20, 20, 20, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1159, 13156, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1159, 9280, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1159, 424, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1159, 378, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1159, 9281, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1159, 6919, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1159, 6921, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1159, 6920, 63, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1159, 1674, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1159, 2035, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1159, 6922, 63, 1, 1, 1, 1, 1, 1, 300);


-- Monster 1160 : Elite Bontarian Commander

-- Monster 1178 : Exploding Spimush

-- Monster 1179 : Itzting
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1179, 12132, 63, 100, 100, 100, 100, 100, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1179, 12838, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1179, 10912, 63, 30, 30, 30, 30, 30, 1, 400);


-- Monster 1180 : Pokipik
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1180, 12132, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1180, 12838, 63, 5, 10, 25, 20, 25, 1, 0);


-- Monster 1181 : Greedoblop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1181, 1974, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1181, 1773, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1181, 1775, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1181, 1770, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1181, 1777, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1181, 9381, 63, 10, 11, 12, 13, 14, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1181, 1774, 63, 4, 5, 6, 7, 8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1181, 1776, 63, 4, 5, 6, 7, 8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1181, 1778, 63, 4, 5, 6, 7, 8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1181, 1772, 63, 4, 5, 6, 7, 8, 1, 100);


-- Monster 1182 : Blopshroom
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1182, 1974, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1182, 1777, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1182, 1770, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1182, 1773, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1182, 1775, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1182, 9382, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1182, 1776, 63, 4, 5, 6, 7, 8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1182, 1772, 63, 4, 5, 6, 7, 8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1182, 1778, 63, 4, 5, 6, 7, 8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1182, 1774, 63, 4, 5, 6, 7, 8, 1, 100);


-- Monster 1183 : Trunkiblop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1183, 1974, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1183, 1777, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1183, 1775, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1183, 1773, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1183, 1770, 63, 20, 21, 22, 23, 24, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1183, 9383, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1183, 1774, 63, 4, 5, 6, 7, 8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1183, 1772, 63, 4, 5, 6, 7, 8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1183, 1776, 63, 4, 5, 6, 7, 8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1183, 1778, 63, 4, 5, 6, 7, 8, 1, 100);


-- Monster 1184 : Royal Coco Blop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1184, 1974, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1184, 1775, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1184, 1773, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1184, 1770, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1184, 1777, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1184, 9388, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1184, 1778, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1184, 1772, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1184, 1776, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1184, 1774, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1184, 9385, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 1185 : Royal Morello Cherry Blop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1185, 1974, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1185, 1775, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1185, 1773, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1185, 1770, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1185, 1777, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1185, 9388, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1185, 1776, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1185, 1774, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1185, 1772, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1185, 1778, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1185, 9384, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 1186 : Royal Indigo Blop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1186, 1974, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1186, 1775, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1186, 1773, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1186, 1777, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1186, 1770, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1186, 9388, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1186, 1774, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1186, 1778, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1186, 1776, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1186, 1772, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1186, 9386, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 1187 : Royal Pippin Blop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1187, 1974, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1187, 1770, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1187, 1777, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1187, 1775, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1187, 1773, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1187, 9388, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1187, 1774, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1187, 1776, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1187, 1772, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1187, 1778, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1187, 9387, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 1188 : Royal Rainbow Blop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1188, 1974, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1188, 1770, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1188, 1773, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1188, 1775, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1188, 1777, 63, 25, 25, 25, 25, 25, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1188, 9389, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1188, 1778, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1188, 1772, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1188, 1774, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1188, 1776, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1188, 9391, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 1189 : Dark Vlad Puppet

-- Monster 1190 : Royal Gobball Puppet

-- Monster 1191 : Dragon Pig Puppet

-- Monster 1192 : Minotoror Puppet

-- Monster 1193 : Boowolf Puppet

-- Monster 1194 : Father Whupper
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1194, 12840, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1194, 12132, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1194, 10914, 63, 30, 30, 30, 30, 30, 1, 400);


-- Monster 1196 : Half Father Whupper

-- Monster 1229 : Drunken Boomba
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1229, 2618, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1229, 466, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 1230 : Beginner Cannon Dorf
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1230, 2618, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1230, 467, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 1231 : Lonely Hazwonarm
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1231, 2618, 63, 10, 11, 12, 13, 14, 1, 100);


-- Monster 1232 : Chocrosis Gobball
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 384, 63, 60, 65, 70, 75, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 9380, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 9472, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 304, 63, 40, 45, 50, 55, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 385, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 383, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 395, 63, 15, 20, 25, 30, 35, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 9379, 63, 5, 5, 5, 5, 5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 2448, 63, 2, 2, 2, 2, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 2449, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 2453, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 2451, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 2422, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 2416, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 2425, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 2455, 63, 0.09, 0.09, 0.09, 0.09, 0.09, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1232, 2454, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 0);


-- Monster 1245 : Lady in the water

-- Monster 1247 : Leprechaun
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1247, 6857, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1247, 351, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1247, 8145, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1247, 395, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1247, 182, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1247, 8815, 63, 2, 2.1, 2.2, 2.3, 2.4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (1247, 2318, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 2270 : Piralhaka the Intimidator
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2270, 7904, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2270, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2270, 8084, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 400);


-- Monster 2271 : Fisheralf the Stewart
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2271, 8083, 63, 20, 22, 24, 26, 28, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2271, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2271, 7904, 63, 10, 10, 10, 10, 10, 1, 100);


-- Monster 2272 : Snappy the Fishfrier
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2272, 8680, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2272, 598, 63, 2, 2, 2, 2, 2, 1, 0);


-- Monster 2273 : Rattle the Hummer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2273, 8481, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2273, 2322, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2273, 8482, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2275 : Ratilla the Hun

-- Monster 2276 : Rib the Torn
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2276, 432, 63, 40, 50, 60, 70, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2276, 2642, 63, 12, 14, 16, 18, 20, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2276, 2323, 63, 1, 2, 3, 4, 5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2276, 1685, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2276, 1132, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 200);


-- Monster 2277 : Zorrose the Messican
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2277, 2498, 1, 0.6, 0.6, 0.6, 0.6, 0.6, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2277, 2663, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2277, 2485, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2277, 2489, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);


-- Monster 2278 : Sorgyo Quiretox the Chatterbox
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2278, 7293, 63, 40, 44, 48, 52, 56, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2278, 7299, 1, 10, 12, 14, 16, 18, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2278, 7024, 1, 10, 10, 10, 10, 10, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2278, 7225, 1, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2278, 7305, 1, 1, 1, 1, 1, 1, 1, 300);


-- Monster 2279 : Boarealis the Bright
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2279, 2511, 63, 100, 100, 100, 100, 100, 1, 50);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2279, 393, 63, 60, 64, 72, 76, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2279, 1894, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2279, 2514, 63, 10, 10, 10, 10, 10, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2279, 2516, 63, 8, 10, 12, 16, 20, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2279, 2513, 63, 8, 10, 12, 14, 18, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2279, 2515, 63, 2.2, 2.4, 2.6, 2.8, 3, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2279, 2512, 63, 0.8, 3, 3.2, 3.4, 3.6, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2279, 2510, 63, 0.4, 0.6, 1, 1.4, 1.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2279, 2082, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);


-- Monster 2280 : Scaratheef the Pincher
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2280, 1467, 63, 32, 34, 36, 38, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2280, 398, 63, 20, 24, 28, 32, 36, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2280, 2290, 63, 8, 10, 12, 16, 20, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2280, 1456, 63, 4, 4, 6, 8, 10, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2280, 2294, 63, 0.1, 0.12, 0.14, 0.16, 0.2, 1, 400);


-- Monster 2281 : Scarahazad the Storyteller
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2281, 1464, 63, 32, 34, 36, 38, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2281, 398, 63, 20, 24, 28, 32, 36, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2281, 2291, 63, 8, 10, 12, 16, 20, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2281, 1455, 63, 4, 4, 6, 8, 10, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2281, 2294, 63, 0.1, 0.12, 0.14, 0.16, 0.2, 1, 400);


-- Monster 2282 : Scarabreef the Short
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2282, 1465, 63, 32, 34, 36, 38, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2282, 398, 63, 20, 24, 28, 32, 36, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2282, 2292, 63, 8, 10, 12, 16, 20, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2282, 1457, 63, 4, 4, 6, 8, 10, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2282, 2294, 63, 0.1, 0.12, 0.14, 0.16, 0.2, 1, 400);


-- Monster 2283 : Scaramel the Melty
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2283, 1466, 63, 32, 34, 36, 38, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2283, 398, 63, 20, 24, 28, 32, 36, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2283, 2293, 63, 8, 10, 12, 16, 20, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2283, 1458, 63, 4, 4, 6, 8, 10, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2283, 2294, 63, 0.1, 0.12, 0.14, 0.16, 0.2, 1, 400);


-- Monster 2285 : Scorbison the Lonely
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2285, 2150, 63, 60, 64, 68, 72, 76, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2285, 2245, 63, 30, 32, 34, 36, 38, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2285, 1893, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2285, 2012, 63, 10, 12, 14, 16, 18, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2285, 2487, 63, 10, 10, 10, 10, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2285, 2490, 63, 8, 10, 12, 14, 16, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2285, 2488, 63, 0.1, 0.14, 0.16, 0.18, 0.2, 1, 150);


-- Monster 2286 : Serpico the Honest
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2286, 2509, 63, 24, 28, 32, 36, 40, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2286, 1891, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2286, 2507, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2286, 2506, 63, 1, 1, 1, 1, 1, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2286, 2508, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 200);


-- Monster 2287 : Quetnin the Fictional
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2287, 8309, 63, 40, 42, 44, 46, 48, 1, 100);


-- Monster 2289 : Suzessman the Enthusiastic
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2289, 13489, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2289, 965, 1, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2292 : Eskoko the Baron
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2292, 2618, 63, 20, 22, 24, 26, 28, 1, 100);


-- Monster 2293 : Tiwaldo the Hidden
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2293, 360, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2293, 361, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2293, 305, 63, 2, 4, 6, 8, 10, 1, 0);


-- Monster 2294 : Tiwascal the Wapper
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2294, 361, 63, 2, 4, 6, 8, 10, 1, 0);


-- Monster 2295 : Turtan'ernie the Streetwise
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2295, 2613, 1, 20, 24, 28, 32, 36, 1, 200);


-- Monster 2296 : Turtrenalds the Tragic
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2296, 2609, 1, 20, 24, 28, 32, 40, 1, 200);


-- Monster 2297 : Turticorn the Horned
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2297, 2610, 1, 20, 24, 28, 32, 36, 1, 200);


-- Monster 2298 : Turture the Hooded
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2298, 2611, 1, 20, 24, 28, 32, 36, 1, 200);


-- Monster 2299 : Trunkbeard the Gentle
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2299, 2249, 63, 46, 52, 58, 64, 70, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2299, 2250, 63, 8, 10, 12, 16, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2299, 2563, 63, 8, 10, 12, 14, 16, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2299, 2565, 63, 8, 10, 12, 14, 16, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2299, 2564, 63, 4, 4, 4, 4, 4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2299, 2566, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2300 : Trooligan the Bulldogg
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2300, 8519, 63, 60, 60, 60, 60, 60, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2300, 2559, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2300, 2560, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2300, 2561, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2300, 2562, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2300, 1694, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2300, 1475, 1, 2, 2.2, 2.4, 2.6, 2.8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2300, 546, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2300, 547, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2301 : Glukoko the Slow
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2301, 1018, 4, 44, 48, 52, 56, 60, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2301, 2602, 63, 40, 44, 50, 56, 64, 1, 50);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2301, 2605, 4, 8, 10, 12, 14, 16, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2301, 2607, 1, 2, 2.4, 2.8, 3.2, 3.6, 1, 400);


-- Monster 2302 : Vamp the Impalest
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2302, 2279, 63, 20, 22, 24, 26, 28, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2302, 752, 63, 10, 16, 20, 24, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2302, 756, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2302, 2281, 63, 0.4, 0.4, 0.4, 0.4, 0.4, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2302, 2285, 63, 0.4, 0.6, 0.8, 0.8, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2302, 2669, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 125);


-- Monster 2303 : Yokai the Choral
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2303, 7294, 63, 40, 44, 48, 52, 56, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2303, 7300, 1, 10, 12, 14, 16, 18, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2303, 7225, 1, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2303, 7025, 1, 10, 10, 10, 10, 10, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2303, 7306, 1, 1, 1, 1, 1, 1, 1, 300);


-- Monster 2304 : McWhabbit the Diehard
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2304, 305, 63, 50, 54, 56, 60, 64, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2304, 2288, 63, 30, 32, 34, 36, 38, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2304, 648, 63, 16, 18, 20, 22, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2304, 654, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2304, 361, 63, 2, 4, 6, 8, 10, 1, 0);


-- Monster 2305 : Wabbitor the Apt
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2305, 305, 63, 20, 22, 24, 26, 28, 1, 0);


-- Monster 2306 : Warko the Inky
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2306, 7903, 63, 4, 4, 4, 4, 4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2306, 7904, 63, 4, 4, 4, 4, 4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2306, 8065, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2306, 8077, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 100);


-- Monster 2307 : Worka the Willful
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2307, 13700, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2307, 7903, 63, 4, 4, 4, 4, 4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2307, 7904, 63, 4, 4, 4, 4, 4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2307, 8066, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2308 : Wowalker the Egyptian
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2308, 305, 63, 32, 36, 40, 44, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2308, 649, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2308, 361, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2308, 406, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2309 : Treekniddioo the Needy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2309, 437, 63, 40, 48, 56, 60, 70, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2309, 434, 63, 30, 40, 60, 80, 100, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2309, 435, 63, 30, 40, 60, 80, 100, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2309, 463, 63, 4, 8, 12, 16, 20, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2309, 464, 63, 2, 4, 6, 8, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2309, 792, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2309, 926, 63, 0.02, 0.04, 0.06, 0.08, 0.1, 1, 400);


-- Monster 2310 : Arakula the Carpature
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2310, 519, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2310, 365, 63, 90, 92, 94, 96, 98, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2310, 1673, 63, 2, 2, 2, 2, 2, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2310, 6912, 1, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2310, 6914, 1, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2310, 6913, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2310, 2478, 1, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2310, 6911, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 2311 : Arachma the Greek
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2311, 1652, 63, 20, 40, 60, 80, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2311, 2656, 63, 4, 4.4, 4.8, 5.2, 5.6, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2311, 2654, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2311, 2653, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 400);


-- Monster 2312 : Arachnangel the Hopeful
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2312, 365, 63, 90, 92, 94, 96, 98, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2312, 854, 1, 20, 40, 60, 80, 100, 1, 0);


-- Monster 2313 : Bandirty the Messy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2313, 13342, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2313, 6923, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 200);


-- Monster 2314 : Bandinamit the Explosive
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2314, 13339, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2314, 276, 63, 2, 4, 6, 8, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2314, 274, 63, 2, 4, 6, 8, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2314, 6924, 1, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2314, 6925, 1, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2314, 6923, 1, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2314, 6961, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2314, 1683, 63, 0.4, 0.8, 1.2, 1.6, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2314, 1626, 63, 0.4, 0.4, 0.4, 0.4, 0.4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2314, 1684, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 2315 : Gobbach the Contrapuntaler
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2315, 881, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2315, 883, 63, 80, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2315, 385, 63, 50, 60, 70, 80, 90, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2315, 2460, 63, 2, 2, 2, 2, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2315, 2419, 1, 1, 1, 1, 1, 1, 1, 200);


-- Monster 2316 : Gobballyhoo the Noisy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2316, 884, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2316, 885, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2316, 385, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2316, 2460, 63, 2, 2, 2, 2, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2316, 2428, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 2317 : Gobballad the Romantic
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2317, 384, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2317, 304, 63, 80, 90, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2317, 383, 63, 40, 50, 60, 70, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2317, 385, 63, 40, 50, 60, 70, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2317, 395, 63, 30, 40, 50, 60, 70, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2317, 2448, 63, 4, 4, 4, 4, 4, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2317, 2451, 63, 2, 2, 2, 2, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2317, 2453, 63, 2, 2, 2, 2, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2317, 2449, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2317, 2422, 1, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2317, 2416, 1, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2317, 2425, 1, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2317, 2455, 63, 0.18, 0.18, 0.18, 0.18, 0.18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2317, 2454, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 2318 : Bakeraider the Tomb
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2318, 1986, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2318, 519, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2318, 311, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2318, 13730, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2318, 586, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2318, 528, 63, 4, 8, 12, 16, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2318, 1626, 63, 2, 2, 2, 2, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2318, 743, 63, 0.4, 0.4, 0.4, 0.4, 0.4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2318, 931, 63, 0.12, 0.14, 0.16, 0.18, 0.2, 1, 0);


-- Monster 2319 : Chaferanho the Essential
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2319, 310, 63, 60, 60, 60, 60, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2319, 2336, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2319, 2323, 63, 8, 8, 8, 8, 8, 1, 400);


-- Monster 2320 : Chaferotix the Sixtininth
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2320, 310, 63, 90, 90, 90, 90, 90, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2320, 2323, 1, 8, 8, 8, 8, 8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2320, 433, 2, 4, 6, 8, 10, 12, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2320, 2297, 1, 1, 1, 1, 1, 1, 1, 120);


-- Monster 2321 : Chafaldrag the Charming
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2321, 310, 63, 70, 72, 74, 76, 78, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2321, 430, 63, 6, 7, 8, 9, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2321, 2278, 63, 6, 7, 8, 9, 10, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2321, 2323, 63, 1, 2, 3, 4, 5, 1, 400);


-- Monster 2322 : Chafermented the Drinker
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2322, 310, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2322, 2323, 63, 8, 8, 8, 8, 8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2322, 485, 63, 4, 6, 6, 6, 8, 1, 0);


-- Monster 2323 : Matmushmush the Flasher
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2323, 377, 63, 20, 40, 60, 80, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2323, 300, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2323, 290, 63, 20, 40, 60, 80, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2323, 2473, 1, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2323, 6920, 1, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2323, 6921, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2323, 6919, 1, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2323, 6922, 1, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2323, 378, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2323, 1674, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 120);


-- Monster 2324 : Gobbalky the Stubborn
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2324, 887, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2324, 882, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2324, 2466, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2324, 2463, 63, 6, 6, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2324, 2465, 63, 2, 2, 2, 2, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2324, 2467, 63, 2, 2, 2, 2, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2324, 2464, 63, 1.2, 1.2, 1.2, 1.2, 1.2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2324, 2411, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2324, 2414, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2324, 2462, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2324, 2468, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2324, 911, 1, 0.02, 0.02, 0.02, 0.02, 0.02, 1, 400);


-- Monster 2325 : Pigoblet the Useful
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2325, 901, 63, 30, 32, 34, 36, 38, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2325, 2647, 63, 30, 32, 34, 36, 38, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2325, 479, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2325, 2266, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2325, 2646, 63, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2326 : Crackrockisree the Tiger
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2326, 448, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2326, 450, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2326, 544, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2326, 543, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2326, 546, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2326, 545, 63, 1, 1, 1, 1, 1, 1, 300);


-- Monster 2327 : Krabaoly the Patient
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2327, 311, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2327, 379, 63, 60, 62, 64, 66, 68, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2327, 2583, 63, 10, 12, 14, 16, 18, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2327, 2302, 63, 2, 3, 4, 5, 6, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2327, 2303, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 400);


-- Monster 2328 : Smitherz the Licker
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2328, 446, 63, 2, 4, 6, 8, 10, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2328, 350, 63, 2, 4, 6, 8, 10, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2328, 313, 63, 2, 4, 6, 8, 10, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2328, 6458, 63, 0.4, 0.8, 1.2, 1.6, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2328, 747, 63, 0.4, 0.8, 1.2, 1.6, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2328, 750, 63, 0.4, 0.8, 1.2, 1.6, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2328, 749, 63, 0.4, 0.8, 1.2, 1.6, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2328, 746, 63, 0.4, 0.8, 1.2, 1.6, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2328, 6457, 63, 0.4, 0.8, 1.2, 1.6, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2328, 748, 63, 0.4, 0.8, 1.2, 1.6, 2, 1, 300);


-- Monster 2329 : Kitsewey the Blue
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2329, 7282, 63, 60, 64, 68, 72, 76, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2329, 7222, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2329, 7278, 63, 10, 12, 14, 16, 18, 1, 0);


-- Monster 2331 : Larvalencia the Orange
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2331, 519, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2331, 363, 63, 30, 40, 50, 60, 70, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2331, 7423, 63, 2, 2, 2, 2, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2331, 643, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 200);


-- Monster 2332 : Larvadelaide the Ozie
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2332, 519, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2332, 364, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2332, 7423, 63, 2, 2, 2, 2, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2332, 643, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2332, 1681, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 2333 : Milivanilli the Mime
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2333, 1690, 63, 20, 30, 40, 50, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2333, 2579, 63, 10, 12, 14, 16, 18, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2333, 2576, 63, 2, 3, 4, 5, 6, 1, 400);


-- Monster 2334 : Miliopold the Bloomer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2334, 2322, 63, 40, 60, 80, 100, 100, 1, 0);


-- Monster 2335 : Minoskittle the Coloured
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2335, 13505, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2335, 350, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2335, 446, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2335, 312, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2335, 313, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2335, 2274, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2335, 2282, 63, 1.2, 1.4, 1.6, 1.8, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2335, 544, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2335, 6476, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2335, 480, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2335, 545, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2335, 543, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2335, 546, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2335, 547, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2335, 930, 63, 0.12, 0.14, 0.16, 0.18, 0.2, 1, 0);


-- Monster 2336 : Moskoitus the Interruptor
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2336, 519, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2336, 307, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2336, 371, 63, 30, 40, 50, 60, 70, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2336, 6917, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2336, 6918, 1, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2336, 6915, 1, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2336, 6916, 1, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2336, 2477, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 2337 : Booty the Beast
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2337, 438, 63, 20, 40, 60, 80, 100, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2337, 440, 63, 20, 40, 60, 80, 100, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2337, 291, 63, 20, 40, 60, 80, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2337, 439, 63, 20, 40, 60, 80, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2337, 2578, 63, 8, 10, 12, 14, 18, 1, 150);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2337, 8396, 63, 4, 4, 4, 4, 4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2337, 2575, 63, 4, 4, 5, 6, 6, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2337, 292, 63, 2, 2, 2, 2, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2337, 2805, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2337, 651, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 400);


-- Monster 2338 : Snappu the Shopkeep
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2338, 13494, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2338, 8680, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2338, 598, 63, 2, 2, 2, 2, 2, 1, 0);


-- Monster 2339 : Snappster the Sued
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2339, 13502, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2339, 8680, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2339, 598, 63, 2, 4, 6, 8, 10, 1, 0);


-- Monster 2340 : Snapp the Dragon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2340, 8680, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2340, 598, 63, 2, 2, 2, 2, 2, 1, 0);


-- Monster 2341 : Piwiki the Witty
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2341, 6897, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2341, 287, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2341, 2475, 1, 2, 2, 2, 2, 2, 1, 100);


-- Monster 2342 : Piwicker the Manly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2342, 6902, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2342, 287, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2342, 2475, 1, 2, 2, 2, 2, 2, 1, 100);


-- Monster 2343 : Piwi the Ermine
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2343, 287, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2343, 6903, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2343, 2475, 1, 2, 2, 2, 2, 2, 1, 100);


-- Monster 2344 : Piwilde the Bossie
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2344, 6900, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2344, 287, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2344, 2475, 1, 2, 2, 2, 2, 2, 1, 100);


-- Monster 2345 : Piwiliam the Brave
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2345, 287, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2345, 6899, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2345, 2475, 1, 2, 2, 2, 2, 2, 1, 100);


-- Monster 2346 : Snapple the Wise
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2346, 13487, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2346, 8680, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2346, 598, 63, 2, 2, 2, 2, 2, 1, 0);


-- Monster 2347 : Piwinston the Churlish
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2347, 287, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2347, 6898, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2347, 2475, 1, 2, 2, 2, 2, 2, 1, 100);


-- Monster 2348 : Dandel the Boy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2348, 306, 63, 44, 48, 52, 56, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2348, 373, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2348, 374, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2348, 2665, 63, 2, 2, 2, 2, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2348, 642, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2348, 718, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2348, 2663, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 200);


-- Monster 2349 : Prestreet the Fighter
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2349, 407, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2349, 2571, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2349, 2573, 63, 20, 22, 24, 26, 28, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2349, 2572, 63, 4, 4, 4, 4, 4, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2349, 653, 63, 2, 2, 2, 2, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2349, 2574, 63, 2, 2, 2, 2, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2349, 1672, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);


-- Monster 2350 : Ratatouille the Stirrer

-- Monster 2351 : Raul Modrid the Chulo
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2351, 8680, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2351, 8681, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2352 : Roseanne the Yanker
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2352, 309, 63, 20, 40, 60, 80, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2352, 2662, 63, 8, 10, 12, 14, 16, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2352, 719, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2352, 2663, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2352, 641, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 400);


-- Monster 2353 : Boarnigen the Damasker
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2353, 388, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2353, 486, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2353, 394, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2353, 393, 63, 84, 88, 92, 96, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2353, 386, 63, 80, 84, 88, 92, 96, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2353, 387, 63, 40, 50, 60, 70, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2353, 652, 63, 2, 2, 2, 2, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2353, 6909, 1, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2353, 6910, 1, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2353, 6908, 1, 1, 1, 1, 1, 1, 1, 200);


-- Monster 2354 : Famouse the Little-Known
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2354, 761, 1, 20, 40, 60, 80, 100, 1, 0);


-- Monster 2355 : Tofudd the Hunter
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2355, 287, 63, 50, 52, 54, 56, 58, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2355, 367, 63, 30, 40, 50, 60, 70, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2355, 301, 63, 20, 40, 60, 80, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2355, 366, 63, 20, 30, 40, 50, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2355, 2476, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 2356 : Tofull the Optimist
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2356, 367, 63, 30, 40, 50, 60, 70, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2356, 301, 63, 20, 30, 40, 50, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2356, 366, 63, 10, 20, 30, 40, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2356, 764, 1, 0, 20, 40, 60, 80, 1, 0);


-- Monster 2357 : Tofulsom the Jailer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2357, 375, 63, 20, 40, 60, 80, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2357, 376, 2, 20, 40, 60, 80, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2357, 2675, 63, 8, 10, 12, 16, 20, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2357, 2673, 1, 1, 1, 1, 1, 1, 1, 400);


-- Monster 2358 : Hunflower the Sinful
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2358, 2661, 63, 810, 12, 14, 16, 18, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2358, 288, 63, 20, 24, 28, 32, 36, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2358, 2659, 63, 20, 24, 28, 32, 36, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2358, 2663, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2358, 720, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 400);


-- Monster 2359 : Fung Ku the Master
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2359, 290, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2359, 426, 63, 20, 24, 28, 32, 36, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2359, 424, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2359, 1674, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2359, 2035, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 300);


-- Monster 2360 : Barkricrac the Unsteady
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2360, 8781, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2360, 8774, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2361 : Mopfeet the Circular
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2361, 8801, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2361, 13732, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2361, 8810, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2363 : Mopidyk the Mire
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2363, 8801, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2363, 13498, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2363, 8811, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2367 : Drakween the Cross Dresser
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2367, 842, 63, 50, 55, 60, 65, 70, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2367, 544, 63, 1, 2, 3, 4, 5, 1, 0);


-- Monster 2368 : Dreggershween the Tinpanalley
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2368, 1129, 63, 50, 55, 60, 65, 70, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2368, 546, 63, 2, 3, 4, 5, 6, 1, 0);


-- Monster 2369 : Dreggonzola the Cheesy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2369, 308, 63, 50, 55, 60, 65, 70, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2369, 545, 63, 2, 3, 4, 5, 6, 1, 0);


-- Monster 2370 : Leorio the Haunted
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2370, 7225, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2370, 7223, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2370, 7224, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2370, 7222, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2370, 7024, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2370, 7403, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2370, 7382, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2371 : Jelleno the Chinny
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2371, 380, 63, 20, 22, 24, 26, 28, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2371, 369, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2371, 993, 63, 1, 1, 1, 1, 1, 1, 200);


-- Monster 2372 : Dreggatón the Latino
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2372, 1129, 63, 40, 50, 60, 70, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2372, 546, 63, 2, 4, 6, 8, 10, 1, 400);


-- Monster 2373 : Romush the Montecchi
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2373, 290, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2373, 377, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2373, 426, 63, 20, 24, 28, 32, 36, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2373, 424, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2373, 300, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2373, 9278, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2373, 378, 63, 2, 4, 6, 8, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2373, 6922, 1, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2373, 6921, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2373, 6919, 1, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2373, 6920, 1, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2373, 2035, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2373, 1674, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 120);


-- Monster 2374 : Satonuki the Plastikpaddy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2374, 7224, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2374, 7222, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2374, 7223, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2374, 7024, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2374, 7225, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2374, 7408, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2374, 7381, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2376 : Jellyposukshion the Slim
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2376, 381, 63, 20, 22, 24, 26, 28, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2376, 368, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2376, 994, 63, 1, 1, 1, 1, 1, 1, 200);


-- Monster 2377 : Dragamemnon the Deadtroyer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2377, 308, 63, 40, 50, 60, 70, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2377, 545, 63, 2, 4, 6, 8, 10, 1, 0);


-- Monster 2378 : Snowhitisha the Pure
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2378, 2315, 63, 60, 70, 80, 90, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2378, 2316, 63, 50, 60, 70, 80, 90, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2378, 2502, 63, 20, 22, 24, 26, 30, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2378, 2501, 63, 20, 20, 22, 22, 24, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2378, 2317, 63, 8, 10, 12, 16, 20, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2378, 2504, 63, 4, 4, 6, 6, 8, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2378, 2503, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2378, 2505, 63, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2379 : Dragotitis the Painful
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2379, 2599, 63, 40, 46, 52, 64, 72, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2379, 2179, 63, 30, 32, 34, 36, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2379, 2596, 63, 20, 22, 24, 26, 28, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2379, 2588, 63, 10, 12, 14, 16, 20, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2379, 2594, 63, 4, 4, 5, 6, 6, 1, 0);


-- Monster 2381 : Treekstalbal the Psychic
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2381, 8797, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2381, 8796, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2382 : Follikoko the Tufted
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2382, 8788, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2382, 8792, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2383 : Cheech the Pussycat
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2383, 8787, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2383, 8786, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2384 : Abounteous the Generous
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2384, 13729, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2384, 965, 1, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2385 : Treeknidylus
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2385, 463, 63, 20, 22, 24, 26, 28, 1, 0);


-- Monster 2386 : Treekalack the Sad
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2386, 463, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2386, 544, 63, 20, 20, 20, 20, 20, 1, 200);


-- Monster 2388 : Treekonk the Stunned
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2388, 1612, 63, 10, 12, 14, 16, 20, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2388, 1611, 63, 10, 12, 14, 16, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2388, 1610, 63, 10, 14, 16, 18, 20, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2388, 1660, 63, 2, 2, 2, 2, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2388, 1682, 63, 2, 2.4, 2.8, 3.2, 3.6, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2388, 926, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 400);


-- Monster 2389 : Treektamak the Loud
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2389, 434, 63, 100, 100, 100, 100, 100, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2389, 435, 63, 100, 100, 100, 100, 100, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2389, 437, 63, 76, 80, 82, 86, 90, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2389, 463, 63, 20, 30, 40, 50, 60, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2389, 464, 63, 12, 14, 16, 18, 20, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2389, 792, 63, 2, 2, 2, 2, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2389, 926, 63, 0.02, 0.04, 0.06, 0.08, 0.1, 1, 400);


-- Monster 2391 : Amlullabeye the Dreamer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2391, 13489, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2391, 965, 1, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2392 : Arachnawar the Killinmachin
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2392, 7285, 63, 40, 44, 48, 52, 56, 1, 0);


-- Monster 2393 : Arachnekros the Aggressive
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2393, 365, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2393, 519, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2393, 1673, 63, 2, 2, 2, 2, 2, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2393, 2478, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 2396 : Billbiblop the Great
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2396, 1974, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2396, 1777, 63, 40, 40, 40, 40, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2396, 1778, 63, 8, 8, 8, 8, 8, 1, 0);


-- Monster 2397 : Pigstol the Sexy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2397, 2486, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2397, 2275, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2397, 2484, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2397, 2481, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2397, 2482, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2397, 2483, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2397, 2554, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2397, 7510, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2397, 2479, 63, 1, 1.2, 1.4, 1.6, 1.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2397, 2480, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 400);


-- Monster 2398 : Bambono the Holy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2398, 7288, 63, 40, 44, 48, 52, 56, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2398, 7370, 63, 20, 22, 24, 26, 28, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2398, 7289, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2398, 7291, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 2399 : Biblopopo the Organiser
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2399, 1974, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2399, 1770, 63, 40, 40, 40, 40, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2399, 1772, 63, 8, 8, 8, 8, 8, 1, 0);


-- Monster 2400 : Biblokajin the Bald
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2400, 1974, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2400, 1775, 63, 40, 40, 40, 40, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2400, 1776, 63, 8, 8, 8, 8, 8, 1, 0);


-- Monster 2401 : Bibloponey the Entertainer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2401, 1974, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2401, 1773, 63, 40, 40, 40, 40, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2401, 1774, 63, 8, 8, 8, 8, 8, 1, 0);


-- Monster 2402 : Tiwana the Tokin'
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2402, 372, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2402, 646, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2402, 305, 63, 4, 8, 12, 16, 20, 1, 0);


-- Monster 2403 : Wabbin the Wich
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2403, 305, 63, 24, 28, 32, 36, 40, 1, 0);


-- Monster 2404 : Blopulent the Pretentious
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2404, 1974, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2404, 1770, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2404, 2556, 63, 40, 44, 48, 52, 60, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2404, 2557, 63, 10, 12, 14, 16, 20, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2404, 1772, 63, 8, 8, 8, 8, 8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2404, 2558, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 2405 : Blorchid the Gorgeous
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2405, 1775, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2405, 1974, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2405, 2556, 63, 40, 44, 48, 52, 60, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2405, 2557, 63, 10, 12, 14, 16, 20, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2405, 1776, 63, 8, 8, 8, 8, 8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2405, 2558, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 2406 : Blopium the Delirious
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2406, 1777, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2406, 1974, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2406, 2556, 63, 40, 44, 48, 52, 60, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2406, 2557, 63, 10, 12, 14, 16, 20, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2406, 1778, 63, 8, 8, 8, 8, 8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2406, 2558, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 2407 : Blopal the Precious
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2407, 1974, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2407, 1773, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2407, 2556, 63, 40, 44, 48, 52, 60, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2407, 2557, 63, 10, 12, 14, 16, 20, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2407, 1774, 63, 8, 8, 8, 8, 8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2407, 2558, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 2408 : Boombora the Dangerous
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2408, 13343, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2408, 466, 63, 0.4, 0.4, 0.4, 0.4, 0.4, 1, 0);


-- Monster 2411 : Bulbamoon the Trumpeter
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2411, 7264, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2411, 7268, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2411, 7272, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 300);


-- Monster 2412 : Pocher the Kingponger
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2412, 12739, 63, 20, 22, 24, 26, 28, 1, 100);


-- Monster 2413 : Bulbisonic the Penetrating
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2413, 7262, 63, 20, 24, 28, 32, 36, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2413, 7266, 63, 20, 24, 28, 32, 36, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2413, 7270, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 400);


-- Monster 2414 : Bulbigroov the Dancer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2414, 7265, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2414, 7224, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2414, 7223, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2414, 7225, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2414, 7222, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2414, 7269, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 2415 : Bulbushisu the Makisan
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2415, 7263, 63, 20, 24, 28, 32, 36, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2415, 7267, 63, 20, 24, 28, 32, 36, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2415, 7223, 63, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2415, 7271, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 400);


-- Monster 2416 : Bworak the Bohemian
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2416, 8145, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2416, 391, 63, 50, 54, 60, 64, 70, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2416, 392, 63, 50, 74, 60, 64, 70, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2416, 448, 63, 36, 40, 44, 48, 52, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2416, 3001, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2416, 3002, 63, 8, 10, 10, 12, 12, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2416, 2271, 63, 4, 5, 5, 6, 6, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2416, 3000, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 400);


-- Monster 2417 : Blorko the Colourful
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2417, 8145, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2417, 429, 63, 70, 72, 74, 76, 79, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2417, 391, 63, 40, 42, 44, 46, 48, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2417, 420, 63, 30, 32, 34, 36, 38, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2417, 3208, 63, 20, 22, 24, 26, 28, 1, 150);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2417, 2271, 63, 2, 3, 4, 5, 6, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2417, 2268, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 200);


-- Monster 2418 : Bworkoder the Mazter
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2418, 8145, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2418, 409, 63, 40, 42, 44, 46, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2418, 2273, 63, 20, 22, 24, 26, 30, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2418, 2330, 63, 20, 22, 24, 26, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2418, 410, 63, 2, 2, 2, 2, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2418, 720, 1, 2, 2, 2, 2, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2418, 1671, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2418, 3209, 63, 0.6, 0.6, 0.6, 0.6, 0.6, 1, 200);


-- Monster 2419 : Ganon the Dwarf
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2419, 467, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);


-- Monster 2420 : Pygknightlion the Lousy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2420, 2481, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2420, 2482, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2420, 2486, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2420, 2275, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2420, 7510, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2420, 2483, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2420, 2554, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2420, 2553, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2420, 2555, 63, 0.1, 0.12, 0.14, 0.16, 0.18, 1, 300);


-- Monster 2421 : Chafred the Fish
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2421, 310, 63, 50, 50, 50, 50, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2421, 1675, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2421, 2323, 63, 1, 2, 3, 4, 5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2421, 6475, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 400);


-- Monster 2422 : Chaferuption the Volcanic
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2422, 310, 63, 60, 70, 80, 90, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2422, 2300, 63, 1, 2, 3, 4, 5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2422, 408, 63, 1, 2, 3, 4, 5, 1, 0);


-- Monster 2423 : Shamassel the Off
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2423, 8483, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2423, 2322, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2423, 8484, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2423, 2404, 1, 0.2, 0.4, 0.6, 0.8, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2423, 686, 1, 0.2, 0.4, 0.6, 0.8, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2423, 816, 1, 0.02, 0.04, 0.06, 0.08, 0.1, 1, 400);


-- Monster 2424 : Spimushty the Smelly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2424, 377, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2424, 290, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2424, 300, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2424, 378, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2424, 1674, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 120);


-- Monster 2425 : Speedmush the Racer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2425, 290, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2425, 377, 63, 20, 40, 60, 80, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2425, 300, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2425, 378, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2425, 1674, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 120);


-- Monster 2426 : Spimushuaia the Traveller
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2426, 290, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2426, 300, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2426, 377, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2426, 378, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2426, 1674, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 120);


-- Monster 2427 : Spimushtache the Hairy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2427, 300, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2427, 290, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2427, 377, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2427, 1676, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2427, 1674, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 120);


-- Monster 2428 : Crokdylann the Rebel
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2428, 1613, 63, 20, 30, 40, 50, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2428, 2664, 63, 4, 4, 4, 4, 4, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2428, 1614, 63, 4, 6, 8, 10, 12, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2428, 6740, 63, 3, 4, 5, 6, 7, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2428, 2296, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2428, 2295, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2428, 6738, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2428, 1635, 1, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2428, 1676, 63, 0.4, 0.4, 0.4, 0.4, 0.4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2428, 1719, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2428, 1718, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);


-- Monster 2429 : Koelloggs the Creator
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2429, 7904, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2429, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2429, 8053, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2430 : Karnyona the Rider
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2430, 382, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2430, 2259, 63, 20, 20, 20, 20, 20, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2430, 2264, 63, 2, 2, 2, 2, 2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2430, 533, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2430, 1679, 63, 0, 0.2, 0.2, 0.2, 0.2, 1, 0);


-- Monster 2431 : Jackellington the Lantewn
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2431, 2674, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2431, 302, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 0);


-- Monster 2432 : Pighatchoo the Electrical
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2432, 8390, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2432, 479, 63, 2, 2, 2, 2, 2, 1, 300);


-- Monster 2433 : Codemonic the Mean
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2433, 13489, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2433, 965, 1, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2434 : Grasnakizanami the Ruler
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2434, 7273, 63, 40, 44, 48, 52, 56, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2434, 7223, 63, 20, 24, 28, 32, 36, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2434, 7222, 63, 4, 6, 8, 10, 12, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2434, 7274, 63, 2, 2, 2, 2, 2, 1, 300);


-- Monster 2436 : Jiminicrackler the Conscious
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2436, 431, 63, 12, 14, 18, 28, 32, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2436, 2252, 63, 6, 6, 8, 10, 12, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2436, 545, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2436, 543, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2436, 547, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2436, 546, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2436, 447, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2436, 2306, 63, 1, 1, 1.2, 1.2, 1.4, 1, 400);


-- Monster 2437 : Crowmanion the Primitive
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2437, 2060, 63, 30, 40, 50, 60, 70, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2437, 1889, 63, 20, 30, 40, 50, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2437, 2056, 63, 2, 4, 6, 8, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2437, 2058, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2437, 2059, 63, 0.06, 0.06, 0.06, 0.06, 0.06, 1, 400);


-- Monster 2438 : Cracklerod the Old
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2438, 2252, 63, 8, 10, 12, 14, 16, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2438, 2304, 63, 8, 10, 12, 16, 20, 1, 130);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2438, 547, 63, 6, 7, 8, 9, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2438, 543, 63, 6, 7, 8, 9, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2438, 545, 63, 6, 7, 8, 9, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2438, 546, 63, 6, 7, 8, 9, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2438, 544, 63, 6, 7, 8, 9, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2438, 2306, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2438, 2305, 63, 0.4, 0.6, 1, 1.4, 1.8, 1, 130);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2438, 2251, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);


-- Monster 2439 : Croccyx the Bummer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2439, 1663, 63, 20, 30, 40, 50, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2439, 1664, 63, 20, 30, 40, 50, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2439, 6739, 63, 3, 4, 5, 6, 7, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2439, 1635, 1, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2439, 544, 63, 0.4, 0.4, 0.4, 0.4, 0.4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2439, 2664, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2439, 1719, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2439, 1718, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 2440 : Lupisnockio the Woodwolf
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2440, 2315, 63, 60, 60, 60, 60, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2440, 2316, 63, 50, 50, 50, 50, 50, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2440, 2502, 63, 20, 20, 20, 20, 20, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2440, 2501, 63, 20, 20, 20, 20, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2440, 2317, 63, 8, 8, 8, 8, 8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2440, 2504, 63, 4, 4, 4, 4, 4, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2440, 2503, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2440, 2505, 63, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2442 : Dokterwho the Tardisporter
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2442, 8002, 63, 20, 22, 24, 26, 28, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2442, 13731, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2442, 7904, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2442, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2442, 8075, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 100);


-- Monster 2445 : Dreggoog the Downunder
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2445, 843, 63, 72, 74, 76, 78, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2445, 846, 63, 20, 30, 40, 50, 60, 1, 130);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2445, 547, 63, 2, 4, 6, 8, 10, 1, 400);


-- Monster 2446 : Dreggooniz the Adventurous
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2446, 842, 63, 72, 74, 76, 78, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2446, 845, 63, 20, 30, 40, 50, 60, 1, 130);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2446, 544, 63, 2, 4, 6, 8, 10, 1, 400);


-- Monster 2447 : Dreggrieg the Pianist
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2447, 1129, 63, 72, 74, 76, 78, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2447, 844, 63, 20, 30, 40, 50, 60, 1, 130);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2447, 546, 63, 2, 4, 6, 8, 10, 1, 400);


-- Monster 2448 : Dreggooliz the Macho
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2448, 308, 63, 72, 74, 76, 78, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2448, 847, 63, 20, 30, 40, 50, 60, 1, 130);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2448, 545, 63, 2, 4, 6, 8, 10, 1, 400);


-- Monster 2449 : Dreggommomm the Chewer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2449, 843, 63, 40, 50, 60, 70, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2449, 547, 63, 2, 4, 6, 8, 10, 1, 400);


-- Monster 2450 : Dreggump the Shrimp
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2450, 842, 63, 40, 50, 60, 70, 80, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2450, 544, 63, 2, 4, 6, 8, 10, 1, 400);


-- Monster 2451 : Dragoolash the Stewed
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2451, 843, 63, 50, 55, 60, 65, 70, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2451, 547, 63, 2, 3, 4, 5, 6, 1, 0);


-- Monster 2452 : Dragory the Violent
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2452, 8360, 63, 20, 22, 24, 26, 28, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2452, 8359, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2452, 8361, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2453 : Dragaustin the Power
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2453, 8354, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2453, 8350, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2453, 8346, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 2454 : Dragorse the Wild
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2454, 8354, 63, 20, 22, 24, 26, 28, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2454, 8350, 63, 4, 4.2, 4.4, 4.6, 4.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2454, 8346, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 100);


-- Monster 2455 : Dragostino the Tiny
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2455, 8355, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2455, 8351, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2455, 8347, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 2456 : Draigovsky the SocalledSwan
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2456, 8355, 63, 20, 22, 24, 26, 28, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2456, 8351, 63, 4, 4.2, 4.4, 4.6, 4.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2456, 8347, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 100);


-- Monster 2457 : Dragospel the Black
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2457, 8352, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2457, 8348, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2457, 8344, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 2458 : Draghouse the Cynical
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2458, 8352, 63, 20, 22, 24, 26, 28, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2458, 8348, 63, 4, 4.2, 4.4, 4.6, 4.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2458, 8344, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 100);


-- Monster 2459 : Dragoskovit the Barefoot
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2459, 8353, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2459, 8349, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2459, 8345, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 2460 : Dragossiper the Nag
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2460, 8353, 63, 20, 22, 24, 26, 28, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2460, 8349, 63, 4, 4.2, 4.4, 4.6, 4.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2460, 8345, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 100);


-- Monster 2461 : Dragangora the Softy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2461, 8356, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2461, 8357, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2461, 8358, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2462 : Drakoamax the Mad
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2462, 8054, 63, 20, 22, 24, 26, 28, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2462, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2462, 7904, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2462, 8086, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2464 : Dregguantico the Trainer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2464, 8362, 63, 20, 22, 24, 26, 28, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2464, 8364, 63, 4, 4.2, 4.4, 4.6, 4.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2464, 8363, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2465 : Drakokidoki the Volunteer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2465, 2598, 63, 40, 46, 52, 64, 72, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2465, 2179, 63, 30, 32, 34, 36, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2465, 2586, 63, 10, 12, 14, 16, 20, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2465, 2591, 63, 4, 4, 5, 6, 6, 1, 0);


-- Monster 2466 : Aperobics the Dynamic
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2466, 13491, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2466, 351, 63, 20, 22, 24, 26, 28, 1, 100);


-- Monster 2467 : Polterghaisk the Stray Soul
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2467, 2277, 1, 4, 4, 4, 4, 4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2467, 6478, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 400);


-- Monster 2468 : Ghostabrava the Tourist
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2468, 2277, 63, 4, 4, 4, 4, 4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2468, 6480, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 400);


-- Monster 2469 : Pandipoopik the Wondrous
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2469, 7225, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2469, 7224, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2469, 7223, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2469, 7025, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2469, 7222, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2469, 7405, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2469, 7379, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2470 : Pandoracle the Opposing Force
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2470, 7222, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2470, 7225, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2470, 7024, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2470, 7223, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2470, 7224, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2470, 7406, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2470, 7393, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2471 : Pandumonium the Joker
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2471, 7224, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2471, 7023, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2471, 7222, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2471, 7225, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2471, 7223, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2471, 7407, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2471, 7384, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2472 : Tanukhiraru the Gifted

-- Monster 2473 : TanuKiki the Deliveryghost

-- Monster 2474 : Tanukhuina the Drawer

-- Monster 2475 : Tanno the Dominator

-- Monster 2476 : Tanaked the Stalker

-- Monster 2477 : Tanuktonik the Doofdoof
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2477, 7025, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2477, 7225, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2477, 7222, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2477, 7224, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2477, 7223, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2477, 7385, 1, 10, 10, 10, 10, 10, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2477, 7410, 1, 10, 10, 10, 10, 10, 1, 300);


-- Monster 2482 : Koalarchitect the Balancing Force
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2482, 8057, 63, 20, 22, 24, 26, 28, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2482, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2482, 7904, 63, 10, 10, 10, 10, 10, 1, 100);


-- Monster 2483 : Yoksai the Spirited
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2483, 7224, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2483, 7025, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2483, 7223, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2483, 7225, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2483, 7222, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2483, 7411, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2483, 7383, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2484 : Ambushapens the Unlucky
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2484, 448, 1, 40, 70, 60, 70, 80, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2484, 2254, 63, 20, 20, 22, 24, 26, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2484, 6736, 1, 20, 24, 32, 36, 40, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2484, 2634, 1, 4, 4, 5, 5, 6, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2484, 6737, 8, 3, 4, 5, 6, 7, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2484, 2633, 2, 0.4, 0.6, 0.8, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2484, 6735, 8, 0.2, 0.4, 0.6, 0.8, 1, 1, 400);


-- Monster 2486 : Gargoyla the Paranoiac
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2486, 718, 1, 1, 1, 1, 1, 1, 1, 200);


-- Monster 2487 : Jellvis the King
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2487, 757, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2487, 311, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2487, 995, 63, 1, 1, 1, 1, 1, 1, 200);


-- Monster 2493 : Ginsync the Hyperactive
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2493, 13729, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2493, 965, 1, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2494 : Goblimp the Bis Kit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2494, 2318, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);


-- Monster 2495 : Greetdoff the Gentleman
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2495, 2253, 5, 32, 36, 40, 44, 48, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2495, 1018, 1, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2495, 2632, 1, 8, 10, 14, 18, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2495, 2630, 1, 4, 4, 5, 6, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2495, 2631, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2495, 2607, 3, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 300);


-- Monster 2497 : Chukoalak the Norris
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2497, 13697, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2497, 8001, 63, 10, 12, 14, 16, 18, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2497, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2497, 7904, 63, 10, 10, 10, 10, 10, 1, 100);


-- Monster 2498 : Gwabbit the Wunner
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2498, 419, 63, 40, 44, 48, 52, 56, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2498, 418, 63, 30, 32, 34, 36, 38, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2498, 650, 63, 10, 12, 14, 16, 18, 1, 0);


-- Monster 2502 : Kannimantha the Maneater
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2502, 2628, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);


-- Monster 2503 : Kaniedoss the Giggling
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2503, 1890, 63, 50, 56, 64, 68, 76, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2503, 2528, 63, 40, 44, 48, 52, 56, 1, 60);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2503, 2551, 2, 28, 32, 36, 40, 44, 1, 160);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2503, 7030, 1, 20, 24, 28, 32, 36, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2503, 2550, 4, 8, 8, 10, 12, 14, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2503, 2549, 1, 6, 6, 6, 6, 6, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2503, 2552, 2, 0.4, 0.4, 0.4, 0.4, 0.4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2503, 796, 1, 0.04, 0.04, 0.04, 0.04, 0.04, 1, 0);


-- Monster 2505 : Kannemik the Skinny
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2505, 2627, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);


-- Monster 2506 : Kannarrie the Reckless
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2506, 2625, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 80);


-- Monster 2507 : Kanniranda the Maniac
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2507, 2626, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);


-- Monster 2508 : Kirevampiro the Wrestler
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2508, 13489, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2508, 965, 1, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2509 : Crackoalak the Blonde
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2509, 8062, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2509, 7904, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2509, 7903, 63, 10, 10, 10, 10, 10, 1, 100);


-- Monster 2510 : Kitchy the Scratcher
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2510, 7279, 63, 60, 64, 68, 72, 76, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2510, 7275, 63, 10, 12, 14, 16, 18, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2510, 7223, 63, 10, 20, 30, 40, 50, 1, 100);


-- Monster 2511 : Kitsouie the Green
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2511, 7281, 63, 60, 64, 68, 72, 76, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2511, 7224, 63, 20, 30, 40, 50, 60, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2511, 7277, 63, 10, 12, 14, 16, 18, 1, 0);


-- Monster 2512 : Kitsuey the Red
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2512, 7280, 63, 60, 64, 68, 72, 76, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2512, 7276, 63, 10, 12, 14, 16, 18, 1, 0);


-- Monster 2513 : Jackoalak the Ripper
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2513, 7904, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2513, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2513, 8060, 63, 10, 11, 12, 13, 14, 1, 300);


-- Monster 2514 : Koalsen the Similar
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2514, 13699, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2514, 7904, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2514, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2514, 8056, 63, 4, 4.2, 4.4, 4.6, 4.8, 1, 400);


-- Monster 2515 : Popoalak the Mousibrown
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2515, 8085, 63, 20, 22, 24, 26, 28, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2515, 7904, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2515, 7903, 63, 10, 10, 10, 10, 10, 1, 100);


-- Monster 2516 : Koaly the Fiddler
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2516, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2516, 8059, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2516, 7904, 63, 10, 10, 10, 10, 10, 1, 100);


-- Monster 2517 : Koaldmen the Grumpy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2517, 7904, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2517, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2517, 8050, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2518 : Snapoalak the Redhead
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2518, 8061, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2518, 7904, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2518, 7903, 63, 10, 10, 10, 10, 10, 1, 100);


-- Monster 2519 : Koaldman the Garish
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2519, 13698, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2519, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2519, 7904, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2519, 8063, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2519, 8064, 63, 2, 2, 2, 2, 2, 1, 100);


-- Monster 2520 : Koleraspootin the Anesthesialogist
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2520, 2496, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2520, 2301, 63, 100, 100, 100, 100, 100, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2520, 2497, 63, 30, 32, 36, 40, 46, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2520, 2499, 63, 20, 26, 30, 34, 40, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2520, 2495, 63, 10, 12, 14, 16, 20, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2520, 2500, 63, 0.02, 0.04, 0.06, 0.08, 0.1, 1, 100);


-- Monster 2521 : Khameleltux the Tolerant
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2521, 8314, 63, 40, 42, 44, 46, 48, 1, 100);


-- Monster 2522 : Kwoanium the Smart
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2522, 2283, 2, 40, 42, 44, 46, 48, 1, 120);


-- Monster 2524 : Ouassup the Irritating
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2524, 8801, 1, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2524, 8807, 1, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2524, 1575, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 400);


-- Monster 2525 : Lert Macraken the Used Emo
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2525, 13489, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2525, 965, 1, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2526 : MoMaho the Modernist
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2526, 7292, 63, 40, 40, 40, 40, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2526, 7225, 3, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2526, 7023, 1, 10, 10, 10, 10, 10, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2526, 7298, 1, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2526, 7304, 1, 1, 1, 1, 1, 1, 1, 300);


-- Monster 2527 : Leopardon the Sorry
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2527, 7343, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2527, 7225, 1, 40, 60, 80, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2527, 7301, 63, 30, 32, 34, 36, 38, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2527, 7024, 1, 2, 2, 2, 2, 2, 1, 300);


-- Monster 2528 : Mamankalak the Bibliomaniac
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2528, 7904, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2528, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2528, 8055, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2529 : Salamaa the Henpeck
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2529, 8315, 63, 40, 42, 44, 46, 48, 1, 100);


-- Monster 2531 : Koalakropolis the King of the Hill
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2531, 8052, 63, 20, 22, 24, 26, 28, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2531, 7904, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2531, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2531, 8076, 63, 2, 2, 2, 2, 2, 1, 100);


-- Monster 2532 : Lord Lacedhat the Vampyric
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2532, 2279, 63, 40, 42, 44, 46, 48, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2532, 752, 63, 20, 30, 40, 50, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2532, 756, 63, 4, 4, 4, 4, 4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2532, 2280, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2532, 2285, 63, 0.8, 0.8, 1, 1, 1.2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2532, 2281, 63, 0.6, 0.6, 0.8, 0.8, 0.8, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2532, 2669, 63, 0.3, 0.32, 0.34, 0.34, 0.4, 1, 125);


-- Monster 2533 : Milikkybum the Informer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2533, 8312, 63, 40, 42, 44, 46, 48, 1, 100);


-- Monster 2534 : Jackoalak the Moonwalker
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2534, 8058, 63, 20, 22, 24, 26, 28, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2534, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2534, 7904, 63, 10, 10, 10, 10, 10, 1, 100);


-- Monster 2538 : Hazwonball the Hickler
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2538, 13496, 63, 20, 22, 24, 26, 28, 1, 0);


-- Monster 2539 : Nebuchadnezzar the Conqueror
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2539, 13729, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2539, 965, 1, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2540 : Niptuk the Plasticynic
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2540, 13729, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2540, 965, 1, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2541 : Kokonan the Talker
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2541, 2618, 1, 8, 10, 12, 16, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2541, 2617, 1, 4, 4, 4, 4, 4, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2541, 997, 1, 4, 6, 8, 10, 12, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2541, 2619, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 400);


-- Monster 2542 : Eyemi the Narcissist
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2542, 2676, 1, 2, 2, 2, 2, 3, 1, 130);


-- Monster 2543 : Oni'orses the Foolish
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2543, 838, 1, 20, 20, 20, 20, 20, 1, 150);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2543, 546, 1, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2543, 1536, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2543, 1538, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2543, 1535, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2543, 1537, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 0);


-- Monster 2544 : Treekness the Dark
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2544, 8785, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2544, 8784, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2545 : Barbrosskam the Chief
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2545, 8757, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2545, 8754, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2546 : Pikhoven the Deaf
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2546, 8783, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2546, 8776, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2547 : Prikoko the Witless
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2547, 8767, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2547, 8769, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2548 : Bambottinit the Quiet
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2548, 7287, 63, 40, 44, 48, 52, 56, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2548, 7286, 63, 30, 32, 34, 36, 38, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2548, 7369, 63, 20, 22, 24, 26, 28, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2548, 7290, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 300);


-- Monster 2549 : Mushdrill the Piercer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2549, 417, 63, 60, 64, 68, 72, 76, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2549, 2584, 63, 20, 24, 28, 32, 36, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2549, 2585, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 400);


-- Monster 2550 : Floratio the Investigator
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2550, 8779, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2550, 8772, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2551 : Mushuliet the Catapulet
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2551, 290, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2551, 300, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2551, 426, 63, 20, 24, 28, 32, 36, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2551, 377, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2551, 424, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2551, 9279, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2551, 378, 63, 2, 4, 6, 8, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2551, 6921, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2551, 6919, 1, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2551, 6922, 1, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2551, 6920, 1, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2551, 2035, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2551, 1674, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 120);


-- Monster 2552 : Edvushmunch the Screamer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2552, 290, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2552, 300, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2552, 426, 63, 20, 24, 28, 32, 36, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2552, 424, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2552, 377, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2552, 9269, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2552, 378, 63, 2, 4, 6, 8, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2552, 6920, 1, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2552, 6922, 1, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2552, 6921, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2552, 6919, 1, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2552, 2035, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2552, 1674, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 120);


-- Monster 2553 : Nidsally the Mushtang
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2553, 290, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2553, 424, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2553, 300, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2553, 426, 63, 20, 24, 28, 32, 36, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2553, 377, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2553, 9263, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2553, 378, 63, 2, 4, 6, 8, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2553, 6922, 1, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2553, 6920, 1, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2553, 6921, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2553, 6919, 1, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2553, 2035, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2553, 1674, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 120);


-- Monster 2554 : Warazpacho the Cherrilla
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2554, 8782, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2554, 8775, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2555 : Kojaklator the Lollipoper
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2555, 8730, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2555, 8729, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2555, 9940, 63, 1, 2, 3, 4, 5, 1, 0);


-- Monster 2556 : Crackrodilrock the Helltune
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2556, 8737, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2557 : Crabartanian the Allforone
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2557, 8744, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2557, 8732, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2557, 9940, 63, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2558 : Craborthos the All
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2558, 8744, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2558, 8733, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2558, 9940, 63, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2559 : Crabaramis the One
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2559, 8744, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2559, 8735, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2559, 9940, 63, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2560 : Crabathos the For
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2560, 8744, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2560, 8734, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2560, 9940, 63, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2561 : Crackedral the Majestic
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2561, 8762, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2561, 8736, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2562 : Ezothbeitor the Neighbour
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2562, 13338, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2562, 8800, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2562, 8803, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2562, 929, 63, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2563 : Arepotair the Bespectacled
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2563, 2277, 63, 4, 4, 4, 4, 4, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2563, 6479, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 2564 : Miomaho the Siciliano
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2564, 7222, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2564, 7224, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2564, 7223, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2564, 7023, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2564, 7225, 63, 40, 42, 44, 46, 48, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2564, 7404, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2564, 7261, 63, 20, 22, 24, 26, 28, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2564, 7380, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2565 : Floramodovar the Stoned
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2565, 8778, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2565, 8771, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2566 : Calipzoth the Icy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2566, 13499, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2566, 8800, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2566, 8805, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2566, 929, 63, 1, 1, 1, 1, 1, 1, 100);


-- Monster 2567 : Ryukualak the Bored
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2567, 13495, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2567, 7904, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2567, 7903, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2567, 8082, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);


-- Monster 2568 : Minoknok the Visitor
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2568, 8311, 63, 40, 42, 44, 46, 48, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2568, 13504, 63, 20, 22, 24, 26, 28, 1, 100);


-- Monster 2569 : Zigzoth the Indecisive
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2569, 8800, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2569, 8802, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2569, 929, 63, 1, 1, 1, 1, 1, 1, 100);


-- Monster 2570 : Snailmetalika the Garagician
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2570, 8832, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2570, 8793, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2571 : Kidodo the Extinct
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2571, 8766, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2571, 8741, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2572 : Killua the Assassin
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2572, 8756, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2572, 8765, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2573 : Misskokoko the Channel
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2573, 997, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2573, 1002, 1, 80, 82, 84, 86, 88, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2573, 2618, 1, 16, 16, 18, 18, 20, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2573, 2624, 3, 4, 4, 5, 7, 8, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2573, 2619, 1, 0.4, 0.4, 0.4, 0.4, 0.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2573, 1678, 3, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 2574 : Larvalaska the Cold
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2574, 519, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2574, 362, 63, 20, 30, 40, 50, 60, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2574, 7423, 63, 2, 2, 2, 2, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2574, 2474, 1, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2574, 643, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 200);


-- Monster 2575 : Ouassingiam the Tyrant
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2575, 8801, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2575, 13490, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2575, 8807, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2575, 1575, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 400);


-- Monster 2577 : Moops the Bubbleboy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2577, 8790, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2577, 8791, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2578 : Don Quizothe the Stubborn
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2578, 13503, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2578, 8800, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2578, 8804, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2578, 929, 63, 1, 1, 1, 1, 1, 1, 100);


-- Monster 2579 : Supergwass the Free
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2579, 8780, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2579, 8773, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2580 : Mufavabeenz the Cannibal
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2580, 8763, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2580, 8753, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2581 : Palmella the Hefty
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2581, 8749, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2581, 8745, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2582 : Palmpilot the Yuppie
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2582, 8750, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2582, 8746, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2583 : Palmoleaf the Greasy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2583, 8752, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2583, 8748, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2584 : Naypalm the Herbivorous
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2584, 8751, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2584, 8747, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2585 : Osurcus the Tamer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2585, 13729, 63, 20, 22, 24, 26, 28, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2585, 965, 1, 1, 1, 1, 1, 1, 1, 0);


-- Monster 2587 : Zouzoth the Cuddly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2587, 13733, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2587, 8800, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2587, 8806, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2587, 929, 63, 1, 1, 1, 1, 1, 1, 100);


-- Monster 2588 : Sparodi the Python
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2588, 8760, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2588, 8739, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2589 : Ougathard the Fortunate
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2589, 2321, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2589, 2248, 63, 84, 88, 92, 96, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2589, 2491, 63, 24, 28, 32, 36, 40, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2589, 2286, 63, 20, 26, 32, 38, 44, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2589, 2492, 63, 4, 5, 6, 7, 8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2589, 350, 63, 2, 4, 6, 8, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2589, 2320, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2589, 2494, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2589, 2285, 63, 0.2, 0.6, 1, 1.4, 1.8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2589, 315, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2589, 2104, 1, 0.1, 0.12, 0.14, 0.16, 0.2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2589, 2493, 63, 0.02, 0.04, 0.06, 0.08, 0.1, 1, 100);


-- Monster 2590 : Pandarwin the Naturist
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2590, 7222, 63, 10, 20, 30, 40, 50, 1, 100);


-- Monster 2591 : Pandan the Desperate
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2591, 7222, 1, 10, 20, 30, 40, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2591, 680, 1, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 200);


-- Monster 2592 : Pandartmoore the Dogged
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2592, 7025, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2592, 350, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2592, 7017, 63, 2, 2, 2, 2, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2592, 2287, 63, 2, 2, 2, 2, 2, 1, 101);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2592, 6923, 1, 1, 1, 1, 1, 1, 1, 200);


-- Monster 2593 : Pandaltry the Unknown
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2593, 7258, 63, 40, 44, 48, 52, 56, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2593, 7260, 63, 40, 44, 48, 52, 56, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2593, 7025, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2593, 7259, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2593, 7017, 63, 2, 2, 2, 2, 2, 1, 300);


-- Monster 2597 : Pandora the Explorer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2597, 7295, 63, 40, 40, 40, 40, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2597, 7261, 63, 20, 22, 24, 26, 28, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2597, 7302, 63, 12, 12, 12, 12, 12, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2597, 7307, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 2598 : Pandali the Surreal
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2598, 7297, 63, 40, 44, 48, 52, 56, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2598, 7222, 63, 20, 30, 40, 50, 60, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2598, 7303, 63, 10, 12, 14, 16, 18, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2598, 7308, 63, 1, 1, 1, 1, 1, 1, 300);


-- Monster 2600 : Mushketeer the Loyal
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2600, 290, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2600, 424, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2600, 300, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2600, 426, 63, 20, 24, 28, 32, 36, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2600, 377, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2600, 9277, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2600, 378, 63, 2, 4, 6, 8, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2600, 6920, 1, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2600, 6919, 1, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2600, 6921, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2600, 6922, 1, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2600, 2035, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2600, 1674, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 120);


-- Monster 2601 : Trumpaynor the Survivor
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2601, 290, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2601, 300, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2601, 377, 63, 20, 40, 60, 80, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2601, 424, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2601, 426, 63, 20, 24, 28, 32, 36, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2601, 9267, 63, 10, 10.2, 10.4, 10.6, 10.8, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2601, 378, 63, 2, 4, 6, 8, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2601, 6919, 1, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2601, 6921, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2601, 6922, 1, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2601, 6920, 1, 1, 1, 1, 1, 1, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2601, 2035, 63, 0.2, 0.2, 0.2, 0.2, 0.2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2601, 1674, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 120);


-- Monster 2602 : Angora Bow Meow

-- Monster 2605 : Miniminotot

-- Monster 2606 : Crocodyl Ghost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2606, 2239, 1, 5, 5, 5, 5, 5, 1, 0);


-- Monster 2607 : Black Dragoone

-- Monster 2608 : Sacrier Dopple

-- Monster 2609 : Osamodas Dopple

-- Monster 2611 : Short-Tempered Minotoror

-- Monster 2612 : Demonic Icikle
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2612, 8335, 63, 50, 51, 52, 53, 54, 1, 0);


-- Monster 2614 : Roy the Rover
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2614, 8808, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2614, 8809, 63, 2, 2, 2, 2, 2, 1, 400);


-- Monster 2615 : Scaraheath the Hanger
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2615, 8308, 63, 20, 22, 24, 26, 28, 1, 100);


-- Monster 2616 : Ougineemo the Lost
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2616, 8801, 1, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2616, 8807, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2616, 1575, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 400);


-- Monster 2617 : Sumens's Living Bag

-- Monster 2618 : Ondine

-- Monster 2619 : The Fruitful

-- Monster 2622 : Kwismas Whitish Fang
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2622, 12838, 63, 5, 10, 15, 20, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2622, 12132, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 2627 : Birthday Imp
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2627, 8145, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2627, 6857, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2627, 351, 63, 50, 51, 52, 53, 54, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2627, 6593, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2627, 6550, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2627, 6643, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2627, 6647, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2627, 182, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2627, 6646, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2627, 8815, 63, 2, 2.1, 2.2, 2.3, 2.4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2627, 2318, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 2630 : Major Arachnee

-- Monster 2631 : Innkeeper Bagrutte

-- Monster 2642 : Newly Tamed Whitish Fang

-- Monster 2672 : Gobtubby Ghost

-- Monster 2680 : Swimp the Simple
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2680, 8145, 63, 50, 52, 54, 56, 58, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2680, 678, 1, 20, 21, 22, 23, 24, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2680, 680, 1, 5, 6, 7, 8, 9, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2680, 479, 1, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 400);


-- Monster 2681 : Dikal

-- Monster 2682 : Bone Idol

-- Monster 2683 : Nemik

-- Monster 2684 : Nekros

-- Monster 2685 : Chafer Axit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2685, 310, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2685, 2323, 1, 5, 5, 5, 5, 5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2685, 1675, 1, 2, 2, 2, 2, 2, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2685, 6475, 1, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 400);


-- Monster 2691 : Pandawa Dopple

-- Monster 2727 : Barrel

-- Monster 2750 : Tree of Life

-- Monster 2757 : Grimpil's Kitten

-- Monster 2770 : Bony Chafer

-- Monster 2771 : Summoned Treechnid
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2771, 434, 63, 60, 70, 80, 90, 100, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2771, 435, 63, 60, 70, 80, 90, 100, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2771, 437, 63, 50, 55, 60, 65, 70, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2771, 792, 63, 20, 22, 24, 26, 28, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2771, 463, 63, 10, 20, 30, 40, 50, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2771, 464, 63, 10, 20, 30, 40, 50, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2771, 926, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 400);


-- Monster 2772 : Jorbak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2772, 10665, 63, 10, 10, 10, 10, 10, 1, 200);


-- Monster 2773 : Goultard's Evil Essence
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2773, 10680, 63, 100, 100, 100, 100, 100, 1, 0);


-- Monster 2774 : Goultard's Residual Essence

-- Monster 2781 : Lupus Minimus

-- Monster 2782 : Golden Dragoone Ghost

-- Monster 2785 : Incarnam Scarecrow

-- Monster 2786 : Bloodthirsty Vampyre
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2786, 10825, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2786, 2279, 63, 100, 100, 100, 100, 100, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2786, 752, 63, 100, 100, 100, 100, 100, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2786, 10826, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2786, 756, 63, 10, 10, 10, 10, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2786, 2285, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2786, 2281, 63, 2, 2, 2, 2, 2, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2786, 2669, 63, 1, 1, 1, 1, 1, 1, 100);


-- Monster 2788 : Extremely Evil Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2788, 375, 63, 20, 40, 60, 80, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2788, 376, 2, 20, 40, 60, 80, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2788, 2675, 63, 8, 10, 12, 16, 20, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2788, 2673, 1, 1, 1, 1, 1, 1, 1, 400);


-- Monster 2789 : Summoned Crackrock
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2789, 448, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2789, 450, 63, 10, 10.2, 10.4, 10.4, 10.8, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2789, 544, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2789, 545, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2789, 546, 63, 1, 1, 1, 1, 1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2789, 543, 63, 1, 1, 1, 1, 1, 1, 300);


-- Monster 2790 : Dusty Piwi

-- Monster 2791 : Aggressive Moumouse

-- Monster 2793 : Krtek
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2793, 10843, 63, 100, 100, 100, 100, 100, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2793, 313, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2793, 312, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2793, 446, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2793, 350, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2793, 2274, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2793, 2282, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2793, 546, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2793, 543, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2793, 930, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2793, 545, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2793, 544, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2793, 480, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2793, 547, 63, 1, 1, 1, 1, 1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2793, 6476, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 2794 : Boombomb

-- Monster 2795 : Sauroshell

-- Monster 2796 : Krtek Dark Miner
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2796, 446, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2796, 312, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2796, 350, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2796, 313, 63, 5, 5, 5, 5, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2796, 2282, 63, 1, 1, 1, 1, 1, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2796, 2274, 63, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2796, 545, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2796, 547, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2796, 6476, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2796, 544, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2796, 543, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2796, 546, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2796, 480, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2796, 930, 63, 0.05, 0.06, 0.07, 0.08, 0.1, 1, 0);


-- Monster 2797 : Krusty

-- Monster 2798 : Will-o'-the-Wisp

-- Monster 2799 : Attila

-- Monster 2803 : Altruistic Air Spirit

-- Monster 2804 : Aggressive Air Spirit

-- Monster 2805 : Altruistic Water Spirit

-- Monster 2806 : Aggressive Water Spirit

-- Monster 2807 : Altruistic Fire Spirit

-- Monster 2808 : Aggressive Fire Spirit

-- Monster 2809 : Altruistic Earth Spirit

-- Monster 2810 : Aggressive Earth Spirit

-- Monster 2811 : Air Spirit

-- Monster 2812 : Water Spirit

-- Monster 2813 : Fire Spirit

-- Monster 2814 : Earth Spirit

-- Monster 2815 : Red Totem

-- Monster 2816 : Blue Totem

-- Monster 2817 : Green Totem

-- Monster 2818 : Yellow Totem

-- Monster 2821 : Kit Knapping's Chest
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2821, 746, 3, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2821, 6458, 3, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2821, 745, 3, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2821, 7035, 3, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2821, 748, 3, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2821, 7036, 3, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2821, 749, 3, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2821, 6457, 3, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2821, 747, 3, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2821, 750, 3, 10, 10, 10, 10, 10, 1, 100);


-- Monster 2823 : Spectral Tofu

-- Monster 2824 : Spectral Boar

-- Monster 2827 : YeCh'Ti

-- Monster 2839 : Bulbisnow

-- Monster 2841 : Icefish Devourer

-- Monster 2842 : Bewitched Aspen

-- Monster 2843 : Frosteez Scarecrow

-- Monster 2844 : Obsidian Crackler

-- Monster 2845 : Woolly Piggoth
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2845, 11243, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2845, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2845, 11242, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2846 : Frighog
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2846, 11247, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2846, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2846, 11246, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2847 : Kanigloo
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2847, 11248, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2847, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2847, 11249, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2848 : Royal Pingwin
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2848, 11232, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2848, 11475, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2848, 11233, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 2849 : Pingwinkle
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2849, 11223, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2849, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2849, 11222, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2850 : Mastogob
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2850, 11117, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2850, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2850, 11118, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2851 : Mastogob Warrior
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2851, 11122, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2851, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2851, 11121, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2852 : Venerable Mastogob
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2852, 11136, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2852, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2852, 11135, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2853 : Mastogobbly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2853, 11119, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2853, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2853, 11120, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2854 : Royal Mastogob
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2854, 11221, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2854, 11475, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2854, 11219, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 2855 : Shaman Pingwin
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2855, 11229, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2855, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2855, 11228, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2856 : Pingwobble
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2856, 11225, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2856, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2856, 11224, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2857 : Mama Pingwin
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2857, 11227, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2857, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2857, 11226, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2858 : Kung-Fu Pingwin
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2858, 11230, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2858, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2858, 11231, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2859 : Cromagmunk
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2859, 11238, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2859, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2859, 11239, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2860 : Sabredon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2860, 11241, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2860, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2860, 11240, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2860, 11498, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 300);


-- Monster 2861 : Apewicubic Bearbarian
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2861, 11934, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2861, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2861, 11933, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2862 : Torpid Bearbarian
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2862, 11936, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2862, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2862, 11935, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2863 : Vespal Bearbarian
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2863, 11942, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2863, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2863, 11941, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2864 : Celestial Bearbarian
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2864, 11943, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2864, 11475, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2864, 11944, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 2865 : Steam Crackler
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2865, 11327, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2865, 11328, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2866 : Atomystique
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2866, 11323, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2866, 11324, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2867 : Mofette
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2867, 11331, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2867, 11332, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2868 : Fumaroller
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2868, 11329, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2868, 11330, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2869 : Solfatara
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2869, 11325, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2869, 11326, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2874 : Talklyka Pirate
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2874, 11309, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2874, 11310, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2875 : Karrybean Pirate
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2875, 11312, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2875, 11311, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2876 : Vigi Pirate
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2876, 11319, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2876, 11320, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2877 : Buck Anear
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2877, 11321, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2877, 11322, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 2878 : Harpy Pirate
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2878, 11318, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2878, 11317, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2880 : Retspan Pirate
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2880, 11315, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2880, 11316, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2881 : Yuara Pirate
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2881, 11314, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2881, 11313, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2882 : Mega Plain Crackler
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2882, 2304, 3, 15, 15, 15, 15, 15, 1, 130);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2882, 2252, 2, 13, 13, 13, 13, 13, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2882, 545, 1, 7.5, 7.5, 7.5, 7.5, 7.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2882, 546, 1, 7.5, 7.5, 7.5, 7.5, 7.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2882, 543, 1, 7.5, 7.5, 7.5, 7.5, 7.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2882, 547, 1, 7.5, 7.5, 7.5, 7.5, 7.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2882, 544, 1, 7.5, 7.5, 7.5, 7.5, 7.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2882, 2305, 2, 1.9, 1.9, 1.9, 1.9, 1.9, 1, 130);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2882, 2306, 1, 1, 1, 1, 1, 1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2882, 2251, 1, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);


-- Monster 2883 : Bestial Brockhard
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2883, 11923, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2883, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2883, 11924, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2884 : Venomous Brockhard
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2884, 11930, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2884, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2884, 11929, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2885 : Gluttonous Brockhard
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2885, 11921, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2885, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2885, 11922, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2886 : Nightcrawling Brockhard
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2886, 11926, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2886, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2886, 11925, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2887 : Cromignon

-- Monster 2888 : Yokai Snowfoux
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2888, 11339, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2888, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2888, 11340, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2889 : Maho Snowfoux
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2889, 11338, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2889, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2889, 11337, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2890 : Soryo Snowfoux
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2890, 11336, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2890, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2890, 11335, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2891 : Yomi Snowfoux
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2891, 11880, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2891, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2891, 11882, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2892 : Stunted Rat
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2892, 11253, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2892, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2892, 11252, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2893 : Crabeye
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2893, 11254, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2893, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2893, 11255, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2895 : Dramanita
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2895, 11885, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2895, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2895, 11886, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2896 : Fistulina
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2896, 11888, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2896, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2896, 11887, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2897 : Fungore
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2897, 11890, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2897, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2897, 11889, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2898 : Treecherous
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2898, 11891, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2898, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2898, 11892, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2899 : Clump

-- Monster 2900 : Serpula
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2900, 11893, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2900, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2900, 11894, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2913 : Woolly Bow Meow
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2913, 11250, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2913, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2913, 11251, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2914 : Gullipop
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2914, 11257, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2914, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2914, 11256, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2924 : Obsidemon
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2924, 11333, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2924, 11334, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 2925 : Bulbydro

-- Monster 2926 : Bulbydromatic

-- Monster 2930 : Frigostian Bandit

-- Monster 2932 : Buzta
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2932, 11519, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2932, 11520, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2932, 11522, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2933 : Asploda
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2933, 11521, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2933, 11522, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2933, 11523, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2934 : Grabba
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2934, 11527, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2934, 11522, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2934, 11526, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2935 : Stabba
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2935, 11525, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2935, 11524, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2935, 11522, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2936 : Bomma
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2936, 11529, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2936, 11522, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2936, 11528, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2937 : Drilla
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2937, 11530, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2937, 11531, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2937, 11522, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2941 : Gobshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2941, 11532, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2941, 11533, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2941, 11522, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2942 : N
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2942, 13154, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2942, 11518, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2942, 11522, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2942, 11517, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2942, 11516, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 2950 : Ectobomb

-- Monster 2951 : Maglob

-- Monster 2955 : Hamrack

-- Monster 2956 : Reine Rate

-- Monster 2957 : Rat Daim

-- Monster 2958 : Air Ouaivz

-- Monster 2959 : Nasty Little Lousy Pig

-- Monster 2960 : Kanniball Andchain
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2960, 11695, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2960, 11696, 63, 10, 10, 10, 10, 10, 1, 200);


-- Monster 2961 : Haloperi Doll

-- Monster 2965 : Pyrotechnic Brockhard
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2965, 11920, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2965, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2965, 11919, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2966 : Icy Brockhard
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2966, 11927, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2966, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2966, 11928, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2967 : Tengu Snowfoux
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2967, 11341, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2967, 11475, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2967, 11342, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 2968 : Korriander
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2968, 11895, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2968, 11475, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2968, 11896, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 2969 : Sporachnee

-- Monster 2975 : Boostache
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2975, 11815, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2975, 11814, 63, 10, 10, 10, 10, 10, 1, 200);


-- Monster 2976 : Ashi-Magari

-- Monster 2980 : Lava Residue

-- Monster 2981 : Melted Ice

-- Monster 2985 : Maho 'Cole' da Zice
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2985, 11338, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2985, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2985, 11337, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2986 : Kolosso
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2986, 11931, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2986, 11475, 63, 10, 10, 10, 10, 10, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2986, 11932, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 2987 : Esurient Bearbarian
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2987, 11939, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2987, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2987, 11940, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2988 : Rampant Bearbarian
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2988, 11938, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2988, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2988, 11937, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2989 : Mellifluous Bearbarian
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2989, 11945, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2989, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2989, 11946, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2992 : Professor Xa

-- Monster 2993 : Trained Mastogob
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2993, 11117, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2993, 11475, 63, 5, 5.5, 6, 6.5, 7, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2993, 11118, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 2995 : Kwakwa
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2995, 12016, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (2995, 12015, 63, 10, 10, 10, 10, 10, 1, 200);


-- Monster 3095 : Mirh Station

-- Monster 3096 : Terrestrial Crow
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3096, 2060, 63, 15, 20, 25, 30, 35, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3096, 1889, 63, 10, 15, 20, 25, 30, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3096, 2056, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3096, 2058, 63, 0.1, 0.1, 0.1, 0.1, 0.1, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3096, 2059, 63, 0.03, 0.03, 0.03, 0.03, 0.03, 1, 400);


-- Monster 3097 : Lab Kolerat
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3097, 2301, 63, 60, 60, 60, 60, 60, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3097, 2496, 63, 50, 50, 50, 50, 50, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3097, 2497, 63, 15, 17, 19, 21, 23, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3097, 2499, 63, 10, 13, 16, 19, 21, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3097, 2495, 63, 5, 6, 7, 8, 9, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3097, 2500, 63, 0.01, 0.02, 0.03, 0.04, 0.05, 1, 100);


-- Monster 3098 : Rabid Ouginak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3098, 2321, 63, 50, 51, 52, 53, 54, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3098, 2248, 63, 42, 44, 46, 48, 50, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3098, 2491, 63, 12, 14, 16, 18, 20, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3098, 2286, 63, 10, 13, 16, 19, 23, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3098, 2492, 63, 2, 2, 2, 3, 3, 1, 120);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3098, 350, 63, 1, 2, 3, 4, 5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3098, 2320, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3098, 2494, 63, 0.2, 0.3, 0.3, 0.4, 0.4, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3098, 2285, 63, 0.2, 0.3, 0.5, 0.7, 0.9, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3098, 315, 63, 0.05, 0.05, 0.05, 0.05, 0.05, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3098, 2104, 1, 0.05, 0.05, 0.06, 0.07, 0.1, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3098, 2493, 63, 0.01, 0.02, 0.03, 0.04, 0.05, 1, 0);


-- Monster 3099 : Reinforced Scurvion
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3099, 2150, 63, 30, 32, 34, 36, 38, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3099, 2245, 63, 15, 16, 17, 18, 19, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3099, 1893, 63, 10, 11, 12, 13, 14, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3099, 2012, 63, 5, 6, 7, 8, 9, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3099, 2487, 63, 5, 5, 5, 5, 5, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3099, 2490, 63, 4, 5, 6, 7, 8, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3099, 2488, 63, 0.05, 0.07, 0.08, 0.09, 0.1, 1, 150);


-- Monster 3100 : Nelween
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3100, 2316, 63, 60, 60, 60, 60, 60, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3100, 2315, 63, 60, 60, 60, 60, 60, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3100, 12076, 63, 30, 30, 30, 30, 30, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3100, 2502, 63, 20, 20, 20, 20, 20, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3100, 2501, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3100, 2317, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3100, 12075, 63, 10, 10, 10, 10, 10, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3100, 2504, 63, 5, 5, 5, 5, 5, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3100, 2503, 63, 2, 2, 2, 2, 2, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3100, 2505, 63, 1, 1, 1, 1, 1, 1, 100);


-- Monster 3101 : Crocodyl Crockmaker

-- Monster 3106 : Animated Gift

-- Monster 3107 : Stuffed Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3107, 12132, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3107, 12840, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 3108 : Stuffed Gobball
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3108, 12132, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3108, 12840, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 3109 : Stuffed Wabbit
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3109, 12132, 63, 20, 25, 30, 35, 40, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3109, 12840, 63, 5, 10, 15, 20, 25, 1, 0);


-- Monster 3111 : Rogue Dopple

-- Monster 3112 : Explobomb

-- Monster 3113 : Grenado

-- Monster 3114 : Water Bomb

-- Monster 3115 : Bearendizer

-- Monster 3117 : Kwismas Dragoturkey
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3117, 12838, 63, 10, 15, 0, 0, 0, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3117, 12132, 63, 5, 10, 0, 0, 0, 1, 0);


-- Monster 3118 : Wild Kwismas Dragoturkey
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3118, 12132, 63, 15, 20, 0, 0, 0, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3118, 12839, 63, 10, 15, 0, 0, 0, 1, 0);


-- Monster 3119 : Impetuous Kwismas Dragoturkey
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3119, 12132, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3119, 12840, 63, 15, 15, 15, 15, 15, 1, 0);


-- Monster 3120 : Boombot

-- Monster 3131 : Rogue Dopple

-- Monster 3132 : Masqueraider Dopple

-- Monster 3133 : Small Animated Gift

-- Monster 3136 : Masqueraider Dopple

-- Monster 3137 : Gorgoyle
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3137, 12375, 63, 10, 11, 12, 13, 14, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3137, 12374, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 3141 : Goultard

-- Monster 3142 : Grozilla
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3142, 12467, 63, 25, 25, 25, 25, 25, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3142, 12468, 63, 2.5, 2.5, 2.5, 2.5, 2.5, 1, 400);


-- Monster 3143 : Grasmera

-- Monster 3144 : Raw Juvenile Sauroshell

-- Monster 3145 : Insipid Juvenile Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3145, 12432, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3145, 12448, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3145, 545, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3146 : Muddy Juvenile Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3146, 12428, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3146, 12448, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3146, 7024, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3147 : Incandescent Juvenile Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3147, 12436, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3147, 12448, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3147, 547, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3148 : Moist Juvenile Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3148, 12440, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3148, 12448, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3148, 546, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3149 : Dry Juvenile Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3149, 12444, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3149, 12448, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3149, 544, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3150 : Insipid Novice Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3150, 12433, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3150, 12449, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3150, 545, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3151 : Muddy Novice Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3151, 12429, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3151, 12449, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3151, 7024, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3152 : Incandescent Novice Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3152, 12437, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3152, 12449, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3152, 547, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3153 : Moist Novice Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3153, 12441, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3153, 12449, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3153, 546, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3154 : Dry Novice Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3154, 12445, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3154, 12449, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3154, 544, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3155 : Insipid Mature Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3155, 12434, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3155, 12450, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3155, 545, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3156 : Muddy Mature Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3156, 12430, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3156, 12450, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3156, 7024, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3157 : Incandescent Mature Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3157, 12438, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3157, 12450, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3157, 547, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3158 : Moist Mature Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3158, 12442, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3158, 12450, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3158, 546, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3159 : Dry Mature Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3159, 12446, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3159, 12450, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3159, 544, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3160 : Insipid Venerable Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3160, 12435, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3160, 12451, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3160, 545, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3161 : Muddy Venerable Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3161, 12431, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3161, 12451, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3161, 7024, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3162 : Incandescent Venerable Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3162, 12439, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3162, 12451, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3162, 547, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3163 : Moist Venerable Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3163, 12443, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3163, 12451, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3163, 546, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3164 : Dry Venerable Sauroshell
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3164, 12447, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3164, 12451, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3164, 544, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 100);


-- Monster 3165 : Raw Novice Sauroshell

-- Monster 3166 : Raw Mature Sauroshell

-- Monster 3167 : Raw Venerable Sauroshell

-- Monster 3168 : Sea Sauroshell

-- Monster 3171 : Artich Rogue

-- Monster 3172 : Moumouse Tourist

-- Monster 3173 : Rubilax

-- Monster 3174 : "The Sword"

-- Monster 3178 : Blubasaur the Weepy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3178, 12428, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3178, 12448, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3178, 7024, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3179 : Ikthyosaur the Fishy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3179, 12440, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3179, 12448, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3179, 546, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3180 : Prontosaur the Punctual
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3180, 12436, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3180, 12448, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3180, 547, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3181 : Segasaur the Megadriven
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3181, 12432, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3181, 12448, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3181, 7024, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3182 : Dynasaur the Scaly
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3182, 12444, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3182, 12448, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3182, 7024, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3183 : Saurbic the Acidic
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3183, 12429, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3183, 12449, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3183, 7024, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3184 : Absaurbo the Spongy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3184, 12441, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3184, 12449, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3184, 546, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3185 : Bedsaur the Blotchy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3185, 12437, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3185, 12449, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3185, 547, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3186 : Sauroful the Regretting
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3186, 12433, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3186, 12449, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3186, 545, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3187 : Saurlax the Sleepy
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3187, 12445, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3187, 12449, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3187, 544, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3188 : Saurdid the Scandalous
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3188, 12430, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3188, 12450, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3188, 7024, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3189 : Saurbet the Refreshing
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3189, 12442, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3189, 12450, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3189, 546, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3190 : Saurgei the Rushing
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3190, 12438, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3190, 12450, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3190, 547, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3191 : Scisaur the Pointed
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3191, 12434, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3191, 12450, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3191, 545, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3192 : Sauriasis the Flaky
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3192, 12446, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3192, 12450, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3192, 544, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3193 : Sauruman the Destructive
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3193, 12431, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3193, 12451, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3193, 7024, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3194 : Saurcery the Magical
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3194, 12443, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3194, 12451, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3194, 546, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3195 : Censaur the Forbidding
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3195, 12439, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3195, 12451, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3195, 7024, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3196 : Eyesaur the Repulsive
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3196, 12435, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3196, 12451, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3196, 545, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3197 : Tyranno the Despotic
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3197, 12447, 63, 20, 22, 24, 26, 28, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3197, 12451, 63, 2, 2.2, 2.4, 2.6, 2.8, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3197, 544, 63, 0.2, 0.4, 0.6, 0.8, 1, 1, 100);


-- Monster 3200 : Loofi

-- Monster 3201 : Delminss

-- Monster 3205 : Sick Grossewer Shaman
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3205, 2322, 63, 10, 11, 12, 13, 14, 1, 0);


-- Monster 3208 : Daggero
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3208, 7225, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3208, 7224, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3208, 7223, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3208, 7222, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3208, 12741, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3208, 12742, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 3209 : Log

-- Monster 3220 : Prez' Boom

-- Monster 3225 : Prez' Ley

-- Monster 3232 : Bulbig Brotha
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3232, 7265, 63, 15, 15, 15, 15, 15, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3232, 7223, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3232, 7222, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3232, 7225, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3232, 7224, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3232, 7269, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 3233 : King Allister's Envoy

-- Monster 3234 : Foster Fuji Snowfoux
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3234, 12001, 63, 30, 30, 30, 30, 30, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3234, 11884, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3234, 11883, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 3235 : Prepubescent Boostache

-- Monster 3237 : Gokwa Disciple

-- Monster 3238 : Ronin Chafer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3238, 310, 63, 30, 30, 30, 30, 30, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3238, 13096, 63, 15, 15, 15, 15, 15, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3238, 2323, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 3239 : Draugur Chafer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3239, 310, 63, 25, 26, 27, 28, 29, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3239, 13097, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3239, 2323, 63, 0.5, 1, 1.5, 2, 2.5, 1, 400);


-- Monster 3240 : Primitive Chafer
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3240, 310, 63, 25, 26, 27, 28, 29, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3240, 13098, 63, 10, 11, 12, 13, 14, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3240, 2323, 63, 0.5, 1, 1.5, 2, 2.5, 1, 400);


-- Monster 3244 : Scampihorse

-- Monster 3270 : Blue Lilpiwi
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3270, 6897, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3270, 287, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3270, 2475, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 3271 : Yellow Lilpiwi
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3271, 6902, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3271, 287, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3271, 2475, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 3272 : Pink Lilpiwi
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3272, 287, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3272, 6903, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3272, 2475, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 3273 : Red Lilpiwi
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3273, 6900, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3273, 287, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3273, 2475, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 3274 : Green Lilpiwi
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3274, 287, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3274, 6899, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3274, 2475, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 3275 : Purple Lilpiwi
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3275, 6898, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3275, 287, 63, 25, 25, 25, 25, 25, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3275, 2475, 1, 1, 1, 1, 1, 1, 1, 100);


-- Monster 3281 : Wind Bwak

-- Monster 3282 : Ice Bwak

-- Monster 3283 : Fire Bwak

-- Monster 3284 : Earth Bwak

-- Monster 3286 : Foggernaut Dopple

-- Monster 3287 : Harpooner

-- Monster 3288 : Lifesaver

-- Monster 3289 : Tacturret

-- Monster 3293 : Tired Grozilla
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3293, 12467, 63, 15, 15, 15, 15, 15, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3293, 12468, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 400);


-- Monster 3294 : Exhausted Grozilla

-- Monster 3295 : Sleepwalking Grozilla

-- Monster 3296 : Tired Grasmera

-- Monster 3297 : Exhausted Grasmera

-- Monster 3298 : Sleepwalking Grasmera

-- Monster 3299 : Ugly Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3299, 13716, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3299, 13717, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 3300 : Blastofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3300, 13718, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3300, 13720, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 3301 : Tofubine
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3301, 13722, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3301, 13723, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 3302 : Podgy Tofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3302, 13724, 63, 10, 11, 12, 13, 14, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3302, 13725, 63, 1, 1.1, 1.2, 1.3, 1.4, 1, 400);


-- Monster 3303 : Foggernaut Dopple

-- Monster 3304 : Pignolia
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3304, 479, 63, 1, 1, 1, 1, 1, 1, 400);


-- Monster 3306 : Al Howin
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3306, 13335, 63, 30, 30, 30, 30, 30, 1, 400);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3306, 13452, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3306, 13334, 63, 10, 10, 10, 10, 10, 1, 200);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3306, 13450, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3306, 13446, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3306, 13448, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3306, 13444, 63, 5, 5, 5, 5, 5, 1, 100);


-- Monster 3307 : Devhorror
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3307, 13452, 63, 10, 10, 10, 10, 10, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3307, 13446, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3307, 13444, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3307, 13448, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3307, 13450, 63, 5, 5, 5, 5, 5, 1, 100);


-- Monster 3308 : Arachkin
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3308, 13444, 63, 6, 6, 6, 6, 6, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3308, 13450, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3308, 13448, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3308, 13446, 63, 5, 5, 5, 5, 5, 1, 100);


-- Monster 3309 : Toadkin
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3309, 13450, 63, 6, 6, 6, 6, 6, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3309, 13448, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3309, 13444, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3309, 13456, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3309, 13446, 63, 5, 5, 5, 5, 5, 1, 100);


-- Monster 3310 : Worm-O'-Lantern
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3310, 13446, 63, 6, 6, 6, 6, 6, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3310, 13448, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3310, 13450, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3310, 13444, 63, 5, 5, 5, 5, 5, 1, 100);


-- Monster 3311 : Borbkin
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3311, 13448, 63, 6, 6, 6, 6, 6, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3311, 13450, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3311, 13444, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3311, 13454, 63, 5, 5, 5, 5, 5, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3311, 13446, 63, 5, 5, 5, 5, 5, 1, 100);


-- Monster 3313 : Ghostly Young Wild Boar

-- Monster 3314 : Ghostly El Scarador

-- Monster 3315 : Ghostly Croum

-- Monster 3316 : Ghostly Crow

-- Monster 3317 : Fire Kwakere Protector
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3317, 1515, 1, 5, 5, 5, 5, 5, 1, 0);


-- Monster 3318 : Earth Kwakere Protector
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3318, 1516, 1, 5, 5, 5, 5, 5, 1, 0);


-- Monster 3319 : Ice Kwakere Protector
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3319, 1518, 1, 5, 5, 5, 5, 5, 1, 0);


-- Monster 3320 : Wind Kwakere Protector
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3320, 1517, 1, 5, 5, 5, 5, 5, 1, 0);


-- Monster 3328 : Dopplesque Coney

-- Monster 3329 : Dopplesque Living Bag

-- Monster 3330 : Dopplesque Crackler

-- Monster 3331 : Dopplesque Boar

-- Monster 3332 : Dopplesque Gobball

-- Monster 3333 : Dopplesque Tofu

-- Monster 3334 : Dopplesque Bwork Magus

-- Monster 3335 : Dopplesque Red Wyrmling

-- Monster 3336 : Dopplesque Pandawasta

-- Monster 3337 : Dopplesque Madoll

-- Monster 3338 : Dopplesque Block

-- Monster 3339 : Dopplesque Inflatable

-- Monster 3340 : Dopplesque Ultra-Powerful

-- Monster 3341 : Dopplesque Sacrificial Doll

-- Monster 3342 : Dopplesque Xelor's Dial

-- Monster 3343 : Creakrock
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3343, 448, 63, 5.5, 6, 6.5, 7, 7.5, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3343, 450, 63, 5, 5.1, 5.2, 5.3, 5.4, 1, 100);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3343, 546, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3343, 545, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3343, 543, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 300);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3343, 544, 63, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 300);


-- Monster 3344 : Sticky Tree

-- Monster 3345 : Ratworm

-- Monster 3346 : Ratfink

-- Monster 3347 : Packrat

-- Monster 3348 : Rugrat

-- Monster 3349 : Ratter
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3349, 13493, 63, 10, 11, 12, 13, 14, 1, 0);


-- Monster 3350 : Brat

-- Monster 3351 : Riffrat

-- Monster 3352 : Prat

-- Monster 3358 : Lone Green Larva

-- Monster 3359 : Lone Blue Larva

-- Monster 3362 : Kwismas Bomberfu

-- Monster 3366 : Summoned Indigo Blop

-- Monster 3367 : Summoned Coco Blop

-- Monster 3368 : Summoned Morello Cherry Blop

-- Monster 3369 : Summoned Pippin Blop

-- Monster 3371 : Trained Sabredon

-- Monster 3372 : Dolores Kimo

-- Monster 3379 : Harrogant
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3379, 13913, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3379, 13914, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 0);


-- Monster 3380 : Dumple
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3380, 13915, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3380, 13916, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3381 : Putchup
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3381, 13918, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3381, 13917, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3382 : Asteraw
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3382, 13919, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3382, 13920, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3383 : Leatherball
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3383, 13921, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3383, 13922, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3384 : Klime
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3384, 13923, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3384, 13924, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 0);


-- Monster 3385 : Potbellion
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3385, 13925, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3385, 13926, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3386 : Stalak
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3386, 13927, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3386, 13928, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3387 : Karkanik
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3387, 13929, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3387, 13930, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3389 : Skateman
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3389, 13931, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3389, 13932, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3390 : Revish
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3390, 13933, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3390, 13934, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3391 : Missiz Freezz
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3391, 13935, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3391, 13936, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 0);


-- Monster 3392 : Nessil
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3392, 13937, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3392, 13938, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3393 : Krackal
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3393, 13939, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3393, 13940, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3394 : Dodox
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3394, 13941, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3394, 13942, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3395 : Thermite
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3395, 13943, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3395, 13944, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3396 : Snowdew
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3396, 13945, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3396, 13946, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3397 : Nileza
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3397, 13947, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3397, 13948, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 0);


-- Monster 3398 : Unstable Gobshell

-- Monster 3399 : Egg of Death

-- Monster 3400 : Doctor Gobotnegg

-- Monster 3401 : Mekstagob

-- Monster 3402 : Erotegg

-- Monster 3403 : Ice Knight

-- Monster 3404 : Kan-O-Mat
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3404, 13949, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3404, 13950, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3405 : Tinkerbear
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3405, 13971, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3405, 13972, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3406 : Pingwinch
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3406, 13973, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3406, 13974, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3407 : Mecanofoux
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3407, 13975, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3407, 13976, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3408 : Serpulax
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3408, 13977, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3408, 13978, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3409 : Sylargh
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3409, 13982, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3409, 13983, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 0);


-- Monster 3410 : Bubotron
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3410, 13984, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3410, 13985, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3411 : Treadfast
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3411, 13986, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3411, 13987, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3412 : Cycloid
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3412, 13988, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3412, 13989, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3413 : Psychopump

-- Monster 3414 : Sinistrofu
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3414, 13990, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3414, 13991, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3415 : Nocturnowl
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3415, 13992, 63, 11, 12, 13, 14, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3415, 13993, 63, 1.1, 1.2, 1.3, 1.4, 1.5, 1, 0);


-- Monster 3416 : Count Harebourg
INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3416, 13994, 63, 15, 15, 15, 15, 15, 1, 0);

INSERT INTO `monsters_drops` (MonsterOwnerId, ItemId, DropLimit, DropRateForGrade1, DropRateForGrade2, DropRateForGrade3, DropRateForGrade4, DropRateForGrade5, RollsCounter, ProspectingLock) VALUES (3416, 13995, 63, 1.5, 1.5, 1.5, 1.5, 1.5, 1, 0);


-- Monster 3417 : Traumatist

-- Monster 3420 : Freezing Traumatist

-- Monster 3425 : Kan-O-Mat Kidnapper

-- Monster 3426 : Mecanofoux Kidnapper

-- Monster 3427 : Serpulax Kidnapper


DELETE FROM monsters_drops WHERE MonsterOwnerId NOT IN (SELECT Id FROM monsters_templates);
DELETE FROM monsters_drops WHERE ItemId NOT IN (SELECT Id FROM items_templates);