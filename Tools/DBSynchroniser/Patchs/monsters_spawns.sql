

DELETE FROM monsters_spawns;
ALTER TABLE `monsters_spawns` AUTO_INCREMENT=1;

-- Monster 31 : Blue Larva in Madrestam Harbour
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (1, 31, 1, 1, 5);
-- Monster 63 : Crab in Madrestam Harbour
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (1, 63, 1, 1, 5);
-- Monster 2327 : Krabaoly the Patient in Madrestam Harbour
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (1, 2327, 0.1, 1, 5);
-- Monster 2574 : Larvalaska the Cold in Madrestam Harbour
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (1, 2574, 0.1, 1, 5);
-- Monster 81 : Wind Kwak in Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (2, 81, 1, 1, 5);
-- Monster 83 : Fire Kwak in Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (2, 83, 1, 1, 5);
-- Monster 84 : Ice Kwak in Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (2, 84, 1, 1, 5);
-- Monster 102 : Boowolf in Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (2, 102, 1, 1, 5);
-- Monster 106 : Crackler in Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (2, 106, 1, 1, 5);
-- Monster 123 : Piglet in Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (2, 123, 1, 1, 5);
-- Monster 235 : Earth Kwak in Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (2, 235, 1, 1, 5);
-- Monster 483 : Crackrock in Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (2, 483, 1, 1, 5);
-- Monster 2326 : Crackrockisree the Tiger in Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (2, 2326, 0.1, 1, 5);
-- Monster 2436 : Jiminicrackler the Conscious in Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (2, 2436, 0.1, 1, 5);
-- Monster 31 : Blue Larva in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 31, 1, 1, 5);
-- Monster 34 : Green Larva in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 34, 1, 1, 5);
-- Monster 46 : Orange Larva in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 46, 1, 1, 5);
-- Monster 48 : Wild Sunflower in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 48, 1, 1, 5);
-- Monster 52 : Arachnee in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 52, 1, 1, 5);
-- Monster 59 : Mush Mush in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 59, 1, 1, 5);
-- Monster 61 : Moskito in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 61, 1, 1, 5);
-- Monster 78 : Demonic Rose in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 78, 1, 1, 5);
-- Monster 79 : Evil Dandelion in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 79, 1, 1, 5);
-- Monster 98 : Tofu in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 98, 1, 1, 5);
-- Monster 157 : Arachnid in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 157, 1, 1, 5);
-- Monster 467 : Dark Rose in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 467, 1, 1, 5);
-- Monster 2277 : Zorrose the Messican in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 2277, 0.1, 1, 5);
-- Monster 2323 : Matmushmush the Flasher in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 2323, 0.1, 1, 5);
-- Monster 2331 : Larvalencia the Orange in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 2331, 0.1, 1, 5);
-- Monster 2332 : Larvadelaide the Ozie in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 2332, 0.1, 1, 5);
-- Monster 2336 : Moskoitus the Interruptor in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 2336, 0.1, 1, 5);
-- Monster 2348 : Dandel the Boy in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 2348, 0.1, 1, 5);
-- Monster 2352 : Roseanne the Yanker in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 2352, 0.1, 1, 5);
-- Monster 2355 : Tofudd the Hunter in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 2355, 0.1, 1, 5);
-- Monster 2358 : Hunflower the Sinful in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 2358, 0.1, 1, 5);
-- Monster 2574 : Larvalaska the Cold in The Ingalsses' Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (3, 2574, 0.1, 1, 5);
-- Monster 31 : Blue Larva in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 31, 1, 1, 5);
-- Monster 34 : Green Larva in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 34, 1, 1, 5);
-- Monster 46 : Orange Larva in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 46, 1, 1, 5);
-- Monster 47 : Treechnid in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 47, 1, 1, 5);
-- Monster 52 : Arachnee in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 52, 1, 1, 5);
-- Monster 61 : Moskito in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 61, 1, 1, 5);
-- Monster 101 : Gobball in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 101, 1, 1, 5);
-- Monster 103 : Prespic in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 103, 1, 1, 5);
-- Monster 104 : Boar in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 104, 1, 1, 5);
-- Monster 134 : White Gobbly in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 134, 1, 1, 5);
-- Monster 149 : Black Gobbly in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 149, 1, 1, 5);
-- Monster 159 : Miliboowolf in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 159, 1, 1, 5);
-- Monster 255 : Aggressive Arachnee in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 255, 1, 1, 5);
-- Monster 2309 : Treekniddioo the Needy in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 2309, 0.1, 1, 5);
-- Monster 2310 : Arakula the Carpature in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 2310, 0.1, 1, 5);
-- Monster 2315 : Gobbach the Contrapuntaler in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 2315, 0.1, 1, 5);
-- Monster 2316 : Gobballyhoo the Noisy in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 2316, 0.1, 1, 5);
-- Monster 2317 : Gobballad the Romantic in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 2317, 0.1, 1, 5);
-- Monster 2331 : Larvalencia the Orange in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 2331, 0.1, 1, 5);
-- Monster 2332 : Larvadelaide the Ozie in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 2332, 0.1, 1, 5);
-- Monster 2333 : Milivanilli the Mime in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 2333, 0.1, 1, 5);
-- Monster 2336 : Moskoitus the Interruptor in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 2336, 0.1, 1, 5);
-- Monster 2349 : Prestreet the Fighter in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 2349, 0.1, 1, 5);
-- Monster 2353 : Boarnigen the Damasker in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 2353, 0.1, 1, 5);
-- Monster 2393 : Arachnekros the Aggressive in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 2393, 0.1, 1, 5);
-- Monster 2574 : Larvalaska the Cold in The Amakna Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (4, 2574, 0.1, 1, 5);
-- Monster 59 : Mush Mush in Gobball Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (5, 59, 1, 1, 5);
-- Monster 78 : Demonic Rose in Gobball Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (5, 78, 1, 1, 5);
-- Monster 101 : Gobball in Gobball Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (5, 101, 1, 1, 5);
-- Monster 134 : White Gobbly in Gobball Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (5, 134, 1, 1, 5);
-- Monster 148 : Gobball War Chief in Gobball Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (5, 148, 1, 1, 5);
-- Monster 149 : Black Gobbly in Gobball Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (5, 149, 1, 1, 5);
-- Monster 2315 : Gobbach the Contrapuntaler in Gobball Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (5, 2315, 0.1, 1, 5);
-- Monster 2316 : Gobballyhoo the Noisy in Gobball Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (5, 2316, 0.1, 1, 5);
-- Monster 2317 : Gobballad the Romantic in Gobball Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (5, 2317, 0.1, 1, 5);
-- Monster 2323 : Matmushmush the Flasher in Gobball Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (5, 2323, 0.1, 1, 5);
-- Monster 2324 : Gobbalky the Stubborn in Gobball Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (5, 2324, 0.1, 1, 5);
-- Monster 2352 : Roseanne the Yanker in Gobball Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (5, 2352, 0.1, 1, 5);
-- Monster 54 : Chafer in The Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (6, 54, 1, 1, 5);
-- Monster 108 : Rib in The Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (6, 108, 1, 1, 5);
-- Monster 110 : Invisible Chafer in The Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (6, 110, 1, 1, 5);
-- Monster 111 : Evil Tofu in The Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (6, 111, 1, 1, 5);
-- Monster 154 : Kwoan in The Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (6, 154, 1, 1, 5);
-- Monster 396 : Chafer Foot Soldier in The Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (6, 396, 1, 1, 5);
-- Monster 2276 : Rib the Torn in The Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (6, 2276, 0.1, 1, 5);
-- Monster 2319 : Chaferanho the Essential in The Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (6, 2319, 0.1, 1, 5);
-- Monster 2321 : Chafaldrag the Charming in The Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (6, 2321, 0.1, 1, 5);
-- Monster 2357 : Tofulsom the Jailer in The Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (6, 2357, 0.1, 1, 5);
-- Monster 2421 : Chafred the Fish in The Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (6, 2421, 0.1, 1, 5);
-- Monster 2522 : Kwoanium the Smart in The Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (6, 2522, 0.1, 1, 5);
-- Monster 3239 : Draugur Chafer in The Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (6, 3239, 1, 1, 5);
-- Monster 54 : Chafer in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 54, 1, 1, 5);
-- Monster 108 : Rib in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 108, 1, 1, 5);
-- Monster 111 : Evil Tofu in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 111, 1, 1, 5);
-- Monster 124 : Vampyre Master in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 124, 1, 1, 5);
-- Monster 126 : Grey Mouse in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 126, 1, 1, 5);
-- Monster 127 : Vampyre in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 127, 1, 1, 5);
-- Monster 154 : Kwoan in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 154, 1, 1, 5);
-- Monster 290 : Chafer Lancer in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 290, 1, 1, 5);
-- Monster 291 : Chafer Archer in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 291, 1, 1, 5);
-- Monster 292 : Elite Chafer in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 292, 1, 1, 5);
-- Monster 396 : Chafer Foot Soldier in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 396, 1, 1, 5);
-- Monster 2302 : Vamp the Impalest in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 2302, 0.1, 1, 5);
-- Monster 2354 : Famouse the Little-Known in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 2354, 0.1, 1, 5);
-- Monster 2357 : Tofulsom the Jailer in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 2357, 0.1, 1, 5);
-- Monster 2532 : Lord Lacedhat the Vampyric in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 2532, 0.1, 1, 5);
-- Monster 2772 : Jorbak in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 2772, 1, 1, 5);
-- Monster 2786 : Bloodthirsty Vampyre in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 2786, 1, 1, 5);
-- Monster 2788 : Extremely Evil Tofu in The Crypts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (7, 2788, 1, 1, 5);
-- Monster 53 : Bwork Magus in The Bwork Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (8, 53, 1, 1, 5);
-- Monster 62 : Karne Rider in The Bwork Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (8, 62, 1, 1, 5);
-- Monster 74 : Bwork Archer in The Bwork Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (8, 74, 1, 1, 5);
-- Monster 178 : Goblin in The Bwork Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (8, 178, 1, 1, 5);
-- Monster 233 : Trool in The Bwork Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (8, 233, 1, 1, 5);
-- Monster 236 : Purple Piwi in The Bwork Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (8, 236, 1, 1, 5);
-- Monster 492 : Pink Piwi in The Bwork Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (8, 492, 1, 1, 5);
-- Monster 876 : Bwork in The Bwork Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (8, 876, 1, 1, 5);
-- Monster 2416 : Bworak the Bohemian in The Bwork Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (8, 2416, 0.1, 1, 5);
-- Monster 2417 : Blorko the Colourful in The Bwork Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (8, 2417, 0.1, 1, 5);
-- Monster 2418 : Bworkoder the Mazter in The Bwork Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (8, 2418, 0.1, 1, 5);
-- Monster 2430 : Karnyona the Rider in The Bwork Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (8, 2430, 0.1, 1, 5);
-- Monster 2494 : Goblimp the Bis Kit in The Bwork Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (8, 2494, 0.1, 1, 5);
-- Monster 52 : Arachnee in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 52, 1, 1, 5);
-- Monster 53 : Bwork Magus in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 53, 1, 1, 5);
-- Monster 54 : Chafer in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 54, 1, 1, 5);
-- Monster 74 : Bwork Archer in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 74, 1, 1, 5);
-- Monster 107 : Dark Vlad in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 107, 1, 1, 5);
-- Monster 108 : Rib in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 108, 1, 1, 5);
-- Monster 110 : Invisible Chafer in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 110, 1, 1, 5);
-- Monster 111 : Evil Tofu in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 111, 1, 1, 5);
-- Monster 150 : Neye in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 150, 1, 1, 5);
-- Monster 154 : Kwoan in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 154, 1, 1, 5);
-- Monster 207 : Pumpkwin in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 207, 1, 1, 5);
-- Monster 249 : Oni in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 249, 1, 1, 5);
-- Monster 259 : Major Arachnee in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 259, 1, 1, 5);
-- Monster 291 : Chafer Archer in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 291, 1, 1, 5);
-- Monster 876 : Bwork in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 876, 1, 1, 5);
-- Monster 2276 : Rib the Torn in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 2276, 0.1, 1, 5);
-- Monster 2310 : Arakula the Carpature in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 2310, 0.1, 1, 5);
-- Monster 2311 : Arachma the Greek in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 2311, 0.1, 1, 5);
-- Monster 2319 : Chaferanho the Essential in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 2319, 0.1, 1, 5);
-- Monster 2320 : Chaferotix the Sixtininth in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 2320, 0.1, 1, 5);
-- Monster 2321 : Chafaldrag the Charming in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 2321, 0.1, 1, 5);
-- Monster 2357 : Tofulsom the Jailer in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 2357, 0.1, 1, 5);
-- Monster 2416 : Bworak the Bohemian in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 2416, 0.1, 1, 5);
-- Monster 2417 : Blorko the Colourful in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 2417, 0.1, 1, 5);
-- Monster 2418 : Bworkoder the Mazter in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 2418, 0.1, 1, 5);
-- Monster 2431 : Jackellington the Lantewn in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 2431, 0.1, 1, 5);
-- Monster 2522 : Kwoanium the Smart in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 2522, 0.1, 1, 5);
-- Monster 2542 : Eyemi the Narcissist in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 2542, 0.1, 1, 5);
-- Monster 2543 : Oni'orses the Foolish in The Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (9, 2543, 0.1, 1, 5);
-- Monster 101 : Gobball in The Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (10, 101, 1, 1, 5);
-- Monster 236 : Purple Piwi in The Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (10, 236, 1, 1, 5);
-- Monster 489 : Red Piwi in The Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (10, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in The Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (10, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in The Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (10, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in The Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (10, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in The Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (10, 493, 1, 1, 5);
-- Monster 494 : Poutch Ingball in The Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (10, 494, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in The Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (10, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in The Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (10, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in The Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (10, 2343, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in The Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (10, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in The Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (10, 2347, 0.1, 1, 5);
-- Monster 2827 : YeCh'Ti in The Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (10, 2827, 1, 1, 5);
-- Monster 603 : Farle's Pig in Porco Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (11, 603, 1, 1, 5);
-- Monster 2432 : Pighatchoo the Electrical in Porco Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (11, 2432, 0.1, 1, 5);
-- Monster 3304 : Pignolia in Porco Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (11, 3304, 1, 1, 5);
-- Monster 55 : Blue Jelly in The Jelly Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (12, 55, 1, 1, 5);
-- Monster 56 : Mint Jelly in The Jelly Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (12, 56, 1, 1, 5);
-- Monster 57 : Strawberry Jelly in The Jelly Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (12, 57, 1, 1, 5);
-- Monster 2371 : Jelleno the Chinny in The Jelly Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (12, 2371, 0.1, 1, 5);
-- Monster 2376 : Jellyposukshion the Slim in The Jelly Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (12, 2376, 0.1, 1, 5);
-- Monster 2487 : Jellvis the King in The Jelly Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (12, 2487, 0.1, 1, 5);
-- Monster 31 : Blue Larva in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 31, 1, 1, 5);
-- Monster 34 : Green Larva in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 34, 1, 1, 5);
-- Monster 46 : Orange Larva in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 46, 1, 1, 5);
-- Monster 47 : Treechnid in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 47, 1, 1, 5);
-- Monster 61 : Moskito in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 61, 1, 1, 5);
-- Monster 103 : Prespic in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 103, 1, 1, 5);
-- Monster 104 : Boar in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 104, 1, 1, 5);
-- Monster 255 : Aggressive Arachnee in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 255, 1, 1, 5);
-- Monster 2309 : Treekniddioo the Needy in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 2309, 0.1, 1, 5);
-- Monster 2331 : Larvalencia the Orange in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 2331, 0.1, 1, 5);
-- Monster 2332 : Larvadelaide the Ozie in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 2332, 0.1, 1, 5);
-- Monster 2336 : Moskoitus the Interruptor in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 2336, 0.1, 1, 5);
-- Monster 2349 : Prestreet the Fighter in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 2349, 0.1, 1, 5);
-- Monster 2353 : Boarnigen the Damasker in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 2353, 0.1, 1, 5);
-- Monster 2393 : Arachnekros the Aggressive in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 2393, 0.1, 1, 5);
-- Monster 2574 : Larvalaska the Cold in Edge of the Evil Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (22, 2574, 0.1, 1, 5);
-- Monster 75 : Immature Golden Dreggon in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 75, 1, 1, 5);
-- Monster 76 : White Dreggon in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 76, 1, 1, 5);
-- Monster 82 : Immature Black Dreggon in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 82, 1, 1, 5);
-- Monster 87 : Golden Dreggon in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 87, 1, 1, 5);
-- Monster 88 : Sapphire Dreggon in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 88, 1, 1, 5);
-- Monster 89 : Black Dreggon in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 89, 1, 1, 5);
-- Monster 90 : Immature White Dreggon in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 90, 1, 1, 5);
-- Monster 91 : Alert Black Dreggon in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 91, 1, 1, 5);
-- Monster 93 : Alert White Dreggon in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 93, 1, 1, 5);
-- Monster 94 : Alert Golden Dreggon in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 94, 1, 1, 5);
-- Monster 95 : Alert Sapphire Dreggon in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 95, 1, 1, 5);
-- Monster 170 : Immature Sapphire Dreggon in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 170, 1, 1, 5);
-- Monster 2367 : Drakween the Cross Dresser in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 2367, 0.1, 1, 5);
-- Monster 2368 : Dreggershween the Tinpanalley in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 2368, 0.1, 1, 5);
-- Monster 2369 : Dreggonzola the Cheesy in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 2369, 0.1, 1, 5);
-- Monster 2372 : Dreggatón the Latino in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 2372, 0.1, 1, 5);
-- Monster 2377 : Dragamemnon the Deadtroyer in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 2377, 0.1, 1, 5);
-- Monster 2445 : Dreggoog the Downunder in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 2445, 0.1, 1, 5);
-- Monster 2446 : Dreggooniz the Adventurous in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 2446, 0.1, 1, 5);
-- Monster 2447 : Dreggrieg the Pianist in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 2447, 0.1, 1, 5);
-- Monster 2448 : Dreggooliz the Macho in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 2448, 0.1, 1, 5);
-- Monster 2449 : Dreggommomm the Chewer in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 2449, 0.1, 1, 5);
-- Monster 2450 : Dreggump the Shrimp in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 2450, 0.1, 1, 5);
-- Monster 2451 : Dragoolash the Stewed in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 2451, 0.1, 1, 5);
-- Monster 2795 : Sauroshell in The Dreggon Peninsula
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (23, 2795, 1, 1, 5);
-- Monster 182 : GM Wabbit in Wabbit Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (25, 182, 1, 1, 5);
-- Monster 63 : Crab in Asse Coast
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (27, 63, 1, 1, 5);
-- Monster 954 : Mumussel in Asse Coast
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (27, 954, 1, 1, 5);
-- Monster 2327 : Krabaoly the Patient in Asse Coast
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (27, 2327, 0.1, 1, 5);
-- Monster 31 : Blue Larva in Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (29, 31, 1, 1, 5);
-- Monster 34 : Green Larva in Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (29, 34, 1, 1, 5);
-- Monster 46 : Orange Larva in Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (29, 46, 1, 1, 5);
-- Monster 52 : Arachnee in Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (29, 52, 1, 1, 5);
-- Monster 62 : Karne Rider in Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (29, 62, 1, 1, 5);
-- Monster 102 : Boowolf in Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (29, 102, 1, 1, 5);
-- Monster 106 : Crackler in Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (29, 106, 1, 1, 5);
-- Monster 118 : Dark Miner in Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (29, 118, 1, 1, 5);
-- Monster 153 : Dark Baker in Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (29, 153, 1, 1, 5);
-- Monster 483 : Crackrock in Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (29, 483, 1, 1, 5);
-- Monster 48 : Wild Sunflower in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 48, 1, 1, 5);
-- Monster 52 : Arachnee in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 52, 1, 1, 5);
-- Monster 59 : Mush Mush in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 59, 1, 1, 5);
-- Monster 61 : Moskito in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 61, 1, 1, 5);
-- Monster 78 : Demonic Rose in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 78, 1, 1, 5);
-- Monster 79 : Evil Dandelion in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 79, 1, 1, 5);
-- Monster 98 : Tofu in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 98, 1, 1, 5);
-- Monster 101 : Gobball in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 101, 1, 1, 5);
-- Monster 134 : White Gobbly in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 134, 1, 1, 5);
-- Monster 148 : Gobball War Chief in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 148, 1, 1, 5);
-- Monster 149 : Black Gobbly in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 149, 1, 1, 5);
-- Monster 2310 : Arakula the Carpature in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 2310, 0.1, 1, 5);
-- Monster 2315 : Gobbach the Contrapuntaler in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 2315, 0.1, 1, 5);
-- Monster 2316 : Gobballyhoo the Noisy in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 2316, 0.1, 1, 5);
-- Monster 2317 : Gobballad the Romantic in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 2317, 0.1, 1, 5);
-- Monster 2323 : Matmushmush the Flasher in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 2323, 0.1, 1, 5);
-- Monster 2324 : Gobbalky the Stubborn in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 2324, 0.1, 1, 5);
-- Monster 2336 : Moskoitus the Interruptor in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 2336, 0.1, 1, 5);
-- Monster 2348 : Dandel the Boy in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 2348, 0.1, 1, 5);
-- Monster 2352 : Roseanne the Yanker in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 2352, 0.1, 1, 5);
-- Monster 2355 : Tofudd the Hunter in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 2355, 0.1, 1, 5);
-- Monster 2358 : Hunflower the Sinful in The Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (30, 2358, 0.1, 1, 5);
-- Monster 52 : Arachnee in The Swamp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (31, 52, 1, 1, 5);
-- Monster 112 : Mushd in The Swamp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (31, 112, 1, 1, 5);
-- Monster 259 : Major Arachnee in The Swamp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (31, 259, 1, 1, 5);
-- Monster 261 : Crocodyl in The Swamp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (31, 261, 1, 1, 5);
-- Monster 263 : Crocodyl Chief in The Swamp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (31, 263, 1, 1, 5);
-- Monster 2311 : Arachma the Greek in The Swamp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (31, 2311, 0.1, 1, 5);
-- Monster 2428 : Crokdylann the Rebel in The Swamp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (31, 2428, 0.1, 1, 5);
-- Monster 2439 : Croccyx the Bummer in The Swamp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (31, 2439, 0.1, 1, 5);
-- Monster 2549 : Mushdrill the Piercer in The Swamp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (31, 2549, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Sufokia
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (32, 236, 1, 1, 5);
-- Monster 490 : Green Piwi in Sufokia
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (32, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Sufokia
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (32, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Sufokia
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (32, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Sufokia
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (32, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Sufokia
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (32, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Sufokia
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (32, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Sufokia
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (32, 2343, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Sufokia
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (32, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Sufokia
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (32, 2347, 0.1, 1, 5);
-- Monster 3244 : Scampihorse in Sufokia
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (32, 3244, 1, 1, 5);
-- Monster 47 : Treechnid in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 47, 1, 1, 5);
-- Monster 102 : Boowolf in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 102, 1, 1, 5);
-- Monster 159 : Miliboowolf in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 159, 1, 1, 5);
-- Monster 253 : Dark Treechnid in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 253, 1, 1, 5);
-- Monster 254 : Venerable Treechnid in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 254, 1, 1, 5);
-- Monster 256 : Trunknid in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 256, 1, 1, 5);
-- Monster 259 : Major Arachnee in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 259, 1, 1, 5);
-- Monster 650 : Treechnee in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 650, 1, 1, 5);
-- Monster 651 : Dark Treechnee in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 651, 1, 1, 5);
-- Monster 652 : Green Spimush in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 652, 1, 1, 5);
-- Monster 653 : Red Spimush in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 653, 1, 1, 5);
-- Monster 654 : Blue Spimush in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 654, 1, 1, 5);
-- Monster 655 : Brown Spimush in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 655, 1, 1, 5);
-- Monster 2299 : Trunkbeard the Gentle in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 2299, 0.1, 1, 5);
-- Monster 2309 : Treekniddioo the Needy in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 2309, 0.1, 1, 5);
-- Monster 2311 : Arachma the Greek in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 2311, 0.1, 1, 5);
-- Monster 2333 : Milivanilli the Mime in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 2333, 0.1, 1, 5);
-- Monster 2385 : Treeknidylus in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 2385, 0.1, 1, 5);
-- Monster 2424 : Spimushty the Smelly in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 2424, 0.1, 1, 5);
-- Monster 2425 : Speedmush the Racer in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 2425, 0.1, 1, 5);
-- Monster 2426 : Spimushuaia the Traveller in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 2426, 0.1, 1, 5);
-- Monster 2427 : Spimushtache the Hairy in Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (33, 2427, 0.1, 1, 5);
-- Monster 54 : Chafer in Prison
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (34, 54, 1, 1, 5);
-- Monster 273 : Coco Blop in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 273, 1, 1, 5);
-- Monster 274 : Indigo Blop in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 274, 1, 1, 5);
-- Monster 275 : Morello Cherry Blop in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 275, 1, 1, 5);
-- Monster 276 : Pippin Blop in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 276, 1, 1, 5);
-- Monster 277 : Indigo Biblop in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 277, 1, 1, 5);
-- Monster 278 : Coco Biblop in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 278, 1, 1, 5);
-- Monster 279 : Morello Cherry Biblop in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 279, 1, 1, 5);
-- Monster 280 : Pippin Biblop in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 280, 1, 1, 5);
-- Monster 287 : Kaniger in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 287, 1, 1, 5);
-- Monster 288 : Plissken in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 288, 1, 1, 5);
-- Monster 2286 : Serpico the Honest in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 2286, 0.1, 1, 5);
-- Monster 2396 : Billbiblop the Great in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 2396, 0.1, 1, 5);
-- Monster 2399 : Biblopopo the Organiser in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 2399, 0.1, 1, 5);
-- Monster 2400 : Biblokajin the Bald in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 2400, 0.1, 1, 5);
-- Monster 2401 : Bibloponey the Entertainer in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 2401, 0.1, 1, 5);
-- Monster 2404 : Blopulent the Pretentious in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 2404, 0.1, 1, 5);
-- Monster 2405 : Blorchid the Gorgeous in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 2405, 0.1, 1, 5);
-- Monster 2406 : Blopium the Delirious in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 2406, 0.1, 1, 5);
-- Monster 2407 : Blopal the Precious in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 2407, 0.1, 1, 5);
-- Monster 2503 : Kaniedoss the Giggling in Cania Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (38, 2503, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Bonta City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (43, 236, 1, 1, 5);
-- Monster 296 : Militiaman in Bonta City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (43, 296, 1, 1, 5);
-- Monster 489 : Red Piwi in Bonta City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (43, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Bonta City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (43, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Bonta City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (43, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Bonta City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (43, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Bonta City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (43, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Bonta City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (43, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Bonta City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (43, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Bonta City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (43, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Bonta City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (43, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Bonta City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (43, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Bonta City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (43, 2347, 0.1, 1, 5);
-- Monster 98 : Tofu in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (44, 98, 1, 1, 5);
-- Monster 236 : Purple Piwi in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (44, 236, 1, 1, 5);
-- Monster 296 : Militiaman in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (44, 296, 1, 1, 5);
-- Monster 489 : Red Piwi in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (44, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (44, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (44, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (44, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (44, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (44, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (44, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (44, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (44, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (44, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (44, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (46, 236, 1, 1, 5);
-- Monster 296 : Militiaman in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (46, 296, 1, 1, 5);
-- Monster 489 : Red Piwi in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (46, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (46, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (46, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (46, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (46, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (46, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (46, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (46, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (46, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (46, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (46, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (47, 236, 1, 1, 5);
-- Monster 296 : Militiaman in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (47, 296, 1, 1, 5);
-- Monster 489 : Red Piwi in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (47, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (47, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (47, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (47, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (47, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (47, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (47, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (47, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (47, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (47, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (47, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (48, 236, 1, 1, 5);
-- Monster 296 : Militiaman in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (48, 296, 1, 1, 5);
-- Monster 489 : Red Piwi in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (48, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (48, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (48, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (48, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (48, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (48, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (48, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (48, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (48, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (48, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (48, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (49, 236, 1, 1, 5);
-- Monster 296 : Militiaman in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (49, 296, 1, 1, 5);
-- Monster 489 : Red Piwi in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (49, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (49, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (49, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (49, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (49, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (49, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (49, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (49, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (49, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (49, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (49, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (50, 236, 1, 1, 5);
-- Monster 296 : Militiaman in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (50, 296, 1, 1, 5);
-- Monster 489 : Red Piwi in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (50, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (50, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (50, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (50, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (50, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (50, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (50, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (50, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (50, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (50, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (50, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (51, 236, 1, 1, 5);
-- Monster 296 : Militiaman in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (51, 296, 1, 1, 5);
-- Monster 365 : Lost Crow in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (51, 365, 1, 1, 5);
-- Monster 489 : Red Piwi in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (51, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (51, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (51, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (51, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (51, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (51, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (51, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (51, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (51, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (51, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (51, 2347, 0.1, 1, 5);
-- Monster 273 : Coco Blop in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 273, 1, 1, 5);
-- Monster 274 : Indigo Blop in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 274, 1, 1, 5);
-- Monster 275 : Morello Cherry Blop in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 275, 1, 1, 5);
-- Monster 276 : Pippin Blop in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 276, 1, 1, 5);
-- Monster 277 : Indigo Biblop in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 277, 1, 1, 5);
-- Monster 278 : Coco Biblop in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 278, 1, 1, 5);
-- Monster 279 : Morello Cherry Biblop in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 279, 1, 1, 5);
-- Monster 280 : Pippin Biblop in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 280, 1, 1, 5);
-- Monster 281 : Crow in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 281, 1, 1, 5);
-- Monster 287 : Kaniger in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 287, 1, 1, 5);
-- Monster 288 : Plissken in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 288, 1, 1, 5);
-- Monster 293 : Plain Crackler in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 293, 1, 1, 5);
-- Monster 2396 : Billbiblop the Great in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 2396, 0.1, 1, 5);
-- Monster 2399 : Biblopopo the Organiser in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 2399, 0.1, 1, 5);
-- Monster 2400 : Biblokajin the Bald in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 2400, 0.1, 1, 5);
-- Monster 2401 : Bibloponey the Entertainer in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 2401, 0.1, 1, 5);
-- Monster 2404 : Blopulent the Pretentious in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 2404, 0.1, 1, 5);
-- Monster 2405 : Blorchid the Gorgeous in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 2405, 0.1, 1, 5);
-- Monster 2406 : Blopium the Delirious in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 2406, 0.1, 1, 5);
-- Monster 2407 : Blopal the Precious in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 2407, 0.1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 2437, 0.1, 1, 5);
-- Monster 2438 : Cracklerod the Old in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 2438, 0.1, 1, 5);
-- Monster 2882 : Mega Plain Crackler in Cania Massif
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (54, 2882, 1, 1, 5);
-- Monster 281 : Crow in The Crow's Domain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (55, 281, 1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in The Crow's Domain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (55, 2437, 0.1, 1, 5);
-- Monster 48 : Wild Sunflower in Cania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (56, 48, 1, 1, 5);
-- Monster 78 : Demonic Rose in Cania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (56, 78, 1, 1, 5);
-- Monster 79 : Evil Dandelion in Cania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (56, 79, 1, 1, 5);
-- Monster 297 : Plain Boar in Cania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (56, 297, 1, 1, 5);
-- Monster 2279 : Boarealis the Bright in Cania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (56, 2279, 0.1, 1, 5);
-- Monster 2348 : Dandel the Boy in Cania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (56, 2348, 0.1, 1, 5);
-- Monster 2352 : Roseanne the Yanker in Cania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (56, 2352, 0.1, 1, 5);
-- Monster 2358 : Hunflower the Sinful in Cania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (56, 2358, 0.1, 1, 5);
-- Monster 281 : Crow in Sidimote Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (57, 281, 1, 1, 5);
-- Monster 298 : Scurvion in Sidimote Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (57, 298, 1, 1, 5);
-- Monster 299 : Whitish Fang in Sidimote Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (57, 299, 1, 1, 5);
-- Monster 300 : Kolerat in Sidimote Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (57, 300, 1, 1, 5);
-- Monster 301 : Ouginak in Sidimote Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (57, 301, 1, 1, 5);
-- Monster 2285 : Scorbison the Lonely in Sidimote Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (57, 2285, 0.1, 1, 5);
-- Monster 2378 : Snowhitisha the Pure in Sidimote Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (57, 2378, 0.1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in Sidimote Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (57, 2437, 0.1, 1, 5);
-- Monster 2520 : Koleraspootin the Anesthesialogist in Sidimote Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (57, 2520, 0.1, 1, 5);
-- Monster 2589 : Ougathard the Fortunate in Sidimote Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (57, 2589, 0.1, 1, 5);
-- Monster 54 : Chafer in Bonta Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (59, 54, 1, 1, 5);
-- Monster 108 : Rib in Bonta Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (59, 108, 1, 1, 5);
-- Monster 110 : Invisible Chafer in Bonta Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (59, 110, 1, 1, 5);
-- Monster 290 : Chafer Lancer in Bonta Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (59, 290, 1, 1, 5);
-- Monster 291 : Chafer Archer in Bonta Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (59, 291, 1, 1, 5);
-- Monster 292 : Elite Chafer in Bonta Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (59, 292, 1, 1, 5);
-- Monster 2276 : Rib the Torn in Bonta Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (59, 2276, 0.1, 1, 5);
-- Monster 2319 : Chaferanho the Essential in Bonta Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (59, 2319, 0.1, 1, 5);
-- Monster 2320 : Chaferotix the Sixtininth in Bonta Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (59, 2320, 0.1, 1, 5);
-- Monster 2321 : Chafaldrag the Charming in Bonta Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (59, 2321, 0.1, 1, 5);
-- Monster 2322 : Chafermented the Drinker in Bonta Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (59, 2322, 0.1, 1, 5);
-- Monster 2422 : Chaferuption the Volcanic in Bonta Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (59, 2422, 0.1, 1, 5);
-- Monster 3239 : Draugur Chafer in Bonta Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (59, 3239, 1, 1, 5);
-- Monster 54 : Chafer in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 54, 1, 1, 5);
-- Monster 110 : Invisible Chafer in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 110, 1, 1, 5);
-- Monster 124 : Vampyre Master in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 124, 1, 1, 5);
-- Monster 127 : Vampyre in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 127, 1, 1, 5);
-- Monster 281 : Crow in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 281, 1, 1, 5);
-- Monster 290 : Chafer Lancer in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 290, 1, 1, 5);
-- Monster 291 : Chafer Archer in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 291, 1, 1, 5);
-- Monster 292 : Elite Chafer in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 292, 1, 1, 5);
-- Monster 378 : Burning Ghost in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 378, 1, 1, 5);
-- Monster 379 : Brave Ghost in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 379, 1, 1, 5);
-- Monster 380 : Arepo Ghost in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 380, 1, 1, 5);
-- Monster 932 : Apero Ghost in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 932, 1, 1, 5);
-- Monster 2302 : Vamp the Impalest in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 2302, 0.1, 1, 5);
-- Monster 2319 : Chaferanho the Essential in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 2319, 0.1, 1, 5);
-- Monster 2320 : Chaferotix the Sixtininth in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 2320, 0.1, 1, 5);
-- Monster 2321 : Chafaldrag the Charming in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 2321, 0.1, 1, 5);
-- Monster 2322 : Chafermented the Drinker in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 2322, 0.1, 1, 5);
-- Monster 2422 : Chaferuption the Volcanic in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 2422, 0.1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 2437, 0.1, 1, 5);
-- Monster 2466 : Aperobics the Dynamic in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 2466, 0.1, 1, 5);
-- Monster 2467 : Polterghaisk the Stray Soul in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 2467, 0.1, 1, 5);
-- Monster 2468 : Ghostabrava the Tourist in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 2468, 0.1, 1, 5);
-- Monster 2532 : Lord Lacedhat the Vampyric in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 2532, 0.1, 1, 5);
-- Monster 2563 : Arepotair the Bespectacled in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 2563, 0.1, 1, 5);
-- Monster 3239 : Draugur Chafer in Cemetery of the Tortured
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (61, 3239, 1, 1, 5);
-- Monster 303 : Let Emoliug the Saturnine in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 303, 1, 1, 5);
-- Monster 304 : Let Emoliug the White in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 304, 1, 1, 5);
-- Monster 305 : Susej the Pure in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 305, 1, 1, 5);
-- Monster 306 : Susej the Impure in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 306, 1, 1, 5);
-- Monster 308 : Osurc the Vile in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 308, 1, 1, 5);
-- Monster 310 : Osurc the Kindly in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 310, 1, 1, 5);
-- Monster 337 : Nipul the Saviour in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 337, 1, 1, 5);
-- Monster 345 : Kirevam the Determined in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 345, 1, 1, 5);
-- Monster 346 : Kirevam the Terrifying in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 346, 1, 1, 5);
-- Monster 347 : Amlub the Protector in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 347, 1, 1, 5);
-- Monster 348 : Amlub the Gravedigger in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 348, 1, 1, 5);
-- Monster 349 : Nebgib the Pacifier in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 349, 1, 1, 5);
-- Monster 350 : Nebgib the Tormentor in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 350, 1, 1, 5);
-- Monster 351 : Nipul the Destroyer in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 351, 1, 1, 5);
-- Monster 353 : Aboub the Social in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 353, 1, 1, 5);
-- Monster 355 : Aboub the Antisocial in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 355, 1, 1, 5);
-- Monster 356 : Gink the Swindling in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 356, 1, 1, 5);
-- Monster 357 : Gink the Sincere in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 357, 1, 1, 5);
-- Monster 359 : Codem the Altruist in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 359, 1, 1, 5);
-- Monster 360 : Codem the Exile in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 360, 1, 1, 5);
-- Monster 575 : Yadrutas the Bloodthirsty in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 575, 1, 1, 5);
-- Monster 576 : Yadrutas the Launderer in Dopple Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (62, 576, 1, 1, 5);
-- Monster 363 : Heart of the Dopple Territory in Dopple Prism Hall
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (63, 363, 1, 1, 5);
-- Monster 364 : Heart of the Dopple Territory in Dopple Prism Hall
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (63, 364, 1, 1, 5);
-- Monster 303 : Let Emoliug the Saturnine in First Hall of the Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (64, 303, 1, 1, 5);
-- Monster 304 : Let Emoliug the White in First Hall of the Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (64, 304, 1, 1, 5);
-- Monster 305 : Susej the Pure in First Hall of the Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (64, 305, 1, 1, 5);
-- Monster 306 : Susej the Impure in First Hall of the Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (64, 306, 1, 1, 5);
-- Monster 308 : Osurc the Vile in First Hall of the Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (64, 308, 1, 1, 5);
-- Monster 310 : Osurc the Kindly in First Hall of the Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (64, 310, 1, 1, 5);
-- Monster 345 : Kirevam the Determined in First Hall of the Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (64, 345, 1, 1, 5);
-- Monster 346 : Kirevam the Terrifying in First Hall of the Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (64, 346, 1, 1, 5);
-- Monster 48 : Wild Sunflower in The Cania Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (68, 48, 1, 1, 5);
-- Monster 59 : Mush Mush in The Cania Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (68, 59, 1, 1, 5);
-- Monster 78 : Demonic Rose in The Cania Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (68, 78, 1, 1, 5);
-- Monster 79 : Evil Dandelion in The Cania Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (68, 79, 1, 1, 5);
-- Monster 371 : Fungi Master in The Cania Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (68, 371, 1, 1, 5);
-- Monster 2323 : Matmushmush the Flasher in The Cania Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (68, 2323, 0.1, 1, 5);
-- Monster 2348 : Dandel the Boy in The Cania Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (68, 2348, 0.1, 1, 5);
-- Monster 2352 : Roseanne the Yanker in The Cania Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (68, 2352, 0.1, 1, 5);
-- Monster 2358 : Hunflower the Sinful in The Cania Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (68, 2358, 0.1, 1, 5);
-- Monster 2359 : Fung Ku the Master in The Cania Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (68, 2359, 0.1, 1, 5);
-- Monster 54 : Chafer in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 54, 1, 1, 5);
-- Monster 102 : Boowolf in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 102, 1, 1, 5);
-- Monster 108 : Rib in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 108, 1, 1, 5);
-- Monster 110 : Invisible Chafer in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 110, 1, 1, 5);
-- Monster 159 : Miliboowolf in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 159, 1, 1, 5);
-- Monster 233 : Trool in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 233, 1, 1, 5);
-- Monster 290 : Chafer Lancer in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 290, 1, 1, 5);
-- Monster 291 : Chafer Archer in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 291, 1, 1, 5);
-- Monster 396 : Chafer Foot Soldier in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 396, 1, 1, 5);
-- Monster 2276 : Rib the Torn in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 2276, 0.1, 1, 5);
-- Monster 2300 : Trooligan the Bulldogg in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 2300, 0.1, 1, 5);
-- Monster 2319 : Chaferanho the Essential in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 2319, 0.1, 1, 5);
-- Monster 2320 : Chaferotix the Sixtininth in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 2320, 0.1, 1, 5);
-- Monster 2321 : Chafaldrag the Charming in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 2321, 0.1, 1, 5);
-- Monster 2322 : Chafermented the Drinker in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 2322, 0.1, 1, 5);
-- Monster 2333 : Milivanilli the Mime in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 2333, 0.1, 1, 5);
-- Monster 2337 : Booty the Beast in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 2337, 0.1, 1, 5);
-- Monster 2421 : Chafred the Fish in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 2421, 0.1, 1, 5);
-- Monster 2685 : Chafer Axit in Eltneg Wood
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (69, 2685, 1, 1, 5);
-- Monster 281 : Crow in Rocky Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (70, 281, 1, 1, 5);
-- Monster 293 : Plain Crackler in Rocky Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (70, 293, 1, 1, 5);
-- Monster 343 : Lousy Pig Knight in Rocky Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (70, 343, 1, 1, 5);
-- Monster 344 : Lousy Pig Shepherd in Rocky Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (70, 344, 1, 1, 5);
-- Monster 2397 : Pigstol the Sexy in Rocky Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (70, 2397, 0.1, 1, 5);
-- Monster 2420 : Pygknightlion the Lousy in Rocky Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (70, 2420, 0.1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in Rocky Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (70, 2437, 0.1, 1, 5);
-- Monster 2438 : Cracklerod the Old in Rocky Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (70, 2438, 0.1, 1, 5);
-- Monster 53 : Bwork Magus in Gisgoul, the Devastated Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (71, 53, 1, 1, 5);
-- Monster 62 : Karne Rider in Gisgoul, the Devastated Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (71, 62, 1, 1, 5);
-- Monster 74 : Bwork Archer in Gisgoul, the Devastated Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (71, 74, 1, 1, 5);
-- Monster 178 : Goblin in Gisgoul, the Devastated Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (71, 178, 1, 1, 5);
-- Monster 281 : Crow in Gisgoul, the Devastated Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (71, 281, 1, 1, 5);
-- Monster 876 : Bwork in Gisgoul, the Devastated Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (71, 876, 1, 1, 5);
-- Monster 2416 : Bworak the Bohemian in Gisgoul, the Devastated Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (71, 2416, 0.1, 1, 5);
-- Monster 2417 : Blorko the Colourful in Gisgoul, the Devastated Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (71, 2417, 0.1, 1, 5);
-- Monster 2418 : Bworkoder the Mazter in Gisgoul, the Devastated Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (71, 2418, 0.1, 1, 5);
-- Monster 2430 : Karnyona the Rider in Gisgoul, the Devastated Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (71, 2430, 0.1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in Gisgoul, the Devastated Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (71, 2437, 0.1, 1, 5);
-- Monster 2494 : Goblimp the Bis Kit in Gisgoul, the Devastated Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (71, 2494, 0.1, 1, 5);
-- Monster 259 : Major Arachnee in Bonta Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (73, 259, 1, 1, 5);
-- Monster 442 : Grossewer Rat in Bonta Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (73, 442, 1, 1, 5);
-- Monster 447 : Grossewer Shaman in Bonta Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (73, 447, 1, 1, 5);
-- Monster 449 : Hyoactive Rat in Bonta Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (73, 449, 1, 1, 5);
-- Monster 450 : Sewer Keeper in Bonta Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (73, 450, 1, 1, 5);
-- Monster 2273 : Rattle the Hummer in Bonta Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (73, 2273, 0.1, 1, 5);
-- Monster 2275 : Ratilla the Hun in Bonta Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (73, 2275, 0.1, 1, 5);
-- Monster 2311 : Arachma the Greek in Bonta Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (73, 2311, 0.1, 1, 5);
-- Monster 2423 : Shamassel the Off in Bonta Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (73, 2423, 0.1, 1, 5);
-- Monster 384 : Aboub in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 384, 1, 1, 5);
-- Monster 385 : Amlub in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 385, 1, 1, 5);
-- Monster 386 : Codem in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 386, 1, 1, 5);
-- Monster 387 : Gink in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 387, 1, 1, 5);
-- Monster 388 : Kirevam in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 388, 1, 1, 5);
-- Monster 389 : Let Emoliug in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 389, 1, 1, 5);
-- Monster 390 : Nebgib in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 390, 1, 1, 5);
-- Monster 391 : Nipul in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 391, 1, 1, 5);
-- Monster 392 : Osurc in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 392, 1, 1, 5);
-- Monster 393 : Susej in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 393, 1, 1, 5);
-- Monster 422 : Dark Vlad Dopple in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 422, 1, 1, 5);
-- Monster 570 : Yadrutas in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 570, 1, 1, 5);
-- Monster 2289 : Suzessman the Enthusiastic in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 2289, 0.1, 1, 5);
-- Monster 2384 : Abounteous the Generous in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 2384, 0.1, 1, 5);
-- Monster 2391 : Amlullabeye the Dreamer in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 2391, 0.1, 1, 5);
-- Monster 2433 : Codemonic the Mean in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 2433, 0.1, 1, 5);
-- Monster 2493 : Ginsync the Hyperactive in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 2493, 0.1, 1, 5);
-- Monster 2508 : Kirevampiro the Wrestler in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 2508, 0.1, 1, 5);
-- Monster 2525 : Lert Macraken the Used Emo in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 2525, 0.1, 1, 5);
-- Monster 2539 : Nebuchadnezzar the Conqueror in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 2539, 0.1, 1, 5);
-- Monster 2540 : Niptuk the Plasticynic in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 2540, 0.1, 1, 5);
-- Monster 2585 : Osurcus the Tamer in Dopple Training
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (74, 2585, 0.1, 1, 5);
-- Monster 259 : Major Arachnee in Brakmar Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (75, 259, 1, 1, 5);
-- Monster 442 : Grossewer Rat in Brakmar Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (75, 442, 1, 1, 5);
-- Monster 447 : Grossewer Shaman in Brakmar Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (75, 447, 1, 1, 5);
-- Monster 449 : Hyoactive Rat in Brakmar Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (75, 449, 1, 1, 5);
-- Monster 450 : Sewer Keeper in Brakmar Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (75, 450, 1, 1, 5);
-- Monster 2273 : Rattle the Hummer in Brakmar Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (75, 2273, 0.1, 1, 5);
-- Monster 2275 : Ratilla the Hun in Brakmar Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (75, 2275, 0.1, 1, 5);
-- Monster 2311 : Arachma the Greek in Brakmar Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (75, 2311, 0.1, 1, 5);
-- Monster 2423 : Shamassel the Off in Brakmar Sewers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (75, 2423, 0.1, 1, 5);
-- Monster 55 : Blue Jelly in The Jellith Dimension
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (77, 55, 1, 1, 5);
-- Monster 56 : Mint Jelly in The Jellith Dimension
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (77, 56, 1, 1, 5);
-- Monster 57 : Strawberry Jelly in The Jellith Dimension
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (77, 57, 1, 1, 5);
-- Monster 429 : Lemon Jelly in The Jellith Dimension
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (77, 429, 1, 1, 5);
-- Monster 55 : Blue Jelly in The (Royal) Jellith Dimension
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (78, 55, 1, 1, 5);
-- Monster 56 : Mint Jelly in The (Royal) Jellith Dimension
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (78, 56, 1, 1, 5);
-- Monster 57 : Strawberry Jelly in The (Royal) Jellith Dimension
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (78, 57, 1, 1, 5);
-- Monster 58 : Royal Blue Jelly in The (Royal) Jellith Dimension
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (78, 58, 1, 1, 5);
-- Monster 85 : Royal Mint Jelly in The (Royal) Jellith Dimension
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (78, 85, 1, 1, 5);
-- Monster 86 : Royal Strawberry Jelly in The (Royal) Jellith Dimension
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (78, 86, 1, 1, 5);
-- Monster 429 : Lemon Jelly in The (Royal) Jellith Dimension
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (78, 429, 1, 1, 5);
-- Monster 430 : Royal Lemon Jelly in The (Royal) Jellith Dimension
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (78, 430, 1, 1, 5);
-- Monster 439 : Robot Mace in First Platform
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (79, 439, 1, 1, 5);
-- Monster 440 : Pushy Robot in First Platform
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (79, 440, 1, 1, 5);
-- Monster 441 : Handyman in First Platform
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (79, 441, 1, 1, 5);
-- Monster 443 : Handyman in First Platform
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (79, 443, 1, 1, 5);
-- Monster 444 : Robot Mace in First Platform
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (79, 444, 1, 1, 5);
-- Monster 445 : Pushy Robot in First Platform
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (79, 445, 1, 1, 5);
-- Monster 101 : Gobball in Gobball Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (82, 101, 1, 1, 5);
-- Monster 134 : White Gobbly in Gobball Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (82, 134, 1, 1, 5);
-- Monster 147 : Royal Gobball in Gobball Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (82, 147, 1, 1, 5);
-- Monster 148 : Gobball War Chief in Gobball Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (82, 148, 1, 1, 5);
-- Monster 149 : Black Gobbly in Gobball Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (82, 149, 1, 1, 5);
-- Monster 47 : Treechnid in Trool Fair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (84, 47, 1, 1, 5);
-- Monster 52 : Arachnee in Trool Fair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (84, 52, 1, 1, 5);
-- Monster 55 : Blue Jelly in Trool Fair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (84, 55, 1, 1, 5);
-- Monster 149 : Black Gobbly in Trool Fair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (84, 149, 1, 1, 5);
-- Monster 240 : Green Scaraleaf in Trool Fair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (84, 240, 1, 1, 5);
-- Monster 261 : Crocodyl in Trool Fair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (84, 261, 1, 1, 5);
-- Monster 297 : Plain Boar in Trool Fair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (84, 297, 1, 1, 5);
-- Monster 298 : Scurvion in Trool Fair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (84, 298, 1, 1, 5);
-- Monster 299 : Whitish Fang in Trool Fair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (84, 299, 1, 1, 5);
-- Monster 31 : Blue Larva in Astrub Outskirts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (92, 31, 1, 1, 5);
-- Monster 52 : Arachnee in Astrub Outskirts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (92, 52, 1, 1, 5);
-- Monster 98 : Tofu in Astrub Outskirts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (92, 98, 1, 1, 5);
-- Monster 101 : Gobball in Astrub Outskirts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (92, 101, 1, 1, 5);
-- Monster 134 : White Gobbly in Astrub Outskirts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (92, 134, 1, 1, 5);
-- Monster 149 : Black Gobbly in Astrub Outskirts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (92, 149, 1, 1, 5);
-- Monster 2310 : Arakula the Carpature in Astrub Outskirts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (92, 2310, 0.1, 1, 5);
-- Monster 2315 : Gobbach the Contrapuntaler in Astrub Outskirts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (92, 2315, 0.1, 1, 5);
-- Monster 2316 : Gobballyhoo the Noisy in Astrub Outskirts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (92, 2316, 0.1, 1, 5);
-- Monster 2317 : Gobballad the Romantic in Astrub Outskirts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (92, 2317, 0.1, 1, 5);
-- Monster 2355 : Tofudd the Hunter in Astrub Outskirts
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (92, 2355, 0.1, 1, 5);
-- Monster 208 : Tikokoko in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 208, 1, 1, 5);
-- Monster 209 : Kokoko in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 209, 1, 1, 5);
-- Monster 217 : Ambusher in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 217, 1, 1, 5);
-- Monster 218 : Kokonut in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 218, 1, 1, 5);
-- Monster 220 : Red Turtle in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 220, 1, 1, 5);
-- Monster 221 : Blue Turtle in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 221, 1, 1, 5);
-- Monster 222 : Green Turtle in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 222, 1, 1, 5);
-- Monster 223 : Yellow Turtle in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 223, 1, 1, 5);
-- Monster 2292 : Eskoko the Baron in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 2292, 0.1, 1, 5);
-- Monster 2295 : Turtan'ernie the Streetwise in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 2295, 0.1, 1, 5);
-- Monster 2296 : Turtrenalds the Tragic in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 2296, 0.1, 1, 5);
-- Monster 2297 : Turticorn the Horned in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 2297, 0.1, 1, 5);
-- Monster 2298 : Turture the Hooded in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 2298, 0.1, 1, 5);
-- Monster 2484 : Ambushapens the Unlucky in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 2484, 0.1, 1, 5);
-- Monster 2541 : Kokonan the Talker in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 2541, 0.1, 1, 5);
-- Monster 2573 : Misskokoko the Channel in Moon Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (93, 2573, 0.1, 1, 5);
-- Monster 478 : Bworker in Bworker Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (94, 478, 1, 1, 5);
-- Monster 479 : Mama Bwork in Bworker Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (94, 479, 1, 1, 5);
-- Monster 484 : Elemental Fire Bwork in Bworker Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (94, 484, 1, 1, 5);
-- Monster 485 : Elemental Water Bwork in Bworker Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (94, 485, 1, 1, 5);
-- Monster 486 : Elemental Air Bwork in Bworker Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (94, 486, 1, 1, 5);
-- Monster 487 : Elemental Earth Bwork in Bworker Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (94, 487, 1, 1, 5);
-- Monster 488 : Cybwork in Bworker Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (94, 488, 1, 1, 5);
-- Monster 126 : Grey Mouse in Astrub City
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (95, 126, 1, 1, 5);
-- Monster 236 : Purple Piwi in Astrub City
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (95, 236, 1, 1, 5);
-- Monster 474 : Sick Arachnee in Astrub City
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (95, 474, 1, 1, 5);
-- Monster 489 : Red Piwi in Astrub City
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (95, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Astrub City
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (95, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Astrub City
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (95, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Astrub City
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (95, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Astrub City
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (95, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Astrub City
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (95, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Astrub City
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (95, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Astrub City
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (95, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Astrub City
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (95, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Astrub City
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (95, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Astrub City
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (95, 2347, 0.1, 1, 5);
-- Monster 2354 : Famouse the Little-Known in Astrub City
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (95, 2354, 0.1, 1, 5);
-- Monster 118 : Dark Miner in Astrub Mines
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (96, 118, 1, 1, 5);
-- Monster 3343 : Creakrock in Astrub Mines
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (96, 3343, 1, 1, 5);
-- Monster 54 : Chafer in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 54, 1, 1, 5);
-- Monster 59 : Mush Mush in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 59, 1, 1, 5);
-- Monster 61 : Moskito in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 61, 1, 1, 5);
-- Monster 103 : Prespic in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 103, 1, 1, 5);
-- Monster 104 : Boar in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 104, 1, 1, 5);
-- Monster 110 : Invisible Chafer in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 110, 1, 1, 5);
-- Monster 159 : Miliboowolf in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 159, 1, 1, 5);
-- Monster 290 : Chafer Lancer in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 290, 1, 1, 5);
-- Monster 291 : Chafer Archer in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 291, 1, 1, 5);
-- Monster 453 : Bearman in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 453, 1, 1, 5);
-- Monster 466 : One-armed Bandit in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 466, 1, 1, 5);
-- Monster 2323 : Matmushmush the Flasher in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 2323, 0.1, 1, 5);
-- Monster 2333 : Milivanilli the Mime in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 2333, 0.1, 1, 5);
-- Monster 2336 : Moskoitus the Interruptor in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 2336, 0.1, 1, 5);
-- Monster 2349 : Prestreet the Fighter in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 2349, 0.1, 1, 5);
-- Monster 2353 : Boarnigen the Damasker in Astrub Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (97, 2353, 0.1, 1, 5);
-- Monster 31 : Blue Larva in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 31, 1, 1, 5);
-- Monster 34 : Green Larva in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 34, 1, 1, 5);
-- Monster 46 : Orange Larva in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 46, 1, 1, 5);
-- Monster 48 : Wild Sunflower in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 48, 1, 1, 5);
-- Monster 59 : Mush Mush in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 59, 1, 1, 5);
-- Monster 61 : Moskito in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 61, 1, 1, 5);
-- Monster 78 : Demonic Rose in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 78, 1, 1, 5);
-- Monster 79 : Evil Dandelion in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 79, 1, 1, 5);
-- Monster 98 : Tofu in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 98, 1, 1, 5);
-- Monster 2323 : Matmushmush the Flasher in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 2323, 0.1, 1, 5);
-- Monster 2331 : Larvalencia the Orange in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 2331, 0.1, 1, 5);
-- Monster 2332 : Larvadelaide the Ozie in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 2332, 0.1, 1, 5);
-- Monster 2336 : Moskoitus the Interruptor in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 2336, 0.1, 1, 5);
-- Monster 2348 : Dandel the Boy in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 2348, 0.1, 1, 5);
-- Monster 2352 : Roseanne the Yanker in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 2352, 0.1, 1, 5);
-- Monster 2355 : Tofudd the Hunter in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 2355, 0.1, 1, 5);
-- Monster 2358 : Hunflower the Sinful in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 2358, 0.1, 1, 5);
-- Monster 2574 : Larvalaska the Cold in Astrub Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (98, 2574, 0.1, 1, 5);
-- Monster 473 : Sick Tofu in Astrub Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (99, 473, 1, 1, 5);
-- Monster 474 : Sick Arachnee in Astrub Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (99, 474, 1, 1, 5);
-- Monster 2312 : Arachnangel the Hopeful in Astrub Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (99, 2312, 0.1, 1, 5);
-- Monster 2356 : Tofull the Optimist in Astrub Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (99, 2356, 0.1, 1, 5);
-- Monster 465 : Sick Grossewer Rat in Astrub Deep Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (100, 465, 1, 1, 5);
-- Monster 473 : Sick Tofu in Astrub Deep Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (100, 473, 1, 1, 5);
-- Monster 475 : Sick Grossewer Milirat in Astrub Deep Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (100, 475, 1, 1, 5);
-- Monster 2334 : Miliopold the Bloomer in Astrub Deep Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (100, 2334, 0.1, 1, 5);
-- Monster 2356 : Tofull the Optimist in Astrub Deep Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (100, 2356, 0.1, 1, 5);
-- Monster 3205 : Sick Grossewer Shaman in Astrub Deep Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (100, 3205, 1, 1, 5);
-- Monster 31 : Blue Larva in Tofu Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (101, 31, 1, 1, 5);
-- Monster 34 : Green Larva in Tofu Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (101, 34, 1, 1, 5);
-- Monster 46 : Orange Larva in Tofu Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (101, 46, 1, 1, 5);
-- Monster 52 : Arachnee in Tofu Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (101, 52, 1, 1, 5);
-- Monster 61 : Moskito in Tofu Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (101, 61, 1, 1, 5);
-- Monster 98 : Tofu in Tofu Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (101, 98, 1, 1, 5);
-- Monster 2310 : Arakula the Carpature in Tofu Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (101, 2310, 0.1, 1, 5);
-- Monster 2331 : Larvalencia the Orange in Tofu Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (101, 2331, 0.1, 1, 5);
-- Monster 2332 : Larvadelaide the Ozie in Tofu Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (101, 2332, 0.1, 1, 5);
-- Monster 2336 : Moskoitus the Interruptor in Tofu Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (101, 2336, 0.1, 1, 5);
-- Monster 2355 : Tofudd the Hunter in Tofu Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (101, 2355, 0.1, 1, 5);
-- Monster 2574 : Larvalaska the Cold in Tofu Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (101, 2574, 0.1, 1, 5);
-- Monster 154 : Kwoan in Cemetery of Heroes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (102, 154, 1, 1, 5);
-- Monster 495 : Ouassingal in Cemetery of Heroes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (102, 495, 1, 1, 5);
-- Monster 496 : Ouassingue in Cemetery of Heroes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (102, 496, 1, 1, 5);
-- Monster 498 : Gargoyl in Cemetery of Heroes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (102, 498, 1, 1, 5);
-- Monster 2486 : Gargoyla the Paranoiac in Cemetery of Heroes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (102, 2486, 0.1, 1, 5);
-- Monster 2522 : Kwoanium the Smart in Cemetery of Heroes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (102, 2522, 0.1, 1, 5);
-- Monster 2524 : Ouassup the Irritating in Cemetery of Heroes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (102, 2524, 0.1, 1, 5);
-- Monster 2616 : Ougineemo the Lost in Cemetery of Heroes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (102, 2616, 0.1, 1, 5);
-- Monster 118 : Dark Miner in Bandit Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (103, 118, 1, 1, 5);
-- Monster 119 : Dark Smith in Bandit Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (103, 119, 1, 1, 5);
-- Monster 153 : Dark Baker in Bandit Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (103, 153, 1, 1, 5);
-- Monster 155 : Rogue Clan Bandit in Bandit Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (103, 155, 1, 1, 5);
-- Monster 2314 : Bandinamit the Explosive in Bandit Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (103, 2314, 0.1, 1, 5);
-- Monster 2318 : Bakeraider the Tomb in Bandit Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (103, 2318, 0.1, 1, 5);
-- Monster 2328 : Smitherz the Licker in Bandit Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (103, 2328, 0.1, 1, 5);
-- Monster 2335 : Minoskittle the Coloured in Bandit Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (103, 2335, 0.1, 1, 5);
-- Monster 520 : Araknawa in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 520, 1, 1, 5);
-- Monster 522 : Grass Snake in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 522, 1, 1, 5);
-- Monster 523 : Kitsou Nakwa in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 523, 1, 1, 5);
-- Monster 524 : Bambooto in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 524, 1, 1, 5);
-- Monster 530 : Kitsou Nere in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 530, 1, 1, 5);
-- Monster 531 : Kitsou Nae in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 531, 1, 1, 5);
-- Monster 532 : Kitsou Nufeu in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 532, 1, 1, 5);
-- Monster 546 : Holy Bambooto in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 546, 1, 1, 5);
-- Monster 560 : Earth Pandawushu Disciple in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 560, 1, 1, 5);
-- Monster 2329 : Kitsewey the Blue in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 2329, 0.1, 1, 5);
-- Monster 2392 : Arachnawar the Killinmachin in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 2392, 0.1, 1, 5);
-- Monster 2398 : Bambono the Holy in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 2398, 0.1, 1, 5);
-- Monster 2434 : Grasnakizanami the Ruler in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 2434, 0.1, 1, 5);
-- Monster 2510 : Kitchy the Scratcher in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 2510, 0.1, 1, 5);
-- Monster 2511 : Kitsouie the Green in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 2511, 0.1, 1, 5);
-- Monster 2512 : Kitsuey the Red in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 2512, 0.1, 1, 5);
-- Monster 2548 : Bambottinit the Quiet in Neutral Pandala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (105, 2548, 0.1, 1, 5);
-- Monster 515 : Bulbiflor in Border of Akwadala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (106, 515, 1, 1, 5);
-- Monster 518 : Bulbush in Border of Akwadala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (106, 518, 1, 1, 5);
-- Monster 520 : Araknawa in Border of Akwadala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (106, 520, 1, 1, 5);
-- Monster 524 : Bambooto in Border of Akwadala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (106, 524, 1, 1, 5);
-- Monster 548 : Bulbamboo in Border of Akwadala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (106, 548, 1, 1, 5);
-- Monster 552 : Musha the Oni in Border of Akwadala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (106, 552, 1, 1, 5);
-- Monster 2392 : Arachnawar the Killinmachin in Border of Akwadala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (106, 2392, 0.1, 1, 5);
-- Monster 2411 : Bulbamoon the Trumpeter in Border of Akwadala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (106, 2411, 0.1, 1, 5);
-- Monster 2413 : Bulbisonic the Penetrating in Border of Akwadala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (106, 2413, 0.1, 1, 5);
-- Monster 2415 : Bulbushisu the Makisan in Border of Akwadala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (106, 2415, 0.1, 1, 5);
-- Monster 2548 : Bambottinit the Quiet in Border of Akwadala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (106, 2548, 0.1, 1, 5);
-- Monster 527 : Yokai Firefoux in Border of Feudala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (107, 527, 1, 1, 5);
-- Monster 528 : Soryo Firefoux in Border of Feudala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (107, 528, 1, 1, 5);
-- Monster 529 : Maho Firefoux in Border of Feudala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (107, 529, 1, 1, 5);
-- Monster 535 : Leopardo in Border of Feudala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (107, 535, 1, 1, 5);
-- Monster 550 : Rok Gnorok in Border of Feudala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (107, 550, 1, 1, 5);
-- Monster 2278 : Sorgyo Quiretox the Chatterbox in Border of Feudala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (107, 2278, 0.1, 1, 5);
-- Monster 2303 : Yokai the Choral in Border of Feudala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (107, 2303, 0.1, 1, 5);
-- Monster 2526 : MoMaho the Modernist in Border of Feudala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (107, 2526, 0.1, 1, 5);
-- Monster 2527 : Leopardon the Sorry in Border of Feudala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (107, 2527, 0.1, 1, 5);
-- Monster 517 : Pandit in Border of Aerdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (108, 517, 1, 1, 5);
-- Monster 537 : Pandora in Border of Aerdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (108, 537, 1, 1, 5);
-- Monster 549 : Pandulum in Border of Aerdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (108, 549, 1, 1, 5);
-- Monster 554 : Marzwel the Goblin in Border of Aerdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (108, 554, 1, 1, 5);
-- Monster 566 : Pandikaze in Border of Aerdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (108, 566, 1, 1, 5);
-- Monster 2592 : Pandartmoore the Dogged in Border of Aerdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (108, 2592, 0.1, 1, 5);
-- Monster 2593 : Pandaltry the Unknown in Border of Aerdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (108, 2593, 0.1, 1, 5);
-- Monster 2597 : Pandora the Explorer in Border of Aerdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (108, 2597, 0.1, 1, 5);
-- Monster 2598 : Pandali the Surreal in Border of Aerdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (108, 2598, 0.1, 1, 5);
-- Monster 517 : Pandit in Border of Terrdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (109, 517, 1, 1, 5);
-- Monster 534 : Drunken Pandawa in Border of Terrdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (109, 534, 1, 1, 5);
-- Monster 537 : Pandora in Border of Terrdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (109, 537, 1, 1, 5);
-- Monster 547 : Drunken Pandalette in Border of Terrdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (109, 547, 1, 1, 5);
-- Monster 549 : Pandulum in Border of Terrdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (109, 549, 1, 1, 5);
-- Monster 555 : Zatoishwan in Border of Terrdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (109, 555, 1, 1, 5);
-- Monster 2590 : Pandarwin the Naturist in Border of Terrdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (109, 2590, 0.1, 1, 5);
-- Monster 2591 : Pandan the Desperate in Border of Terrdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (109, 2591, 0.1, 1, 5);
-- Monster 2592 : Pandartmoore the Dogged in Border of Terrdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (109, 2592, 0.1, 1, 5);
-- Monster 2597 : Pandora the Explorer in Border of Terrdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (109, 2597, 0.1, 1, 5);
-- Monster 2598 : Pandali the Surreal in Border of Terrdala
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (109, 2598, 0.1, 1, 5);
-- Monster 508 : Aerdala Guard in Aerdala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (115, 508, 1, 1, 5);
-- Monster 509 : Aerdala Guard in Aerdala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (115, 509, 1, 1, 5);
-- Monster 557 : Air Pandawashu Disciple in Aerdala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (115, 557, 1, 1, 5);
-- Monster 512 : Feudala Guard in Feudala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (116, 512, 1, 1, 5);
-- Monster 513 : Feudala Guard in Feudala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (116, 513, 1, 1, 5);
-- Monster 559 : Fire Pandawushu Disciple in Feudala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (116, 559, 1, 1, 5);
-- Monster 504 : Akwadala Guard in Akwadala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (117, 504, 1, 1, 5);
-- Monster 507 : Akwadala Guard in Akwadala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (117, 507, 1, 1, 5);
-- Monster 510 : Terrdala Guard in Terrdala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (118, 510, 1, 1, 5);
-- Monster 511 : Terrdala Guard in Terrdala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (118, 511, 1, 1, 5);
-- Monster 560 : Earth Pandawushu Disciple in Terrdala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (118, 560, 1, 1, 5);
-- Monster 31 : Blue Larva in Pandala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (119, 31, 1, 1, 5);
-- Monster 52 : Arachnee in Pandala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (119, 52, 1, 1, 5);
-- Monster 61 : Moskito in Pandala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (119, 61, 1, 1, 5);
-- Monster 2310 : Arakula the Carpature in Pandala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (119, 2310, 0.1, 1, 5);
-- Monster 2336 : Moskoitus the Interruptor in Pandala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (119, 2336, 0.1, 1, 5);
-- Monster 2574 : Larvalaska the Cold in Pandala Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (119, 2574, 0.1, 1, 5);
-- Monster 540 : Aerdala's Heart in Aerdala Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (120, 540, 1, 1, 5);
-- Monster 542 : Aerdala's Heart in Aerdala Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (120, 542, 1, 1, 5);
-- Monster 538 : Feudala's Heart in Feudala Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (121, 538, 1, 1, 5);
-- Monster 545 : Feudala's Heart in Feudala Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (121, 545, 1, 1, 5);
-- Monster 541 : Akwadala's Heart in Akwadala Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (122, 541, 1, 1, 5);
-- Monster 544 : Akwadala's Heart in Akwadala Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (122, 544, 1, 1, 5);
-- Monster 539 : Terrdala's Heart in Terrdala Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (123, 539, 1, 1, 5);
-- Monster 543 : Terrdala's Heart in Terrdala Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (123, 543, 1, 1, 5);
-- Monster 515 : Bulbiflor in Bulb Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (124, 515, 1, 1, 5);
-- Monster 518 : Bulbush in Bulb Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (124, 518, 1, 1, 5);
-- Monster 520 : Araknawa in Bulb Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (124, 520, 1, 1, 5);
-- Monster 522 : Grass Snake in Bulb Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (124, 522, 1, 1, 5);
-- Monster 548 : Bulbamboo in Bulb Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (124, 548, 1, 1, 5);
-- Monster 3232 : Bulbig Brotha in Bulb Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (124, 3232, 1, 1, 5);
-- Monster 566 : Pandikaze in Pandikazes' Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (125, 566, 1, 1, 5);
-- Monster 578 : Aerial Pandikaze in Pandikazes' Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (125, 578, 1, 1, 5);
-- Monster 579 : Giddy Pandikaze in Pandikazes' Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (125, 579, 1, 1, 5);
-- Monster 580 : Pandikwakaze in Pandikazes' Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (125, 580, 1, 1, 5);
-- Monster 581 : Pandulkaze in Pandikazes' Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (125, 581, 1, 1, 5);
-- Monster 607 : Pandikaze Warrior in Pandikazes' Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (125, 607, 1, 1, 5);
-- Monster 612 : Pandora Master in Pandikazes' Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (125, 612, 1, 1, 5);
-- Monster 523 : Kitsou Nakwa in Kitsoune Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (133, 523, 1, 1, 5);
-- Monster 530 : Kitsou Nere in Kitsoune Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (133, 530, 1, 1, 5);
-- Monster 531 : Kitsou Nae in Kitsoune Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (133, 531, 1, 1, 5);
-- Monster 532 : Kitsou Nufeu in Kitsoune Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (133, 532, 1, 1, 5);
-- Monster 568 : Tanukouï San in Kitsoune Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (133, 568, 1, 1, 5);
-- Monster 527 : Yokai Firefoux in Firefoux Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (144, 527, 1, 1, 5);
-- Monster 528 : Soryo Firefoux in Firefoux Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (144, 528, 1, 1, 5);
-- Monster 529 : Maho Firefoux in Firefoux Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (144, 529, 1, 1, 5);
-- Monster 532 : Kitsou Nufeu in Firefoux Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (144, 532, 1, 1, 5);
-- Monster 535 : Leopardo in Firefoux Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (144, 535, 1, 1, 5);
-- Monster 582 : Touchparak in Firefoux Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (144, 582, 1, 1, 5);
-- Monster 599 : Zilla in Firefoux Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (144, 599, 1, 1, 5);
-- Monster 605 : Peki Peki in Firefoux Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (144, 605, 1, 1, 5);
-- Monster 583 : Pandikaze Ghost in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 583, 1, 1, 5);
-- Monster 584 : Maho Firefoux Ghost in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 584, 1, 1, 5);
-- Monster 585 : Soryo Firefoux Ghost in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 585, 1, 1, 5);
-- Monster 586 : Leopardo Ghost in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 586, 1, 1, 5);
-- Monster 587 : Yokai Firefoux Ghost in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 587, 1, 1, 5);
-- Monster 588 : Tanukouï San Ghost in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 588, 1, 1, 5);
-- Monster 589 : Pandulum Ghost in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 589, 1, 1, 5);
-- Monster 590 : Tanuki Chan Ghost in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 590, 1, 1, 5);
-- Monster 594 : Tanuki Chan Ghost in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 594, 1, 1, 5);
-- Monster 595 : Tanuki Chan Ghost in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 595, 1, 1, 5);
-- Monster 596 : Tanuki Chan Ghost in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 596, 1, 1, 5);
-- Monster 597 : Pandora Ghost in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 597, 1, 1, 5);
-- Monster 598 : Tanuki Chan Ghost in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 598, 1, 1, 5);
-- Monster 2370 : Leorio the Haunted in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 2370, 0.1, 1, 5);
-- Monster 2374 : Satonuki the Plastikpaddy in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 2374, 0.1, 1, 5);
-- Monster 2469 : Pandipoopik the Wondrous in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 2469, 0.1, 1, 5);
-- Monster 2470 : Pandoracle the Opposing Force in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 2470, 0.1, 1, 5);
-- Monster 2471 : Pandumonium the Joker in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 2471, 0.1, 1, 5);
-- Monster 2472 : Tanukhiraru the Gifted in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 2472, 0.1, 1, 5);
-- Monster 2473 : TanuKiki the Deliveryghost in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 2473, 0.1, 1, 5);
-- Monster 2474 : Tanukhuina the Drawer in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 2474, 0.1, 1, 5);
-- Monster 2475 : Tanno the Dominator in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 2475, 0.1, 1, 5);
-- Monster 2476 : Tanaked the Stalker in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 2476, 0.1, 1, 5);
-- Monster 2477 : Tanuktonik the Doofdoof in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 2477, 0.1, 1, 5);
-- Monster 2483 : Yoksai the Spirited in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 2483, 0.1, 1, 5);
-- Monster 2564 : Miomaho the Siciliano in Nolifis Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (152, 2564, 0.1, 1, 5);
-- Monster 113 : Dragon Pig in Dragon Pig Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (154, 113, 1, 1, 5);
-- Monster 600 : Blodz Uker in Dragon Pig Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (154, 600, 1, 1, 5);
-- Monster 601 : Dorgan Ation in Dragon Pig Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (154, 601, 1, 1, 5);
-- Monster 603 : Farle's Pig in Dragon Pig Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (154, 603, 1, 1, 5);
-- Monster 3137 : Gorgoyle in Dragon Pig Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (154, 3137, 1, 1, 5);
-- Monster 3304 : Pignolia in Dragon Pig Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (154, 3304, 1, 1, 5);
-- Monster 64 : Wabbit in Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (161, 64, 1, 1, 5);
-- Monster 65 : Black Wabbit in Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (161, 65, 1, 1, 5);
-- Monster 68 : Black Tiwabbit in Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (161, 68, 1, 1, 5);
-- Monster 72 : Tiwabbit Wosungwee in Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (161, 72, 1, 1, 5);
-- Monster 96 : Tiwabbit in Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (161, 96, 1, 1, 5);
-- Monster 97 : Wo Wabbit in Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (161, 97, 1, 1, 5);
-- Monster 179 : Skeleton Wabbit in Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (161, 179, 1, 1, 5);
-- Monster 2293 : Tiwaldo the Hidden in Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (161, 2293, 0.1, 1, 5);
-- Monster 2294 : Tiwascal the Wapper in Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (161, 2294, 0.1, 1, 5);
-- Monster 2304 : McWhabbit the Diehard in Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (161, 2304, 0.1, 1, 5);
-- Monster 2308 : Wowalker the Egyptian in Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (161, 2308, 0.1, 1, 5);
-- Monster 2402 : Tiwana the Tokin' in Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (161, 2402, 0.1, 1, 5);
-- Monster 2403 : Wabbin the Wich in Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (161, 2403, 0.1, 1, 5);
-- Monster 64 : Wabbit in Small Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (162, 64, 1, 1, 5);
-- Monster 65 : Black Wabbit in Small Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (162, 65, 1, 1, 5);
-- Monster 68 : Black Tiwabbit in Small Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (162, 68, 1, 1, 5);
-- Monster 72 : Tiwabbit Wosungwee in Small Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (162, 72, 1, 1, 5);
-- Monster 96 : Tiwabbit in Small Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (162, 96, 1, 1, 5);
-- Monster 97 : Wo Wabbit in Small Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (162, 97, 1, 1, 5);
-- Monster 99 : Gwandpa Wabbit in Small Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (162, 99, 1, 1, 5);
-- Monster 2294 : Tiwascal the Wapper in Small Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (162, 2294, 0.1, 1, 5);
-- Monster 2304 : McWhabbit the Diehard in Small Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (162, 2304, 0.1, 1, 5);
-- Monster 2308 : Wowalker the Egyptian in Small Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (162, 2308, 0.1, 1, 5);
-- Monster 2402 : Tiwana the Tokin' in Small Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (162, 2402, 0.1, 1, 5);
-- Monster 2403 : Wabbin the Wich in Small Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (162, 2403, 0.1, 1, 5);
-- Monster 2498 : Gwabbit the Wunner in Small Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (162, 2498, 0.1, 1, 5);
-- Monster 179 : Skeleton Wabbit in Skeleton Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (163, 179, 1, 1, 5);
-- Monster 2305 : Wabbitor the Apt in Skeleton Wabbit Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (163, 2305, 0.1, 1, 5);
-- Monster 64 : Wabbit in Wabbit Castle Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (164, 64, 1, 1, 5);
-- Monster 65 : Black Wabbit in Wabbit Castle Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (164, 65, 1, 1, 5);
-- Monster 68 : Black Tiwabbit in Wabbit Castle Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (164, 68, 1, 1, 5);
-- Monster 96 : Tiwabbit in Wabbit Castle Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (164, 96, 1, 1, 5);
-- Monster 97 : Wo Wabbit in Wabbit Castle Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (164, 97, 1, 1, 5);
-- Monster 99 : Gwandpa Wabbit in Wabbit Castle Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (164, 99, 1, 1, 5);
-- Monster 179 : Skeleton Wabbit in Wabbit Castle Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (164, 179, 1, 1, 5);
-- Monster 2293 : Tiwaldo the Hidden in Wabbit Castle Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (164, 2293, 0.1, 1, 5);
-- Monster 2304 : McWhabbit the Diehard in Wabbit Castle Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (164, 2304, 0.1, 1, 5);
-- Monster 2308 : Wowalker the Egyptian in Wabbit Castle Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (164, 2308, 0.1, 1, 5);
-- Monster 2402 : Tiwana the Tokin' in Wabbit Castle Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (164, 2402, 0.1, 1, 5);
-- Monster 2403 : Wabbin the Wich in Wabbit Castle Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (164, 2403, 0.1, 1, 5);
-- Monster 2498 : Gwabbit the Wunner in Wabbit Castle Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (164, 2498, 0.1, 1, 5);
-- Monster 31 : Blue Larva in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 31, 1, 1, 5);
-- Monster 34 : Green Larva in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 34, 1, 1, 5);
-- Monster 46 : Orange Larva in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 46, 1, 1, 5);
-- Monster 208 : Tikokoko in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 208, 1, 1, 5);
-- Monster 209 : Kokoko in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 209, 1, 1, 5);
-- Monster 211 : Kanniball Archer in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 211, 1, 1, 5);
-- Monster 212 : Kanniball Thierry in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 212, 1, 1, 5);
-- Monster 213 : Kanniball Jav in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 213, 1, 1, 5);
-- Monster 214 : Kanniball Sarbak in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 214, 1, 1, 5);
-- Monster 215 : Glukoko in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 215, 1, 1, 5);
-- Monster 216 : Greedovore in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 216, 1, 1, 5);
-- Monster 217 : Ambusher in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 217, 1, 1, 5);
-- Monster 218 : Kokonut in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 218, 1, 1, 5);
-- Monster 226 : Moon in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 226, 1, 1, 5);
-- Monster 2301 : Glukoko the Slow in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 2301, 0.1, 1, 5);
-- Monster 2331 : Larvalencia the Orange in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 2331, 0.1, 1, 5);
-- Monster 2332 : Larvadelaide the Ozie in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 2332, 0.1, 1, 5);
-- Monster 2484 : Ambushapens the Unlucky in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 2484, 0.1, 1, 5);
-- Monster 2495 : Greetdoff the Gentleman in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 2495, 0.1, 1, 5);
-- Monster 2574 : Larvalaska the Cold in The Deep Moon Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (165, 2574, 0.1, 1, 5);
-- Monster 211 : Kanniball Archer in The Road to Moon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (166, 211, 1, 1, 5);
-- Monster 212 : Kanniball Thierry in The Road to Moon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (166, 212, 1, 1, 5);
-- Monster 213 : Kanniball Jav in The Road to Moon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (166, 213, 1, 1, 5);
-- Monster 214 : Kanniball Sarbak in The Road to Moon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (166, 214, 1, 1, 5);
-- Monster 215 : Glukoko in The Road to Moon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (166, 215, 1, 1, 5);
-- Monster 217 : Ambusher in The Road to Moon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (166, 217, 1, 1, 5);
-- Monster 2301 : Glukoko the Slow in The Road to Moon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (166, 2301, 0.1, 1, 5);
-- Monster 2484 : Ambushapens the Unlucky in The Road to Moon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (166, 2484, 0.1, 1, 5);
-- Monster 2502 : Kannimantha the Maneater in The Road to Moon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (166, 2502, 0.1, 1, 5);
-- Monster 2505 : Kannemik the Skinny in The Road to Moon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (166, 2505, 0.1, 1, 5);
-- Monster 2506 : Kannarrie the Reckless in The Road to Moon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (166, 2506, 0.1, 1, 5);
-- Monster 2507 : Kanniranda the Maniac in The Road to Moon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (166, 2507, 0.1, 1, 5);
-- Monster 228 : Boomba in The Pirate Boat
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (167, 228, 1, 1, 5);
-- Monster 229 : Hazwonarm in The Pirate Boat
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (167, 229, 1, 1, 5);
-- Monster 230 : LeChouque in The Pirate Boat
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (167, 230, 1, 1, 5);
-- Monster 231 : Cannon Dorf in The Pirate Boat
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (167, 231, 1, 1, 5);
-- Monster 2408 : Boombora the Dangerous in The Pirate Boat
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (167, 2408, 0.1, 1, 5);
-- Monster 2419 : Ganon the Dwarf in The Pirate Boat
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (167, 2419, 0.1, 1, 5);
-- Monster 2538 : Hazwonball the Hickler in The Pirate Boat
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (167, 2538, 0.1, 1, 5);
-- Monster 47 : Treechnid in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 47, 1, 1, 5);
-- Monster 52 : Arachnee in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 52, 1, 1, 5);
-- Monster 110 : Invisible Chafer in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 110, 1, 1, 5);
-- Monster 118 : Dark Miner in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 118, 1, 1, 5);
-- Monster 194 : Red Scaraleaf in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 194, 1, 1, 5);
-- Monster 198 : Blue Scaraleaf in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 198, 1, 1, 5);
-- Monster 240 : Green Scaraleaf in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 240, 1, 1, 5);
-- Monster 241 : White Scaraleaf in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 241, 1, 1, 5);
-- Monster 253 : Dark Treechnid in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 253, 1, 1, 5);
-- Monster 254 : Venerable Treechnid in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 254, 1, 1, 5);
-- Monster 255 : Aggressive Arachnee in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 255, 1, 1, 5);
-- Monster 259 : Major Arachnee in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 259, 1, 1, 5);
-- Monster 290 : Chafer Lancer in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 290, 1, 1, 5);
-- Monster 291 : Chafer Archer in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 291, 1, 1, 5);
-- Monster 292 : Elite Chafer in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 292, 1, 1, 5);
-- Monster 649 : Dark Chest in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 649, 1, 1, 5);
-- Monster 651 : Dark Treechnee in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 651, 1, 1, 5);
-- Monster 2386 : Treekalack the Sad in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 2386, 0.1, 1, 5);
-- Monster 2388 : Treekonk the Stunned in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 2388, 0.1, 1, 5);
-- Monster 2389 : Treektamak the Loud in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 2389, 0.1, 1, 5);
-- Monster 2821 : Kit Knapping's Chest in Dark Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (168, 2821, 1, 1, 5);
-- Monster 31 : Blue Larva in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 31, 1, 1, 5);
-- Monster 34 : Green Larva in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 34, 1, 1, 5);
-- Monster 46 : Orange Larva in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 46, 1, 1, 5);
-- Monster 47 : Treechnid in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 47, 1, 1, 5);
-- Monster 48 : Wild Sunflower in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 48, 1, 1, 5);
-- Monster 52 : Arachnee in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 52, 1, 1, 5);
-- Monster 59 : Mush Mush in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 59, 1, 1, 5);
-- Monster 61 : Moskito in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 61, 1, 1, 5);
-- Monster 62 : Karne Rider in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 62, 1, 1, 5);
-- Monster 78 : Demonic Rose in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 78, 1, 1, 5);
-- Monster 79 : Evil Dandelion in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 79, 1, 1, 5);
-- Monster 103 : Prespic in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 103, 1, 1, 5);
-- Monster 178 : Goblin in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 178, 1, 1, 5);
-- Monster 256 : Trunknid in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 256, 1, 1, 5);
-- Monster 259 : Major Arachnee in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 259, 1, 1, 5);
-- Monster 2299 : Trunkbeard the Gentle in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 2299, 0.1, 1, 5);
-- Monster 2309 : Treekniddioo the Needy in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 2309, 0.1, 1, 5);
-- Monster 2310 : Arakula the Carpature in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 2310, 0.1, 1, 5);
-- Monster 2323 : Matmushmush the Flasher in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 2323, 0.1, 1, 5);
-- Monster 2331 : Larvalencia the Orange in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 2331, 0.1, 1, 5);
-- Monster 2332 : Larvadelaide the Ozie in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 2332, 0.1, 1, 5);
-- Monster 2336 : Moskoitus the Interruptor in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 2336, 0.1, 1, 5);
-- Monster 2348 : Dandel the Boy in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 2348, 0.1, 1, 5);
-- Monster 2349 : Prestreet the Fighter in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 2349, 0.1, 1, 5);
-- Monster 2352 : Roseanne the Yanker in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 2352, 0.1, 1, 5);
-- Monster 2358 : Hunflower the Sinful in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 2358, 0.1, 1, 5);
-- Monster 2574 : Larvalaska the Cold in Edge of the Treechnid Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (169, 2574, 0.1, 1, 5);
-- Monster 171 : Wild Almond Dragoturkey in Scaraleaf Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (170, 171, 1, 1, 5);
-- Monster 194 : Red Scaraleaf in Scaraleaf Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (170, 194, 1, 1, 5);
-- Monster 198 : Blue Scaraleaf in Scaraleaf Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (170, 198, 1, 1, 5);
-- Monster 200 : Wild Ginger Dragoturkey in Scaraleaf Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (170, 200, 1, 1, 5);
-- Monster 240 : Green Scaraleaf in Scaraleaf Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (170, 240, 1, 1, 5);
-- Monster 241 : White Scaraleaf in Scaraleaf Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (170, 241, 1, 1, 5);
-- Monster 242 : Fire Spark in Scaraleaf Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (170, 242, 1, 1, 5);
-- Monster 243 : Water Spark in Scaraleaf Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (170, 243, 1, 1, 5);
-- Monster 244 : Earth Spark in Scaraleaf Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (170, 244, 1, 1, 5);
-- Monster 245 : Air Spark in Scaraleaf Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (170, 245, 1, 1, 5);
-- Monster 2280 : Scaratheef the Pincher in Scaraleaf Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (170, 2280, 0.1, 1, 5);
-- Monster 2281 : Scarahazad the Storyteller in Scaraleaf Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (170, 2281, 0.1, 1, 5);
-- Monster 2282 : Scarabreef the Short in Scaraleaf Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (170, 2282, 0.1, 1, 5);
-- Monster 2283 : Scaramel the Melty in Scaraleaf Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (170, 2283, 0.1, 1, 5);
-- Monster 515 : Bulbiflor in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 515, 1, 1, 5);
-- Monster 518 : Bulbush in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 518, 1, 1, 5);
-- Monster 519 : Bulbig in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 519, 1, 1, 5);
-- Monster 520 : Araknawa in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 520, 1, 1, 5);
-- Monster 522 : Grass Snake in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 522, 1, 1, 5);
-- Monster 524 : Bambooto in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 524, 1, 1, 5);
-- Monster 525 : Poacher in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 525, 1, 1, 5);
-- Monster 546 : Holy Bambooto in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 546, 1, 1, 5);
-- Monster 548 : Bulbamboo in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 548, 1, 1, 5);
-- Monster 2392 : Arachnawar the Killinmachin in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 2392, 0.1, 1, 5);
-- Monster 2398 : Bambono the Holy in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 2398, 0.1, 1, 5);
-- Monster 2411 : Bulbamoon the Trumpeter in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 2411, 0.1, 1, 5);
-- Monster 2412 : Pocher the Kingponger in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 2412, 0.1, 1, 5);
-- Monster 2413 : Bulbisonic the Penetrating in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 2413, 0.1, 1, 5);
-- Monster 2414 : Bulbigroov the Dancer in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 2414, 0.1, 1, 5);
-- Monster 2415 : Bulbushisu the Makisan in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 2415, 0.1, 1, 5);
-- Monster 2434 : Grasnakizanami the Ruler in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 2434, 0.1, 1, 5);
-- Monster 2548 : Bambottinit the Quiet in Pandala Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (171, 2548, 0.1, 1, 5);
-- Monster 31 : Blue Larva in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 31, 1, 1, 5);
-- Monster 34 : Green Larva in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 34, 1, 1, 5);
-- Monster 46 : Orange Larva in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 46, 1, 1, 5);
-- Monster 48 : Wild Sunflower in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 48, 1, 1, 5);
-- Monster 52 : Arachnee in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 52, 1, 1, 5);
-- Monster 59 : Mush Mush in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 59, 1, 1, 5);
-- Monster 78 : Demonic Rose in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 78, 1, 1, 5);
-- Monster 79 : Evil Dandelion in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 79, 1, 1, 5);
-- Monster 98 : Tofu in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 98, 1, 1, 5);
-- Monster 101 : Gobball in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 101, 1, 1, 5);
-- Monster 134 : White Gobbly in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 134, 1, 1, 5);
-- Monster 149 : Black Gobbly in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 149, 1, 1, 5);
-- Monster 2310 : Arakula the Carpature in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 2310, 0.1, 1, 5);
-- Monster 2315 : Gobbach the Contrapuntaler in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 2315, 0.1, 1, 5);
-- Monster 2316 : Gobballyhoo the Noisy in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 2316, 0.1, 1, 5);
-- Monster 2317 : Gobballad the Romantic in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 2317, 0.1, 1, 5);
-- Monster 2323 : Matmushmush the Flasher in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 2323, 0.1, 1, 5);
-- Monster 2331 : Larvalencia the Orange in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 2331, 0.1, 1, 5);
-- Monster 2332 : Larvadelaide the Ozie in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 2332, 0.1, 1, 5);
-- Monster 2348 : Dandel the Boy in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 2348, 0.1, 1, 5);
-- Monster 2352 : Roseanne the Yanker in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 2352, 0.1, 1, 5);
-- Monster 2355 : Tofudd the Hunter in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 2355, 0.1, 1, 5);
-- Monster 2358 : Hunflower the Sinful in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 2358, 0.1, 1, 5);
-- Monster 2574 : Larvalaska the Cold in Astrub Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (173, 2574, 0.1, 1, 5);
-- Monster 173 : Ancestral Treechnid in Treechnid Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (175, 173, 1, 1, 5);
-- Monster 253 : Dark Treechnid in Treechnid Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (175, 253, 1, 1, 5);
-- Monster 254 : Venerable Treechnid in Treechnid Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (175, 254, 1, 1, 5);
-- Monster 651 : Dark Treechnee in Treechnid Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (175, 651, 1, 1, 5);
-- Monster 917 : Arachnotron in Treechnid Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (175, 917, 1, 1, 5);
-- Monster 123 : Piglet in Lousy Pig Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (178, 123, 1, 1, 5);
-- Monster 343 : Lousy Pig Knight in Lousy Pig Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (178, 343, 1, 1, 5);
-- Monster 344 : Lousy Pig Shepherd in Lousy Pig Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (178, 344, 1, 1, 5);
-- Monster 2325 : Pigoblet the Useful in Lousy Pig Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (178, 2325, 0.1, 1, 5);
-- Monster 2397 : Pigstol the Sexy in Lousy Pig Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (178, 2397, 0.1, 1, 5);
-- Monster 2420 : Pygknightlion the Lousy in Lousy Pig Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (178, 2420, 0.1, 1, 5);
-- Monster 101 : Gobball in Mushd Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (179, 101, 1, 1, 5);
-- Monster 112 : Mushd in Mushd Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (179, 112, 1, 1, 5);
-- Monster 134 : White Gobbly in Mushd Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (179, 134, 1, 1, 5);
-- Monster 149 : Black Gobbly in Mushd Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (179, 149, 1, 1, 5);
-- Monster 2549 : Mushdrill the Piercer in Mushd Corner
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (179, 2549, 0.1, 1, 5);
-- Monster 232 : Moowolf in Amakna Castle Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (181, 232, 1, 1, 5);
-- Monster 255 : Aggressive Arachnee in Amakna Castle Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (181, 255, 1, 1, 5);
-- Monster 442 : Grossewer Rat in Amakna Castle Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (181, 442, 1, 1, 5);
-- Monster 447 : Grossewer Shaman in Amakna Castle Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (181, 447, 1, 1, 5);
-- Monster 465 : Sick Grossewer Rat in Amakna Castle Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (181, 465, 1, 1, 5);
-- Monster 2273 : Rattle the Hummer in Amakna Castle Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (181, 2273, 0.1, 1, 5);
-- Monster 2350 : Ratatouille the Stirrer in Amakna Castle Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (181, 2350, 0.1, 1, 5);
-- Monster 2393 : Arachnekros the Aggressive in Amakna Castle Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (181, 2393, 0.1, 1, 5);
-- Monster 2423 : Shamassel the Off in Amakna Castle Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (181, 2423, 0.1, 1, 5);
-- Monster 600 : Blodz Uker in The Dragon Pig's Maze
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (200, 600, 1, 1, 5);
-- Monster 601 : Dorgan Ation in The Dragon Pig's Maze
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (200, 601, 1, 1, 5);
-- Monster 603 : Farle's Pig in The Dragon Pig's Maze
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (200, 603, 1, 1, 5);
-- Monster 2432 : Pighatchoo the Electrical in The Dragon Pig's Maze
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (200, 2432, 0.1, 1, 5);
-- Monster 3304 : Pignolia in The Dragon Pig's Maze
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (200, 3304, 1, 1, 5);
-- Monster 98 : Tofu in Tofu House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (201, 98, 1, 1, 5);
-- Monster 796 : Black Tofu in Tofu House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (201, 796, 1, 1, 5);
-- Monster 800 : Batofu in Tofu House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (201, 800, 1, 1, 5);
-- Monster 804 : Tofoone in Tofu House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (201, 804, 1, 1, 5);
-- Monster 806 : Tofukaz in Tofu House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (201, 806, 1, 1, 5);
-- Monster 808 : Tofurby in Tofu House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (201, 808, 1, 1, 5);
-- Monster 829 : Quetsnakiatl in Minotoror Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (209, 829, 1, 1, 5);
-- Monster 834 : Minoskito in Minotoror Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (209, 834, 1, 1, 5);
-- Monster 835 : Manderisha in Minotoror Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (209, 835, 1, 1, 5);
-- Monster 836 : Khamelerost in Minotoror Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (209, 836, 1, 1, 5);
-- Monster 2287 : Quetnin the Fictional in Minotoror Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (209, 2287, 0.1, 1, 5);
-- Monster 2521 : Khameleltux the Tolerant in Minotoror Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (209, 2521, 0.1, 1, 5);
-- Monster 2529 : Salamaa the Henpeck in Minotoror Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (209, 2529, 0.1, 1, 5);
-- Monster 2533 : Milikkybum the Informer in Minotoror Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (209, 2533, 0.1, 1, 5);
-- Monster 121 : Minotoror in Inner Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (210, 121, 1, 1, 5);
-- Monster 668 : Minokid in Inner Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (210, 668, 1, 1, 5);
-- Monster 827 : Minotot in Inner Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (210, 827, 1, 1, 5);
-- Monster 829 : Quetsnakiatl in Inner Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (210, 829, 1, 1, 5);
-- Monster 830 : Scaratos in Inner Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (210, 830, 1, 1, 5);
-- Monster 831 : Mumminotor in Inner Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (210, 831, 1, 1, 5);
-- Monster 832 : Deminoball in Inner Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (210, 832, 1, 1, 5);
-- Monster 2287 : Quetnin the Fictional in Inner Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (210, 2287, 0.1, 1, 5);
-- Monster 2568 : Minoknok the Visitor in Inner Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (210, 2568, 0.1, 1, 5);
-- Monster 2615 : Scaraheath the Hanger in Inner Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (210, 2615, 0.1, 1, 5);
-- Monster 281 : Crow in Lord Crow's Library
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (211, 281, 1, 1, 5);
-- Monster 289 : Lord Crow in Lord Crow's Library
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (211, 289, 1, 1, 5);
-- Monster 818 : Tamed Crow in Lord Crow's Library
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (211, 818, 1, 1, 5);
-- Monster 819 : Drinker in Lord Crow's Library
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (211, 819, 1, 1, 5);
-- Monster 820 : Crowfox in Lord Crow's Library
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (211, 820, 1, 1, 5);
-- Monster 823 : Foxo the Crowfox in Lord Crow's Library
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (211, 823, 1, 1, 5);
-- Monster 824 : Horace the Tamed Crow in Lord Crow's Library
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (211, 824, 1, 1, 5);
-- Monster 825 : Kapotie the Drinker in Lord Crow's Library
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (211, 825, 1, 1, 5);
-- Monster 102 : Boowolf in Canidae Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (213, 102, 1, 1, 5);
-- Monster 159 : Miliboowolf in Canidae Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (213, 159, 1, 1, 5);
-- Monster 232 : Moowolf in Canidae Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (213, 232, 1, 1, 5);
-- Monster 287 : Kaniger in Canidae Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (213, 287, 1, 1, 5);
-- Monster 299 : Whitish Fang in Canidae Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (213, 299, 1, 1, 5);
-- Monster 301 : Ouginak in Canidae Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (213, 301, 1, 1, 5);
-- Monster 670 : Koolich in Koolich Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (223, 670, 1, 1, 5);
-- Monster 671 : Cave Gobball in Koolich Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (223, 671, 1, 1, 5);
-- Monster 672 : Gobkool in Koolich Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (223, 672, 1, 1, 5);
-- Monster 746 : Brown Warko in Koolich Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (223, 746, 1, 1, 5);
-- Monster 747 : Dok Alako in Koolich Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (223, 747, 1, 1, 5);
-- Monster 748 : Immature Koalak in Koolich Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (223, 748, 1, 1, 5);
-- Monster 751 : Indigo Koalak in Koolich Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (223, 751, 1, 1, 5);
-- Monster 752 : Coco Koalak in Koolich Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (223, 752, 1, 1, 5);
-- Monster 753 : Morello Cherry Koalak in Koolich Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (223, 753, 1, 1, 5);
-- Monster 754 : Pippin Koalak in Koolich Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (223, 754, 1, 1, 5);
-- Monster 755 : Mama Koalak in Koolich Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (223, 755, 1, 1, 5);
-- Monster 763 : Drakoalak in Koolich Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (223, 763, 1, 1, 5);
-- Monster 784 : Piralak in Koolich Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (223, 784, 1, 1, 5);
-- Monster 2442 : Dokterwho the Tardisporter in Koolich Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (223, 2442, 0.1, 1, 5);
-- Monster 758 : Koalak Master in Primitive Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (230, 758, 1, 1, 5);
-- Monster 759 : Koalak Rider in Primitive Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (230, 759, 1, 1, 5);
-- Monster 760 : Koalak Mummy in Primitive Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (230, 760, 1, 1, 5);
-- Monster 761 : Koalak Gravedigger in Primitive Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (230, 761, 1, 1, 5);
-- Monster 2429 : Koelloggs the Creator in Primitive Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (230, 2429, 0.1, 1, 5);
-- Monster 2482 : Koalarchitect the Balancing Force in Primitive Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (230, 2482, 0.1, 1, 5);
-- Monster 2531 : Koalakropolis the King of the Hill in Primitive Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (230, 2531, 0.1, 1, 5);
-- Monster 2534 : Jackoalak the Moonwalker in Primitive Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (230, 2534, 0.1, 1, 5);
-- Monster 747 : Dok Alako in Enchanted Lakes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (231, 747, 1, 1, 5);
-- Monster 748 : Immature Koalak in Enchanted Lakes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (231, 748, 1, 1, 5);
-- Monster 749 : Koalak Warrior in Enchanted Lakes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (231, 749, 1, 1, 5);
-- Monster 751 : Indigo Koalak in Enchanted Lakes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (231, 751, 1, 1, 5);
-- Monster 784 : Piralak in Enchanted Lakes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (231, 784, 1, 1, 5);
-- Monster 786 : Fisheralak in Enchanted Lakes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (231, 786, 1, 1, 5);
-- Monster 2270 : Piralhaka the Intimidator in Enchanted Lakes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (231, 2270, 0.1, 1, 5);
-- Monster 2271 : Fisheralf the Stewart in Enchanted Lakes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (231, 2271, 0.1, 1, 5);
-- Monster 2442 : Dokterwho the Tardisporter in Enchanted Lakes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (231, 2442, 0.1, 1, 5);
-- Monster 2517 : Koaldmen the Grumpy in Enchanted Lakes
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (231, 2517, 0.1, 1, 5);
-- Monster 112 : Mushd in Nauseating Swamps
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (232, 112, 1, 1, 5);
-- Monster 261 : Crocodyl in Nauseating Swamps
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (232, 261, 1, 1, 5);
-- Monster 2439 : Croccyx the Bummer in Nauseating Swamps
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (232, 2439, 0.1, 1, 5);
-- Monster 2549 : Mushdrill the Piercer in Nauseating Swamps
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (232, 2549, 0.1, 1, 5);
-- Monster 112 : Mushd in Bottomless Swamps
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (233, 112, 1, 1, 5);
-- Monster 259 : Major Arachnee in Bottomless Swamps
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (233, 259, 1, 1, 5);
-- Monster 261 : Crocodyl in Bottomless Swamps
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (233, 261, 1, 1, 5);
-- Monster 263 : Crocodyl Chief in Bottomless Swamps
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (233, 263, 1, 1, 5);
-- Monster 2311 : Arachma the Greek in Bottomless Swamps
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (233, 2311, 0.1, 1, 5);
-- Monster 2428 : Crokdylann the Rebel in Bottomless Swamps
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (233, 2428, 0.1, 1, 5);
-- Monster 2439 : Croccyx the Bummer in Bottomless Swamps
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (233, 2439, 0.1, 1, 5);
-- Monster 2549 : Mushdrill the Piercer in Bottomless Swamps
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (233, 2549, 0.1, 1, 5);
-- Monster 746 : Brown Warko in Kaliptus Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (234, 746, 1, 1, 5);
-- Monster 747 : Dok Alako in Kaliptus Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (234, 747, 1, 1, 5);
-- Monster 748 : Immature Koalak in Kaliptus Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (234, 748, 1, 1, 5);
-- Monster 785 : Koalak Forester in Kaliptus Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (234, 785, 1, 1, 5);
-- Monster 2306 : Warko the Inky in Kaliptus Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (234, 2306, 0.1, 1, 5);
-- Monster 2442 : Dokterwho the Tardisporter in Kaliptus Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (234, 2442, 0.1, 1, 5);
-- Monster 2515 : Popoalak the Mousibrown in Kaliptus Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (234, 2515, 0.1, 1, 5);
-- Monster 2517 : Koaldmen the Grumpy in Kaliptus Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (234, 2517, 0.1, 1, 5);
-- Monster 171 : Wild Almond Dragoturkey in Wild Dragoturkey Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (235, 171, 1, 1, 5);
-- Monster 200 : Wild Ginger Dragoturkey in Wild Dragoturkey Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (235, 200, 1, 1, 5);
-- Monster 666 : Wild Golden Dragoturkey in Wild Dragoturkey Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (235, 666, 1, 1, 5);
-- Monster 2379 : Dragotitis the Painful in Wild Dragoturkey Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (235, 2379, 0.1, 1, 5);
-- Monster 2465 : Drakokidoki the Volunteer in Wild Dragoturkey Territory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (235, 2465, 0.1, 1, 5);
-- Monster 631 : Crow Ghost in Pet Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (236, 631, 1, 1, 5);
-- Monster 632 : Young Wild Boar Ghost in Pet Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (236, 632, 1, 1, 5);
-- Monster 661 : Croum Ghost in Pet Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (236, 661, 1, 1, 5);
-- Monster 663 : Leopardo Ghost in Pet Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (236, 663, 1, 1, 5);
-- Monster 765 : Willy Peninzias Ghost in Pet Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (236, 765, 1, 1, 5);
-- Monster 813 : Bworky Ghost in Pet Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (236, 813, 1, 1, 5);
-- Monster 815 : El Scarador Ghost in Pet Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (236, 815, 1, 1, 5);
-- Monster 817 : Treechster Ghost in Pet Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (236, 817, 1, 1, 5);
-- Monster 1037 : Ross Ghost in Pet Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (236, 1037, 1, 1, 5);
-- Monster 1038 : Bilby Ghost in Pet Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (236, 1038, 1, 1, 5);
-- Monster 2606 : Crocodyl Ghost in Pet Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (236, 2606, 1, 1, 5);
-- Monster 106 : Crackler in Crackler Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (243, 106, 1, 1, 5);
-- Monster 293 : Plain Crackler in Crackler Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (243, 293, 1, 1, 5);
-- Monster 483 : Crackrock in Crackler Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (243, 483, 1, 1, 5);
-- Monster 669 : Legendary Crackler in Crackler Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (243, 669, 1, 1, 5);
-- Monster 826 : Eroded Kirball in Crackler Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (243, 826, 1, 1, 5);
-- Monster 746 : Brown Warko in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 746, 1, 1, 5);
-- Monster 747 : Dok Alako in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 747, 1, 1, 5);
-- Monster 751 : Indigo Koalak in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 751, 1, 1, 5);
-- Monster 752 : Coco Koalak in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 752, 1, 1, 5);
-- Monster 753 : Morello Cherry Koalak in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 753, 1, 1, 5);
-- Monster 754 : Pippin Koalak in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 754, 1, 1, 5);
-- Monster 755 : Mama Koalak in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 755, 1, 1, 5);
-- Monster 763 : Drakoalak in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 763, 1, 1, 5);
-- Monster 2306 : Warko the Inky in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 2306, 0.1, 1, 5);
-- Monster 2442 : Dokterwho the Tardisporter in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 2442, 0.1, 1, 5);
-- Monster 2462 : Drakoamax the Mad in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 2462, 0.1, 1, 5);
-- Monster 2509 : Crackoalak the Blonde in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 2509, 0.1, 1, 5);
-- Monster 2513 : Jackoalak the Ripper in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 2513, 0.1, 1, 5);
-- Monster 2516 : Koaly the Fiddler in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 2516, 0.1, 1, 5);
-- Monster 2518 : Snapoalak the Redhead in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 2518, 0.1, 1, 5);
-- Monster 2528 : Mamankalak the Bibliomaniac in Wild Canyon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (253, 2528, 0.1, 1, 5);
-- Monster 673 : Emeralda in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 673, 1, 1, 5);
-- Monster 674 : Emerald Doll in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 674, 1, 1, 5);
-- Monster 675 : Sapphira in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 675, 1, 1, 5);
-- Monster 676 : Starving Doll in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 676, 1, 1, 5);
-- Monster 677 : Ruby in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 677, 1, 1, 5);
-- Monster 681 : Diamondine in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 681, 1, 1, 5);
-- Monster 744 : Bloody Koalak in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 744, 1, 1, 5);
-- Monster 745 : Purple Warko in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 745, 1, 1, 5);
-- Monster 747 : Dok Alako in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 747, 1, 1, 5);
-- Monster 749 : Koalak Warrior in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 749, 1, 1, 5);
-- Monster 751 : Indigo Koalak in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 751, 1, 1, 5);
-- Monster 752 : Coco Koalak in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 752, 1, 1, 5);
-- Monster 753 : Morello Cherry Koalak in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 753, 1, 1, 5);
-- Monster 754 : Pippin Koalak in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 754, 1, 1, 5);
-- Monster 755 : Mama Koalak in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 755, 1, 1, 5);
-- Monster 756 : Wild Koalak in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 756, 1, 1, 5);
-- Monster 780 : Skeunk in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 780, 1, 1, 5);
-- Monster 783 : Reapalak in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 783, 1, 1, 5);
-- Monster 784 : Piralak in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 784, 1, 1, 5);
-- Monster 785 : Koalak Forester in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 785, 1, 1, 5);
-- Monster 786 : Fisheralak in Skeunk's Hideout
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (254, 786, 1, 1, 5);
-- Monster 744 : Bloody Koalak in Agony V'Helley
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (275, 744, 1, 1, 5);
-- Monster 745 : Purple Warko in Agony V'Helley
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (275, 745, 1, 1, 5);
-- Monster 749 : Koalak Warrior in Agony V'Helley
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (275, 749, 1, 1, 5);
-- Monster 756 : Wild Koalak in Agony V'Helley
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (275, 756, 1, 1, 5);
-- Monster 783 : Reapalak in Agony V'Helley
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (275, 783, 1, 1, 5);
-- Monster 2307 : Worka the Willful in Agony V'Helley
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (275, 2307, 0.1, 1, 5);
-- Monster 2497 : Chukoalak the Norris in Agony V'Helley
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (275, 2497, 0.1, 1, 5);
-- Monster 2514 : Koalsen the Similar in Agony V'Helley
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (275, 2514, 0.1, 1, 5);
-- Monster 2519 : Koaldman the Garish in Agony V'Helley
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (275, 2519, 0.1, 1, 5);
-- Monster 2567 : Ryukualak the Bored in Agony V'Helley
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (275, 2567, 0.1, 1, 5);
-- Monster 62 : Karne Rider in The Goblin Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (276, 62, 1, 1, 5);
-- Monster 178 : Goblin in The Goblin Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (276, 178, 1, 1, 5);
-- Monster 2430 : Karnyona the Rider in The Goblin Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (276, 2430, 0.1, 1, 5);
-- Monster 2494 : Goblimp the Bis Kit in The Goblin Camp
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (276, 2494, 0.1, 1, 5);
-- Monster 53 : Bwork Magus in Bwork Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (277, 53, 1, 1, 5);
-- Monster 74 : Bwork Archer in Bwork Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (277, 74, 1, 1, 5);
-- Monster 876 : Bwork in Bwork Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (277, 876, 1, 1, 5);
-- Monster 2416 : Bworak the Bohemian in Bwork Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (277, 2416, 0.1, 1, 5);
-- Monster 2417 : Blorko the Colourful in Bwork Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (277, 2417, 0.1, 1, 5);
-- Monster 2418 : Bworkoder the Mazter in Bwork Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (277, 2418, 0.1, 1, 5);
-- Monster 101 : Gobball in Amakna Castle Gobball Breeding Station
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (278, 101, 1, 1, 5);
-- Monster 134 : White Gobbly in Amakna Castle Gobball Breeding Station
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (278, 134, 1, 1, 5);
-- Monster 149 : Black Gobbly in Amakna Castle Gobball Breeding Station
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (278, 149, 1, 1, 5);
-- Monster 2315 : Gobbach the Contrapuntaler in Amakna Castle Gobball Breeding Station
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (278, 2315, 0.1, 1, 5);
-- Monster 2316 : Gobballyhoo the Noisy in Amakna Castle Gobball Breeding Station
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (278, 2316, 0.1, 1, 5);
-- Monster 2317 : Gobballad the Romantic in Amakna Castle Gobball Breeding Station
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (278, 2317, 0.1, 1, 5);
-- Monster 48 : Wild Sunflower in Bonta Pasture
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (279, 48, 1, 1, 5);
-- Monster 59 : Mush Mush in Bonta Pasture
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (279, 59, 1, 1, 5);
-- Monster 371 : Fungi Master in Bonta Pasture
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (279, 371, 1, 1, 5);
-- Monster 2323 : Matmushmush the Flasher in Bonta Pasture
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (279, 2323, 0.1, 1, 5);
-- Monster 2358 : Hunflower the Sinful in Bonta Pasture
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (279, 2358, 0.1, 1, 5);
-- Monster 2359 : Fung Ku the Master in Bonta Pasture
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (279, 2359, 0.1, 1, 5);
-- Monster 118 : Dark Miner in Brakmar City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (280, 118, 1, 1, 5);
-- Monster 119 : Dark Smith in Brakmar City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (280, 119, 1, 1, 5);
-- Monster 153 : Dark Baker in Brakmar City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (280, 153, 1, 1, 5);
-- Monster 281 : Crow in Brakmar City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (280, 281, 1, 1, 5);
-- Monster 298 : Scurvion in Brakmar City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (280, 298, 1, 1, 5);
-- Monster 397 : Furious Whitish Fang in Brakmar City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (280, 397, 1, 1, 5);
-- Monster 2318 : Bakeraider the Tomb in Brakmar City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (280, 2318, 0.1, 1, 5);
-- Monster 2328 : Smitherz the Licker in Brakmar City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (280, 2328, 0.1, 1, 5);
-- Monster 2335 : Minoskittle the Coloured in Brakmar City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (280, 2335, 0.1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in Brakmar City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (280, 2437, 0.1, 1, 5);
-- Monster 2440 : Lupisnockio the Woodwolf in Brakmar City Walls
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (280, 2440, 0.1, 1, 5);
-- Monster 53 : Bwork Magus in Bwork Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (284, 53, 1, 1, 5);
-- Monster 74 : Bwork Archer in Bwork Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (284, 74, 1, 1, 5);
-- Monster 791 : Tamed Trool in Bwork Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (284, 791, 1, 1, 5);
-- Monster 792 : Bworkette in Bwork Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (284, 792, 1, 1, 5);
-- Monster 876 : Bwork in Bwork Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (284, 876, 1, 1, 5);
-- Monster 2680 : Swimp the Simple in Bwork Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (284, 2680, 1, 1, 5);
-- Monster 194 : Red Scaraleaf in Scaraleaf Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (297, 194, 1, 1, 5);
-- Monster 198 : Blue Scaraleaf in Scaraleaf Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (297, 198, 1, 1, 5);
-- Monster 240 : Green Scaraleaf in Scaraleaf Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (297, 240, 1, 1, 5);
-- Monster 241 : White Scaraleaf in Scaraleaf Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (297, 241, 1, 1, 5);
-- Monster 795 : Black Scaraleaf in Scaraleaf Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (297, 795, 1, 1, 5);
-- Monster 797 : Golden Scarabugly in Scaraleaf Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (297, 797, 1, 1, 5);
-- Monster 798 : Immature Scaraleaf in Scaraleaf Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (297, 798, 1, 1, 5);
-- Monster 48 : Wild Sunflower in Field Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (306, 48, 1, 1, 5);
-- Monster 59 : Mush Mush in Field Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (306, 59, 1, 1, 5);
-- Monster 61 : Moskito in Field Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (306, 61, 1, 1, 5);
-- Monster 78 : Demonic Rose in Field Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (306, 78, 1, 1, 5);
-- Monster 79 : Evil Dandelion in Field Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (306, 79, 1, 1, 5);
-- Monster 799 : Famished Sunflower in Field Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (306, 799, 1, 1, 5);
-- Monster 848 : Black Dragoss in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 848, 1, 1, 5);
-- Monster 853 : Dreggon Warrior in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 853, 1, 1, 5);
-- Monster 855 : Dragostess in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 855, 1, 1, 5);
-- Monster 858 : Alert Black Dragoss in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 858, 1, 1, 5);
-- Monster 862 : Flying Dreggon in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 862, 1, 1, 5);
-- Monster 877 : Aqualikros the Merciless in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 877, 1, 1, 5);
-- Monster 878 : Alert White Dragoss in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 878, 1, 1, 5);
-- Monster 879 : Alert Golden Dragoss in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 879, 1, 1, 5);
-- Monster 884 : Sapphire Dragoss in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 884, 1, 1, 5);
-- Monster 885 : White Dragoss in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 885, 1, 1, 5);
-- Monster 886 : Golden Dragoss in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 886, 1, 1, 5);
-- Monster 902 : Ignirkocropos the Famished in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 902, 1, 1, 5);
-- Monster 903 : Terraburkahl the Perfidious in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 903, 1, 1, 5);
-- Monster 904 : Aerogoburius the Malicious in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 904, 1, 1, 5);
-- Monster 905 : Alert Sapphire Dragoss in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 905, 1, 1, 5);
-- Monster 2452 : Dragory the Violent in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 2452, 0.1, 1, 5);
-- Monster 2453 : Dragaustin the Power in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 2453, 0.1, 1, 5);
-- Monster 2454 : Dragorse the Wild in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 2454, 0.1, 1, 5);
-- Monster 2455 : Dragostino the Tiny in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 2455, 0.1, 1, 5);
-- Monster 2456 : Draigovsky the SocalledSwan in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 2456, 0.1, 1, 5);
-- Monster 2457 : Dragospel the Black in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 2457, 0.1, 1, 5);
-- Monster 2458 : Draghouse the Cynical in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 2458, 0.1, 1, 5);
-- Monster 2459 : Dragoskovit the Barefoot in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 2459, 0.1, 1, 5);
-- Monster 2460 : Dragossiper the Nag in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 2460, 0.1, 1, 5);
-- Monster 2461 : Dragangora the Softy in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 2461, 0.1, 1, 5);
-- Monster 2464 : Dregguantico the Trainer in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 2464, 0.1, 1, 5);
-- Monster 2795 : Sauroshell in The Dreggons' Sanctuary
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (314, 2795, 1, 1, 5);
-- Monster 75 : Immature Golden Dreggon in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 75, 1, 1, 5);
-- Monster 76 : White Dreggon in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 76, 1, 1, 5);
-- Monster 82 : Immature Black Dreggon in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 82, 1, 1, 5);
-- Monster 87 : Golden Dreggon in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 87, 1, 1, 5);
-- Monster 88 : Sapphire Dreggon in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 88, 1, 1, 5);
-- Monster 89 : Black Dreggon in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 89, 1, 1, 5);
-- Monster 90 : Immature White Dreggon in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 90, 1, 1, 5);
-- Monster 91 : Alert Black Dreggon in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 91, 1, 1, 5);
-- Monster 93 : Alert White Dreggon in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 93, 1, 1, 5);
-- Monster 94 : Alert Golden Dreggon in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 94, 1, 1, 5);
-- Monster 95 : Alert Sapphire Dreggon in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 95, 1, 1, 5);
-- Monster 158 : Explosive Shell in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 158, 1, 1, 5);
-- Monster 170 : Immature Sapphire Dreggon in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 170, 1, 1, 5);
-- Monster 494 : Poutch Ingball in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 494, 1, 1, 5);
-- Monster 2367 : Drakween the Cross Dresser in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 2367, 0.1, 1, 5);
-- Monster 2368 : Dreggershween the Tinpanalley in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 2368, 0.1, 1, 5);
-- Monster 2369 : Dreggonzola the Cheesy in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 2369, 0.1, 1, 5);
-- Monster 2372 : Dreggatón the Latino in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 2372, 0.1, 1, 5);
-- Monster 2377 : Dragamemnon the Deadtroyer in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 2377, 0.1, 1, 5);
-- Monster 2445 : Dreggoog the Downunder in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 2445, 0.1, 1, 5);
-- Monster 2446 : Dreggooniz the Adventurous in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 2446, 0.1, 1, 5);
-- Monster 2447 : Dreggrieg the Pianist in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 2447, 0.1, 1, 5);
-- Monster 2448 : Dreggooliz the Macho in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 2448, 0.1, 1, 5);
-- Monster 2449 : Dreggommomm the Chewer in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 2449, 0.1, 1, 5);
-- Monster 2450 : Dreggump the Shrimp in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 2450, 0.1, 1, 5);
-- Monster 2451 : Dragoolash the Stewed in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 2451, 0.1, 1, 5);
-- Monster 2795 : Sauroshell in Dreggon Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (315, 2795, 1, 1, 5);
-- Monster 75 : Immature Golden Dreggon in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 75, 1, 1, 5);
-- Monster 76 : White Dreggon in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 76, 1, 1, 5);
-- Monster 82 : Immature Black Dreggon in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 82, 1, 1, 5);
-- Monster 87 : Golden Dreggon in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 87, 1, 1, 5);
-- Monster 88 : Sapphire Dreggon in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 88, 1, 1, 5);
-- Monster 89 : Black Dreggon in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 89, 1, 1, 5);
-- Monster 90 : Immature White Dreggon in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 90, 1, 1, 5);
-- Monster 91 : Alert Black Dreggon in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 91, 1, 1, 5);
-- Monster 93 : Alert White Dreggon in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 93, 1, 1, 5);
-- Monster 94 : Alert Golden Dreggon in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 94, 1, 1, 5);
-- Monster 95 : Alert Sapphire Dreggon in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 95, 1, 1, 5);
-- Monster 158 : Explosive Shell in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 158, 1, 1, 5);
-- Monster 170 : Immature Sapphire Dreggon in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 170, 1, 1, 5);
-- Monster 892 : Terrakubiack the Warrior in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 892, 1, 1, 5);
-- Monster 893 : Ignilicrobur the Warrior in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 893, 1, 1, 5);
-- Monster 894 : Aeroktor the Warrior in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 894, 1, 1, 5);
-- Monster 895 : Aquabralak the Warrior in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 895, 1, 1, 5);
-- Monster 2367 : Drakween the Cross Dresser in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 2367, 0.1, 1, 5);
-- Monster 2368 : Dreggershween the Tinpanalley in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 2368, 0.1, 1, 5);
-- Monster 2369 : Dreggonzola the Cheesy in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 2369, 0.1, 1, 5);
-- Monster 2372 : Dreggatón the Latino in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 2372, 0.1, 1, 5);
-- Monster 2377 : Dragamemnon the Deadtroyer in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 2377, 0.1, 1, 5);
-- Monster 2445 : Dreggoog the Downunder in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 2445, 0.1, 1, 5);
-- Monster 2446 : Dreggooniz the Adventurous in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 2446, 0.1, 1, 5);
-- Monster 2447 : Dreggrieg the Pianist in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 2447, 0.1, 1, 5);
-- Monster 2448 : Dreggooliz the Macho in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 2448, 0.1, 1, 5);
-- Monster 2449 : Dreggommomm the Chewer in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 2449, 0.1, 1, 5);
-- Monster 2450 : Dreggump the Shrimp in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 2450, 0.1, 1, 5);
-- Monster 2451 : Dragoolash the Stewed in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 2451, 0.1, 1, 5);
-- Monster 2795 : Sauroshell in Dreggon Tunnels
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (316, 2795, 1, 1, 5);
-- Monster 668 : Minokid in Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (319, 668, 1, 1, 5);
-- Monster 829 : Quetsnakiatl in Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (319, 829, 1, 1, 5);
-- Monster 830 : Scaratos in Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (319, 830, 1, 1, 5);
-- Monster 831 : Mumminotor in Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (319, 831, 1, 1, 5);
-- Monster 832 : Deminoball in Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (319, 832, 1, 1, 5);
-- Monster 2287 : Quetnin the Fictional in Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (319, 2287, 0.1, 1, 5);
-- Monster 2568 : Minoknok the Visitor in Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (319, 2568, 0.1, 1, 5);
-- Monster 2615 : Scaraheath the Hanger in Labyrinth of the Minotoror
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (319, 2615, 0.1, 1, 5);
-- Monster 840 : Wintry Kaniger in Kwismas Haven
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (320, 840, 1, 1, 5);
-- Monster 841 : Frozen Tofu in Kwismas Haven
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (320, 841, 1, 1, 5);
-- Monster 842 : Unruly Goblimp in Kwismas Haven
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (320, 842, 1, 1, 5);
-- Monster 849 : Pricky in Kwismas Haven
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (320, 849, 1, 1, 5);
-- Monster 868 : Snowy Tofu in Kwismas Haven
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (320, 868, 1, 1, 5);
-- Monster 870 : Larvicy in Kwismas Haven
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (320, 870, 1, 1, 5);
-- Monster 896 : Kitsou Nakwatus in Kwismas Haven
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (320, 896, 1, 1, 5);
-- Monster 899 : Black Tiwabbitus in Kwismas Haven
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (320, 899, 1, 1, 5);
-- Monster 901 : Kwakus in Kwismas Haven
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (320, 901, 1, 1, 5);
-- Monster 1180 : Pokipik in Kwismas Haven
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (320, 1180, 1, 1, 5);
-- Monster 2622 : Kwismas Whitish Fang in Kwismas Haven
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (320, 2622, 1, 1, 5);
-- Monster 3117 : Kwismas Dragoturkey in Kwismas Haven
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (320, 3117, 1, 1, 5);
-- Monster 840 : Wintry Kaniger in Kwismas Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (321, 840, 1, 1, 5);
-- Monster 841 : Frozen Tofu in Kwismas Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (321, 841, 1, 1, 5);
-- Monster 849 : Pricky in Kwismas Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (321, 849, 1, 1, 5);
-- Monster 868 : Snowy Tofu in Kwismas Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (321, 868, 1, 1, 5);
-- Monster 870 : Larvicy in Kwismas Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (321, 870, 1, 1, 5);
-- Monster 896 : Kitsou Nakwatus in Kwismas Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (321, 896, 1, 1, 5);
-- Monster 899 : Black Tiwabbitus in Kwismas Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (321, 899, 1, 1, 5);
-- Monster 901 : Kwakus in Kwismas Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (321, 901, 1, 1, 5);
-- Monster 1179 : Itzting in Kwismas Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (321, 1179, 1, 1, 5);
-- Monster 1180 : Pokipik in Kwismas Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (321, 1180, 1, 1, 5);
-- Monster 848 : Black Dragoss in Dreggon Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (325, 848, 1, 1, 5);
-- Monster 853 : Dreggon Warrior in Dreggon Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (325, 853, 1, 1, 5);
-- Monster 854 : Crocabulia in Dreggon Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (325, 854, 1, 1, 5);
-- Monster 855 : Dragostess in Dreggon Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (325, 855, 1, 1, 5);
-- Monster 858 : Alert Black Dragoss in Dreggon Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (325, 858, 1, 1, 5);
-- Monster 862 : Flying Dreggon in Dreggon Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (325, 862, 1, 1, 5);
-- Monster 878 : Alert White Dragoss in Dreggon Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (325, 878, 1, 1, 5);
-- Monster 879 : Alert Golden Dragoss in Dreggon Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (325, 879, 1, 1, 5);
-- Monster 884 : Sapphire Dragoss in Dreggon Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (325, 884, 1, 1, 5);
-- Monster 885 : White Dragoss in Dreggon Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (325, 885, 1, 1, 5);
-- Monster 886 : Golden Dragoss in Dreggon Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (325, 886, 1, 1, 5);
-- Monster 905 : Alert Sapphire Dragoss in Dreggon Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (325, 905, 1, 1, 5);
-- Monster 63 : Crab in Cania Bay
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (334, 63, 1, 1, 5);
-- Monster 920 : Orange Snapper in Cania Bay
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (334, 920, 1, 1, 5);
-- Monster 921 : Blue Snapper in Cania Bay
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (334, 921, 1, 1, 5);
-- Monster 922 : White Snapper in Cania Bay
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (334, 922, 1, 1, 5);
-- Monster 923 : Green Snapper in Cania Bay
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (334, 923, 1, 1, 5);
-- Monster 924 : Kloon Snapper in Cania Bay
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (334, 924, 1, 1, 5);
-- Monster 926 : Raul Mops in Cania Bay
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (334, 926, 1, 1, 5);
-- Monster 927 : Starfish Trooper in Cania Bay
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (334, 927, 1, 1, 5);
-- Monster 2272 : Snappy the Fishfrier in Cania Bay
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (334, 2272, 0.1, 1, 5);
-- Monster 2327 : Krabaoly the Patient in Cania Bay
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (334, 2327, 0.1, 1, 5);
-- Monster 2338 : Snappu the Shopkeep in Cania Bay
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (334, 2338, 0.1, 1, 5);
-- Monster 2339 : Snappster the Sued in Cania Bay
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (334, 2339, 0.1, 1, 5);
-- Monster 2340 : Snapp the Dragon in Cania Bay
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (334, 2340, 0.1, 1, 5);
-- Monster 2346 : Snapple the Wise in Cania Bay
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (334, 2346, 0.1, 1, 5);
-- Monster 2351 : Raul Modrid the Chulo in Cania Bay
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (334, 2351, 0.1, 1, 5);
-- Monster 63 : Crab in Astrub Rocky Inlet
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (335, 63, 1, 1, 5);
-- Monster 920 : Orange Snapper in Astrub Rocky Inlet
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (335, 920, 1, 1, 5);
-- Monster 921 : Blue Snapper in Astrub Rocky Inlet
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (335, 921, 1, 1, 5);
-- Monster 922 : White Snapper in Astrub Rocky Inlet
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (335, 922, 1, 1, 5);
-- Monster 923 : Green Snapper in Astrub Rocky Inlet
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (335, 923, 1, 1, 5);
-- Monster 927 : Starfish Trooper in Astrub Rocky Inlet
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (335, 927, 1, 1, 5);
-- Monster 2272 : Snappy the Fishfrier in Astrub Rocky Inlet
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (335, 2272, 0.1, 1, 5);
-- Monster 2327 : Krabaoly the Patient in Astrub Rocky Inlet
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (335, 2327, 0.1, 1, 5);
-- Monster 2338 : Snappu the Shopkeep in Astrub Rocky Inlet
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (335, 2338, 0.1, 1, 5);
-- Monster 2340 : Snapp the Dragon in Astrub Rocky Inlet
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (335, 2340, 0.1, 1, 5);
-- Monster 2346 : Snapple the Wise in Astrub Rocky Inlet
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (335, 2346, 0.1, 1, 5);
-- Monster 63 : Crab in Sand Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (336, 63, 1, 1, 5);
-- Monster 920 : Orange Snapper in Sand Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (336, 920, 1, 1, 5);
-- Monster 921 : Blue Snapper in Sand Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (336, 921, 1, 1, 5);
-- Monster 922 : White Snapper in Sand Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (336, 922, 1, 1, 5);
-- Monster 923 : Green Snapper in Sand Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (336, 923, 1, 1, 5);
-- Monster 924 : Kloon Snapper in Sand Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (336, 924, 1, 1, 5);
-- Monster 926 : Raul Mops in Sand Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (336, 926, 1, 1, 5);
-- Monster 927 : Starfish Trooper in Sand Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (336, 927, 1, 1, 5);
-- Monster 928 : Sponge Mob in Sand Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (336, 928, 1, 1, 5);
-- Monster 938 : Rat Basher in Bonta Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (337, 938, 1, 1, 5);
-- Monster 940 : White Rat in Bonta Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (337, 940, 1, 1, 5);
-- Monster 941 : Rat Pakk in Bonta Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (337, 941, 1, 1, 5);
-- Monster 942 : Rat Rah in Bonta Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (337, 942, 1, 1, 5);
-- Monster 935 : Rat Suenami in Brakmar Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (338, 935, 1, 1, 5);
-- Monster 936 : Rat Tchet in Brakmar Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (338, 936, 1, 1, 5);
-- Monster 937 : Rat Bag in Brakmar Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (338, 937, 1, 1, 5);
-- Monster 939 : Black Rat in Brakmar Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (338, 939, 1, 1, 5);
-- Monster 939 : Black Rat in Amakna Castle Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (339, 939, 1, 1, 5);
-- Monster 940 : White Rat in Amakna Castle Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (339, 940, 1, 1, 5);
-- Monster 943 : Sphincter Cell in Amakna Castle Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (339, 943, 1, 1, 5);
-- Monster 3345 : Ratworm in Amakna Castle Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (339, 3345, 1, 1, 5);
-- Monster 3346 : Ratfink in Amakna Castle Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (339, 3346, 1, 1, 5);
-- Monster 3347 : Packrat in Amakna Castle Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (339, 3347, 1, 1, 5);
-- Monster 3348 : Rugrat in Amakna Castle Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (339, 3348, 1, 1, 5);
-- Monster 3349 : Ratter in Amakna Castle Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (339, 3349, 1, 1, 5);
-- Monster 3350 : Brat in Amakna Castle Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (339, 3350, 1, 1, 5);
-- Monster 3351 : Riffrat in Amakna Castle Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (339, 3351, 1, 1, 5);
-- Monster 3352 : Prat in Amakna Castle Rat Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (339, 3352, 1, 1, 5);
-- Monster 998 : Daredevil Crab in Canals
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (442, 998, 1, 1, 5);
-- Monster 3270 : Blue Lilpiwi in Canals
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (442, 3270, 1, 1, 5);
-- Monster 3271 : Yellow Lilpiwi in Canals
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (442, 3271, 1, 1, 5);
-- Monster 3272 : Pink Lilpiwi in Canals
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (442, 3272, 1, 1, 5);
-- Monster 3273 : Red Lilpiwi in Canals
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (442, 3273, 1, 1, 5);
-- Monster 3274 : Green Lilpiwi in Canals
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (442, 3274, 1, 1, 5);
-- Monster 3275 : Purple Lilpiwi in Canals
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (442, 3275, 1, 1, 5);
-- Monster 974 : Fearful Moskito in Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (443, 974, 1, 1, 5);
-- Monster 975 : Immature Blue Larva in Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (443, 975, 1, 1, 5);
-- Monster 976 : Immature Orange Larva in Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (443, 976, 1, 1, 5);
-- Monster 977 : Immature Green Larva in Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (443, 977, 1, 1, 5);
-- Monster 978 : Vulnerable Mush Mush in Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (443, 978, 1, 1, 5);
-- Monster 982 : Young Arachnee in Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (443, 982, 1, 1, 5);
-- Monster 983 : Young Boar in Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (443, 983, 1, 1, 5);
-- Monster 970 : Small Tofu in Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (444, 970, 1, 1, 5);
-- Monster 979 : Frightened Evil Dandelion in Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (444, 979, 1, 1, 5);
-- Monster 980 : Fragile Demonic Rose in Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (444, 980, 1, 1, 5);
-- Monster 981 : Small Wild Sunflower in Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (444, 981, 1, 1, 5);
-- Monster 1003 : Training Scarecrow in Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (444, 1003, 1, 1, 5);
-- Monster 3270 : Blue Lilpiwi in Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (444, 3270, 1, 1, 5);
-- Monster 3271 : Yellow Lilpiwi in Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (444, 3271, 1, 1, 5);
-- Monster 3272 : Pink Lilpiwi in Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (444, 3272, 1, 1, 5);
-- Monster 3273 : Red Lilpiwi in Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (444, 3273, 1, 1, 5);
-- Monster 3274 : Green Lilpiwi in Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (444, 3274, 1, 1, 5);
-- Monster 3275 : Purple Lilpiwi in Fields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (444, 3275, 1, 1, 5);
-- Monster 971 : Little Gobball in Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (445, 971, 1, 1, 5);
-- Monster 972 : Young White Gobbly in Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (445, 972, 1, 1, 5);
-- Monster 973 : Young Black Gobbly in Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (445, 973, 1, 1, 5);
-- Monster 984 : Small Gobball War Chief in Meadow
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (445, 984, 1, 1, 5);
-- Monster 970 : Small Tofu in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 970, 1, 1, 5);
-- Monster 971 : Little Gobball in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 971, 1, 1, 5);
-- Monster 972 : Young White Gobbly in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 972, 1, 1, 5);
-- Monster 973 : Young Black Gobbly in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 973, 1, 1, 5);
-- Monster 974 : Fearful Moskito in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 974, 1, 1, 5);
-- Monster 975 : Immature Blue Larva in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 975, 1, 1, 5);
-- Monster 976 : Immature Orange Larva in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 976, 1, 1, 5);
-- Monster 977 : Immature Green Larva in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 977, 1, 1, 5);
-- Monster 978 : Vulnerable Mush Mush in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 978, 1, 1, 5);
-- Monster 980 : Fragile Demonic Rose in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 980, 1, 1, 5);
-- Monster 981 : Small Wild Sunflower in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 981, 1, 1, 5);
-- Monster 982 : Young Arachnee in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 982, 1, 1, 5);
-- Monster 983 : Young Boar in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 983, 1, 1, 5);
-- Monster 984 : Small Gobball War Chief in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 984, 1, 1, 5);
-- Monster 996 : Prepubescent Chafer in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 996, 1, 1, 5);
-- Monster 998 : Daredevil Crab in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 998, 1, 1, 5);
-- Monster 1001 : Snoowolf in Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (447, 1001, 1, 1, 5);
-- Monster 475 : Sick Grossewer Milirat in Inn
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (448, 475, 1, 1, 5);
-- Monster 3270 : Blue Lilpiwi in Inn
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (448, 3270, 1, 1, 5);
-- Monster 3271 : Yellow Lilpiwi in Inn
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (448, 3271, 1, 1, 5);
-- Monster 3272 : Pink Lilpiwi in Inn
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (448, 3272, 1, 1, 5);
-- Monster 3273 : Red Lilpiwi in Inn
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (448, 3273, 1, 1, 5);
-- Monster 3274 : Green Lilpiwi in Inn
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (448, 3274, 1, 1, 5);
-- Monster 3275 : Purple Lilpiwi in Inn
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (448, 3275, 1, 1, 5);
-- Monster 996 : Prepubescent Chafer in Cemetery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (449, 996, 1, 1, 5);
-- Monster 972 : Young White Gobbly in Exit of the Temple
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (450, 972, 1, 1, 5);
-- Monster 973 : Young Black Gobbly in Exit of the Temple
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (450, 973, 1, 1, 5);
-- Monster 974 : Fearful Moskito in Exit of the Temple
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (450, 974, 1, 1, 5);
-- Monster 982 : Young Arachnee in Exit of the Temple
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (450, 982, 1, 1, 5);
-- Monster 208 : Tikokoko in Castaway Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (451, 208, 1, 1, 5);
-- Monster 209 : Kokoko in Castaway Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (451, 209, 1, 1, 5);
-- Monster 218 : Kokonut in Castaway Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (451, 218, 1, 1, 5);
-- Monster 1078 : Flib's Cursed Chest in Castaway Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (451, 1078, 1, 1, 5);
-- Monster 1229 : Drunken Boomba in Castaway Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (451, 1229, 1, 1, 5);
-- Monster 1230 : Beginner Cannon Dorf in Castaway Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (451, 1230, 1, 1, 5);
-- Monster 1231 : Lonely Hazwonarm in Castaway Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (451, 1231, 1, 1, 5);
-- Monster 2292 : Eskoko the Baron in Castaway Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (451, 2292, 0.1, 1, 5);
-- Monster 2541 : Kokonan the Talker in Castaway Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (451, 2541, 0.1, 1, 5);
-- Monster 2573 : Misskokoko the Channel in Castaway Island
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (451, 2573, 0.1, 1, 5);
-- Monster 1229 : Drunken Boomba in Sea
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (452, 1229, 1, 1, 5);
-- Monster 1230 : Beginner Cannon Dorf in Sea
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (452, 1230, 1, 1, 5);
-- Monster 1231 : Lonely Hazwonarm in Sea
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (452, 1231, 1, 1, 5);
-- Monster 1022 : Coralator in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 1022, 1, 1, 5);
-- Monster 1060 : Kurasso Craboral in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 1060, 1, 1, 5);
-- Monster 1061 : Mahlibuh Craboral in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 1061, 1, 1, 5);
-- Monster 1062 : Passaoh Craboral in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 1062, 1, 1, 5);
-- Monster 1063 : Mojeeto Craboral in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 1063, 1, 1, 5);
-- Monster 1064 : Kurasso Palmflower in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 1064, 1, 1, 5);
-- Monster 1065 : Mahlibuh Palmflower in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 1065, 1, 1, 5);
-- Monster 1066 : Passaoh Palmflower in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 1066, 1, 1, 5);
-- Monster 1067 : Mojeeto Palmflower in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 1067, 1, 1, 5);
-- Monster 2555 : Kojaklator the Lollipoper in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 2555, 0.1, 1, 5);
-- Monster 2557 : Crabartanian the Allforone in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 2557, 0.1, 1, 5);
-- Monster 2558 : Craborthos the All in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 2558, 0.1, 1, 5);
-- Monster 2559 : Crabaramis the One in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 2559, 0.1, 1, 5);
-- Monster 2560 : Crabathos the For in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 2560, 0.1, 1, 5);
-- Monster 2581 : Palmella the Hefty in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 2581, 0.1, 1, 5);
-- Monster 2582 : Palmpilot the Yuppie in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 2582, 0.1, 1, 5);
-- Monster 2583 : Palmoleaf the Greasy in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 2583, 0.1, 1, 5);
-- Monster 2584 : Naypalm the Herbivorous in Coral Beach
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (453, 2584, 0.1, 1, 5);
-- Monster 1019 : Plain Pikoko in Grassy Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (454, 1019, 1, 1, 5);
-- Monster 1025 : Polished Crackler in Grassy Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (454, 1025, 1, 1, 5);
-- Monster 1026 : Polished Crackrock in Grassy Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (454, 1026, 1, 1, 5);
-- Monster 1068 : Mufafah in Grassy Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (454, 1068, 1, 1, 5);
-- Monster 1069 : Kido in Grassy Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (454, 1069, 1, 1, 5);
-- Monster 1070 : Kilibriss in Grassy Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (454, 1070, 1, 1, 5);
-- Monster 2547 : Prikoko the Witless in Grassy Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (454, 2547, 0.1, 1, 5);
-- Monster 2556 : Crackrodilrock the Helltune in Grassy Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (454, 2556, 0.1, 1, 5);
-- Monster 2561 : Crackedral the Majestic in Grassy Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (454, 2561, 0.1, 1, 5);
-- Monster 2571 : Kidodo the Extinct in Grassy Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (454, 2571, 0.1, 1, 5);
-- Monster 2572 : Killua the Assassin in Grassy Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (454, 2572, 0.1, 1, 5);
-- Monster 2580 : Mufavabeenz the Cannibal in Grassy Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (454, 2580, 0.1, 1, 5);
-- Monster 1029 : Floramor in Dark Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (455, 1029, 1, 1, 5);
-- Monster 1041 : Dark Pikoko in Dark Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (455, 1041, 1, 1, 5);
-- Monster 1073 : Rotaflor in Dark Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (455, 1073, 1, 1, 5);
-- Monster 1074 : Gwass in Dark Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (455, 1074, 1, 1, 5);
-- Monster 1075 : Barkritter in Dark Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (455, 1075, 1, 1, 5);
-- Monster 1076 : Warguerite in Dark Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (455, 1076, 1, 1, 5);
-- Monster 1077 : Dark Treeckler in Dark Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (455, 1077, 1, 1, 5);
-- Monster 2360 : Barkricrac the Unsteady in Dark Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (455, 2360, 0.1, 1, 5);
-- Monster 2544 : Treekness the Dark in Dark Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (455, 2544, 0.1, 1, 5);
-- Monster 2546 : Pikhoven the Deaf in Dark Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (455, 2546, 0.1, 1, 5);
-- Monster 2550 : Floratio the Investigator in Dark Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (455, 2550, 0.1, 1, 5);
-- Monster 2554 : Warazpacho the Cherrilla in Dark Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (455, 2554, 0.1, 1, 5);
-- Monster 2565 : Floramodovar the Stoned in Dark Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (455, 2565, 0.1, 1, 5);
-- Monster 2579 : Supergwass the Free in Dark Jungle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (455, 2579, 0.1, 1, 5);
-- Monster 1057 : Mopy King in Bottomless Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (457, 1057, 1, 1, 5);
-- Monster 1058 : Mopeat in Bottomless Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (457, 1058, 1, 1, 5);
-- Monster 1059 : Miremop in Bottomless Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (457, 1059, 1, 1, 5);
-- Monster 1096 : Boggedown Ouassingue in Bottomless Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (457, 1096, 1, 1, 5);
-- Monster 2361 : Mopfeet the Circular in Bottomless Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (457, 2361, 0.1, 1, 5);
-- Monster 2363 : Mopidyk the Mire in Bottomless Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (457, 2363, 0.1, 1, 5);
-- Monster 2575 : Ouassingiam the Tyrant in Bottomless Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (457, 2575, 0.1, 1, 5);
-- Monster 2614 : Roy the Rover in Bottomless Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (457, 2614, 0.1, 1, 5);
-- Monster 1020 : Air Pikoko in Kimbo's Canopy
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (459, 1020, 1, 1, 5);
-- Monster 1043 : Cheeken in Kimbo's Canopy
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (459, 1043, 1, 1, 5);
-- Monster 1044 : Snailmet in Kimbo's Canopy
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (459, 1044, 1, 1, 5);
-- Monster 1045 : Kimbo in Kimbo's Canopy
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (459, 1045, 1, 1, 5);
-- Monster 1046 : Moopet in Kimbo's Canopy
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (459, 1046, 1, 1, 5);
-- Monster 1047 : Light Treeckler in Kimbo's Canopy
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (459, 1047, 1, 1, 5);
-- Monster 1022 : Coralator in Grotto Hesque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (460, 1022, 1, 1, 5);
-- Monster 1027 : Great Coralator in Grotto Hesque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (460, 1027, 1, 1, 5);
-- Monster 1060 : Kurasso Craboral in Grotto Hesque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (460, 1060, 1, 1, 5);
-- Monster 1061 : Mahlibuh Craboral in Grotto Hesque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (460, 1061, 1, 1, 5);
-- Monster 1062 : Passaoh Craboral in Grotto Hesque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (460, 1062, 1, 1, 5);
-- Monster 1063 : Mojeeto Craboral in Grotto Hesque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (460, 1063, 1, 1, 5);
-- Monster 1064 : Kurasso Palmflower in Grotto Hesque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (460, 1064, 1, 1, 5);
-- Monster 1065 : Mahlibuh Palmflower in Grotto Hesque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (460, 1065, 1, 1, 5);
-- Monster 1066 : Passaoh Palmflower in Grotto Hesque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (460, 1066, 1, 1, 5);
-- Monster 1067 : Mojeeto Palmflower in Grotto Hesque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (460, 1067, 1, 1, 5);
-- Monster 1048 : Sparo in Otomai's Ark
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (461, 1048, 1, 1, 5);
-- Monster 1049 : Barbrossa in Otomai's Ark
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (461, 1049, 1, 1, 5);
-- Monster 1050 : Ze Flib in Otomai's Ark
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (461, 1050, 1, 1, 5);
-- Monster 2545 : Barbrosskam the Chief in Otomai's Ark
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (461, 2545, 0.1, 1, 5);
-- Monster 2588 : Sparodi the Python in Otomai's Ark
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (461, 2588, 0.1, 1, 5);
-- Monster 1029 : Floramor in Floramor's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (462, 1029, 1, 1, 5);
-- Monster 1041 : Dark Pikoko in Floramor's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (462, 1041, 1, 1, 5);
-- Monster 1074 : Gwass in Floramor's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (462, 1074, 1, 1, 5);
-- Monster 1075 : Barkritter in Floramor's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (462, 1075, 1, 1, 5);
-- Monster 1076 : Warguerite in Floramor's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (462, 1076, 1, 1, 5);
-- Monster 1077 : Dark Treeckler in Floramor's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (462, 1077, 1, 1, 5);
-- Monster 1029 : Floramor in The Tynril Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (463, 1029, 1, 1, 5);
-- Monster 1041 : Dark Pikoko in The Tynril Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (463, 1041, 1, 1, 5);
-- Monster 1072 : Dismayed Tynril in The Tynril Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (463, 1072, 1, 1, 5);
-- Monster 1073 : Rotaflor in The Tynril Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (463, 1073, 1, 1, 5);
-- Monster 1074 : Gwass in The Tynril Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (463, 1074, 1, 1, 5);
-- Monster 1075 : Barkritter in The Tynril Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (463, 1075, 1, 1, 5);
-- Monster 1076 : Warguerite in The Tynril Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (463, 1076, 1, 1, 5);
-- Monster 1077 : Dark Treeckler in The Tynril Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (463, 1077, 1, 1, 5);
-- Monster 1085 : Disconcerted Tynril in The Tynril Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (463, 1085, 1, 1, 5);
-- Monster 1086 : Perfidious Tynril in The Tynril Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (463, 1086, 1, 1, 5);
-- Monster 1087 : Stunned Tynril in The Tynril Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (463, 1087, 1, 1, 5);
-- Monster 1020 : Air Pikoko in Tree Keeholo Trunk
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (464, 1020, 1, 1, 5);
-- Monster 1043 : Cheeken in Tree Keeholo Trunk
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (464, 1043, 1, 1, 5);
-- Monster 1044 : Snailmet in Tree Keeholo Trunk
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (464, 1044, 1, 1, 5);
-- Monster 1046 : Moopet in Tree Keeholo Trunk
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (464, 1046, 1, 1, 5);
-- Monster 1047 : Light Treeckler in Tree Keeholo Trunk
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (464, 1047, 1, 1, 5);
-- Monster 2381 : Treekstalbal the Psychic in Tree Keeholo Trunk
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (464, 2381, 0.1, 1, 5);
-- Monster 2382 : Follikoko the Tufted in Tree Keeholo Trunk
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (464, 2382, 0.1, 1, 5);
-- Monster 2383 : Cheech the Pussycat in Tree Keeholo Trunk
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (464, 2383, 0.1, 1, 5);
-- Monster 2570 : Snailmetalika the Garagician in Tree Keeholo Trunk
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (464, 2570, 0.1, 1, 5);
-- Monster 2577 : Moops the Bubbleboy in Tree Keeholo Trunk
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (464, 2577, 0.1, 1, 5);
-- Monster 1052 : Zoth Master in Zoth Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (468, 1052, 1, 1, 5);
-- Monster 1053 : Zoth Girl in Zoth Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (468, 1053, 1, 1, 5);
-- Monster 1054 : Zoth Warrior in Zoth Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (468, 1054, 1, 1, 5);
-- Monster 1055 : Zoth Disciple in Zoth Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (468, 1055, 1, 1, 5);
-- Monster 1056 : Zoth Sergeant in Zoth Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (468, 1056, 1, 1, 5);
-- Monster 2562 : Ezothbeitor the Neighbour in Zoth Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (468, 2562, 0.1, 1, 5);
-- Monster 2566 : Calipzoth the Icy in Zoth Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (468, 2566, 0.1, 1, 5);
-- Monster 2569 : Zigzoth the Indecisive in Zoth Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (468, 2569, 0.1, 1, 5);
-- Monster 2578 : Don Quizothe the Stubborn in Zoth Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (468, 2578, 0.1, 1, 5);
-- Monster 2587 : Zouzoth the Cuddly in Zoth Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (468, 2587, 0.1, 1, 5);
-- Monster 1019 : Plain Pikoko in Bherb's Gully
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (470, 1019, 1, 1, 5);
-- Monster 1025 : Polished Crackler in Bherb's Gully
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (470, 1025, 1, 1, 5);
-- Monster 1026 : Polished Crackrock in Bherb's Gully
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (470, 1026, 1, 1, 5);
-- Monster 1068 : Mufafah in Bherb's Gully
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (470, 1068, 1, 1, 5);
-- Monster 1069 : Kido in Bherb's Gully
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (470, 1069, 1, 1, 5);
-- Monster 1070 : Kilibriss in Bherb's Gully
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (470, 1070, 1, 1, 5);
-- Monster 1071 : Silf the Greater Bherb in Bherb's Gully
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (470, 1071, 1, 1, 5);
-- Monster 1057 : Mopy King in Putrid Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (471, 1057, 1, 1, 5);
-- Monster 1058 : Mopeat in Putrid Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (471, 1058, 1, 1, 5);
-- Monster 1059 : Miremop in Putrid Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (471, 1059, 1, 1, 5);
-- Monster 1096 : Boggedown Ouassingue in Putrid Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (471, 1096, 1, 1, 5);
-- Monster 2361 : Mopfeet the Circular in Putrid Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (471, 2361, 0.1, 1, 5);
-- Monster 2363 : Mopidyk the Mire in Putrid Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (471, 2363, 0.1, 1, 5);
-- Monster 2575 : Ouassingiam the Tyrant in Putrid Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (471, 2575, 0.1, 1, 5);
-- Monster 2614 : Roy the Rover in Putrid Peat Bog
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (471, 2614, 0.1, 1, 5);
-- Monster 1020 : Air Pikoko in Tree Keeholo Foliage
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (472, 1020, 1, 1, 5);
-- Monster 1043 : Cheeken in Tree Keeholo Foliage
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (472, 1043, 1, 1, 5);
-- Monster 1044 : Snailmet in Tree Keeholo Foliage
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (472, 1044, 1, 1, 5);
-- Monster 2382 : Follikoko the Tufted in Tree Keeholo Foliage
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (472, 2382, 0.1, 1, 5);
-- Monster 2383 : Cheech the Pussycat in Tree Keeholo Foliage
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (472, 2383, 0.1, 1, 5);
-- Monster 2570 : Snailmetalika the Garagician in Tree Keeholo Foliage
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (472, 2570, 0.1, 1, 5);
-- Monster 1019 : Plain Pikoko in Hidden Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (473, 1019, 1, 1, 5);
-- Monster 1025 : Polished Crackler in Hidden Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (473, 1025, 1, 1, 5);
-- Monster 1068 : Mufafah in Hidden Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (473, 1068, 1, 1, 5);
-- Monster 1069 : Kido in Hidden Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (473, 1069, 1, 1, 5);
-- Monster 1070 : Kilibriss in Hidden Lab
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (473, 1070, 1, 1, 5);
-- Monster 228 : Boomba in Hold of Otomai's Ark
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (474, 228, 1, 1, 5);
-- Monster 229 : Hazwonarm in Hold of Otomai's Ark
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (474, 229, 1, 1, 5);
-- Monster 231 : Cannon Dorf in Hold of Otomai's Ark
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (474, 231, 1, 1, 5);
-- Monster 1048 : Sparo in Hold of Otomai's Ark
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (474, 1048, 1, 1, 5);
-- Monster 1049 : Barbrossa in Hold of Otomai's Ark
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (474, 1049, 1, 1, 5);
-- Monster 1050 : Ze Flib in Hold of Otomai's Ark
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (474, 1050, 1, 1, 5);
-- Monster 1051 : Gourlo the Terrible in Hold of Otomai's Ark
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (474, 1051, 1, 1, 5);
-- Monster 1109 : Heart of Zoth Village in Zoth Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (477, 1109, 1, 1, 5);
-- Monster 1110 : Heart of Zoth Village in Zoth Prism
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (477, 1110, 1, 1, 5);
-- Monster 1098 : Zoth Disciple in The Zoth Doorkeepers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (478, 1098, 1, 1, 5);
-- Monster 1099 : Zoth Disciple in The Zoth Doorkeepers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (478, 1099, 1, 1, 5);
-- Monster 1100 : Zoth Girl in The Zoth Doorkeepers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (478, 1100, 1, 1, 5);
-- Monster 1101 : Zoth Girl in The Zoth Doorkeepers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (478, 1101, 1, 1, 5);
-- Monster 1102 : Zoth Warrior in The Zoth Doorkeepers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (478, 1102, 1, 1, 5);
-- Monster 1103 : Zoth Warrior in The Zoth Doorkeepers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (478, 1103, 1, 1, 5);
-- Monster 1104 : Zoth Master in The Zoth Doorkeepers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (478, 1104, 1, 1, 5);
-- Monster 1105 : Zoth Master in The Zoth Doorkeepers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (478, 1105, 1, 1, 5);
-- Monster 1106 : Zoth Sergeant in The Zoth Doorkeepers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (478, 1106, 1, 1, 5);
-- Monster 1107 : Zoth Sergeant in The Zoth Doorkeepers
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (478, 1107, 1, 1, 5);
-- Monster 63 : Crab in Kawaii River
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (479, 63, 1, 1, 5);
-- Monster 2327 : Krabaoly the Patient in Kawaii River
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (479, 2327, 0.1, 1, 5);
-- Monster 52 : Arachnee in Low Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (480, 52, 1, 1, 5);
-- Monster 53 : Bwork Magus in Low Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (480, 53, 1, 1, 5);
-- Monster 62 : Karne Rider in Low Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (480, 62, 1, 1, 5);
-- Monster 74 : Bwork Archer in Low Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (480, 74, 1, 1, 5);
-- Monster 106 : Crackler in Low Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (480, 106, 1, 1, 5);
-- Monster 178 : Goblin in Low Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (480, 178, 1, 1, 5);
-- Monster 483 : Crackrock in Low Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (480, 483, 1, 1, 5);
-- Monster 876 : Bwork in Low Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (480, 876, 1, 1, 5);
-- Monster 2326 : Crackrockisree the Tiger in Low Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (480, 2326, 0.1, 1, 5);
-- Monster 2416 : Bworak the Bohemian in Low Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (480, 2416, 0.1, 1, 5);
-- Monster 2417 : Blorko the Colourful in Low Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (480, 2417, 0.1, 1, 5);
-- Monster 2418 : Bworkoder the Mazter in Low Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (480, 2418, 0.1, 1, 5);
-- Monster 2430 : Karnyona the Rider in Low Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (480, 2430, 0.1, 1, 5);
-- Monster 2436 : Jiminicrackler the Conscious in Low Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (480, 2436, 0.1, 1, 5);
-- Monster 2494 : Goblimp the Bis Kit in Low Crackler Mountain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (480, 2494, 0.1, 1, 5);
-- Monster 59 : Mush Mush in Brouce Boulgoure's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (481, 59, 1, 1, 5);
-- Monster 652 : Green Spimush in Brouce Boulgoure's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (481, 652, 1, 1, 5);
-- Monster 653 : Red Spimush in Brouce Boulgoure's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (481, 653, 1, 1, 5);
-- Monster 654 : Blue Spimush in Brouce Boulgoure's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (481, 654, 1, 1, 5);
-- Monster 655 : Brown Spimush in Brouce Boulgoure's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (481, 655, 1, 1, 5);
-- Monster 2323 : Matmushmush the Flasher in Brouce Boulgoure's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (481, 2323, 0.1, 1, 5);
-- Monster 2424 : Spimushty the Smelly in Brouce Boulgoure's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (481, 2424, 0.1, 1, 5);
-- Monster 2425 : Speedmush the Racer in Brouce Boulgoure's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (481, 2425, 0.1, 1, 5);
-- Monster 2426 : Spimushuaia the Traveller in Brouce Boulgoure's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (481, 2426, 0.1, 1, 5);
-- Monster 2427 : Spimushtache the Hairy in Brouce Boulgoure's Clearing
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (481, 2427, 0.1, 1, 5);
-- Monster 103 : Prespic in The Milicluster
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (482, 103, 1, 1, 5);
-- Monster 104 : Boar in The Milicluster
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (482, 104, 1, 1, 5);
-- Monster 159 : Miliboowolf in The Milicluster
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (482, 159, 1, 1, 5);
-- Monster 236 : Purple Piwi in The Milicluster
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (482, 236, 1, 1, 5);
-- Monster 255 : Aggressive Arachnee in The Milicluster
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (482, 255, 1, 1, 5);
-- Monster 489 : Red Piwi in The Milicluster
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (482, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in The Milicluster
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (482, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in The Milicluster
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (482, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in The Milicluster
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (482, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in The Milicluster
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (482, 493, 1, 1, 5);
-- Monster 494 : Poutch Ingball in The Milicluster
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (482, 494, 1, 1, 5);
-- Monster 2333 : Milivanilli the Mime in The Milicluster
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (482, 2333, 0.1, 1, 5);
-- Monster 2349 : Prestreet the Fighter in The Milicluster
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (482, 2349, 0.1, 1, 5);
-- Monster 2353 : Boarnigen the Damasker in The Milicluster
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (482, 2353, 0.1, 1, 5);
-- Monster 2393 : Arachnekros the Aggressive in The Milicluster
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (482, 2393, 0.1, 1, 5);
-- Monster 48 : Wild Sunflower in The Countryside
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (485, 48, 1, 1, 5);
-- Monster 59 : Mush Mush in The Countryside
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (485, 59, 1, 1, 5);
-- Monster 61 : Moskito in The Countryside
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (485, 61, 1, 1, 5);
-- Monster 104 : Boar in The Countryside
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (485, 104, 1, 1, 5);
-- Monster 255 : Aggressive Arachnee in The Countryside
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (485, 255, 1, 1, 5);
-- Monster 2323 : Matmushmush the Flasher in The Countryside
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (485, 2323, 0.1, 1, 5);
-- Monster 2336 : Moskoitus the Interruptor in The Countryside
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (485, 2336, 0.1, 1, 5);
-- Monster 2353 : Boarnigen the Damasker in The Countryside
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (485, 2353, 0.1, 1, 5);
-- Monster 2358 : Hunflower the Sinful in The Countryside
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (485, 2358, 0.1, 1, 5);
-- Monster 2393 : Arachnekros the Aggressive in The Countryside
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (485, 2393, 0.1, 1, 5);
-- Monster 255 : Aggressive Arachnee in Library Basement
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (488, 255, 1, 1, 5);
-- Monster 300 : Kolerat in Library Basement
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (488, 300, 1, 1, 5);
-- Monster 465 : Sick Grossewer Rat in Library Basement
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (488, 465, 1, 1, 5);
-- Monster 63 : Crab in Sufokian Gulf Shoreline
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (490, 63, 1, 1, 5);
-- Monster 954 : Mumussel in Sufokian Gulf Shoreline
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (490, 954, 1, 1, 5);
-- Monster 2327 : Krabaoly the Patient in Sufokian Gulf Shoreline
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (490, 2327, 0.1, 1, 5);
-- Monster 31 : Blue Larva in Larva Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (491, 31, 1, 1, 5);
-- Monster 34 : Green Larva in Larva Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (491, 34, 1, 1, 5);
-- Monster 46 : Orange Larva in Larva Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (491, 46, 1, 1, 5);
-- Monster 412 : Sapphire Larva in Larva Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (491, 412, 1, 1, 5);
-- Monster 413 : Ruby Larva in Larva Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (491, 413, 1, 1, 5);
-- Monster 414 : Emerald Larva in Larva Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (491, 414, 1, 1, 5);
-- Monster 456 : Golden Larva in Larva Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (491, 456, 1, 1, 5);
-- Monster 457 : Shin Larva in Larva Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (491, 457, 1, 1, 5);
-- Monster 3358 : Lone Green Larva in Larva Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (491, 3358, 1, 1, 5);
-- Monster 3359 : Lone Blue Larva in Larva Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (491, 3359, 1, 1, 5);
-- Monster 118 : Dark Miner in Passage to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (492, 118, 1, 1, 5);
-- Monster 119 : Dark Smith in Passage to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (492, 119, 1, 1, 5);
-- Monster 153 : Dark Baker in Passage to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (492, 153, 1, 1, 5);
-- Monster 466 : One-armed Bandit in Passage to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (492, 466, 1, 1, 5);
-- Monster 2313 : Bandirty the Messy in Passage to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (492, 2313, 0.1, 1, 5);
-- Monster 2318 : Bakeraider the Tomb in Passage to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (492, 2318, 0.1, 1, 5);
-- Monster 2328 : Smitherz the Licker in Passage to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (492, 2328, 0.1, 1, 5);
-- Monster 2335 : Minoskittle the Coloured in Passage to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (492, 2335, 0.1, 1, 5);
-- Monster 273 : Coco Blop in Blop Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (493, 273, 1, 1, 5);
-- Monster 274 : Indigo Blop in Blop Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (493, 274, 1, 1, 5);
-- Monster 275 : Morello Cherry Blop in Blop Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (493, 275, 1, 1, 5);
-- Monster 276 : Pippin Blop in Blop Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (493, 276, 1, 1, 5);
-- Monster 1181 : Greedoblop in Blop Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (493, 1181, 1, 1, 5);
-- Monster 1182 : Blopshroom in Blop Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (493, 1182, 1, 1, 5);
-- Monster 1183 : Trunkiblop in Blop Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (493, 1183, 1, 1, 5);
-- Monster 1184 : Royal Coco Blop in Blop Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (493, 1184, 1, 1, 5);
-- Monster 1185 : Royal Morello Cherry Blop in Blop Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (493, 1185, 1, 1, 5);
-- Monster 1186 : Royal Indigo Blop in Blop Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (493, 1186, 1, 1, 5);
-- Monster 1187 : Royal Pippin Blop in Blop Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (493, 1187, 1, 1, 5);
-- Monster 1188 : Royal Rainbow Blop in Blop Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (493, 1188, 1, 1, 5);
-- Monster 59 : Mush Mush in Fungus Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (494, 59, 1, 1, 5);
-- Monster 1153 : Trumperelle in Fungus Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (494, 1153, 1, 1, 5);
-- Monster 1154 : Mushnid in Fungus Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (494, 1154, 1, 1, 5);
-- Monster 1155 : Mushmunch in Fungus Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (494, 1155, 1, 1, 5);
-- Monster 1156 : Mush Mish in Fungus Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (494, 1156, 1, 1, 5);
-- Monster 1157 : Mush Tup in Fungus Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (494, 1157, 1, 1, 5);
-- Monster 1158 : Mush Rhume in Fungus Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (494, 1158, 1, 1, 5);
-- Monster 1159 : Ougaa in Fungus Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (494, 1159, 1, 1, 5);
-- Monster 1153 : Trumperelle in Fungus Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (495, 1153, 1, 1, 5);
-- Monster 1154 : Mushnid in Fungus Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (495, 1154, 1, 1, 5);
-- Monster 1155 : Mushmunch in Fungus Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (495, 1155, 1, 1, 5);
-- Monster 1156 : Mush Mish in Fungus Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (495, 1156, 1, 1, 5);
-- Monster 1157 : Mush Tup in Fungus Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (495, 1157, 1, 1, 5);
-- Monster 1158 : Mush Rhume in Fungus Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (495, 1158, 1, 1, 5);
-- Monster 2373 : Romush the Montecchi in Fungus Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (495, 2373, 0.1, 1, 5);
-- Monster 2551 : Mushuliet the Catapulet in Fungus Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (495, 2551, 0.1, 1, 5);
-- Monster 2552 : Edvushmunch the Screamer in Fungus Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (495, 2552, 0.1, 1, 5);
-- Monster 2553 : Nidsally the Mushtang in Fungus Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (495, 2553, 0.1, 1, 5);
-- Monster 2600 : Mushketeer the Loyal in Fungus Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (495, 2600, 0.1, 1, 5);
-- Monster 2601 : Trumpaynor the Survivor in Fungus Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (495, 2601, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (502, 236, 1, 1, 5);
-- Monster 372 : Warrior in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (502, 372, 1, 1, 5);
-- Monster 489 : Red Piwi in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (502, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (502, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (502, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (502, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (502, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (502, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (502, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (502, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (502, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (502, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Lumberjacks' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (502, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (503, 236, 1, 1, 5);
-- Monster 372 : Warrior in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (503, 372, 1, 1, 5);
-- Monster 489 : Red Piwi in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (503, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (503, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (503, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (503, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (503, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (503, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (503, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (503, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (503, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (503, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Butchers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (503, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Militia's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (504, 236, 1, 1, 5);
-- Monster 372 : Warrior in Militia's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (504, 372, 1, 1, 5);
-- Monster 489 : Red Piwi in Militia's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (504, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Militia's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (504, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Militia's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (504, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Militia's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (504, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Militia's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (504, 493, 1, 1, 5);
-- Monster 236 : Purple Piwi in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (505, 236, 1, 1, 5);
-- Monster 372 : Warrior in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (505, 372, 1, 1, 5);
-- Monster 489 : Red Piwi in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (505, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (505, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (505, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (505, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (505, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (505, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (505, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (505, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (505, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (505, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Bakers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (505, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (506, 236, 1, 1, 5);
-- Monster 372 : Warrior in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (506, 372, 1, 1, 5);
-- Monster 489 : Red Piwi in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (506, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (506, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (506, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (506, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (506, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (506, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (506, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (506, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (506, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (506, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Jewellers' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (506, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (507, 236, 1, 1, 5);
-- Monster 372 : Warrior in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (507, 372, 1, 1, 5);
-- Monster 405 : Puja Mustam in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (507, 405, 1, 1, 5);
-- Monster 489 : Red Piwi in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (507, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (507, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (507, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (507, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (507, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (507, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (507, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (507, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (507, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (507, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Tailors' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (507, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (508, 236, 1, 1, 5);
-- Monster 372 : Warrior in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (508, 372, 1, 1, 5);
-- Monster 489 : Red Piwi in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (508, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (508, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (508, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (508, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (508, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (508, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (508, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (508, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (508, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (508, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Smiths' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (508, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (509, 236, 1, 1, 5);
-- Monster 372 : Warrior in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (509, 372, 1, 1, 5);
-- Monster 489 : Red Piwi in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (509, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (509, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (509, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (509, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (509, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (509, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (509, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (509, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (509, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (509, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Handymen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (509, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (511, 236, 1, 1, 5);
-- Monster 372 : Warrior in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (511, 372, 1, 1, 5);
-- Monster 489 : Red Piwi in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (511, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (511, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (511, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (511, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (511, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (511, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (511, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (511, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (511, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (511, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (511, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (513, 236, 1, 1, 5);
-- Monster 296 : Militiaman in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (513, 296, 1, 1, 5);
-- Monster 398 : Dike Tarak in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (513, 398, 1, 1, 5);
-- Monster 489 : Red Piwi in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (513, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (513, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (513, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (513, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (513, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (513, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (513, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (513, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (513, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (513, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in City Centre
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (513, 2347, 0.1, 1, 5);
-- Monster 54 : Chafer in Skeleton Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (516, 54, 1, 1, 5);
-- Monster 108 : Rib in Skeleton Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (516, 108, 1, 1, 5);
-- Monster 110 : Invisible Chafer in Skeleton Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (516, 110, 1, 1, 5);
-- Monster 396 : Chafer Foot Soldier in Skeleton Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (516, 396, 1, 1, 5);
-- Monster 3238 : Ronin Chafer in Skeleton Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (516, 3238, 1, 1, 5);
-- Monster 3239 : Draugur Chafer in Skeleton Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (516, 3239, 1, 1, 5);
-- Monster 3240 : Primitive Chafer in Skeleton Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (516, 3240, 1, 1, 5);
-- Monster 281 : Crow in Desecrated Highlands
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (517, 281, 1, 1, 5);
-- Monster 298 : Scurvion in Desecrated Highlands
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (517, 298, 1, 1, 5);
-- Monster 299 : Whitish Fang in Desecrated Highlands
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (517, 299, 1, 1, 5);
-- Monster 300 : Kolerat in Desecrated Highlands
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (517, 300, 1, 1, 5);
-- Monster 301 : Ouginak in Desecrated Highlands
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (517, 301, 1, 1, 5);
-- Monster 2285 : Scorbison the Lonely in Desecrated Highlands
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (517, 2285, 0.1, 1, 5);
-- Monster 2378 : Snowhitisha the Pure in Desecrated Highlands
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (517, 2378, 0.1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in Desecrated Highlands
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (517, 2437, 0.1, 1, 5);
-- Monster 2520 : Koleraspootin the Anesthesialogist in Desecrated Highlands
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (517, 2520, 0.1, 1, 5);
-- Monster 2589 : Ougathard the Fortunate in Desecrated Highlands
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (517, 2589, 0.1, 1, 5);
-- Monster 273 : Coco Blop in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 273, 1, 1, 5);
-- Monster 274 : Indigo Blop in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 274, 1, 1, 5);
-- Monster 275 : Morello Cherry Blop in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 275, 1, 1, 5);
-- Monster 276 : Pippin Blop in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 276, 1, 1, 5);
-- Monster 277 : Indigo Biblop in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 277, 1, 1, 5);
-- Monster 278 : Coco Biblop in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 278, 1, 1, 5);
-- Monster 279 : Morello Cherry Biblop in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 279, 1, 1, 5);
-- Monster 280 : Pippin Biblop in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 280, 1, 1, 5);
-- Monster 281 : Crow in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 281, 1, 1, 5);
-- Monster 287 : Kaniger in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 287, 1, 1, 5);
-- Monster 288 : Plissken in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 288, 1, 1, 5);
-- Monster 2286 : Serpico the Honest in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 2286, 0.1, 1, 5);
-- Monster 2396 : Billbiblop the Great in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 2396, 0.1, 1, 5);
-- Monster 2399 : Biblopopo the Organiser in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 2399, 0.1, 1, 5);
-- Monster 2400 : Biblokajin the Bald in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 2400, 0.1, 1, 5);
-- Monster 2401 : Bibloponey the Entertainer in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 2401, 0.1, 1, 5);
-- Monster 2404 : Blopulent the Pretentious in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 2404, 0.1, 1, 5);
-- Monster 2405 : Blorchid the Gorgeous in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 2405, 0.1, 1, 5);
-- Monster 2406 : Blopium the Delirious in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 2406, 0.1, 1, 5);
-- Monster 2407 : Blopal the Precious in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 2407, 0.1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 2437, 0.1, 1, 5);
-- Monster 2503 : Kaniedoss the Giggling in Cania Moors
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (518, 2503, 0.1, 1, 5);
-- Monster 273 : Coco Blop in Cania Desert
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (519, 273, 1, 1, 5);
-- Monster 274 : Indigo Blop in Cania Desert
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (519, 274, 1, 1, 5);
-- Monster 275 : Morello Cherry Blop in Cania Desert
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (519, 275, 1, 1, 5);
-- Monster 276 : Pippin Blop in Cania Desert
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (519, 276, 1, 1, 5);
-- Monster 287 : Kaniger in Cania Desert
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (519, 287, 1, 1, 5);
-- Monster 288 : Plissken in Cania Desert
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (519, 288, 1, 1, 5);
-- Monster 2286 : Serpico the Honest in Cania Desert
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (519, 2286, 0.1, 1, 5);
-- Monster 2404 : Blopulent the Pretentious in Cania Desert
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (519, 2404, 0.1, 1, 5);
-- Monster 2405 : Blorchid the Gorgeous in Cania Desert
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (519, 2405, 0.1, 1, 5);
-- Monster 2406 : Blopium the Delirious in Cania Desert
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (519, 2406, 0.1, 1, 5);
-- Monster 2407 : Blopal the Precious in Cania Desert
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (519, 2407, 0.1, 1, 5);
-- Monster 2503 : Kaniedoss the Giggling in Cania Desert
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (519, 2503, 0.1, 1, 5);
-- Monster 273 : Coco Blop in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 273, 1, 1, 5);
-- Monster 274 : Indigo Blop in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 274, 1, 1, 5);
-- Monster 275 : Morello Cherry Blop in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 275, 1, 1, 5);
-- Monster 276 : Pippin Blop in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 276, 1, 1, 5);
-- Monster 277 : Indigo Biblop in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 277, 1, 1, 5);
-- Monster 278 : Coco Biblop in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 278, 1, 1, 5);
-- Monster 279 : Morello Cherry Biblop in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 279, 1, 1, 5);
-- Monster 280 : Pippin Biblop in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 280, 1, 1, 5);
-- Monster 287 : Kaniger in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 287, 1, 1, 5);
-- Monster 288 : Plissken in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 288, 1, 1, 5);
-- Monster 2286 : Serpico the Honest in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 2286, 0.1, 1, 5);
-- Monster 2396 : Billbiblop the Great in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 2396, 0.1, 1, 5);
-- Monster 2399 : Biblopopo the Organiser in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 2399, 0.1, 1, 5);
-- Monster 2400 : Biblokajin the Bald in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 2400, 0.1, 1, 5);
-- Monster 2401 : Bibloponey the Entertainer in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 2401, 0.1, 1, 5);
-- Monster 2404 : Blopulent the Pretentious in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 2404, 0.1, 1, 5);
-- Monster 2405 : Blorchid the Gorgeous in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 2405, 0.1, 1, 5);
-- Monster 2406 : Blopium the Delirious in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 2406, 0.1, 1, 5);
-- Monster 2407 : Blopal the Precious in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 2407, 0.1, 1, 5);
-- Monster 2503 : Kaniedoss the Giggling in Cania Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (520, 2503, 0.1, 1, 5);
-- Monster 273 : Coco Blop in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 273, 1, 1, 5);
-- Monster 274 : Indigo Blop in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 274, 1, 1, 5);
-- Monster 275 : Morello Cherry Blop in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 275, 1, 1, 5);
-- Monster 276 : Pippin Blop in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 276, 1, 1, 5);
-- Monster 277 : Indigo Biblop in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 277, 1, 1, 5);
-- Monster 278 : Coco Biblop in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 278, 1, 1, 5);
-- Monster 279 : Morello Cherry Biblop in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 279, 1, 1, 5);
-- Monster 280 : Pippin Biblop in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 280, 1, 1, 5);
-- Monster 281 : Crow in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 281, 1, 1, 5);
-- Monster 287 : Kaniger in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 287, 1, 1, 5);
-- Monster 288 : Plissken in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 288, 1, 1, 5);
-- Monster 293 : Plain Crackler in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 293, 1, 1, 5);
-- Monster 2286 : Serpico the Honest in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 2286, 0.1, 1, 5);
-- Monster 2396 : Billbiblop the Great in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 2396, 0.1, 1, 5);
-- Monster 2399 : Biblopopo the Organiser in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 2399, 0.1, 1, 5);
-- Monster 2400 : Biblokajin the Bald in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 2400, 0.1, 1, 5);
-- Monster 2401 : Bibloponey the Entertainer in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 2401, 0.1, 1, 5);
-- Monster 2404 : Blopulent the Pretentious in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 2404, 0.1, 1, 5);
-- Monster 2405 : Blorchid the Gorgeous in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 2405, 0.1, 1, 5);
-- Monster 2406 : Blopium the Delirious in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 2406, 0.1, 1, 5);
-- Monster 2407 : Blopal the Precious in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 2407, 0.1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 2437, 0.1, 1, 5);
-- Monster 2438 : Cracklerod the Old in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 2438, 0.1, 1, 5);
-- Monster 2503 : Kaniedoss the Giggling in Cania Peaks
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (521, 2503, 0.1, 1, 5);
-- Monster 273 : Coco Blop in Road to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (522, 273, 1, 1, 5);
-- Monster 274 : Indigo Blop in Road to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (522, 274, 1, 1, 5);
-- Monster 275 : Morello Cherry Blop in Road to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (522, 275, 1, 1, 5);
-- Monster 276 : Pippin Blop in Road to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (522, 276, 1, 1, 5);
-- Monster 281 : Crow in Road to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (522, 281, 1, 1, 5);
-- Monster 287 : Kaniger in Road to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (522, 287, 1, 1, 5);
-- Monster 288 : Plissken in Road to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (522, 288, 1, 1, 5);
-- Monster 2286 : Serpico the Honest in Road to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (522, 2286, 0.1, 1, 5);
-- Monster 2404 : Blopulent the Pretentious in Road to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (522, 2404, 0.1, 1, 5);
-- Monster 2405 : Blorchid the Gorgeous in Road to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (522, 2405, 0.1, 1, 5);
-- Monster 2406 : Blopium the Delirious in Road to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (522, 2406, 0.1, 1, 5);
-- Monster 2407 : Blopal the Precious in Road to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (522, 2407, 0.1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in Road to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (522, 2437, 0.1, 1, 5);
-- Monster 2503 : Kaniedoss the Giggling in Road to Brakmar
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (522, 2503, 0.1, 1, 5);
-- Monster 281 : Crow in Cania Cirque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (523, 281, 1, 1, 5);
-- Monster 293 : Plain Crackler in Cania Cirque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (523, 293, 1, 1, 5);
-- Monster 343 : Lousy Pig Knight in Cania Cirque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (523, 343, 1, 1, 5);
-- Monster 344 : Lousy Pig Shepherd in Cania Cirque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (523, 344, 1, 1, 5);
-- Monster 2397 : Pigstol the Sexy in Cania Cirque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (523, 2397, 0.1, 1, 5);
-- Monster 2420 : Pygknightlion the Lousy in Cania Cirque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (523, 2420, 0.1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in Cania Cirque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (523, 2437, 0.1, 1, 5);
-- Monster 2438 : Cracklerod the Old in Cania Cirque
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (523, 2438, 0.1, 1, 5);
-- Monster 52 : Arachnee in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 52, 1, 1, 5);
-- Monster 118 : Dark Miner in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 118, 1, 1, 5);
-- Monster 273 : Coco Blop in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 273, 1, 1, 5);
-- Monster 274 : Indigo Blop in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 274, 1, 1, 5);
-- Monster 275 : Morello Cherry Blop in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 275, 1, 1, 5);
-- Monster 276 : Pippin Blop in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 276, 1, 1, 5);
-- Monster 277 : Indigo Biblop in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 277, 1, 1, 5);
-- Monster 278 : Coco Biblop in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 278, 1, 1, 5);
-- Monster 279 : Morello Cherry Biblop in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 279, 1, 1, 5);
-- Monster 280 : Pippin Biblop in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 280, 1, 1, 5);
-- Monster 281 : Crow in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 281, 1, 1, 5);
-- Monster 288 : Plissken in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 288, 1, 1, 5);
-- Monster 293 : Plain Crackler in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 293, 1, 1, 5);
-- Monster 2396 : Billbiblop the Great in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 2396, 0.1, 1, 5);
-- Monster 2399 : Biblopopo the Organiser in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 2399, 0.1, 1, 5);
-- Monster 2400 : Biblokajin the Bald in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 2400, 0.1, 1, 5);
-- Monster 2401 : Bibloponey the Entertainer in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 2401, 0.1, 1, 5);
-- Monster 2404 : Blopulent the Pretentious in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 2404, 0.1, 1, 5);
-- Monster 2405 : Blorchid the Gorgeous in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 2405, 0.1, 1, 5);
-- Monster 2406 : Blopium the Delirious in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 2406, 0.1, 1, 5);
-- Monster 2407 : Blopal the Precious in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 2407, 0.1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 2437, 0.1, 1, 5);
-- Monster 2438 : Cracklerod the Old in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 2438, 0.1, 1, 5);
-- Monster 2790 : Dusty Piwi in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 2790, 1, 1, 5);
-- Monster 2791 : Aggressive Moumouse in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 2791, 1, 1, 5);
-- Monster 2793 : Krtek in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 2793, 1, 1, 5);
-- Monster 2796 : Krtek Dark Miner in Rocky Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (524, 2796, 1, 1, 5);
-- Monster 273 : Coco Blop in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 273, 1, 1, 5);
-- Monster 274 : Indigo Blop in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 274, 1, 1, 5);
-- Monster 275 : Morello Cherry Blop in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 275, 1, 1, 5);
-- Monster 276 : Pippin Blop in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 276, 1, 1, 5);
-- Monster 277 : Indigo Biblop in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 277, 1, 1, 5);
-- Monster 278 : Coco Biblop in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 278, 1, 1, 5);
-- Monster 279 : Morello Cherry Biblop in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 279, 1, 1, 5);
-- Monster 280 : Pippin Biblop in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 280, 1, 1, 5);
-- Monster 281 : Crow in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 281, 1, 1, 5);
-- Monster 293 : Plain Crackler in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 293, 1, 1, 5);
-- Monster 2396 : Billbiblop the Great in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 2396, 0.1, 1, 5);
-- Monster 2399 : Biblopopo the Organiser in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 2399, 0.1, 1, 5);
-- Monster 2400 : Biblokajin the Bald in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 2400, 0.1, 1, 5);
-- Monster 2401 : Bibloponey the Entertainer in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 2401, 0.1, 1, 5);
-- Monster 2404 : Blopulent the Pretentious in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 2404, 0.1, 1, 5);
-- Monster 2405 : Blorchid the Gorgeous in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 2405, 0.1, 1, 5);
-- Monster 2406 : Blopium the Delirious in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 2406, 0.1, 1, 5);
-- Monster 2407 : Blopal the Precious in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 2407, 0.1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 2437, 0.1, 1, 5);
-- Monster 2438 : Cracklerod the Old in Coast Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (525, 2438, 0.1, 1, 5);
-- Monster 281 : Crow in Dark Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (526, 281, 1, 1, 5);
-- Monster 298 : Scurvion in Dark Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (526, 298, 1, 1, 5);
-- Monster 299 : Whitish Fang in Dark Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (526, 299, 1, 1, 5);
-- Monster 300 : Kolerat in Dark Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (526, 300, 1, 1, 5);
-- Monster 301 : Ouginak in Dark Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (526, 301, 1, 1, 5);
-- Monster 2285 : Scorbison the Lonely in Dark Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (526, 2285, 0.1, 1, 5);
-- Monster 2378 : Snowhitisha the Pure in Dark Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (526, 2378, 0.1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in Dark Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (526, 2437, 0.1, 1, 5);
-- Monster 2520 : Koleraspootin the Anesthesialogist in Dark Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (526, 2520, 0.1, 1, 5);
-- Monster 2589 : Ougathard the Fortunate in Dark Road
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (526, 2589, 0.1, 1, 5);
-- Monster 273 : Coco Blop in Scree
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (527, 273, 1, 1, 5);
-- Monster 274 : Indigo Blop in Scree
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (527, 274, 1, 1, 5);
-- Monster 275 : Morello Cherry Blop in Scree
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (527, 275, 1, 1, 5);
-- Monster 276 : Pippin Blop in Scree
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (527, 276, 1, 1, 5);
-- Monster 281 : Crow in Scree
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (527, 281, 1, 1, 5);
-- Monster 293 : Plain Crackler in Scree
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (527, 293, 1, 1, 5);
-- Monster 2404 : Blopulent the Pretentious in Scree
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (527, 2404, 0.1, 1, 5);
-- Monster 2405 : Blorchid the Gorgeous in Scree
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (527, 2405, 0.1, 1, 5);
-- Monster 2406 : Blopium the Delirious in Scree
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (527, 2406, 0.1, 1, 5);
-- Monster 2407 : Blopal the Precious in Scree
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (527, 2407, 0.1, 1, 5);
-- Monster 2437 : Crowmanion the Primitive in Scree
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (527, 2437, 0.1, 1, 5);
-- Monster 2438 : Cracklerod the Old in Scree
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (527, 2438, 0.1, 1, 5);
-- Monster 118 : Dark Miner in Blacksmith Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (528, 118, 1, 1, 5);
-- Monster 119 : Dark Smith in Blacksmith Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (528, 119, 1, 1, 5);
-- Monster 153 : Dark Baker in Blacksmith Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (528, 153, 1, 1, 5);
-- Monster 155 : Rogue Clan Bandit in Blacksmith Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (528, 155, 1, 1, 5);
-- Monster 252 : Smiths' Chest in Blacksmith Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (528, 252, 1, 1, 5);
-- Monster 34 : Green Larva in Pandala Suburbs
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (529, 34, 1, 1, 5);
-- Monster 46 : Orange Larva in Pandala Suburbs
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (529, 46, 1, 1, 5);
-- Monster 52 : Arachnee in Pandala Suburbs
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (529, 52, 1, 1, 5);
-- Monster 2310 : Arakula the Carpature in Pandala Suburbs
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (529, 2310, 0.1, 1, 5);
-- Monster 2331 : Larvalencia the Orange in Pandala Suburbs
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (529, 2331, 0.1, 1, 5);
-- Monster 2332 : Larvadelaide the Ozie in Pandala Suburbs
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (529, 2332, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (530, 236, 1, 1, 5);
-- Monster 296 : Militiaman in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (530, 296, 1, 1, 5);
-- Monster 489 : Red Piwi in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (530, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (530, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (530, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (530, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (530, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (530, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (530, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (530, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (530, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (530, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (530, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (531, 236, 1, 1, 5);
-- Monster 372 : Warrior in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (531, 372, 1, 1, 5);
-- Monster 489 : Red Piwi in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (531, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (531, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (531, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (531, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (531, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (531, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (531, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (531, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (531, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (531, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Alchemists' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (531, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (532, 236, 1, 1, 5);
-- Monster 296 : Militiaman in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (532, 296, 1, 1, 5);
-- Monster 489 : Red Piwi in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (532, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (532, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (532, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (532, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (532, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (532, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (532, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (532, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (532, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (532, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (532, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (533, 236, 1, 1, 5);
-- Monster 296 : Militiaman in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (533, 296, 1, 1, 5);
-- Monster 489 : Red Piwi in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (533, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (533, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (533, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (533, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (533, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (533, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (533, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (533, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (533, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (533, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (533, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (534, 236, 1, 1, 5);
-- Monster 372 : Warrior in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (534, 372, 1, 1, 5);
-- Monster 489 : Red Piwi in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (534, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (534, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (534, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (534, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (534, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (534, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (534, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (534, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (534, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (534, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Fishermen's Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (534, 2347, 0.1, 1, 5);
-- Monster 236 : Purple Piwi in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (535, 236, 1, 1, 5);
-- Monster 372 : Warrior in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (535, 372, 1, 1, 5);
-- Monster 489 : Red Piwi in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (535, 489, 1, 1, 5);
-- Monster 490 : Green Piwi in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (535, 490, 1, 1, 5);
-- Monster 491 : Blue Piwi in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (535, 491, 1, 1, 5);
-- Monster 492 : Pink Piwi in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (535, 492, 1, 1, 5);
-- Monster 493 : Yellow Piwi in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (535, 493, 1, 1, 5);
-- Monster 2341 : Piwiki the Witty in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (535, 2341, 0.1, 1, 5);
-- Monster 2342 : Piwicker the Manly in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (535, 2342, 0.1, 1, 5);
-- Monster 2343 : Piwi the Ermine in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (535, 2343, 0.1, 1, 5);
-- Monster 2344 : Piwilde the Bossie in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (535, 2344, 0.1, 1, 5);
-- Monster 2345 : Piwiliam the Brave in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (535, 2345, 0.1, 1, 5);
-- Monster 2347 : Piwinston the Churlish in Breeders' Quarter
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (535, 2347, 0.1, 1, 5);
-- Monster 2781 : Lupus Minimus in Guided Tutorial
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (536, 2781, 1, 1, 5);
-- Monster 2785 : Incarnam Scarecrow in Guided Tutorial
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (536, 2785, 1, 1, 5);
-- Monster 64 : Wabbit in Wabbit Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (537, 64, 1, 1, 5);
-- Monster 65 : Black Wabbit in Wabbit Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (537, 65, 1, 1, 5);
-- Monster 68 : Black Tiwabbit in Wabbit Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (537, 68, 1, 1, 5);
-- Monster 72 : Tiwabbit Wosungwee in Wabbit Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (537, 72, 1, 1, 5);
-- Monster 96 : Tiwabbit in Wabbit Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (537, 96, 1, 1, 5);
-- Monster 97 : Wo Wabbit in Wabbit Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (537, 97, 1, 1, 5);
-- Monster 99 : Gwandpa Wabbit in Wabbit Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (537, 99, 1, 1, 5);
-- Monster 180 : Wa Wabbit in Wabbit Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (537, 180, 1, 1, 5);
-- Monster 181 : Wobot in Wabbit Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (537, 181, 1, 1, 5);
-- Monster 64 : Wabbit in Cawwot Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (538, 64, 1, 1, 5);
-- Monster 65 : Black Wabbit in Cawwot Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (538, 65, 1, 1, 5);
-- Monster 97 : Wo Wabbit in Cawwot Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (538, 97, 1, 1, 5);
-- Monster 99 : Gwandpa Wabbit in Cawwot Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (538, 99, 1, 1, 5);
-- Monster 181 : Wobot in Cawwot Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (538, 181, 1, 1, 5);
-- Monster 2892 : Stunted Rat in Permafrost Port
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (600, 2892, 1, 1, 5);
-- Monster 2893 : Crabeye in Permafrost Port
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (600, 2893, 1, 1, 5);
-- Monster 2913 : Woolly Bow Meow in Permafrost Port
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (600, 2913, 1, 1, 5);
-- Monster 2914 : Gullipop in Permafrost Port
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (600, 2914, 1, 1, 5);
-- Monster 2892 : Stunted Rat in Frigost Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (601, 2892, 1, 1, 5);
-- Monster 2893 : Crabeye in Frigost Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (601, 2893, 1, 1, 5);
-- Monster 2913 : Woolly Bow Meow in Frigost Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (601, 2913, 1, 1, 5);
-- Monster 2914 : Gullipop in Frigost Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (601, 2914, 1, 1, 5);
-- Monster 2850 : Mastogob in The Icefields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (602, 2850, 1, 1, 5);
-- Monster 2851 : Mastogob Warrior in The Icefields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (602, 2851, 1, 1, 5);
-- Monster 2852 : Venerable Mastogob in The Icefields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (602, 2852, 1, 1, 5);
-- Monster 2853 : Mastogobbly in The Icefields
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (602, 2853, 1, 1, 5);
-- Monster 2888 : Yokai Snowfoux in The Snowbound Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (603, 2888, 1, 1, 5);
-- Monster 2889 : Maho Snowfoux in The Snowbound Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (603, 2889, 1, 1, 5);
-- Monster 2890 : Soryo Snowfoux in The Snowbound Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (603, 2890, 1, 1, 5);
-- Monster 2891 : Yomi Snowfoux in The Snowbound Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (603, 2891, 1, 1, 5);
-- Monster 2892 : Stunted Rat in The Snowbound Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (603, 2892, 1, 1, 5);
-- Monster 2845 : Woolly Piggoth in The Lonesome Pine Trails
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (604, 2845, 1, 1, 5);
-- Monster 2846 : Frighog in The Lonesome Pine Trails
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (604, 2846, 1, 1, 5);
-- Monster 2847 : Kanigloo in The Lonesome Pine Trails
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (604, 2847, 1, 1, 5);
-- Monster 2859 : Cromagmunk in The Lonesome Pine Trails
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (604, 2859, 1, 1, 5);
-- Monster 2860 : Sabredon in The Lonesome Pine Trails
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (604, 2860, 1, 1, 5);
-- Monster 2895 : Dramanita in The Petrified Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (605, 2895, 1, 1, 5);
-- Monster 2896 : Fistulina in The Petrified Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (605, 2896, 1, 1, 5);
-- Monster 2897 : Fungore in The Petrified Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (605, 2897, 1, 1, 5);
-- Monster 2898 : Treecherous in The Petrified Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (605, 2898, 1, 1, 5);
-- Monster 2888 : Yokai Snowfoux in The Asparah Gorge
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (606, 2888, 1, 1, 5);
-- Monster 2889 : Maho Snowfoux in The Asparah Gorge
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (606, 2889, 1, 1, 5);
-- Monster 2890 : Soryo Snowfoux in The Asparah Gorge
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (606, 2890, 1, 1, 5);
-- Monster 2891 : Yomi Snowfoux in The Asparah Gorge
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (606, 2891, 1, 1, 5);
-- Monster 2883 : Bestial Brockhard in The Fangs of Glass
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (608, 2883, 1, 1, 5);
-- Monster 2885 : Gluttonous Brockhard in The Fangs of Glass
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (608, 2885, 1, 1, 5);
-- Monster 2886 : Nightcrawling Brockhard in The Fangs of Glass
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (608, 2886, 1, 1, 5);
-- Monster 2965 : Pyrotechnic Brockhard in The Fangs of Glass
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (608, 2965, 1, 1, 5);
-- Monster 2966 : Icy Brockhard in The Fangs of Glass
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (608, 2966, 1, 1, 5);
-- Monster 2874 : Talklyka Pirate in Alma's Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (609, 2874, 1, 1, 5);
-- Monster 2875 : Karrybean Pirate in Alma's Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (609, 2875, 1, 1, 5);
-- Monster 2876 : Vigi Pirate in Alma's Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (609, 2876, 1, 1, 5);
-- Monster 2878 : Harpy Pirate in Alma's Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (609, 2878, 1, 1, 5);
-- Monster 2880 : Retspan Pirate in Alma's Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (609, 2880, 1, 1, 5);
-- Monster 2881 : Yuara Pirate in Alma's Cradle
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (609, 2881, 1, 1, 5);
-- Monster 2865 : Steam Crackler in The Tears of Ouronigride
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (610, 2865, 1, 1, 5);
-- Monster 2866 : Atomystique in The Tears of Ouronigride
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (610, 2866, 1, 1, 5);
-- Monster 2869 : Solfatara in The Tears of Ouronigride
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (610, 2869, 1, 1, 5);
-- Monster 2861 : Apewicubic Bearbarian in Mount Scauldron
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (611, 2861, 1, 1, 5);
-- Monster 2862 : Torpid Bearbarian in Mount Scauldron
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (611, 2862, 1, 1, 5);
-- Monster 2863 : Vespal Bearbarian in Mount Scauldron
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (611, 2863, 1, 1, 5);
-- Monster 2849 : Pingwinkle in The Frozen Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (615, 2849, 1, 1, 5);
-- Monster 2855 : Shaman Pingwin in The Frozen Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (615, 2855, 1, 1, 5);
-- Monster 2856 : Pingwobble in The Frozen Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (615, 2856, 1, 1, 5);
-- Monster 2857 : Mama Pingwin in The Frozen Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (615, 2857, 1, 1, 5);
-- Monster 2858 : Kung-Fu Pingwin in The Frozen Lake
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (615, 2858, 1, 1, 5);
-- Monster 2850 : Mastogob in Royal Mastogob's Greenhouse
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (617, 2850, 1, 1, 5);
-- Monster 2851 : Mastogob Warrior in Royal Mastogob's Greenhouse
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (617, 2851, 1, 1, 5);
-- Monster 2852 : Venerable Mastogob in Royal Mastogob's Greenhouse
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (617, 2852, 1, 1, 5);
-- Monster 2853 : Mastogobbly in Royal Mastogob's Greenhouse
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (617, 2853, 1, 1, 5);
-- Monster 2854 : Royal Mastogob in Royal Mastogob's Greenhouse
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (617, 2854, 1, 1, 5);
-- Monster 2848 : Royal Pingwin in Royal Pingwin's Excavation
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (618, 2848, 1, 1, 5);
-- Monster 2849 : Pingwinkle in Royal Pingwin's Excavation
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (618, 2849, 1, 1, 5);
-- Monster 2855 : Shaman Pingwin in Royal Pingwin's Excavation
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (618, 2855, 1, 1, 5);
-- Monster 2856 : Pingwobble in Royal Pingwin's Excavation
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (618, 2856, 1, 1, 5);
-- Monster 2857 : Mama Pingwin in Royal Pingwin's Excavation
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (618, 2857, 1, 1, 5);
-- Monster 2858 : Kung-Fu Pingwin in Royal Pingwin's Excavation
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (618, 2858, 1, 1, 5);
-- Monster 2874 : Talklyka Pirate in The Wreck of the Hesperus
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (619, 2874, 1, 1, 5);
-- Monster 2875 : Karrybean Pirate in The Wreck of the Hesperus
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (619, 2875, 1, 1, 5);
-- Monster 2876 : Vigi Pirate in The Wreck of the Hesperus
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (619, 2876, 1, 1, 5);
-- Monster 2877 : Buck Anear in The Wreck of the Hesperus
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (619, 2877, 1, 1, 5);
-- Monster 2878 : Harpy Pirate in The Wreck of the Hesperus
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (619, 2878, 1, 1, 5);
-- Monster 2880 : Retspan Pirate in The Wreck of the Hesperus
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (619, 2880, 1, 1, 5);
-- Monster 2881 : Yuara Pirate in The Wreck of the Hesperus
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (619, 2881, 1, 1, 5);
-- Monster 2950 : Ectobomb in The Wreck of the Hesperus
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (619, 2950, 1, 1, 5);
-- Monster 2865 : Steam Crackler in The Obsidemon's Hypogeum
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (620, 2865, 1, 1, 5);
-- Monster 2866 : Atomystique in The Obsidemon's Hypogeum
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (620, 2866, 1, 1, 5);
-- Monster 2867 : Mofette in The Obsidemon's Hypogeum
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (620, 2867, 1, 1, 5);
-- Monster 2868 : Fumaroller in The Obsidemon's Hypogeum
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (620, 2868, 1, 1, 5);
-- Monster 2869 : Solfatara in The Obsidemon's Hypogeum
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (620, 2869, 1, 1, 5);
-- Monster 2924 : Obsidemon in The Obsidemon's Hypogeum
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (620, 2924, 1, 1, 5);
-- Monster 2888 : Yokai Snowfoux in Snowfoux Den
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (621, 2888, 1, 1, 5);
-- Monster 2889 : Maho Snowfoux in Snowfoux Den
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (621, 2889, 1, 1, 5);
-- Monster 2890 : Soryo Snowfoux in Snowfoux Den
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (621, 2890, 1, 1, 5);
-- Monster 2891 : Yomi Snowfoux in Snowfoux Den
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (621, 2891, 1, 1, 5);
-- Monster 2967 : Tengu Snowfoux in Snowfoux Den
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (621, 2967, 1, 1, 5);
-- Monster 3234 : Foster Fuji Snowfoux in Snowfoux Den
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (621, 3234, 1, 1, 5);
-- Monster 2888 : Yokai Snowfoux in Snowfoux Caverns
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (622, 2888, 1, 1, 5);
-- Monster 2889 : Maho Snowfoux in Snowfoux Caverns
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (622, 2889, 1, 1, 5);
-- Monster 2890 : Soryo Snowfoux in Snowfoux Caverns
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (622, 2890, 1, 1, 5);
-- Monster 2891 : Yomi Snowfoux in Snowfoux Caverns
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (622, 2891, 1, 1, 5);
-- Monster 2895 : Dramanita in Korriander's Lair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (623, 2895, 1, 1, 5);
-- Monster 2896 : Fistulina in Korriander's Lair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (623, 2896, 1, 1, 5);
-- Monster 2897 : Fungore in Korriander's Lair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (623, 2897, 1, 1, 5);
-- Monster 2898 : Treecherous in Korriander's Lair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (623, 2898, 1, 1, 5);
-- Monster 2900 : Serpula in Korriander's Lair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (623, 2900, 1, 1, 5);
-- Monster 2968 : Korriander in Korriander's Lair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (623, 2968, 1, 1, 5);
-- Monster 2883 : Bestial Brockhard in Kolosso's Caverns
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (624, 2883, 1, 1, 5);
-- Monster 2884 : Venomous Brockhard in Kolosso's Caverns
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (624, 2884, 1, 1, 5);
-- Monster 2885 : Gluttonous Brockhard in Kolosso's Caverns
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (624, 2885, 1, 1, 5);
-- Monster 2886 : Nightcrawling Brockhard in Kolosso's Caverns
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (624, 2886, 1, 1, 5);
-- Monster 2965 : Pyrotechnic Brockhard in Kolosso's Caverns
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (624, 2965, 1, 1, 5);
-- Monster 2966 : Icy Brockhard in Kolosso's Caverns
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (624, 2966, 1, 1, 5);
-- Monster 2986 : Kolosso in Kolosso's Caverns
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (624, 2986, 1, 1, 5);
-- Monster 2992 : Professor Xa in Kolosso's Caverns
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (624, 2992, 1, 1, 5);
-- Monster 2932 : Buzta in Sakai Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (651, 2932, 1, 1, 5);
-- Monster 2933 : Asploda in Sakai Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (651, 2933, 1, 1, 5);
-- Monster 2934 : Grabba in Sakai Plain
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (651, 2934, 1, 1, 5);
-- Monster 2932 : Buzta in Snowy Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (652, 2932, 1, 1, 5);
-- Monster 2933 : Asploda in Snowy Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (652, 2933, 1, 1, 5);
-- Monster 2934 : Grabba in Snowy Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (652, 2934, 1, 1, 5);
-- Monster 2935 : Stabba in Snowy Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (652, 2935, 1, 1, 5);
-- Monster 2937 : Drilla in Snowy Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (652, 2937, 1, 1, 5);
-- Monster 2941 : Gobshell in Snowy Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (652, 2941, 1, 1, 5);
-- Monster 3400 : Doctor Gobotnegg in Snowy Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (652, 3400, 1, 1, 5);
-- Monster 2932 : Buzta in Sakai Abandoned Mine
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (653, 2932, 1, 1, 5);
-- Monster 2933 : Asploda in Sakai Abandoned Mine
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (653, 2933, 1, 1, 5);
-- Monster 2934 : Grabba in Sakai Abandoned Mine
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (653, 2934, 1, 1, 5);
-- Monster 2935 : Stabba in Sakai Abandoned Mine
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (653, 2935, 1, 1, 5);
-- Monster 2936 : Bomma in Sakai Abandoned Mine
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (653, 2936, 1, 1, 5);
-- Monster 2937 : Drilla in Sakai Abandoned Mine
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (653, 2937, 1, 1, 5);
-- Monster 2941 : Gobshell in Sakai Abandoned Mine
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (653, 2941, 1, 1, 5);
-- Monster 2942 : N in Sakai Abandoned Mine
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (653, 2942, 1, 1, 5);
-- Monster 211 : Kanniball Archer in Kanniball Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (700, 211, 1, 1, 5);
-- Monster 212 : Kanniball Thierry in Kanniball Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (700, 212, 1, 1, 5);
-- Monster 213 : Kanniball Jav in Kanniball Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (700, 213, 1, 1, 5);
-- Monster 214 : Kanniball Sarbak in Kanniball Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (700, 214, 1, 1, 5);
-- Monster 2960 : Kanniball Andchain in Kanniball Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (700, 2960, 1, 1, 5);
-- Monster 2961 : Haloperi Doll in Kanniball Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (700, 2961, 1, 1, 5);
-- Monster 124 : Vampyre Master in Haunted House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (702, 124, 1, 1, 5);
-- Monster 127 : Vampyre in Haunted House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (702, 127, 1, 1, 5);
-- Monster 150 : Neye in Haunted House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (702, 150, 1, 1, 5);
-- Monster 154 : Kwoan in Haunted House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (702, 154, 1, 1, 5);
-- Monster 292 : Elite Chafer in Haunted House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (702, 292, 1, 1, 5);
-- Monster 498 : Gargoyl in Haunted House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (702, 498, 1, 1, 5);
-- Monster 2975 : Boostache in Haunted House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (702, 2975, 1, 1, 5);
-- Monster 3235 : Prepubescent Boostache in Haunted House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (702, 3235, 1, 1, 5);
-- Monster 3141 : Goultard in Goultarminator
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (750, 3141, 1, 1, 5);
-- Monster 2861 : Apewicubic Bearbarian in Bearbarian Antichamber
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (752, 2861, 1, 1, 5);
-- Monster 2862 : Torpid Bearbarian in Bearbarian Antichamber
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (752, 2862, 1, 1, 5);
-- Monster 2863 : Vespal Bearbarian in Bearbarian Antichamber
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (752, 2863, 1, 1, 5);
-- Monster 2864 : Celestial Bearbarian in Bearbarian Antichamber
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (752, 2864, 1, 1, 5);
-- Monster 2987 : Esurient Bearbarian in Bearbarian Antichamber
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (752, 2987, 1, 1, 5);
-- Monster 2988 : Rampant Bearbarian in Bearbarian Antichamber
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (752, 2988, 1, 1, 5);
-- Monster 2989 : Mellifluous Bearbarian in Bearbarian Antichamber
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (752, 2989, 1, 1, 5);
-- Monster 2861 : Apewicubic Bearbarian in Bearbarian Hive
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (753, 2861, 1, 1, 5);
-- Monster 2862 : Torpid Bearbarian in Bearbarian Hive
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (753, 2862, 1, 1, 5);
-- Monster 2863 : Vespal Bearbarian in Bearbarian Hive
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (753, 2863, 1, 1, 5);
-- Monster 2988 : Rampant Bearbarian in Bearbarian Hive
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (753, 2988, 1, 1, 5);
-- Monster 2989 : Mellifluous Bearbarian in Bearbarian Hive
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (753, 2989, 1, 1, 5);
-- Monster 3115 : Bearendizer in Bearbarian Hive
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (753, 3115, 1, 1, 5);
-- Monster 81 : Wind Kwak in The Kwakwa's Nest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (755, 81, 1, 1, 5);
-- Monster 83 : Fire Kwak in The Kwakwa's Nest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (755, 83, 1, 1, 5);
-- Monster 84 : Ice Kwak in The Kwakwa's Nest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (755, 84, 1, 1, 5);
-- Monster 235 : Earth Kwak in The Kwakwa's Nest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (755, 235, 1, 1, 5);
-- Monster 269 : Fire Kwakere in The Kwakwa's Nest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (755, 269, 1, 1, 5);
-- Monster 270 : Earth Kwakere in The Kwakwa's Nest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (755, 270, 1, 1, 5);
-- Monster 271 : Ice Kwakere in The Kwakwa's Nest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (755, 271, 1, 1, 5);
-- Monster 272 : Wind Kwakere in The Kwakwa's Nest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (755, 272, 1, 1, 5);
-- Monster 2995 : Kwakwa in The Kwakwa's Nest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (755, 2995, 1, 1, 5);
-- Monster 3281 : Wind Bwak in The Kwakwa's Nest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (755, 3281, 1, 1, 5);
-- Monster 3282 : Ice Bwak in The Kwakwa's Nest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (755, 3282, 1, 1, 5);
-- Monster 3283 : Fire Bwak in The Kwakwa's Nest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (755, 3283, 1, 1, 5);
-- Monster 3284 : Earth Bwak in The Kwakwa's Nest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (755, 3284, 1, 1, 5);
-- Monster 281 : Crow in Brumen Tinctorias's Laboratory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (756, 281, 1, 1, 5);
-- Monster 298 : Scurvion in Brumen Tinctorias's Laboratory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (756, 298, 1, 1, 5);
-- Monster 300 : Kolerat in Brumen Tinctorias's Laboratory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (756, 300, 1, 1, 5);
-- Monster 301 : Ouginak in Brumen Tinctorias's Laboratory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (756, 301, 1, 1, 5);
-- Monster 3096 : Terrestrial Crow in Brumen Tinctorias's Laboratory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (756, 3096, 1, 1, 5);
-- Monster 3097 : Lab Kolerat in Brumen Tinctorias's Laboratory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (756, 3097, 1, 1, 5);
-- Monster 3098 : Rabid Ouginak in Brumen Tinctorias's Laboratory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (756, 3098, 1, 1, 5);
-- Monster 3099 : Reinforced Scurvion in Brumen Tinctorias's Laboratory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (756, 3099, 1, 1, 5);
-- Monster 3100 : Nelween in Brumen Tinctorias's Laboratory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (756, 3100, 1, 1, 5);
-- Monster 839 : Abominable Snow Yiti in Kwismas Land
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (757, 839, 1, 1, 5);
-- Monster 845 : Snowman in Kwismas Land
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (757, 845, 1, 1, 5);
-- Monster 847 : Hibernal Tofu in Kwismas Land
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (757, 847, 1, 1, 5);
-- Monster 851 : Cranky Goblimp in Kwismas Land
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (757, 851, 1, 1, 5);
-- Monster 867 : Snowball in Kwismas Land
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (757, 867, 1, 1, 5);
-- Monster 869 : Ice Crackler in Kwismas Land
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (757, 869, 1, 1, 5);
-- Monster 871 : Larvicily in Kwismas Land
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (757, 871, 1, 1, 5);
-- Monster 907 : Minimini Inuit in Kwismas Land
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (757, 907, 1, 1, 5);
-- Monster 3118 : Wild Kwismas Dragoturkey in Kwismas Land
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (757, 3118, 1, 1, 5);
-- Monster 839 : Abominable Snow Yiti in Kwismas Taiga
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (758, 839, 1, 1, 5);
-- Monster 843 : Nefarious Goblimp in Kwismas Taiga
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (758, 843, 1, 1, 5);
-- Monster 864 : Sakai Firefoux in Kwismas Taiga
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (758, 864, 1, 1, 5);
-- Monster 869 : Ice Crackler in Kwismas Taiga
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (758, 869, 1, 1, 5);
-- Monster 888 : Prez' Plozion in Kwismas Taiga
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (758, 888, 1, 1, 5);
-- Monster 891 : Mini Inuit in Kwismas Taiga
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (758, 891, 1, 1, 5);
-- Monster 906 : Prez in Kwismas Taiga
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (758, 906, 1, 1, 5);
-- Monster 3119 : Impetuous Kwismas Dragoturkey in Kwismas Taiga
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (758, 3119, 1, 1, 5);
-- Monster 839 : Abominable Snow Yiti in Kwismas Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (759, 839, 1, 1, 5);
-- Monster 845 : Snowman in Kwismas Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (759, 845, 1, 1, 5);
-- Monster 847 : Hibernal Tofu in Kwismas Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (759, 847, 1, 1, 5);
-- Monster 867 : Snowball in Kwismas Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (759, 867, 1, 1, 5);
-- Monster 869 : Ice Crackler in Kwismas Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (759, 869, 1, 1, 5);
-- Monster 871 : Larvicily in Kwismas Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (759, 871, 1, 1, 5);
-- Monster 872 : Father Kwismas in Kwismas Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (759, 872, 1, 1, 5);
-- Monster 907 : Minimini Inuit in Kwismas Cavern
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (759, 907, 1, 1, 5);
-- Monster 864 : Sakai Firefoux in Father Kwismas's House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (760, 864, 1, 1, 5);
-- Monster 888 : Prez' Plozion in Father Kwismas's House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (760, 888, 1, 1, 5);
-- Monster 891 : Mini Inuit in Father Kwismas's House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (760, 891, 1, 1, 5);
-- Monster 906 : Prez in Father Kwismas's House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (760, 906, 1, 1, 5);
-- Monster 1194 : Father Whupper in Father Kwismas's House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (760, 1194, 1, 1, 5);
-- Monster 3107 : Stuffed Tofu in Father Kwismas's House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (760, 3107, 1, 1, 5);
-- Monster 3108 : Stuffed Gobball in Father Kwismas's House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (760, 3108, 1, 1, 5);
-- Monster 3109 : Stuffed Wabbit in Father Kwismas's House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (760, 3109, 1, 1, 5);
-- Monster 3172 : Moumouse Tourist in Vulkania Village
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (762, 3172, 1, 1, 5);
-- Monster 3145 : Insipid Juvenile Sauroshell in Traktopel Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (763, 3145, 1, 1, 5);
-- Monster 3146 : Muddy Juvenile Sauroshell in Traktopel Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (763, 3146, 1, 1, 5);
-- Monster 3147 : Incandescent Juvenile Sauroshell in Traktopel Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (763, 3147, 1, 1, 5);
-- Monster 3148 : Moist Juvenile Sauroshell in Traktopel Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (763, 3148, 1, 1, 5);
-- Monster 3149 : Dry Juvenile Sauroshell in Traktopel Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (763, 3149, 1, 1, 5);
-- Monster 3178 : Blubasaur the Weepy in Traktopel Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (763, 3178, 0.1, 1, 5);
-- Monster 3179 : Ikthyosaur the Fishy in Traktopel Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (763, 3179, 0.1, 1, 5);
-- Monster 3180 : Prontosaur the Punctual in Traktopel Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (763, 3180, 0.1, 1, 5);
-- Monster 3181 : Segasaur the Megadriven in Traktopel Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (763, 3181, 0.1, 1, 5);
-- Monster 3182 : Dynasaur the Scaly in Traktopel Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (763, 3182, 0.1, 1, 5);
-- Monster 3150 : Insipid Novice Sauroshell in Spartania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (764, 3150, 1, 1, 5);
-- Monster 3151 : Muddy Novice Sauroshell in Spartania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (764, 3151, 1, 1, 5);
-- Monster 3152 : Incandescent Novice Sauroshell in Spartania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (764, 3152, 1, 1, 5);
-- Monster 3153 : Moist Novice Sauroshell in Spartania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (764, 3153, 1, 1, 5);
-- Monster 3154 : Dry Novice Sauroshell in Spartania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (764, 3154, 1, 1, 5);
-- Monster 3183 : Saurbic the Acidic in Spartania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (764, 3183, 0.1, 1, 5);
-- Monster 3184 : Absaurbo the Spongy in Spartania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (764, 3184, 0.1, 1, 5);
-- Monster 3185 : Bedsaur the Blotchy in Spartania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (764, 3185, 0.1, 1, 5);
-- Monster 3186 : Sauroful the Regretting in Spartania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (764, 3186, 0.1, 1, 5);
-- Monster 3187 : Saurlax the Sleepy in Spartania Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (764, 3187, 0.1, 1, 5);
-- Monster 3155 : Insipid Mature Sauroshell in Stratigra Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (765, 3155, 1, 1, 5);
-- Monster 3156 : Muddy Mature Sauroshell in Stratigra Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (765, 3156, 1, 1, 5);
-- Monster 3157 : Incandescent Mature Sauroshell in Stratigra Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (765, 3157, 1, 1, 5);
-- Monster 3158 : Moist Mature Sauroshell in Stratigra Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (765, 3158, 1, 1, 5);
-- Monster 3159 : Dry Mature Sauroshell in Stratigra Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (765, 3159, 1, 1, 5);
-- Monster 3188 : Saurdid the Scandalous in Stratigra Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (765, 3188, 0.1, 1, 5);
-- Monster 3189 : Saurbet the Refreshing in Stratigra Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (765, 3189, 0.1, 1, 5);
-- Monster 3190 : Saurgei the Rushing in Stratigra Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (765, 3190, 0.1, 1, 5);
-- Monster 3191 : Scisaur the Pointed in Stratigra Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (765, 3191, 0.1, 1, 5);
-- Monster 3192 : Sauriasis the Flaky in Stratigra Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (765, 3192, 0.1, 1, 5);
-- Monster 3160 : Insipid Venerable Sauroshell in Ponifar Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (766, 3160, 1, 1, 5);
-- Monster 3161 : Muddy Venerable Sauroshell in Ponifar Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (766, 3161, 1, 1, 5);
-- Monster 3162 : Incandescent Venerable Sauroshell in Ponifar Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (766, 3162, 1, 1, 5);
-- Monster 3163 : Moist Venerable Sauroshell in Ponifar Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (766, 3163, 1, 1, 5);
-- Monster 3164 : Dry Venerable Sauroshell in Ponifar Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (766, 3164, 1, 1, 5);
-- Monster 3193 : Sauruman the Destructive in Ponifar Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (766, 3193, 0.1, 1, 5);
-- Monster 3194 : Saurcery the Magical in Ponifar Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (766, 3194, 0.1, 1, 5);
-- Monster 3195 : Censaur the Forbidding in Ponifar Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (766, 3195, 0.1, 1, 5);
-- Monster 3196 : Eyesaur the Repulsive in Ponifar Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (766, 3196, 0.1, 1, 5);
-- Monster 3197 : Tyranno the Despotic in Ponifar Forest
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (766, 3197, 0.1, 1, 5);
-- Monster 3144 : Raw Juvenile Sauroshell in Jeez Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (767, 3144, 1, 1, 5);
-- Monster 3145 : Insipid Juvenile Sauroshell in Jeez Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (767, 3145, 1, 1, 5);
-- Monster 3146 : Muddy Juvenile Sauroshell in Jeez Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (767, 3146, 1, 1, 5);
-- Monster 3147 : Incandescent Juvenile Sauroshell in Jeez Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (767, 3147, 1, 1, 5);
-- Monster 3148 : Moist Juvenile Sauroshell in Jeez Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (767, 3148, 1, 1, 5);
-- Monster 3149 : Dry Juvenile Sauroshell in Jeez Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (767, 3149, 1, 1, 5);
-- Monster 3150 : Insipid Novice Sauroshell in Onthea Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (768, 3150, 1, 1, 5);
-- Monster 3151 : Muddy Novice Sauroshell in Onthea Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (768, 3151, 1, 1, 5);
-- Monster 3152 : Incandescent Novice Sauroshell in Onthea Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (768, 3152, 1, 1, 5);
-- Monster 3153 : Moist Novice Sauroshell in Onthea Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (768, 3153, 1, 1, 5);
-- Monster 3154 : Dry Novice Sauroshell in Onthea Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (768, 3154, 1, 1, 5);
-- Monster 3165 : Raw Novice Sauroshell in Onthea Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (768, 3165, 1, 1, 5);
-- Monster 3155 : Insipid Mature Sauroshell in Paleozoi Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (769, 3155, 1, 1, 5);
-- Monster 3156 : Muddy Mature Sauroshell in Paleozoi Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (769, 3156, 1, 1, 5);
-- Monster 3157 : Incandescent Mature Sauroshell in Paleozoi Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (769, 3157, 1, 1, 5);
-- Monster 3158 : Moist Mature Sauroshell in Paleozoi Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (769, 3158, 1, 1, 5);
-- Monster 3159 : Dry Mature Sauroshell in Paleozoi Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (769, 3159, 1, 1, 5);
-- Monster 3166 : Raw Mature Sauroshell in Paleozoi Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (769, 3166, 1, 1, 5);
-- Monster 3160 : Insipid Venerable Sauroshell in Mountsaleia Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (770, 3160, 1, 1, 5);
-- Monster 3161 : Muddy Venerable Sauroshell in Mountsaleia Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (770, 3161, 1, 1, 5);
-- Monster 3162 : Incandescent Venerable Sauroshell in Mountsaleia Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (770, 3162, 1, 1, 5);
-- Monster 3163 : Moist Venerable Sauroshell in Mountsaleia Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (770, 3163, 1, 1, 5);
-- Monster 3164 : Dry Venerable Sauroshell in Mountsaleia Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (770, 3164, 1, 1, 5);
-- Monster 3167 : Raw Venerable Sauroshell in Mountsaleia Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (770, 3167, 1, 1, 5);
-- Monster 3142 : Grozilla in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3142, 1, 1, 5);
-- Monster 3143 : Grasmera in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3143, 1, 1, 5);
-- Monster 3145 : Insipid Juvenile Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3145, 1, 1, 5);
-- Monster 3146 : Muddy Juvenile Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3146, 1, 1, 5);
-- Monster 3147 : Incandescent Juvenile Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3147, 1, 1, 5);
-- Monster 3148 : Moist Juvenile Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3148, 1, 1, 5);
-- Monster 3149 : Dry Juvenile Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3149, 1, 1, 5);
-- Monster 3150 : Insipid Novice Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3150, 1, 1, 5);
-- Monster 3151 : Muddy Novice Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3151, 1, 1, 5);
-- Monster 3152 : Incandescent Novice Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3152, 1, 1, 5);
-- Monster 3153 : Moist Novice Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3153, 1, 1, 5);
-- Monster 3154 : Dry Novice Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3154, 1, 1, 5);
-- Monster 3155 : Insipid Mature Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3155, 1, 1, 5);
-- Monster 3156 : Muddy Mature Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3156, 1, 1, 5);
-- Monster 3157 : Incandescent Mature Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3157, 1, 1, 5);
-- Monster 3158 : Moist Mature Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3158, 1, 1, 5);
-- Monster 3159 : Dry Mature Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3159, 1, 1, 5);
-- Monster 3160 : Insipid Venerable Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3160, 1, 1, 5);
-- Monster 3161 : Muddy Venerable Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3161, 1, 1, 5);
-- Monster 3162 : Incandescent Venerable Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3162, 1, 1, 5);
-- Monster 3163 : Moist Venerable Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3163, 1, 1, 5);
-- Monster 3164 : Dry Venerable Sauroshell in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3164, 1, 1, 5);
-- Monster 3293 : Tired Grozilla in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3293, 1, 1, 5);
-- Monster 3294 : Exhausted Grozilla in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3294, 1, 1, 5);
-- Monster 3295 : Sleepwalking Grozilla in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3295, 1, 1, 5);
-- Monster 3296 : Tired Grasmera in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3296, 1, 1, 5);
-- Monster 3297 : Exhausted Grasmera in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3297, 1, 1, 5);
-- Monster 3298 : Sleepwalking Grasmera in Pinki Crater
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (771, 3298, 1, 1, 5);
-- Monster 523 : Kitsou Nakwa in Daggero's Lair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (776, 523, 1, 1, 5);
-- Monster 530 : Kitsou Nere in Daggero's Lair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (776, 530, 1, 1, 5);
-- Monster 531 : Kitsou Nae in Daggero's Lair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (776, 531, 1, 1, 5);
-- Monster 532 : Kitsou Nufeu in Daggero's Lair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (776, 532, 1, 1, 5);
-- Monster 3208 : Daggero in Daggero's Lair
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (776, 3208, 1, 1, 5);
-- Monster 515 : Bulbiflor in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 515, 1, 1, 5);
-- Monster 518 : Bulbush in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 518, 1, 1, 5);
-- Monster 519 : Bulbig in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 519, 1, 1, 5);
-- Monster 520 : Araknawa in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 520, 1, 1, 5);
-- Monster 522 : Grass Snake in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 522, 1, 1, 5);
-- Monster 523 : Kitsou Nakwa in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 523, 1, 1, 5);
-- Monster 525 : Poacher in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 525, 1, 1, 5);
-- Monster 530 : Kitsou Nere in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 530, 1, 1, 5);
-- Monster 531 : Kitsou Nae in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 531, 1, 1, 5);
-- Monster 548 : Bulbamboo in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 548, 1, 1, 5);
-- Monster 2329 : Kitsewey the Blue in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 2329, 0.1, 1, 5);
-- Monster 2392 : Arachnawar the Killinmachin in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 2392, 0.1, 1, 5);
-- Monster 2411 : Bulbamoon the Trumpeter in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 2411, 0.1, 1, 5);
-- Monster 2412 : Pocher the Kingponger in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 2412, 0.1, 1, 5);
-- Monster 2413 : Bulbisonic the Penetrating in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 2413, 0.1, 1, 5);
-- Monster 2414 : Bulbigroov the Dancer in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 2414, 0.1, 1, 5);
-- Monster 2415 : Bulbushisu the Makisan in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 2415, 0.1, 1, 5);
-- Monster 2434 : Grasnakizanami the Ruler in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 2434, 0.1, 1, 5);
-- Monster 2510 : Kitchy the Scratcher in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 2510, 0.1, 1, 5);
-- Monster 2511 : Kitsouie the Green in The Squirming Snapper
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (777, 2511, 0.1, 1, 5);
-- Monster 978 : Vulnerable Mush Mush in Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (778, 978, 1, 1, 5);
-- Monster 980 : Fragile Demonic Rose in Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (778, 980, 1, 1, 5);
-- Monster 981 : Small Wild Sunflower in Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (778, 981, 1, 1, 5);
-- Monster 983 : Young Boar in Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (778, 983, 1, 1, 5);
-- Monster 984 : Small Gobball War Chief in Plains
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (778, 984, 1, 1, 5);
-- Monster 98 : Tofu in Royal Tofu House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (780, 98, 1, 1, 5);
-- Monster 382 : Royal Tofu in Royal Tofu House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (780, 382, 1, 1, 5);
-- Monster 807 : Tofuzmo in Royal Tofu House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (780, 807, 1, 1, 5);
-- Monster 3299 : Ugly Tofu in Royal Tofu House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (780, 3299, 1, 1, 5);
-- Monster 3300 : Blastofu in Royal Tofu House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (780, 3300, 1, 1, 5);
-- Monster 3301 : Tofubine in Royal Tofu House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (780, 3301, 1, 1, 5);
-- Monster 3302 : Podgy Tofu in Royal Tofu House
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (780, 3302, 1, 1, 5);
-- Monster 257 : Soft Oak in Soft Oak Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (781, 257, 1, 1, 5);
-- Monster 258 : Summoning Branch in Soft Oak Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (781, 258, 1, 1, 5);
-- Monster 260 : Healing Branch in Soft Oak Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (781, 260, 1, 1, 5);
-- Monster 990 : Short-Tempered Arachnotron in Soft Oak Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (781, 990, 1, 1, 5);
-- Monster 991 : Short-Tempered Dark Treechnid in Soft Oak Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (781, 991, 1, 1, 5);
-- Monster 992 : Short-Tempered Dark Treechnee in Soft Oak Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (781, 992, 1, 1, 5);
-- Monster 423 : Giant Kralove in Lair of the Giant Kralove
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (783, 423, 1, 1, 5);
-- Monster 1057 : Mopy King in Lair of the Giant Kralove
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (783, 1057, 1, 1, 5);
-- Monster 1058 : Mopeat in Lair of the Giant Kralove
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (783, 1058, 1, 1, 5);
-- Monster 1059 : Miremop in Lair of the Giant Kralove
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (783, 1059, 1, 1, 5);
-- Monster 1096 : Boggedown Ouassingue in Lair of the Giant Kralove
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (783, 1096, 1, 1, 5);
-- Monster 3306 : Al Howin in Al Howin's Stewpot
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (784, 3306, 1, 1, 5);
-- Monster 3307 : Devhorror in Al Howin's Stewpot
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (784, 3307, 1, 1, 5);
-- Monster 3308 : Arachkin in Al Howin's Stewpot
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (784, 3308, 1, 1, 5);
-- Monster 3309 : Toadkin in Al Howin's Stewpot
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (784, 3309, 1, 1, 5);
-- Monster 3310 : Worm-O'-Lantern in Al Howin's Stewpot
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (784, 3310, 1, 1, 5);
-- Monster 3311 : Borbkin in Al Howin's Stewpot
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (784, 3311, 1, 1, 5);
-- Monster 3392 : Nessil in Winter Gardens
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (785, 3392, 1, 1, 5);
-- Monster 3393 : Krackal in Winter Gardens
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (785, 3393, 1, 1, 5);
-- Monster 3394 : Dodox in Winter Gardens
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (785, 3394, 1, 1, 5);
-- Monster 3396 : Snowdew in Winter Gardens
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (785, 3396, 1, 1, 5);
-- Monster 3401 : Mekstagob in Windblast Barricade
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (786, 3401, 1, 1, 5);
-- Monster 3404 : Kan-O-Mat in Windblast Barricade
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (786, 3404, 1, 1, 5);
-- Monster 3406 : Pingwinch in Windblast Barricade
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (786, 3406, 1, 1, 5);
-- Monster 3407 : Mecanofoux in Windblast Barricade
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (786, 3407, 1, 1, 5);
-- Monster 3408 : Serpulax in Windblast Barricade
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (786, 3408, 1, 1, 5);
-- Monster 3379 : Harrogant in Scarlet Tannery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (787, 3379, 1, 1, 5);
-- Monster 3380 : Dumple in Scarlet Tannery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (787, 3380, 1, 1, 5);
-- Monster 3382 : Asteraw in Scarlet Tannery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (787, 3382, 1, 1, 5);
-- Monster 3383 : Leatherball in Scarlet Tannery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (787, 3383, 1, 1, 5);
-- Monster 3413 : Psychopump in Scarlet Tannery
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (787, 3413, 1, 1, 5);
-- Monster 3385 : Potbellion in Bleak Legion Bastion
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (788, 3385, 1, 1, 5);
-- Monster 3386 : Stalak in Bleak Legion Bastion
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (788, 3386, 1, 1, 5);
-- Monster 3387 : Karkanik in Bleak Legion Bastion
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (788, 3387, 1, 1, 5);
-- Monster 3389 : Skateman in Bleak Legion Bastion
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (788, 3389, 1, 1, 5);
-- Monster 3403 : Ice Knight in Bleak Legion Bastion
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (788, 3403, 1, 1, 5);
-- Monster 3402 : Erotegg in Water Clock Tower
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (789, 3402, 1, 1, 5);
-- Monster 3410 : Bubotron in Water Clock Tower
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (789, 3410, 1, 1, 5);
-- Monster 3412 : Cycloid in Water Clock Tower
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (789, 3412, 1, 1, 5);
-- Monster 3414 : Sinistrofu in Water Clock Tower
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (789, 3414, 1, 1, 5);
-- Monster 3415 : Nocturnowl in Water Clock Tower
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (789, 3415, 1, 1, 5);
-- Monster 3392 : Nessil in Nileza's Laboratory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (790, 3392, 1, 1, 5);
-- Monster 3393 : Krackal in Nileza's Laboratory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (790, 3393, 1, 1, 5);
-- Monster 3394 : Dodox in Nileza's Laboratory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (790, 3394, 1, 1, 5);
-- Monster 3395 : Thermite in Nileza's Laboratory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (790, 3395, 1, 1, 5);
-- Monster 3396 : Snowdew in Nileza's Laboratory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (790, 3396, 1, 1, 5);
-- Monster 3397 : Nileza in Nileza's Laboratory
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (790, 3397, 1, 1, 5);
-- Monster 3404 : Kan-O-Mat in Sylargh's Carrier
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (791, 3404, 1, 1, 5);
-- Monster 3405 : Tinkerbear in Sylargh's Carrier
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (791, 3405, 1, 1, 5);
-- Monster 3406 : Pingwinch in Sylargh's Carrier
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (791, 3406, 1, 1, 5);
-- Monster 3407 : Mecanofoux in Sylargh's Carrier
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (791, 3407, 1, 1, 5);
-- Monster 3408 : Serpulax in Sylargh's Carrier
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (791, 3408, 1, 1, 5);
-- Monster 3409 : Sylargh in Sylargh's Carrier
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (791, 3409, 1, 1, 5);
-- Monster 3379 : Harrogant in Klime's Private Suite
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (792, 3379, 1, 1, 5);
-- Monster 3380 : Dumple in Klime's Private Suite
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (792, 3380, 1, 1, 5);
-- Monster 3381 : Putchup in Klime's Private Suite
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (792, 3381, 1, 1, 5);
-- Monster 3382 : Asteraw in Klime's Private Suite
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (792, 3382, 1, 1, 5);
-- Monster 3383 : Leatherball in Klime's Private Suite
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (792, 3383, 1, 1, 5);
-- Monster 3384 : Klime in Klime's Private Suite
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (792, 3384, 1, 1, 5);
-- Monster 3385 : Potbellion in Missiz Freezz's Frostforge
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (793, 3385, 1, 1, 5);
-- Monster 3386 : Stalak in Missiz Freezz's Frostforge
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (793, 3386, 1, 1, 5);
-- Monster 3387 : Karkanik in Missiz Freezz's Frostforge
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (793, 3387, 1, 1, 5);
-- Monster 3389 : Skateman in Missiz Freezz's Frostforge
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (793, 3389, 1, 1, 5);
-- Monster 3390 : Revish in Missiz Freezz's Frostforge
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (793, 3390, 1, 1, 5);
-- Monster 3391 : Missiz Freezz in Missiz Freezz's Frostforge
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (793, 3391, 1, 1, 5);
-- Monster 3384 : Klime in Count Harebourg's Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (794, 3384, 1, 1, 5);
-- Monster 3391 : Missiz Freezz in Count Harebourg's Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (794, 3391, 1, 1, 5);
-- Monster 3397 : Nileza in Count Harebourg's Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (794, 3397, 1, 1, 5);
-- Monster 3409 : Sylargh in Count Harebourg's Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (794, 3409, 1, 1, 5);
-- Monster 3410 : Bubotron in Count Harebourg's Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (794, 3410, 1, 1, 5);
-- Monster 3411 : Treadfast in Count Harebourg's Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (794, 3411, 1, 1, 5);
-- Monster 3412 : Cycloid in Count Harebourg's Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (794, 3412, 1, 1, 5);
-- Monster 3414 : Sinistrofu in Count Harebourg's Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (794, 3414, 1, 1, 5);
-- Monster 3415 : Nocturnowl in Count Harebourg's Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (794, 3415, 1, 1, 5);
-- Monster 3416 : Count Harebourg in Count Harebourg's Dungeon
INSERT INTO `monsters_spawns` (SubAreaId, MonsterId, Frequency, MinGrade, MaxGrade) VALUES (794, 3416, 1, 1, 5);

DELETE FROM monsters_spawns WHERE MonsterId NOT IN (SELECT Id FROM monsters_templates);