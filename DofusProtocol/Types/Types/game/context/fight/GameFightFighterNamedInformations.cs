// Generated on 03/02/2014 20:42:59
using Stump.Core.IO;
using System.Text;

namespace Stump.DofusProtocol.Types
{
    public class GameFightFighterNamedInformations : GameFightFighterInformations
    {
        public const short Id = 158;

        public override short TypeId
        {
            get { return Id; }
        }

        public string name;

        public GameFightFighterNamedInformations()
        {
        }

        public GameFightFighterNamedInformations(int contextualId, Types.EntityLook look, Types.EntityDispositionInformations disposition, sbyte teamId, bool alive, Types.GameFightMinimalStats stats, string name)
         : base(contextualId, look, disposition, teamId, alive, stats)
        {
            this.name = name;
        }

        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(name);
        }

        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            name = reader.ReadUTF();
        }

        public override int GetSerializationSize()
        {
            return base.GetSerializationSize() + sizeof(short) + Encoding.UTF8.GetByteCount(name);
        }
    }
}