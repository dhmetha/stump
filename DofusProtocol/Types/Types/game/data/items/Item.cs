// Generated on 03/02/2014 20:43:01
using Stump.Core.IO;

namespace Stump.DofusProtocol.Types
{
    public class Item
    {
        public const short Id = 7;

        public virtual short TypeId
        {
            get { return Id; }
        }

        public Item()
        {
        }

        public virtual void Serialize(IDataWriter writer)
        {
        }

        public virtual void Deserialize(IDataReader reader)
        {
        }

        public virtual int GetSerializationSize()
        {
            return 0;
            ;
        }
    }
}