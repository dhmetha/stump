// Generated on 03/02/2014 20:43:01
using Stump.Core.IO;
using System;

namespace Stump.DofusProtocol.Types
{
    public class ObjectEffectCreature : ObjectEffect
    {
        public const short Id = 71;

        public override short TypeId
        {
            get { return Id; }
        }

        public short monsterFamilyId;

        public ObjectEffectCreature()
        {
        }

        public ObjectEffectCreature(short actionId, short monsterFamilyId)
         : base(actionId)
        {
            this.monsterFamilyId = monsterFamilyId;
        }

        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteShort(monsterFamilyId);
        }

        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            monsterFamilyId = reader.ReadShort();
            if (monsterFamilyId < 0)
                throw new Exception("Forbidden value on monsterFamilyId = " + monsterFamilyId + ", it doesn't respect the following condition : monsterFamilyId < 0");
        }

        public override int GetSerializationSize()
        {
            return base.GetSerializationSize() + sizeof(short);
        }
    }
}