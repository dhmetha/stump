// Generated on 03/02/2014 20:43:02
using Stump.Core.IO;
using System.Text;

namespace Stump.DofusProtocol.Types
{
    public class ObjectEffectString : ObjectEffect
    {
        public const short Id = 74;

        public override short TypeId
        {
            get { return Id; }
        }

        public string value;

        public ObjectEffectString()
        {
        }

        public ObjectEffectString(short actionId, string value)
         : base(actionId)
        {
            this.value = value;
        }

        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(value);
        }

        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            value = reader.ReadUTF();
        }

        public override int GetSerializationSize()
        {
            return base.GetSerializationSize() + sizeof(short) + Encoding.UTF8.GetByteCount(value);
        }
    }
}