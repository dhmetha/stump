// Generated on 03/02/2014 20:43:03
using Stump.Core.IO;

namespace Stump.DofusProtocol.Types
{
    public class ShortcutObject : Shortcut
    {
        public const short Id = 367;

        public override short TypeId
        {
            get { return Id; }
        }

        public ShortcutObject()
        {
        }

        public ShortcutObject(int slot)
         : base(slot)
        {
        }

        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
        }

        public override int GetSerializationSize()
        {
            return base.GetSerializationSize();
        }
    }
}