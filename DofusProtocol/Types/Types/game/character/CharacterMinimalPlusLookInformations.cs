// Generated on 03/02/2014 20:42:58
using Stump.Core.IO;

namespace Stump.DofusProtocol.Types
{
    public class CharacterMinimalPlusLookInformations : CharacterMinimalInformations
    {
        public const short Id = 163;

        public override short TypeId
        {
            get { return Id; }
        }

        public Types.EntityLook entityLook;

        public CharacterMinimalPlusLookInformations()
        {
        }

        public CharacterMinimalPlusLookInformations(int id, byte level, string name, Types.EntityLook entityLook)
         : base(id, level, name)
        {
            this.entityLook = entityLook;
        }

        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            entityLook.Serialize(writer);
        }

        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            entityLook = new Types.EntityLook();
            entityLook.Deserialize(reader);
        }

        public override int GetSerializationSize()
        {
            return base.GetSerializationSize() + entityLook.GetSerializationSize();
        }
    }
}