// Generated on 03/02/2014 20:43:02
using Stump.Core.IO;

namespace Stump.DofusProtocol.Types
{
    public class IgnoredInformations : AbstractContactInformations
    {
        public const short Id = 106;

        public override short TypeId
        {
            get { return Id; }
        }

        public IgnoredInformations()
        {
        }

        public IgnoredInformations(int accountId, string accountName)
         : base(accountId, accountName)
        {
        }

        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
        }

        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
        }

        public override int GetSerializationSize()
        {
            return base.GetSerializationSize();
        }
    }
}