// Generated on 03/02/2014 20:43:02
using Stump.Core.IO;
using System;
using System.Text;

namespace Stump.DofusProtocol.Types
{
    public class IgnoredOnlineInformations : IgnoredInformations
    {
        public const short Id = 105;

        public override short TypeId
        {
            get { return Id; }
        }

        public string playerName;
        public sbyte breed;
        public bool sex;

        public IgnoredOnlineInformations()
        {
        }

        public IgnoredOnlineInformations(int accountId, string accountName, string playerName, sbyte breed, bool sex)
         : base(accountId, accountName)
        {
            this.playerName = playerName;
            this.breed = breed;
            this.sex = sex;
        }

        public override void Serialize(IDataWriter writer)
        {
            base.Serialize(writer);
            writer.WriteUTF(playerName);
            writer.WriteSByte(breed);
            writer.WriteBoolean(sex);
        }

        public override void Deserialize(IDataReader reader)
        {
            base.Deserialize(reader);
            playerName = reader.ReadUTF();
            breed = reader.ReadSByte();
            if (breed < (byte)Enums.PlayableBreedEnum.Feca || breed > (byte)Enums.PlayableBreedEnum.Steamer)
                throw new Exception("Forbidden value on breed = " + breed + ", it doesn't respect the following condition : breed < (byte)Enums.PlayableBreedEnum.Feca || breed > (byte)Enums.PlayableBreedEnum.Steamer");
            sex = reader.ReadBoolean();
        }

        public override int GetSerializationSize()
        {
            return base.GetSerializationSize() + sizeof(short) + Encoding.UTF8.GetByteCount(playerName) + sizeof(sbyte) + sizeof(bool);
        }
    }
}