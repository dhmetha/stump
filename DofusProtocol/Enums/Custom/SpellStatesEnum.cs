namespace Stump.DofusProtocol.Enums
{
	public enum SpellStatesEnum
	{
		/// <summary>
		/// Neutral
		/// </summary>
		Neutral = 0,
		/// <summary>
		/// Drunk
		/// </summary>
		Drunk = 1,
		/// <summary>
		/// Soul Seeker
		/// </summary>
		Soul_Seeker = 2,
		/// <summary>
		/// Carrying
		/// </summary>
		Carrying = 3,
		/// <summary>
		/// Coward
		/// </summary>
		Coward = 4,
		/// <summary>
		/// Disorientated
		/// </summary>
		Disorientated = 5,
		/// <summary>
		/// Rooted
		/// </summary>
		Rooted = 6,
		/// <summary>
		/// Gravity
		/// </summary>
		Gravity = 7,
		/// <summary>
		/// Carried
		/// </summary>
		Carried = 8,
		/// <summary>
		/// Sylvan Motivation
		/// </summary>
		Sylvan_Motivation = 9,
		/// <summary>
		/// Taming
		/// </summary>
		Taming = 10,
		/// <summary>
		/// Riding
		/// </summary>
		Riding = 11,
		/// <summary>
		/// Unruly
		/// </summary>
		Unruly = 12,
		/// <summary>
		/// Extremely disobedient
		/// </summary>
		Extremely_disobedient = 13,
		/// <summary>
		/// Snowbound
		/// </summary>
		Snowbound = 14,
		/// <summary>
		/// Awake
		/// </summary>
		Awake = 15,
		/// <summary>
		/// Vulnerable
		/// </summary>
		Vulnerable = 16,
		/// <summary>
		/// Parted
		/// </summary>
		Parted = 17,
		/// <summary>
		/// Frozen
		/// </summary>
		Frozen = 18,
		/// <summary>
		/// Cracked
		/// </summary>
		Cracked = 19,
		/// <summary>
		/// Asleep
		/// </summary>
		Asleep = 26,
		/// <summary>
		/// Leopardo
		/// </summary>
		Leopardo = 27,
		/// <summary>
		/// Free
		/// </summary>
		Free = 28,
		/// <summary>
		/// Odd Glyph
		/// </summary>
		Odd_Glyph = 29,
		/// <summary>
		/// Even Glyph
		/// </summary>
		Even_Glyph = 30,
		/// <summary>
		/// First Ink
		/// </summary>
		First_Ink = 31,
		/// <summary>
		/// Second Ink
		/// </summary>
		Second_Ink = 32,
		/// <summary>
		/// Third Ink
		/// </summary>
		Third_Ink = 33,
		/// <summary>
		/// Fourth Ink
		/// </summary>
		Fourth_Ink = 34,
		/// <summary>
		/// Desire to Kill
		/// </summary>
		Desire_to_Kill = 35,
		/// <summary>
		/// Desire to Paralyze
		/// </summary>
		Desire_to_Paralyze = 36,
		/// <summary>
		/// Desire to Curse
		/// </summary>
		Desire_to_Curse = 37,
		/// <summary>
		/// Desire to Poison
		/// </summary>
		Desire_to_Poison = 38,
		/// <summary>
		/// Blurry
		/// </summary>
		Blurry = 39,
		/// <summary>
		/// Corrupted
		/// </summary>
		Corrupted = 40,
		/// <summary>
		/// Silent
		/// </summary>
		Silent = 41,
		/// <summary>
		/// Weakened
		/// </summary>
		Weakened = 42,
		/// <summary>
		/// (not found)
		/// </summary>
		State_43 = 43,
		/// <summary>
		/// (not found)
		/// </summary>
		State_44 = 44,
		/// <summary>
		/// (not found)
		/// </summary>
		State_46 = 46,
		/// <summary>
		/// (not found)
		/// </summary>
		State_47 = 47,
		/// <summary>
		/// Confused
		/// </summary>
		Confused = 48,
		/// <summary>
		/// Ghoulified
		/// </summary>
		Ghoulified = 49,
		/// <summary>
		/// Altruistic
		/// </summary>
		Altruistic = 50,
		/// <summary>
		/// (not found)
		/// </summary>
		State_51 = 51,
		/// <summary>
		/// (not found)
		/// </summary>
		State_52 = 52,
		/// <summary>
		/// (not found)
		/// </summary>
		State_53 = 53,
		/// <summary>
		/// (not found)
		/// </summary>
		State_54 = 54,
		/// <summary>
		/// Retired
		/// </summary>
		Retired = 55,
		/// <summary>
		/// Invulnerable
		/// </summary>
		Invulnerable = 56,
		/// <summary>
		/// Countdown - 2
		/// </summary>
		Countdown__2 = 57,
		/// <summary>
		/// Countdown - 1
		/// </summary>
		Countdown__1 = 58,
		/// <summary>
		/// Devoted
		/// </summary>
		Devoted = 60,
		/// <summary>
		/// Aggressive
		/// </summary>
		Aggressive = 61,
		/// <summary>
		/// Heavy
		/// </summary>
		Heavy = 63,
		/// <summary>
		/// Glyphanger
		/// </summary>
		Glyphanger = 64,
		/// <summary>
		/// Omega
		/// </summary>
		Omega = 65,
		/// <summary>
		/// Alpha
		/// </summary>
		Alpha = 66,
		/// <summary>
		/// Beta
		/// </summary>
		Beta = 67,
		/// <summary>
		/// Gamma
		/// </summary>
		Gamma = 68,
		/// <summary>
		/// Delta
		/// </summary>
		Delta = 69,
		/// <summary>
		/// Kordis Alpha
		/// </summary>
		Kordis_Alpha = 70,
		/// <summary>
		/// Kordis Beta
		/// </summary>
		Kordis_Beta = 71,
		/// <summary>
		/// Kordis Gamma
		/// </summary>
		Kordis_Gamma = 72,
		/// <summary>
		/// Enraged
		/// </summary>
		Enraged = 73,
		/// <summary>
		/// Zombie
		/// </summary>
		Zombie = 74,
		/// <summary>
		/// Unhealable
		/// </summary>
		Unhealable = 76,
		/// <summary>
		/// Weakness
		/// </summary>
		Weakness = 77,
		/// <summary>
		/// Sprout
		/// </summary>
		Sprout = 78,
		/// <summary>
		/// Major Second
		/// </summary>
		Major_Second = 79,
		/// <summary>
		/// Major Third
		/// </summary>
		Major_Third = 80,
		/// <summary>
		/// Perfect Fourth
		/// </summary>
		Perfect_Fourth = 81,
		/// <summary>
		/// Perfect Fifth
		/// </summary>
		Perfect_Fifth = 82,
		/// <summary>
		/// Major Sixth
		/// </summary>
		Major_Sixth = 83,
		/// <summary>
		/// Major Seventh
		/// </summary>
		Major_Seventh = 84,
		/// <summary>
		/// Perfect Octave
		/// </summary>
		Perfect_Octave = 85,
		/// <summary>
		/// Fulguration
		/// </summary>
		Fulguration = 86,
		/// <summary>
		/// Blurring
		/// </summary>
		Blurring = 87,
		/// <summary>
		/// Beark to Life
		/// </summary>
		Beark_to_Life = 88,
		/// <summary>
		/// Flamboyant
		/// </summary>
		Flamboyant = 89,
		/// <summary>
		/// Ectoplasmic
		/// </summary>
		Ectoplasmic = 90,
		/// <summary>
		/// Ardent
		/// </summary>
		Ardent = 91,
		/// <summary>
		/// Kaboom
		/// </summary>
		Kaboom = 92,
		/// <summary>
		/// Klaus Kombatnoob
		/// </summary>
		Klaus_Kombatnoob = 93,
		/// <summary>
		/// Notdis Thymnoob
		/// </summary>
		Notdis_Thymnoob = 94,
		/// <summary>
		/// Unable to lock
		/// </summary>
		Unable_to_lock = 95,
		/// <summary>
		/// Unlockable
		/// </summary>
		Unlockable = 96,
		/// <summary>
		/// Unmovable
		/// </summary>
		Unmovable = 97,
		/// <summary>
		/// Classic
		/// </summary>
		Classic = 98,
		/// <summary>
		/// Psychopathic
		/// </summary>
		Psychopathic = 99,
		/// <summary>
		/// Cowardly
		/// </summary>
		Cowardly = 100,
		/// <summary>
		/// Archer
		/// </summary>
		Archer = 101,
		/// <summary>
		/// Gorgoylie
		/// </summary>
		Gorgoylie = 102,
		/// <summary>
		/// Fortified
		/// </summary>
		Fortified = 103,
		/// <summary>
		/// Broken
		/// </summary>
		Broken = 104,
		/// <summary>
		/// Tyrannised
		/// </summary>
		Tyrannised = 105,
		/// <summary>
		/// Meteoroid
		/// </summary>
		Meteoroid = 106,
		/// <summary>
		/// Critical Mass
		/// </summary>
		Critical_Mass = 107,
		/// <summary>
		/// Apocalypse
		/// </summary>
		Apocalypse = 108,
		/// <summary>
		/// Rubilax
		/// </summary>
		Rubilax = 109,
		/// <summary>
		/// "The Sword"
		/// </summary>
		The_Sword = 110,
		/// <summary>
		/// Daggero's First Mark
		/// </summary>
		Daggeros_First_Mark = 111,
		/// <summary>
		/// Daggero's Second Mark
		/// </summary>
		Daggeros_Second_Mark = 112,
		/// <summary>
		/// Daggero's Third Mark
		/// </summary>
		Daggeros_Third_Mark = 113,
		/// <summary>
		/// Daggero's Fourth Mark
		/// </summary>
		Daggeros_Fourth_Mark = 114,
		/// <summary>
		/// Daggero's Fifth Mark
		/// </summary>
		Daggeros_Fifth_Mark = 115,
		/// <summary>
		/// Earthy Green
		/// </summary>
		Earthy_Green = 116,
		/// <summary>
		/// Flame-Tickled Pink
		/// </summary>
		FlameTickled_Pink = 117,
		/// <summary>
		/// Blue Lagoon
		/// </summary>
		Blue_Lagoon = 118,
		/// <summary>
		/// Yellow Zephyr
		/// </summary>
		Yellow_Zephyr = 119,
		/// <summary>
		/// Displaced
		/// </summary>
		Displaced = 120,
		/// <summary>
		/// Load
		/// </summary>
		Load = 121,
		/// <summary>
		/// Unload
		/// </summary>
		Unload = 122,
		/// <summary>
		/// Overload
		/// </summary>
		Overload = 123,
		/// <summary>
		/// Big Brother
		/// </summary>
		Big_Brother = 124,
		/// <summary>
		/// Little Sister
		/// </summary>
		Little_Sister = 125,
		/// <summary>
		/// Magnatron
		/// </summary>
		Magnatron = 126,
		/// <summary>
		/// Earth
		/// </summary>
		Earth = 127,
		/// <summary>
		/// Water
		/// </summary>
		Water = 128,
		/// <summary>
		/// Fire
		/// </summary>
		Fire = 129,
		/// <summary>
		/// Ambush
		/// </summary>
		Ambush = 130,
		/// <summary>
		/// First Aid
		/// </summary>
		First_Aid = 131,
		/// <summary>
		/// Periscope
		/// </summary>
		Periscope = 132,
		/// <summary>
		/// Dreadnaut
		/// </summary>
		Dreadnaut = 133,
		/// <summary>
		/// Evolution II
		/// </summary>
		Evolution_II = 134,
		/// <summary>
		/// Evolution III
		/// </summary>
		Evolution_III = 135,
		/// <summary>
		/// Woebegone
		/// </summary>
		Woebegone = 136,
		/// <summary>
		/// Kwaburn
		/// </summary>
		Kwaburn = 137,
		/// <summary>
		/// Kwafreeze
		/// </summary>
		Kwafreeze = 138,
		/// <summary>
		/// Kwakearth
		/// </summary>
		Kwakearth = 139,
		/// <summary>
		/// Kwablow
		/// </summary>
		Kwablow = 140,
		/// <summary>
		/// Kwaneutral
		/// </summary>
		Kwaneutral = 141,
		/// <summary>
		/// Pandawushai
		/// </summary>
		Pandawushai = 142,
		/// <summary>
		/// Pandawushwa
		/// </summary>
		Pandawushwa = 143,
		/// <summary>
		/// Pandawushfi
		/// </summary>
		Pandawushfi = 144,
		/// <summary>
		/// Pandawushea
		/// </summary>
		Pandawushea = 145,
		/// <summary>
		/// Pandawushne
		/// </summary>
		Pandawushne = 146,
		/// <summary>
		/// Spyglass
		/// </summary>
		Spyglass = 147,
		/// <summary>
		/// Corselet
		/// </summary>
		Corselet = 148,
		/// <summary>
		/// Barmy
		/// </summary>
		Barmy = 149,
		/// <summary>
		/// Entangled
		/// </summary>
		Entangled = 150,
		/// <summary>
		/// Hasty
		/// </summary>
		Hasty = 151,
	}
}
