
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum PlayerStateEnum
    {
        NOT_CONNECTED = 0,
        GAME_TYPE_ROLEPLAY = 1,
        GAME_TYPE_FIGHT = 2,
        UNKNOWN_STATE = 99,
    }
}