
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum NicknameGeneratingFailureEnum
    {
        NICKNAME_GENERATOR_RETRY_TOO_SHORT = 1,
        NICKNAME_GENERATOR_UNAVAILABLE = 2,
    }
}