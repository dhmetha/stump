
// Generated on 03/25/2013 19:24:41
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum PresetUseResultEnum
    {
        PRESET_USE_OK = 1,
        PRESET_USE_OK_PARTIAL = 2,
        PRESET_USE_ERR_UNKNOWN = 3,
        PRESET_USE_ERR_CRITERION = 4,
        PRESET_USE_ERR_BAD_PRESET_ID = 5,
    }
}