
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum HardcoreDeathStateEnum
    {
        DEATH_STATE_ALIVE = 0,
        DEATH_STATE_DEAD = 1,
        DEATH_STATE_WAITING_CONFIRMATION = 2,
    }
}