
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum MountEquipedErrorEnum
    {
        UNSET = 0,
        SET = 1,
        RIDING = 2,
    }
}