
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum GameActionFightInvisibilityStateEnum
    {
        INVISIBLE = 1,
        DETECTED = 2,
        VISIBLE = 3,
    }
}