
// Generated on 03/25/2013 19:24:41
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum TeleporterTypeEnum
    {
        TELEPORTER_ZAAP = 0,
        TELEPORTER_SUBWAY = 1,
        TELEPORTER_PRISM = 2,
    }
}