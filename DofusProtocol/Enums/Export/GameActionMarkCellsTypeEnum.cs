
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum GameActionMarkCellsTypeEnum
    {
        CELLS_CIRCLE = 0,
        CELLS_CROSS = 1,
    }
}