
// Generated on 03/25/2013 19:24:39
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum ClientTechnologyEnum
    {
        CLIENT_TECHNOLOGY_UNKNOWN = 0,
        CLIENT_AIR = 1,
        CLIENT_FLASH = 2,
    }
}