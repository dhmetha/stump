
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum FightSpellCastCriticalEnum
    {
        NORMAL = 1,
        CRITICAL_HIT = 2,
        CRITICAL_FAIL = 3,
    }
}