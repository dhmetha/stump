
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum NumericalValueTypeEnum
    {
        NUMERICAL_VALUE_DEFAULT = 0,
        NUMERICAL_VALUE_COLLECT = 1,
        NUMERICAL_VALUE_CRAFT = 2,
        NUMERICAL_VALUE_PADDOCK = 3,
        NUMERICAL_VALUE_RED = 64,
        NUMERICAL_VALUE_BLUE = 65,
        NUMERICAL_VALUE_GREEN = 66,
    }
}