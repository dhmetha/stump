
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum CraftResultEnum
    {
        CRAFT_IMPOSSIBLE = 0,
        CRAFT_FAILED = 1,
        CRAFT_SUCCESS = 2,
        CRAFT_NEUTRAL = 3,
    }
}