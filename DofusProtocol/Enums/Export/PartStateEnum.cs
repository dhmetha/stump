
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum PartStateEnum
    {
        PART_NOT_INSTALLED = 0,
        PART_BEING_UPDATER = 1,
        PART_UP_TO_DATE = 2,
    }
}