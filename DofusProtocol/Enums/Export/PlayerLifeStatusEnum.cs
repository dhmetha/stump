
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum PlayerLifeStatusEnum
    {
        STATUS_ALIVE_AND_KICKING = 0,
        STATUS_TOMBSTONE = 1,
        STATUS_PHANTOM = 2,
    }
}