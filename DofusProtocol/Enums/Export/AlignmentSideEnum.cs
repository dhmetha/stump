
// Generated on 03/25/2013 19:24:39
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum AlignmentSideEnum
    {
        ALIGNMENT_UNKNOWN = -2,
        ALIGNMENT_WITHOUT = -1,
        ALIGNMENT_NEUTRAL = 0,
        ALIGNMENT_ANGEL = 1,
        ALIGNMENT_EVIL = 2,
        ALIGNMENT_MERCENARY = 3,
    }
}