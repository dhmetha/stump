
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum FightDispellableEnum
    {
        DISPELLABLE = 1,
        DISPELLABLE_BY_DEATH = 2,
        DISPELLABLE_BY_STRONG_DISPEL = 3,
        REALLY_NOT_DISPELLABLE = 4,
    }
}