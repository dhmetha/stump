
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum PaymentTypeEnum
    {
        PAYMENT_ON_SUCCESS_ONLY = 0,
        PAYMENT_IN_ANY_CASE = 1,
    }
}