
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum ExchangeReplayStopReasonEnum
    {
        STOPPED_REASON_OK = 1,
        STOPPED_REASON_USER = 2,
        STOPPED_REASON_MISSING_RESSOURCE = 3,
        STOPPED_REASON_IMPOSSIBLE_CRAFT = 4,
    }
}