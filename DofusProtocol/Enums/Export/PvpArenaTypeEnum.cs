
// Generated on 03/25/2013 19:24:41
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum PvpArenaTypeEnum
    {
        ARENA_TYPE_3VS3 = 3,
        ARENA_TYPE_5VS5 = 5,
    }
}