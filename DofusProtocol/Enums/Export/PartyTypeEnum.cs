
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum PartyTypeEnum
    {
        PARTY_TYPE_UNDEFINED = 0,
        PARTY_TYPE_CLASSICAL = 1,
        PARTY_TYPE_DUNGEON = 2,
        PARTY_TYPE_ARENA = 3,
    }
}