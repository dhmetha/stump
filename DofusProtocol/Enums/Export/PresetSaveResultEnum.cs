
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum PresetSaveResultEnum
    {
        PRESET_SAVE_OK = 1,
        PRESET_SAVE_ERR_UNKNOWN = 2,
        PRESET_SAVE_ERR_TOO_MANY = 3,
    }
}