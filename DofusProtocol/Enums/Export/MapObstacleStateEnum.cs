
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum MapObstacleStateEnum
    {
        OBSTACLE_OPENED = 1,
        OBSTACLE_CLOSED = 2,
    }
}