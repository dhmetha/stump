
// Generated on 03/25/2013 19:24:39
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum ClientInstallTypeEnum
    {
        CLIENT_INSTALL_UNKNOWN = 0,
        CLIENT_BUNDLE = 1,
        CLIENT_STREAMING = 2,
    }
}