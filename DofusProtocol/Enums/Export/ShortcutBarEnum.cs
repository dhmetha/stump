
// Generated on 03/25/2013 19:24:41
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum ShortcutBarEnum
    {
        GENERAL_SHORTCUT_BAR = 0,
        SPELL_SHORTCUT_BAR = 1,
    }
}