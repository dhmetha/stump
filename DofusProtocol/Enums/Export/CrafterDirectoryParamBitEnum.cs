
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum CrafterDirectoryParamBitEnum
    {
        CRAFT_OPTION_NONE = 0,
        CRAFT_OPTION_NOT_FREE = 1,
        CRAFT_OPTION_NOT_FREE_EXCEPT_ON_FAIL = 2,
        CRAFT_OPTION_RESOURCES_REQUIRED = 4,
    }
}