
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum GameActionMarkTypeEnum
    {
        GLYPH = 1,
        TRAP = 2,
        WALL = 3,
    }
}