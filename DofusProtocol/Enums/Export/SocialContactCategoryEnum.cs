
// Generated on 03/25/2013 19:24:41
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum SocialContactCategoryEnum
    {
        SOCIAL_CONTACT_FRIEND = 0,
        SOCIAL_CONTACT_SPOUSE = 1,
        SOCIAL_CONTACT_PARTY = 2,
        SOCIAL_CONTACT_GUILD = 3,
        SOCIAL_CONTACT_CRAFTER = 4,
        SOCIAL_CONTACT_INTERLOCUTOR = 5,
    }
}