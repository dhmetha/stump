
// Generated on 03/25/2013 19:24:40
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum GuildInvitationStateEnum
    {
        GUILD_INVITATION_SENT = 1,
        GUILD_INVITATION_CANCELED = 2,
        GUILD_INVITATION_OK = 3,
    }
}