
// Generated on 03/25/2013 19:24:39
using System;
using System.Collections.Generic;

namespace Stump.DofusProtocol.Enums
{
    public enum ConsoleMessageTypeEnum
    {
        CONSOLE_TEXT_MESSAGE = 0,
        CONSOLE_INFO_MESSAGE = 1,
        CONSOLE_ERR_MESSAGE = 2,
    }
}